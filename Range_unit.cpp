//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop

#include "Range_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
//--- implemetation TMeasure ------------------------------------------------
//---------------------------------------------------------------------------
int TRange::ReadVarCount;

__fastcall TRange::TRange()
{
   memset(FVar,0,sizeof(TVar)*VarCount);
}
//---------------------------------------------------------------------------
TVar* __fastcall TRange::GetVar(int __index)
{
   if((__index>=VarCount)||(__index<0)) return NULL;
      else return &FVar[__index];
}
//---------------------------------------------------------------------------
void __fastcall TRange::Save(IStream* Stream)
{
   Stream->Write(FVar,VarCount*sizeof(TVar),0);
}
//---------------------------------------------------------------------------
void __fastcall TRange::Load(IStream* Stream)
{
   Stream->Read(FVar,VarCount*sizeof(TVar),0);
}
//---------------------------------------------------------------------------