//---------------------------------------------------------------------------
#ifndef KMXPoint_unitH
#define KMXPoint_unitH

#include "variable.h"
#include "KMXMeasure_unit.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TKMXMeasurePoint:public TGridPoint
{
public:
   static const VarCount=13;
   static ReadVarCount;

   static const Used       =0;//������������� ������� ��������
   static const Number     =1;//����� ���������
   static const n          =2;//���������� ���������

   static const Q          =3;//������ �/�
   static const Qp         =4;//������ %
   static const f          =5;//�������

   static const K          =6;
   static const SKO        =7;
   static const K_mx       =8;

   static const d          =9;

   static const Notes      =10;

	__fastcall TKMXMeasurePoint();
	__fastcall ~TKMXMeasurePoint();
	__property TKMXMeasure* Measures[int index] = { read=GetMeasures};
   virtual int __fastcall Add(TVarContainer*);
   virtual int __fastcall AddNew();
   virtual void __fastcall Delete(int Index);
   virtual void __fastcall ClearMeasures();
   virtual void __fastcall Load(IStream* Stream);
   virtual void __fastcall Save(IStream* Stream);
private:
	TVar FVar[VarCount];
   virtual TVar* __fastcall GetVar(int index);
   virtual int __fastcall GetVarCount(){return VarCount;};
   virtual TVarContainer*  __fastcall GetMeasures(int index);
   virtual int __fastcall GetMeasureVarCount();
	TList* FMeasures;
   int __fastcall GetCount();
};
//---------------------------------------------------------------------------
#endif
