//---------------------------------------------------------------------------

#ifndef CalculateOptionsForm_unitH
#define CalculateOptionsForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <CSPIN.h>
#include <Buttons.hpp>

#define MAX_OPTIONS_COUNT 40
//---------------------------------------------------------------------------
class TCalculateOptionsForm : public TForm
{
__published:	// IDE-managed Components
   TRadioGroup *GroupRadioGroup;
   TGroupBox *GroupBox1;
   TListBox *ListBox1;
   TGroupBox *GroupBox2;
   TGroupBox *GroupBox3;
   TLabel *DescriptionLabel;
   TGroupBox *GroupBox4;
   TRadioButton *RadioButton1;
   TRadioButton *RadioButton2;
   TLabel *Label1;
   TRadioButton *RadioButton3;
   TGroupBox *GroupBox5;
   TLabel *Label2;
   TRadioButton *RadioButton4;
   TRadioButton *RadioButton5;
   TRadioButton *RadioButton6;
   TBitBtn *BitBtn1;
   TPaintBox *UnitPaintBox;
   TCSpinEdit *CSpinEdit1;
   TCSpinEdit *CSpinEdit2;
   TBitBtn *BitBtn2;
   TGroupBox *GroupBox6;
   TButton *Button1;
   TButton *Button2;
   void __fastcall GroupRadioGroupClick(TObject *Sender);
   void __fastcall ListBox1DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall ListBox1Click(TObject *Sender);
   void __fastcall UnitPaintBoxPaint(TObject *Sender);
   void __fastcall CalculateChange(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
private:	// User declarations

   TVarOptions* vo;
   int VarCount;
   TCalcOptions* CalcOptions;
   TCalcOptions MeasureOptions[MAX_OPTIONS_COUNT];
   TCalcOptions PointOptions[MAX_OPTIONS_COUNT];
   TCalcOptions MIOptions[MAX_OPTIONS_COUNT];
   TCalcOptions RangeOptions[MAX_OPTIONS_COUNT];

   struct
      {
      TCalcOptions MeasureOptions[MAX_OPTIONS_COUNT];
      TCalcOptions PointOptions[MAX_OPTIONS_COUNT];
      TCalcOptions MIOptions[MAX_OPTIONS_COUNT];
      TCalcOptions RangeOptions[MAX_OPTIONS_COUNT];
      } PreSet1, PreSet2;
   int CurSel;
   bool ItemChange;
   void __fastcall EnableControls(bool IfKMX);

public:		// User declarations
   TMI1974* FData;
   TKMX*    FKMX;
   __fastcall TCalculateOptionsForm(TComponent* Owner);
   bool __fastcall ShowDialog(TMI1974* Data);
   void __fastcall SaveChanges();
   bool __fastcall ShowKMXDialog(TKMX* KMX);
};
//---------------------------------------------------------------------------
extern PACKAGE TCalculateOptionsForm *CalculateOptionsForm;
//---------------------------------------------------------------------------
#endif
