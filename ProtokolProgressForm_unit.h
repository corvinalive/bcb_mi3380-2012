//---------------------------------------------------------------------------

#ifndef ProtokolProgressForm_unitH
#define ProtokolProgressForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CGAUGES.h"
#include <Buttons.hpp>

//---------------------------------------------------------------------------
class TProtokolProgressForm : public TForm
{
__published:	// IDE-managed Components
   TCGauge *Gauge;
   TLabel *Label1;
   TLabel *Label2;
private:	// User declarations
public:		// User declarations
   __fastcall TProtokolProgressForm(TComponent* Owner);
   __fastcall ~TProtokolProgressForm();
   void __fastcall Completed();
};
//---------------------------------------------------------------------------
extern PACKAGE TProtokolProgressForm *ProtokolProgressForm;
//---------------------------------------------------------------------------
#endif
