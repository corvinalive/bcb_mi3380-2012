//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "MI1974CommonData_unit.h"
#include "TPRDLLInterface.h"
#include "ProverDLLInterface.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TMI1974CommonData::~TMI1974CommonData()
{
   int c=Products->Count;
   for(int i=0;i<c;i++)
      delete (TProduct*)Products->Items[i];
   delete Products;

};
//---------------------------------------------------------------------------
__fastcall TMI1974CommonData::TMI1974CommonData()
{
   Products=new TList();
   ProductCount=0;

   ProductCount=MITools.GetProductCount();
   for(int i=0;i<ProductCount;i++)
         Products->Add(new TProduct);
   MITools.GetProductList2(Products);

}
//---------------------------------------------------------------------------
