//---------------------------------------------------------------------------

#ifndef SelectMIForm_unitH
#define SelectMIForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "MI1974_unit.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TSelectMIForm : public TForm
{
__published:	// IDE-managed Components
   TListBox *ListBox1;
   TLabel *Label1;
   TGroupBox *GroupBox1;
   TLabel *Label2;
   TLabel *ProductLabel;
   TLabel *Label3;
   TLabel *Label4;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *Label8;
   TLabel *Label9;
   TStaticText *DescriptionLabel;
   TLabel *NameLabel;
   TLabel *LocationLabel;
   TLabel *Label10;
   TLabel *ProverLabel;
   TLabel *ProverSNLabel;
   TLabel *Label12;
   TLabel *Label13;
   TLabel *Label14;
   TLabel *Label15;
   TLabel *Label17;
   TLabel *Label18;
   TLabel *SensorNameLabel;
   TLabel *RFTNameLabel;
   TLabel *MMLocationLabel;
   TLabel *OwnerLabel;
   TLabel *SensorSNLabel;
   TBitBtn *Button1;
   TBitBtn *Button2;
   TBitBtn *DeleteButton;
   TBitBtn *OpenButton;
   TBitBtn *CopyButton;
   TLabel *Label19;
   TEdit *Edit1;
   void __fastcall ListBox1DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall ListBox1Click(TObject *Sender);
   void __fastcall ListBox1DblClick(TObject *Sender);
   void __fastcall OpenButtonClick(TObject *Sender);
   void __fastcall DeleteButtonClick(TObject *Sender);
   void __fastcall CopyButtonClick(TObject *Sender);
   void __fastcall Edit1Change(TObject *Sender);
private:	// User declarations
   IStorage* MIRoot;
   IStorage* Root;
   TList* Data;
   TList* Data1;
//   TIntList IDs;
   int h;
   bool IsManage;
   void __fastcall Sort();
   void __fastcall FillGrid();
public:		// User declarations

   __fastcall TSelectMIForm(TComponent* Owner);
   int __fastcall Select(IStorage* Root,IStorage* MIRoot);
   int __fastcall Manage(IStorage* Root, IStorage* MIRoot);
   void __fastcall FillData();
   void __fastcall ClearList();
   __fastcall ~TSelectMIForm();
};
//---------------------------------------------------------------------------
extern PACKAGE TSelectMIForm *SelectMIForm;
//---------------------------------------------------------------------------
#endif
