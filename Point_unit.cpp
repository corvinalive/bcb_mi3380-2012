//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "Point_unit.h"
#include "measure_unit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
//--- implemetation TMeasurePoint -------------------------------------------
//---------------------------------------------------------------------------
int TMeasurePoint::ReadVarCount;


__fastcall TMeasurePoint::TMeasurePoint()
{
   FMeasures = new TList();
   Add(new TMeasure());
   memset(FVar,0,sizeof(TVar)*VarCount);
   FVar[Used].Value.b=false;
};
//---------------------------------------------------------------------------
__fastcall TMeasurePoint::~TMeasurePoint()
{
  int i=FMeasures->Count;
  for (int j=0;j<i;j++)
    {
    TMeasure* m=(TMeasure*)FMeasures->Items[j];
    delete m;
    }

  delete FMeasures;
};
//---------------------------------------------------------------------------
int __fastcall TMeasurePoint::GetCount()
{
   return FMeasures->Count;
};
//---------------------------------------------------------------------------
int __fastcall TMeasurePoint::Add(TVarContainer* m)
{
   TMeasure* mm=static_cast<TMeasure*>(m);
   if(mm) return FMeasures->Add(mm);
   else return -1;
};
//---------------------------------------------------------------------------
int __fastcall TMeasurePoint::AddNew()
{
   return Add(new TMeasure());
};
//---------------------------------------------------------------------------
void __fastcall TMeasurePoint::Delete(int Index)
{
   if((Index>=0)&&(Index<FMeasures->Count))
      {
      TMeasure* m=(TMeasure*)FMeasures->Items[Index];
      FMeasures->Delete(Index);
      delete m;
      }
};
//---------------------------------------------------------------------------
TVar* __fastcall TMeasurePoint::GetVar(int index)
{
   if((index>=VarCount)||(index<0)) return NULL;
      else return &FVar[index];
}
//---------------------------------------------------------------------------
TVarContainer*  __fastcall TMeasurePoint::GetMeasures(int index)
{
   if(index>=FMeasures->Count) return NULL;
      else return (TVarContainer*)FMeasures->Items[index];
}
//---------------------------------------------------------------------------
void __fastcall TMeasurePoint::Save(IStream* Stream)
{
   ULONG ul;
   Stream->Write(FVar,VarCount*sizeof(TVar),&ul);
   int c=FMeasures->Count;
   Stream->Write(&c,sizeof(c),&ul);
   TVarContainer* v;
   for(int i=0;i<c;i++)
      {
      v=Measures[i];
      v->Save(Stream);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMeasurePoint::Load(IStream* Stream)
{
   ULONG ul;
   Stream->Read(FVar,ReadVarCount*sizeof(TVar),&ul);
   int c;
   Stream->Read(&c,sizeof(c),&ul);
   ClearMeasures();
   TMeasure* v;
   for(int i=0;i<c;i++)
      {
      v=new TMeasure();
      v->Load(Stream);
      Add(v);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMeasurePoint::ClearMeasures()
{
  int i=FMeasures->Count;
  for (int j=0;j<i;j++)
    {
    TMeasure* m=(TMeasure*)FMeasures->Items[j];
    delete m;
    }
  FMeasures->Clear();
}
//---------------------------------------------------------------------------
int __fastcall TMeasurePoint::GetMeasureVarCount()
{
   return TMeasure::VarCount;
}
//---------------------------------------------------------------------------
