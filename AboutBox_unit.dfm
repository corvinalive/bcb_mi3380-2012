object AboutBox: TAboutBox
  Left = 470
  Top = 258
  HelpKeyword = '200'
  BorderStyle = bsDialog
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  ClientHeight = 277
  ClientWidth = 413
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Times New Roman'
  Font.Style = []
  HelpFile = 'mi1974.hlp'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 19
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 393
    Height = 233
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object ProgramIcon: TImage
      Left = 16
      Top = 11
      Width = 32
      Height = 32
      Center = True
      Picture.Data = {
        07544269746D6170F6000000424DF60000000000000076000000280000001000
        0000100000000100040000000000800000000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF0088800000000008888880FFFFFFFF08888880FAAAEEEF08888880AAAAEEEE
        0888888AAFF77FFEE88888AAFFFC6FFFE88888AAFFCFF6FFEE8888AA7CFFFF67
        EE8888EE76FFFFC7AA8888EEFF6FFCFFAA8888EEFFF6CFFFA888888EEFF77FFA
        A8888880EEEEAAAA08888880FEEEAAAF08888880FFFFFFFF0888888000000000
        0888}
      Stretch = True
      Transparent = True
      IsControl = True
    end
    object ProductName: TLabel
      Left = 62
      Top = 10
      Width = 204
      Height = 19
      Caption = #1055#1086#1074#1077#1088#1082#1072' '#1055#1056' '#1087#1086' '#1052#1048' 3380-2012'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      IsControl = True
    end
    object Version: TLabel
      Left = 60
      Top = 31
      Width = 72
      Height = 19
      Caption = #1042#1077#1088#1089#1080#1103' 0.1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      IsControl = True
    end
    object Copyright: TLabel
      Left = 7
      Top = 59
      Width = 248
      Height = 19
      Caption = #1047#1086#1085#1086#1074' '#1042#1072#1083#1077#1088#1080#1081' '#1052#1080#1093#1072#1081#1083#1086#1074#1080#1095', 2013 '#1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      IsControl = True
    end
    object Comments: TLabel
      Left = 6
      Top = 98
      Width = 371
      Height = 38
      Caption = 
        #1042#1089#1077' '#1042#1072#1096#1080' '#1087#1086#1078#1077#1083#1072#1085#1080#1103' '#1080' '#1079#1072#1084#1077#1095#1072#1085#1080#1103' '#1084#1086#1075#1091#1090' '#1073#1099#1090#1100' '#1091#1095#1090#1077#1085#1099', '#1077#1089#1083#1080' '#1074#1099' '#1085#1072#1087#1080#1096#1080 +
        #1090#1077' '#1084#1085#1077' '#1086' '#1085#1080#1093' '#1085#1072' '#1084#1086#1081' '#1072#1076#1088#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      IsControl = True
    end
    object Label1: TLabel
      Left = 90
      Top = 159
      Width = 156
      Height = 20
      Cursor = crHandPoint
      Caption = 'corvinalive@yandex.ru'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = Label1Click
    end
    object Label2: TLabel
      Left = 8
      Top = 184
      Width = 162
      Height = 19
      Caption = #1057#1090#1088#1072#1085#1080#1095#1082#1072' '#1074' '#1080#1085#1090#1077#1088#1085#1077#1090#1077':'
    end
    object Label3: TLabel
      Left = 8
      Top = 208
      Width = 348
      Height = 20
      Cursor = crHandPoint
      Caption = 'https://bitbucket.org/corvinalive/bcb_mi3380-2012'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      OnClick = Label3Click
    end
  end
  object OKButton: TButton
    Left = 175
    Top = 246
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
end
