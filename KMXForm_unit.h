//---------------------------------------------------------------------------

#ifndef KMXForm_unitH
#define KMXForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MIGrid\MIGrid.h"
#include "MI1974_unit.h"
#include <Grids.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TKMXForm : public TForm
{
__published:	// IDE-managed Components
   TMIGrid *MIGrid;
   TGroupBox *GroupBox1;
   TStringGrid *ResGrid;
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall ResGridDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State);
   void __fastcall ResGridColumnMoved(TObject *Sender, int FromIndex,
          int ToIndex);
   void __fastcall FormActivate(TObject *Sender);
   void __fastcall FormDeactivate(TObject *Sender);

private:
   void __fastcall MIChange(TObject* Sender);	// User declarations
   void __fastcall ResChange(TObject* Sender);	// User declarations
public:		// User declarations
   //New calibration
   bool IsNew;
   IStorage* MIRoot;
   IStorage* Root;
   TKMX* MI;
   TKMX_data data;

   __fastcall TKMXForm(TComponent* Owner,IStorage* Root,IStorage* MIRoot,TKMX_data data);

   __fastcall TKMXForm(TComponent* Owner,IStorage* Root,IStorage* MIRoot,int ID);
   __fastcall ~TKMXForm();
   void __fastcall Save();
   void __fastcall MakeCaption();
   void __fastcall SetupResGrid();

};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
