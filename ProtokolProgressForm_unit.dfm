object ProtokolProgressForm: TProtokolProgressForm
  Left = 252
  Top = 351
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1090#1086#1082#1086#1083#1072' '#1087#1086#1074#1077#1088#1082#1080
  ClientHeight = 73
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000FFFFFFFF00000000FAAAEEEF00000000AAAAEEEE0000000A
    AFF77FFEE00000AAFFFC6FFFE00000AAFFCFF6FFEE0000AA7CFFFF67EE0000EE
    76FFFFC7AA0000EEFF6FFCFFAA0000EEFFF6CFFFA000000EEFF77FFAA0000000
    EEEEAAAA00000000FEEEAAAF00000000FFFFFFFF00000000000000000000E007
    0000E0070000E0070000E0070000E0070000C0070000C0030000C0030000C003
    0000C0030000C0070000E0070000E0070000E0070000E0070000E0070000}
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Gauge: TCGauge
    Left = 2
    Top = 44
    Width = 465
    Height = 25
    ForeColor = clSkyBlue
    Progress = 50
  end
  object Label1: TLabel
    Left = 8
    Top = 24
    Width = 118
    Height = 15
    Caption = #1060#1072#1079#1099' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 2
    Top = 2
    Width = 93
    Height = 19
    Caption = #1042#1099#1087#1086#1083#1085#1103#1077#1090#1089#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
end
