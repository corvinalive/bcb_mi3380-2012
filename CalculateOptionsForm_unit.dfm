object CalculateOptionsForm: TCalculateOptionsForm
  Left = 258
  Top = 137
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1103
  ClientHeight = 402
  ClientWidth = 622
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF007000
    00000000000777777777777777777E8E8E8E8E8E8E8777777777777777777000
    FF7EE7FF00070000F7E77E7F000000007E7FF7E700000007E7FFFF7E7000007E
    7FFFFFF7E70007E7FFFFFFFF7E707E70FFFFFFFF07E77E777777777777E77EEE
    EEEEEEEEEEE707777777777777700000FFFFFFFF000000000000000000006006
    000000000000000000000000000060060000E0070000E0070000E0070000C003
    00008001000000000000000000000000000080010000E0070000E0070000}
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupRadioGroup: TRadioGroup
    Left = 4
    Top = 4
    Width = 613
    Height = 77
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1075#1088#1091#1087#1087#1091' '#1079#1085#1072#1095#1077#1085#1080#1081
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      #1048#1079#1084#1077#1088#1077#1085#1080#1103
      #1058#1086#1095#1082#1080
      #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1086#1074#1077#1088#1082#1080
      #1055#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1072)
    TabOrder = 0
    OnClick = GroupRadioGroupClick
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 84
    Width = 613
    Height = 237
    Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1079#1085#1072#1095#1077#1085#1080#1081
    TabOrder = 1
    object ListBox1: TListBox
      Left = 4
      Top = 16
      Width = 173
      Height = 213
      Style = lbOwnerDrawFixed
      Color = clWhite
      ExtendedSelect = False
      ItemHeight = 28
      TabOrder = 0
      OnClick = ListBox1Click
      OnDrawItem = ListBox1DrawItem
    end
    object GroupBox2: TGroupBox
      Left = 184
      Top = 16
      Width = 217
      Height = 57
      Caption = #1045#1076#1080#1085#1080#1094#1072' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
      TabOrder = 1
      object UnitPaintBox: TPaintBox
        Left = 2
        Top = 15
        Width = 213
        Height = 40
        Align = alClient
        OnPaint = UnitPaintBoxPaint
      end
    end
    object GroupBox3: TGroupBox
      Left = 184
      Top = 76
      Width = 217
      Height = 151
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077
      TabOrder = 2
      object DescriptionLabel: TLabel
        Left = 2
        Top = 15
        Width = 213
        Height = 134
        Align = alClient
        Alignment = taCenter
        Caption = 'DescriptionLabel'
        WordWrap = True
      end
    end
    object GroupBox4: TGroupBox
      Left = 436
      Top = 16
      Width = 173
      Height = 105
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 40
        Width = 75
        Height = 13
        Caption = #1054#1082#1088#1091#1075#1083#1077#1085#1080#1077' '#1076#1086
      end
      object RadioButton1: TRadioButton
        Left = 8
        Top = 16
        Width = 113
        Height = 17
        Caption = #1041#1077#1079' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = CalculateChange
      end
      object RadioButton2: TRadioButton
        Left = 32
        Top = 60
        Width = 134
        Height = 17
        Caption = #1047#1085#1072#1082#1086#1074' '#1087#1086#1089#1083#1077' '#1079#1072#1087#1103#1090#1086#1081
        TabOrder = 1
        OnClick = CalculateChange
      end
      object RadioButton3: TRadioButton
        Left = 32
        Top = 80
        Width = 133
        Height = 17
        Caption = #1047#1085#1072#1095#1072#1097#1080#1093' '#1094#1080#1092#1088
        TabOrder = 2
        OnClick = CalculateChange
      end
      object CSpinEdit1: TCSpinEdit
        Left = 92
        Top = 36
        Width = 49
        Height = 22
        TabOrder = 3
        OnChange = CalculateChange
      end
    end
    object GroupBox5: TGroupBox
      Left = 436
      Top = 124
      Width = 173
      Height = 105
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
      TabOrder = 4
      object Label2: TLabel
        Left = 8
        Top = 40
        Width = 75
        Height = 13
        Caption = #1054#1082#1088#1091#1075#1083#1077#1085#1080#1077' '#1076#1086
      end
      object RadioButton4: TRadioButton
        Left = 8
        Top = 16
        Width = 113
        Height = 17
        Caption = #1041#1077#1079' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = CalculateChange
      end
      object RadioButton5: TRadioButton
        Left = 32
        Top = 60
        Width = 137
        Height = 17
        Caption = #1047#1085#1072#1082#1086#1074' '#1087#1086#1089#1083#1077' '#1079#1072#1087#1103#1090#1086#1081
        TabOrder = 1
        OnClick = CalculateChange
      end
      object RadioButton6: TRadioButton
        Left = 32
        Top = 80
        Width = 133
        Height = 17
        Caption = #1047#1085#1072#1095#1072#1097#1080#1093' '#1094#1080#1092#1088
        TabOrder = 2
        OnClick = CalculateChange
      end
      object CSpinEdit2: TCSpinEdit
        Left = 96
        Top = 36
        Width = 65
        Height = 22
        TabOrder = 3
        OnChange = CalculateChange
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 4
    Top = 372
    Width = 303
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 312
    Top = 372
    Width = 303
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 4
    Kind = bkCancel
  end
  object GroupBox6: TGroupBox
    Left = 4
    Top = 324
    Width = 613
    Height = 45
    Caption = #1055#1088#1077#1076#1091#1089#1090#1072#1085#1086#1074#1082#1080
    TabOrder = 2
    object Button1: TButton
      Left = 6
      Top = 14
      Width = 297
      Height = 25
      Caption = #1054#1082#1088#1091#1075#1083#1103#1090#1100' '#1087#1088#1080' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1080' '#1087#1086' '#1052#1048' 1974-2004'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 308
      Top = 14
      Width = 297
      Height = 25
      Caption = #1042#1099#1095#1080#1089#1083#1103#1090#1100' '#1073#1077#1079' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
