object CalculateFreqForm: TCalculateFreqForm
  Left = 309
  Top = 303
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1095#1080#1089#1083#1077#1085#1080#1077' '#1095#1072#1089#1090#1086#1090#1099
  ClientHeight = 59
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 175
    Top = 32
    Width = 11
    Height = 24
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 191
    Top = 34
    Width = 70
    Height = 18
    Caption = #1054#1096#1080#1073#1082#1072'!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 2
    Top = 2
    Width = 86
    Height = 13
    Caption = #1058#1077#1082#1091#1097#1080#1077' '#1076#1072#1085#1085#1099#1077
  end
  object Label6: TLabel
    Left = 16
    Top = 14
    Width = 7
    Height = 20
    Caption = 'f'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 78
    Top = 14
    Width = 14
    Height = 20
    Caption = 'Q'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 116
    Top = 2
    Width = 44
    Height = 26
    Caption = #1053#1091#1078#1085#1099#1081' '#1088#1072#1089#1093#1086#1076
    WordWrap = True
  end
  object Label8: TLabel
    Left = 171
    Top = 2
    Width = 118
    Height = 26
    Alignment = taCenter
    BiDiMode = bdLeftToRight
    Caption = #1042#1099#1095#1080#1089#1083#1077#1085#1085#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1095#1072#1089#1090#1086#1090#1099
    ParentBiDiMode = False
    WordWrap = True
  end
  object NumberEdit1: TNumberEdit
    Left = 2
    Top = 34
    Width = 53
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
  object NumberEdit2: TNumberEdit
    Left = 59
    Top = 34
    Width = 50
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
  object NumberEdit3: TNumberEdit
    Left = 114
    Top = 34
    Width = 52
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
end
