object KMXOptionsForm: TKMXOptionsForm
  Left = 314
  Top = 111
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1082#1086#1085#1090#1088#1086#1083#1103' '#1084#1077#1090#1088#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1093' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082' '#1058#1055#1056
  ClientHeight = 436
  ClientWidth = 670
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF007000
    00000000000777777777777777777E8E8E8E8E8E8E8777777777777777777000
    FF7EE7FF00070000F7E77E7F000000007E7FF7E700000007E7FFFF7E7000007E
    7FFFFFF7E70007E7FFFFFFFF7E707E70FFFFFFFF07E77E777777777777E77EEE
    EEEEEEEEEEE707777777777777700000FFFFFFFF000000000000000000006006
    000000000000000000000000000060060000E0070000E0070000E0070000C003
    00008001000000000000000000000000000080010000E0070000E0070000}
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 649
    Height = 393
    ActivePage = TabSheet1
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1044#1072#1085#1085#1099#1077' '#1078#1080#1076#1082#1086#1089#1090#1080
      object Label21: TLabel
        Left = 243
        Top = 201
        Width = 174
        Height = 13
        Caption = #1042#1103#1079#1082#1086#1089#1090#1100' '#1078#1080#1076#1082#1086#1089#1090#1080' '#1087#1088#1080' '#1050#1052#1061', '#1089#1057#1090':'
      end
      object ProductGroupBox: TGroupBox
        Left = 4
        Top = 20
        Width = 543
        Height = 93
        Caption = #1055#1086#1074#1077#1088#1086#1095#1085#1072#1103' '#1078#1080#1076#1082#1086#1089#1090#1100
        TabOrder = 0
        object Label6: TLabel
          Left = 6
          Top = 14
          Width = 214
          Height = 26
          Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1086#1074' '#1086#1073#1098#1077#1084#1085#1086#1075#1086' '#1088#1072#1089#1096#1080#1088#1077#1085#1080#1103' '#1080' '#1089#1078#1080#1084#1072#1077#1084#1086#1089#1090#1080
          WordWrap = True
        end
        object Label7: TLabel
          Left = 8
          Top = 50
          Width = 125
          Height = 13
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1076#1091#1082#1090#1072
        end
        object ProductComboBox: TComboBox
          Left = 232
          Top = 16
          Width = 307
          Height = 21
          Style = csDropDownList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          OnChange = ProductComboBoxChange
        end
        object ProductNameEdit: TEdit
          Left = 232
          Top = 44
          Width = 307
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 36
          ParentFont = False
          TabOrder = 1
        end
        object DensRecalcCheckBox: TCheckBox
          Left = 8
          Top = 70
          Width = 401
          Height = 17
          Caption = #1055#1088#1080#1074#1086#1076#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1086#1074' '#1082' '#1091#1089#1083#1086#1074#1080#1103#1084' '#1055#1059
          TabOrder = 2
          OnClick = DensRecalcCheckBoxClick
        end
      end
      object DensityGroupBox: TGroupBox
        Left = 6
        Top = 130
        Width = 211
        Height = 195
        Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1087#1083#1086#1090#1085#1086#1089#1090#1080
        TabOrder = 1
        object Label13: TLabel
          Left = 20
          Top = 62
          Width = 90
          Height = 13
          Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100', '#1082#1075'/'#1084'3'
        end
        object Label14: TLabel
          Left = 20
          Top = 84
          Width = 102
          Height = 13
          Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072' '#1087#1088#1086#1073#1099
        end
        object Image1: TImage
          Left = 20
          Top = 104
          Width = 14
          Height = 25
          AutoSize = True
          Picture.Data = {
            07544269746D6170A2000000424DA2000000000000003E000000280000000E00
            0000190000000100010000000000640000000000000000000000020000000000
            000000000000FFFFFF00FFFC0000FFFC0000FFFC0000FFFC0000FFFC0000CFFC
            0000CFFC0000CFFC0000CFFC0000C0FC0000CE7C0000CF3C0000CF3C0000CF3C
            0000CF3C0000CE7C0000C9FC0000CEFC0000CE7C0000CE7C0000EE7C0000F0FC
            0000FFFC0000FFFC0000FFFC0000}
          Transparent = True
        end
        object Image2: TImage
          Left = 20
          Top = 134
          Width = 14
          Height = 21
          AutoSize = True
          Picture.Data = {
            07544269746D617092000000424D92000000000000003E000000280000000E00
            0000150000000100010000000000540000000000000000000000020000000000
            000000000000FFFFFF00FFFC0000FFFC0000FFFC0000FFFC0000FFFC0000FCFC
            0000FCFC0000FCFC0000FCFC0000FEFC0000FEFC0000FCFC0000FD7C0000FD7C
            0000FD3C0000ED3C0000EDBC0000F39C0000FFFC0000FFFC0000FFFC0000}
          Transparent = True
        end
        object UseFDRadioButton: TRadioButton
          Left = 8
          Top = 18
          Width = 113
          Height = 17
          Caption = #1055#1086' '#1055#1055
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = NotUseFDRadioButtonClick
        end
        object NotUseFDRadioButton: TRadioButton
          Left = 8
          Top = 38
          Width = 157
          Height = 17
          Caption = #1055#1086' '#1087#1088#1086#1073#1077', '#1087#1077#1088#1077#1076' '#1087#1086#1074#1077#1088#1082#1086#1081':'
          TabOrder = 1
          OnClick = NotUseFDRadioButtonClick
        end
        object GetButton: TButton
          Left = 6
          Top = 162
          Width = 189
          Height = 25
          Caption = #1054#1087#1088#1077#1076#1077#1083#1080#1090#1100' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1099
          Enabled = False
          TabOrder = 6
          OnClick = GetButtonClick
        end
        object DensityEdit: TNumberEdit
          Left = 125
          Top = 56
          Width = 69
          Height = 24
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Text = '0'
          NumberType = Double
        end
        object tFDEdit: TNumberEdit
          Left = 125
          Top = 80
          Width = 69
          Height = 24
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Text = '0'
          NumberType = Double
        end
        object BettaEdit: TNumberEdit
          Left = 48
          Top = 106
          Width = 146
          Height = 24
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Text = '0'
          NumberType = Double
        end
        object FEdit: TNumberEdit
          Left = 48
          Top = 132
          Width = 146
          Height = 24
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          Text = '0'
          NumberType = Double
        end
      end
      object vEdit: TNumberEdit
        Left = 425
        Top = 195
        Width = 41
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '0'
        NumberType = Double
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1072#1085#1085#1099#1077' '#1087#1088#1086#1090#1086#1082#1086#1083#1072
      ImageIndex = 1
      object GroupBox6: TGroupBox
        Left = 6
        Top = 7
        Width = 271
        Height = 69
        Caption = #1055#1088#1086#1090#1086#1082#1086#1083
        TabOrder = 0
        object Label18: TLabel
          Left = 6
          Top = 22
          Width = 90
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1087#1088#1086#1090#1086#1082#1086#1083#1072
        end
        object Label19: TLabel
          Left = 6
          Top = 44
          Width = 121
          Height = 13
          Caption = #1052#1077#1089#1090#1086' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103' '#1050#1052#1061
        end
        object ProtokolNEdit: TEdit
          Left = 150
          Top = 16
          Width = 115
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Text = 'ProtokolNEdit'
        end
        object LocationEdit: TEdit
          Left = 150
          Top = 40
          Width = 115
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 1
        end
      end
      object GroupBox5: TGroupBox
        Left = 6
        Top = 86
        Width = 547
        Height = 172
        Caption = #1044#1072#1085#1085#1099#1077' '#1086' '#1083#1080#1094#1072#1093', '#1087#1088#1086#1074#1086#1076#1080#1074#1096#1080#1093' '#1050#1052#1061
        TabOrder = 1
        object Label4: TLabel
          Left = 156
          Top = 48
          Width = 67
          Height = 13
          Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
        end
        object Label8: TLabel
          Left = 284
          Top = 48
          Width = 58
          Height = 13
          Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
        end
        object Label10: TLabel
          Left = 408
          Top = 48
          Width = 77
          Height = 13
          Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'. '#1054'.'
        end
        object Label15: TLabel
          Left = 6
          Top = 57
          Width = 123
          Height = 13
          Caption = #1057#1077#1088#1074#1080#1089#1085#1072#1103' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
        end
        object Label16: TLabel
          Left = 6
          Top = 90
          Width = 92
          Height = 13
          Caption = #1057#1076#1072#1102#1097#1072#1103' '#1089#1090#1086#1088#1086#1085#1072
        end
        object Label17: TLabel
          Left = 6
          Top = 117
          Width = 143
          Height = 13
          Caption = #1055#1088#1080#1085#1080#1084#1072#1102#1097#1072#1103' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
        end
        object CompanyEdit1: TEdit
          Left = 158
          Top = 63
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'CompanyEdit1'
        end
        object CompanyEdit2: TEdit
          Left = 158
          Top = 87
          Width = 121
          Height = 21
          TabOrder = 3
          Text = 'CompanyEdit2'
        end
        object CompanyEdit3: TEdit
          Left = 158
          Top = 111
          Width = 121
          Height = 21
          TabOrder = 6
          Text = 'CompanyEdit3'
        end
        object RankEdit1: TEdit
          Left = 282
          Top = 63
          Width = 121
          Height = 21
          TabOrder = 1
          Text = 'Edit1'
        end
        object RankEdit2: TEdit
          Left = 282
          Top = 87
          Width = 121
          Height = 21
          TabOrder = 4
          Text = 'Edit2'
        end
        object RankEdit3: TEdit
          Left = 282
          Top = 111
          Width = 121
          Height = 21
          TabOrder = 7
          Text = 'Edit3'
        end
        object FIOEdit1: TEdit
          Left = 408
          Top = 63
          Width = 121
          Height = 21
          TabOrder = 2
          Text = 'Edit1'
        end
        object FIOEdit2: TEdit
          Left = 408
          Top = 87
          Width = 121
          Height = 21
          TabOrder = 5
          Text = 'Edit2'
        end
        object FIOEdit3: TEdit
          Left = 408
          Top = 111
          Width = 121
          Height = 21
          TabOrder = 8
          Text = 'Edit3'
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1044#1072#1085#1085#1099#1077' '#1050#1052#1061
      ImageIndex = 2
      object Label11: TLabel
        Left = 360
        Top = 43
        Width = 204
        Height = 13
        Caption = #1042#1099#1073#1086#1088' '#1075#1088#1072#1076#1091#1080#1088#1086#1074#1086#1095#1085#1086#1081' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080':'
      end
      object Label12: TLabel
        Left = 360
        Top = 93
        Width = 202
        Height = 26
        Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072' '#1080#1085#1074#1072#1088#1086#1074#1086#1075#1086' '#1089#1090#1077#1088#1078#1085#1103' ('#1076#1083#1103' '#1082#1086#1084#1087#1072#1082#1090'-'#1087#1088#1091#1074#1077#1088#1072')'
        WordWrap = True
      end
      object Label9: TLabel
        Left = 364
        Top = 184
        Width = 115
        Height = 13
        Caption = #1044#1072#1090#1072' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103' '#1050#1052#1061
      end
      object Label5: TLabel
        Left = 6
        Top = 256
        Width = 79
        Height = 13
        Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1050#1052#1061':'
      end
      object DescriptionEdit: TMemo
        Left = 6
        Top = 273
        Width = 389
        Height = 71
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        TabOrder = 0
      end
      object GroupBox2: TGroupBox
        Left = 4
        Top = 125
        Width = 345
        Height = 125
        Caption = #1042#1099#1073#1086#1088' '#1080#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1093' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074' '#1087#1086#1074#1077#1088#1082#1080' '#1058#1055#1059
        TabOrder = 1
        object ListBox1: TListBox
          Left = 4
          Top = 18
          Width = 151
          Height = 101
          Style = lbVirtual
          ItemHeight = 30
          TabOrder = 0
          OnDrawItem = ListBox1DrawItem
        end
        object ListBox2: TListBox
          Left = 190
          Top = 22
          Width = 151
          Height = 99
          Style = lbVirtual
          ItemHeight = 30
          TabOrder = 5
          OnDrawItem = ListBox2DrawItem
        end
        object RightButton: TBitBtn
          Left = 157
          Top = 22
          Width = 30
          Height = 23
          TabOrder = 1
          OnClick = RightButtonClick
          Glyph.Data = {
            DE000000424DDE0000000000000076000000280000000D0000000D0000000100
            0400000000006800000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7000777777077777700077777700777770007777770607777000770000066077
            7000770666666607700077066666666070007706666666077000770000066077
            7000777777060777700077777700777770007777770777777000777777777777
            7000}
        end
        object AllRightButton: TBitBtn
          Left = 157
          Top = 46
          Width = 30
          Height = 23
          TabOrder = 2
          OnClick = AllRightButtonClick
          Glyph.Data = {
            DE000000424DDE0000000000000076000000280000000D0000000D0000000100
            0400000000006800000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7000777707707777700077770070077770007777060060777000000006606607
            7000066666660660700006666666606600000666666606607000000006606607
            7000777706006077700077770070077770007777077077777000777777777777
            7000}
        end
        object LeftButton: TBitBtn
          Left = 157
          Top = 70
          Width = 30
          Height = 23
          TabOrder = 3
          OnClick = LeftButtonClick
          Glyph.Data = {
            DE000000424DDE0000000000000076000000280000000D0000000D0000000100
            0400000000006800000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7000777777077777700077777007777770007777060777777000777066000007
            7000770666666607700070666666660770007706666666077000777066000007
            7000777706077777700077777007777770007777770777777000777777777777
            7000}
        end
        object AllLeftButton: TBitBtn
          Left = 157
          Top = 94
          Width = 30
          Height = 23
          TabOrder = 4
          OnClick = AllLeftButtonClick
          Glyph.Data = {
            DE000000424DDE0000000000000076000000280000000D0000000D0000000100
            0400000000006800000000000000000000001000000000000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7000777770770777700077770070077770007770600607777000770660660000
            0000706606666666000006606666666600007066066666660000770660660000
            0000777060060777700077770070077770007777707707777000777777777777
            7000}
        end
      end
      object RateRadioGroup: TRadioGroup
        Left = 4
        Top = 38
        Width = 349
        Height = 85
        Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1095#1072#1089#1090#1086#1090#1099
        ItemIndex = 1
        Items.Strings = (
          #1056#1091#1095#1085#1086#1081' '#1074#1074#1086#1076' '#1095#1072#1089#1090#1086#1090#1099
          #1042#1099#1095#1080#1089#1083#1103#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1095#1072#1089#1090#1086#1090#1099)
        TabOrder = 2
      end
      object MXComboBox: TComboBox
        Left = 360
        Top = 61
        Width = 201
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 0
        ParentFont = False
        TabOrder = 3
      end
      object tinvarEdit: TNumberEdit
        Left = 362
        Top = 130
        Width = 121
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Text = '0'
        NumberType = Double
      end
      object DatePicker: TDateTimePicker
        Left = 362
        Top = 201
        Width = 135
        Height = 24
        CalAlignment = dtaLeft
        Date = 38248.999617246
        Time = 38248.999617246
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 5
      end
    end
  end
  object OKButton: TBitBtn
    Left = 8
    Top = 402
    Width = 75
    Height = 27
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 88
    Top = 402
    Width = 75
    Height = 27
    TabOrder = 2
    Kind = bkCancel
  end
end
