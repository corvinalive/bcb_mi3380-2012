object KMXForm: TKMXForm
  Tag = 100
  Left = 207
  Top = 316
  Width = 706
  Height = 357
  Caption = #1050#1052#1061' '#1058#1055#1056
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000FFFFFFFF00000000FAAAEEEF00000000AAAAEEEE0000000A
    AFF77FFEE00000AAFFFC6FFFE00000AAFFCFF6FFEE0000AA7CFFFF67EE0000EE
    76FFFFC7AA0000EEFF6FFCFFAA0000EEFFF6CFFFA000000EEFF77FFAA0000000
    EEEEAAAA00000000FEEEAAAF00000000FFFFFFFF00000000000000000000E007
    0000E0070000E0070000E0070000E0070000C0070000C0030000C0030000C003
    0000C0030000C0070000E0070000E0070000E0070000E0070000E0070000}
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object MIGrid: TMIGrid
    Tag = 10
    Left = 0
    Top = 0
    Width = 698
    Height = 235
    PointDrawOptions.CellsSpace = 2
    Align = alClient
    HeaderHeight = 28
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 235
    Width = 698
    Height = 88
    Align = alBottom
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1050#1052#1061
    TabOrder = 1
    object ResGrid: TStringGrid
      Left = 2
      Top = 15
      Width = 694
      Height = 71
      Align = alClient
      DefaultRowHeight = 18
      DefaultDrawing = False
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goColMoving, goRowSelect]
      ScrollBars = ssHorizontal
      TabOrder = 0
      OnColumnMoved = ResGridColumnMoved
      OnDrawCell = ResGridDrawCell
      ColWidths = (
        47
        64
        61
        64
        64)
    end
  end
end
