//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "variable.h"
#include "MI1974_unit.h"
#include "KMX_unit.h"
#include "CalculateOptionsForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TCalculateOptionsForm *CalculateOptionsForm;
//---------------------------------------------------------------------------
__fastcall TCalculateOptionsForm::TCalculateOptionsForm(TComponent* Owner)
   : TForm(Owner)
{
   vo=NULL;
   FData=NULL;
   CurSel=-1;
   CalcOptions=0;

   TResourceStream* rs;
   int base;
   //Read Measure data
   short int buf1[512];
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,801,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   short int buf[512];
   memset(buf,0,512);
   rs = new TResourceStream((int)HInstance,811,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;


#pragma warn -8018
   for(int i=0;i<TMeasure::VarCount;i++)
      {
      PreSet1.MeasureOptions[i].CalcRoundType=buf1[i*4];
      PreSet1.MeasureOptions[i].CalcRoundNumber=buf1[i*4+1];
      PreSet1.MeasureOptions[i].ShowRoundType=buf1[i*4+2];
      PreSet1.MeasureOptions[i].ShowRoundNumber=buf1[i*4+3];

      PreSet2.MeasureOptions[i].CalcRoundType=buf[i*4];
      PreSet2.MeasureOptions[i].CalcRoundNumber=buf[i*4+1];
      PreSet2.MeasureOptions[i].ShowRoundType=buf[i*4+2];
      PreSet2.MeasureOptions[i].ShowRoundNumber=buf[i*4+3];
      }
   //Read point data
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,802,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   memset(buf,0,512);
   rs = new TResourceStream((int)HInstance,812,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   for(int i=0;i<TMeasurePoint::VarCount;i++)
      {
      PreSet1.PointOptions[i].CalcRoundType=buf1[i*4];
      PreSet1.PointOptions[i].CalcRoundNumber=buf1[i*4+1];
      PreSet1.PointOptions[i].ShowRoundType=buf1[i*4+2];
      PreSet1.PointOptions[i].ShowRoundNumber=buf1[i*4+3];

      PreSet2.PointOptions[i].CalcRoundType=buf[i*4];
      PreSet2.PointOptions[i].CalcRoundNumber=buf[i*4+1];
      PreSet2.PointOptions[i].ShowRoundType=buf[i*4+2];
      PreSet2.PointOptions[i].ShowRoundNumber=buf[i*4+3];
      }

   //Read MI data
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,803,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   memset(buf,0,512);
   rs = new TResourceStream((int)HInstance,813,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   for(int i=0;i<TMI1974::VarCount;i++)
      {
      PreSet1.MIOptions[i].CalcRoundType=buf1[i*4];
      PreSet1.MIOptions[i].CalcRoundNumber=buf1[i*4+1];
      PreSet1.MIOptions[i].ShowRoundType=buf1[i*4+2];
      PreSet1.MIOptions[i].ShowRoundNumber=buf1[i*4+3];

      PreSet2.MIOptions[i].CalcRoundType=buf[i*4];
      PreSet2.MIOptions[i].CalcRoundNumber=buf[i*4+1];
      PreSet2.MIOptions[i].ShowRoundType=buf[i*4+2];
      PreSet2.MIOptions[i].ShowRoundNumber=buf[i*4+3];
      }

   //Read Range data
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,804,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   memset(buf,0,512);
   rs = new TResourceStream((int)HInstance,814,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   for(int i=0;i<TRange::VarCount;i++)
      {
      PreSet1.RangeOptions[i].CalcRoundType=buf1[i*4];
      PreSet1.RangeOptions[i].CalcRoundNumber=buf1[i*4+1];
      PreSet1.RangeOptions[i].ShowRoundType=buf1[i*4+2];
      PreSet1.RangeOptions[i].ShowRoundNumber=buf1[i*4+3];

      PreSet2.RangeOptions[i].CalcRoundType=buf[i*4];
      PreSet2.RangeOptions[i].CalcRoundNumber=buf[i*4+1];
      PreSet2.RangeOptions[i].ShowRoundType=buf[i*4+2];
      PreSet2.RangeOptions[i].ShowRoundNumber=buf[i*4+3];
      }

#pragma warn +8018
}
//---------------------------------------------------------------------------
bool __fastcall TCalculateOptionsForm::ShowDialog(TMI1974* Data)
{
   FKMX=0;
   EnableControls(false);
   ItemChange=false;
   CurSel=-1;
   FData = Data;
   //Copy Measure CalcOptions;
   int c=FData->MeasureVarCount;
   for(int i=0;i<c;i++)
      memcpy(&MeasureOptions[i],&FData->MeasureVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));
   //Copy Point CalcOptions;
   c=FData->PointVarCount;
   for(int i=0;i<c;i++)
      memcpy(&PointOptions[i],&FData->PointVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));

   //Copy MI CalcOptions;
   c=FData->GridVarCount;
   for(int i=0;i<c;i++)
      memcpy(&MIOptions[i],&FData->MIVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));

   //Copy Range CalcOptions;
   c=FData->RangeVarCount;
   for(int i=0;i<c;i++)
      memcpy(&RangeOptions[i],&FData->RangeVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));


   GroupRadioGroup->ItemIndex=0;
   GroupRadioGroupClick(0);


   if(ShowModal()!=mrOk)
      return false;
   SaveChanges();
   //Save changes
   //Copy Measure CalcOptions;
   c=FData->MeasureVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FData->MeasureVarOptions[i]->CalcOptions,&MeasureOptions[i],
               sizeof(TCalcOptions));
   //Copy Point CalcOptions;
   c=FData->PointVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FData->PointVarOptions[i]->CalcOptions,&PointOptions[i],
               sizeof(TCalcOptions));

   //Copy MI CalcOptions;
   c=FData->GridVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FData->MIVarOptions[i]->CalcOptions,&MIOptions[i],
               sizeof(TCalcOptions));

   //Copy Range CalcOptions;
   c=FData->RangeVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FData->RangeVarOptions[i]->CalcOptions,&RangeOptions[i],
               sizeof(TCalcOptions));

   return true;
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::GroupRadioGroupClick(
      TObject *Sender)
{

   ListBox1->Items->Clear();
   switch (GroupRadioGroup->ItemIndex)
     {
     case 0:
        if(FData!=0)
         {
         vo=FData->MeasureVarOptions[0];
         VarCount=FData->MeasureVarCount;
         }
        else
         {
         vo=FKMX->MeasureVarOptions[0];
         VarCount=FKMX->MeasureVarCount;
         }
        CalcOptions=MeasureOptions;
        break;
     case 1:
        if(FData!=0)
         {
         vo=FData->PointVarOptions[0];
         VarCount=FData->PointVarCount;
         }
        else
         {
         vo=FKMX->PointVarOptions[0];
         VarCount=FKMX->PointVarCount;
         }
        CalcOptions=PointOptions;
        break;
     case 2:
        if(FData!=0)
         {
         vo=FData->MIVarOptions[0];
         VarCount=FData->GridVarCount;
         }
        else
         {
         vo=FKMX->MIVarOptions[0];
         VarCount=FKMX->GridVarCount;
         }
        CalcOptions=MIOptions;
        break;
     case 3:
        vo=FData->RangeVarOptions[0];
        VarCount=FData->RangeVarCount;
        CalcOptions=RangeOptions;
        break;
     default:
//        l= NULL;
        vo=NULL;
        return;
     }
   for(int i=0;i<VarCount;i++)
      ListBox1->Items->Add(vo[i].Name);
   if(ListBox1->Items->Count)
      {
      ListBox1->ItemIndex=0;
      CurSel=0;
      ListBox1->OnClick(0);
      }
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::ListBox1DrawItem(
      TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   if(!vo ) return;
   if(State.Contains(odSelected))
      ListBox1->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox1->Canvas->Brush->Color=clWhite;
   ListBox1->Canvas->FillRect(Rect);

   int vi=Index;
   int h;
   if(vo[vi].Bitmap==-1)
      {
      //Draw text
      ListBox1->Canvas->Font->Color=clBlack;
      h=ListBox1->Canvas->TextHeight("A");
      ListBox1->Canvas->TextOut(Rect.left+2,Rect.top+(Rect.Height()-h)/2,vo[vi].Name);
      }
   else
      {
      Graphics::TBitmap* b;
      if(FData!=0)
         {
         b=FData->OnGetBitmap(vo[vi].Bitmap);
         }
      else
         b=FKMX->OnGetBitmap(vo[vi].Bitmap);
      ListBox1->Canvas->CopyMode=cmSrcAnd;
      ListBox1->Canvas->Draw(Rect.left+2,Rect.top+(Rect.Height()-b->Height)/2,b);
      }
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::ListBox1Click(TObject *Sender)
{
   UnitPaintBox->Invalidate();
   int vi=ListBox1->ItemIndex;
   if(vi==-1)
      {
      //Clear all data;
      return;
      }
   if(!vo) return;
   ItemChange=true;


   DescriptionLabel->Caption=vo[vi].Description;
   if(CalcOptions[vi].CalcRoundType==TCalcOptions::None)
      RadioButton1->Checked=true;
   else
      if(CalcOptions[vi].CalcRoundType==TCalcOptions::AfterDot)
         RadioButton2->Checked=true;
      else
         RadioButton3->Checked=true;
   CSpinEdit1->Value=CalcOptions[vi].CalcRoundNumber;

   if(CalcOptions[vi].ShowRoundType==TCalcOptions::None)
      RadioButton4->Checked=true;
   else
      if(CalcOptions[vi].ShowRoundType==TCalcOptions::AfterDot)
         RadioButton5->Checked=true;
      else
         RadioButton6->Checked=true;
   CSpinEdit2->Value=CalcOptions[vi].ShowRoundNumber;

   CurSel=vi;
   ItemChange=false;
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::UnitPaintBoxPaint(TObject *Sender)
{
   if(ListBox1->ItemIndex==-1)
      {
      //Clear all data;
      return;
      }
   if(!vo) return;
//   ListBox1->Canvas->FillRect(Rect);
   int vi=ListBox1->ItemIndex;
   int h;
   if(vo[vi].UnitBitmap==-1)
      {
      //Draw text
      h=UnitPaintBox->Canvas->TextHeight("A");
      UnitPaintBox->Canvas->TextOut(UnitPaintBox->Left+4,
      /*UnitPaintBox->Top+*/(UnitPaintBox->Height-h)/2,vo[vi].Unit);
      }
   else
      {
      Graphics::TBitmap* b;
      if(FData!=NULL)
         b=FData->OnGetBitmap(vo[vi].UnitBitmap);
      else
         b=FKMX->OnGetBitmap(vo[vi].UnitBitmap);
      UnitPaintBox->Canvas->CopyMode=cmSrcAnd;
      UnitPaintBox->Canvas->Draw(UnitPaintBox->Left+4,
                                    (UnitPaintBox->Height - b->Height)/2,b);
      }
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::SaveChanges()
{
      if(RadioButton1->Checked)
         CalcOptions[CurSel].CalcRoundType=TCalcOptions::None;
      else
         if(RadioButton2->Checked)
            CalcOptions[CurSel].CalcRoundType=TCalcOptions::AfterDot;
         else
            CalcOptions[CurSel].CalcRoundType=TCalcOptions::SignDigits;
      CalcOptions[CurSel].CalcRoundNumber=CSpinEdit1->Value;

      if(RadioButton4->Checked)
         CalcOptions[CurSel].ShowRoundType=TCalcOptions::None;
      else
         if(RadioButton5->Checked)
            CalcOptions[CurSel].ShowRoundType=TCalcOptions::AfterDot;
         else
            CalcOptions[CurSel].ShowRoundType=TCalcOptions::SignDigits;
      CalcOptions[CurSel].ShowRoundNumber=CSpinEdit2->Value;
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::CalculateChange(TObject *Sender)
{
   if( (CurSel!=-1) && (!ItemChange) )
      {//Save changes
      SaveChanges();
      }
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::Button1Click(TObject *Sender)
{
   //Copy Measure CalcOptions;
   memcpy(MeasureOptions,PreSet1.MeasureOptions,sizeof(TCalcOptions)*
                  TMeasure::VarCount);
   //Copy Point CalcOptions;
   memcpy(PointOptions,PreSet1.PointOptions,sizeof(TCalcOptions)*
                  TMeasurePoint::VarCount);
   //Copy MI CalcOptions;
   memcpy(MIOptions,PreSet1.MIOptions,sizeof(TCalcOptions)*
                  TMI1974::VarCount);
   //Copy Range CalcOptions;
   memcpy(RangeOptions,PreSet1.RangeOptions,sizeof(TCalcOptions)*
                  TRange::VarCount);
   GroupRadioGroup->ItemIndex=0;
   GroupRadioGroupClick(0);
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::Button2Click(TObject *Sender)
{
   //Copy Measure CalcOptions;
   memcpy(MeasureOptions,PreSet2.MeasureOptions,sizeof(TCalcOptions)*
                  TMeasure::VarCount);
   //Copy Point CalcOptions;
   memcpy(PointOptions,PreSet2.PointOptions,sizeof(TCalcOptions)*
                  TMeasurePoint::VarCount);
   //Copy MI CalcOptions;
   memcpy(MIOptions,PreSet2.MIOptions,sizeof(TCalcOptions)*
                  TMI1974::VarCount);
   //Copy Range CalcOptions;
   memcpy(RangeOptions,PreSet2.RangeOptions,sizeof(TCalcOptions)*
                  TRange::VarCount);
   GroupRadioGroup->ItemIndex=0;
   GroupRadioGroupClick(0);
}
//---------------------------------------------------------------------------
bool __fastcall TCalculateOptionsForm::ShowKMXDialog(TKMX* KMX)
{
   FKMX=KMX;
   EnableControls(true);
   ItemChange=false;
   CurSel=-1;
   FData = 0;
   //Copy Measure CalcOptions;
   int c=FKMX->MeasureVarCount;
   for(int i=0;i<c;i++)
      memcpy(&MeasureOptions[i],&KMX->MeasureVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));
   //Copy Point CalcOptions;
   c=FKMX->PointVarCount;
   for(int i=0;i<c;i++)
      memcpy(&PointOptions[i],&FKMX->PointVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));

   //Copy MI CalcOptions;
   c=FKMX->GridVarCount;
   for(int i=0;i<c;i++)
      memcpy(&MIOptions[i],&FKMX->MIVarOptions[i]->CalcOptions,
               sizeof(TCalcOptions));

   GroupRadioGroup->ItemIndex=0;
   GroupRadioGroupClick(0);


   if(ShowModal()!=mrOk)
      return false;
   SaveChanges();
   //Save changes
   //Copy Measure CalcOptions;
   c=FKMX->MeasureVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FKMX->MeasureVarOptions[i]->CalcOptions,&MeasureOptions[i],
               sizeof(TCalcOptions));
   //Copy Point CalcOptions;
   c=FKMX->PointVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FKMX->PointVarOptions[i]->CalcOptions,&PointOptions[i],
               sizeof(TCalcOptions));

   //Copy MI CalcOptions;
   c=FKMX->GridVarCount;
   for(int i=0;i<c;i++)
      memcpy(&FKMX->MIVarOptions[i]->CalcOptions,&MIOptions[i],
               sizeof(TCalcOptions));

   return true;
}
//---------------------------------------------------------------------------
void __fastcall TCalculateOptionsForm::EnableControls(bool IfKMX)
{
   GroupRadioGroup->Items->Clear();
   GroupRadioGroup->Items->Add("���������");
   GroupRadioGroup->Items->Add("�����");
   if(IfKMX)
      GroupRadioGroup->Items->Add("���������� ���");
   else
      {
      GroupRadioGroup->Items->Add("���������� �������");
      GroupRadioGroup->Items->Add("������������");
      }
   GroupBox6->Visible=!IfKMX;
//   Button2->Visible=!IfKMX;


}
//---------------------------------------------------------------------------

