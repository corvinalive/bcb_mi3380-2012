//---------------------------------------------------------------------------


#pragma hdrstop

#include "KMXMeasure_unit.h"
#include "KMXPoint_unit.h"



#pragma package(smart_init)

//---------------------------------------------------------------------------
//--- implemetation TKMXMeasurePoint -------------------------------------------
//---------------------------------------------------------------------------
int TKMXMeasurePoint::ReadVarCount;
//---------------------------------------------------------------------------
__fastcall TKMXMeasurePoint::TKMXMeasurePoint()
{
   FMeasures = new TList();
   Add(new TKMXMeasure());
   memset(FVar,0,sizeof(TVar)*VarCount);
   FVar[Used].Value.b=false;
};
//---------------------------------------------------------------------------
__fastcall TKMXMeasurePoint::~TKMXMeasurePoint()
{
  int c=FMeasures->Count;
  for (int j=0;j < c;j++)
    {
    TKMXMeasure* m=(TKMXMeasure*)FMeasures->Items[j];
    delete m;
    }
  delete FMeasures;
};
//---------------------------------------------------------------------------
int __fastcall TKMXMeasurePoint::GetCount()
{
   return FMeasures->Count;
};
//---------------------------------------------------------------------------
int __fastcall TKMXMeasurePoint::Add(TVarContainer* m)
{
   if(m) return FMeasures->Add(m);
   else return -1;
};
//---------------------------------------------------------------------------
int __fastcall TKMXMeasurePoint::AddNew()
{
   return Add(new TKMXMeasure());
};
//---------------------------------------------------------------------------
void __fastcall TKMXMeasurePoint::Delete(int Index)
{
   if((Index>=0)&&( Index < FMeasures->Count))
      {
      TKMXMeasure* m=(TKMXMeasure*)FMeasures->Items[Index];
      delete m;
      FMeasures->Delete(Index);
      }
};
//---------------------------------------------------------------------------
TVar* __fastcall TKMXMeasurePoint::GetVar(int index)
{
   if((index>=VarCount)||(index<0)) return NULL;
      else return &FVar[index];
}
//---------------------------------------------------------------------------
TVarContainer*  __fastcall TKMXMeasurePoint::GetMeasures(int index)
{
   if(index>=FMeasures->Count) return NULL;
      else return (TVarContainer*)FMeasures->Items[index];
}
//---------------------------------------------------------------------------
void __fastcall TKMXMeasurePoint::Save(IStream* Stream)
{
   ULONG ul;
   Stream->Write(FVar,VarCount*sizeof(TVar),&ul);
   int c=FMeasures->Count;
   Stream->Write(&c,sizeof(c),&ul);
   TVarContainer* v;
   for(int i=0;i<c;i++)
      {
      v=Measures[i];
      v->Save(Stream);
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMXMeasurePoint::Load(IStream* Stream)
{
   ULONG ul;
   Stream->Read(FVar,ReadVarCount*sizeof(TVar),&ul);
   int c;
   Stream->Read(&c,sizeof(c),&ul);
   ClearMeasures();
   TKMXMeasure* v;
   for(int i=0;i<c;i++)
      {
      v=new TKMXMeasure();
      v->Load(Stream);
      Add(v);
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMXMeasurePoint::ClearMeasures()
{
  int i=FMeasures->Count;
  for (int j=0;j<i;j++)
    {
    TKMXMeasure* m=(TKMXMeasure*)FMeasures->Items[j];
    delete m;
    }
  FMeasures->Clear();
}
//---------------------------------------------------------------------------
int __fastcall TKMXMeasurePoint::GetMeasureVarCount()
{
   return TKMXMeasure::VarCount;
}
//---------------------------------------------------------------------------
