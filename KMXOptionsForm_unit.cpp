//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "KMX_unit.h"
#include "MI1974_unit.h"
#include "KMXOptionsForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"

#pragma resource "*.dfm"
TKMXOptionsForm *KMXOptionsForm;
//---------------------------------------------------------------------------
__fastcall TKMXOptionsForm::TKMXOptionsForm(TComponent* Owner)
   : TForm(Owner)
{
   mx1=new TList();
   mx2=new TList();
   int c=TMI1974::CommonData->ProductCount;
   ProductComboBox->Clear();
   ProductComboBox->Items->Add("������ ���� �������������");
   TProduct* p;
   for(int i=0;i<c;i++)
      {
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      ProductComboBox->Items->Add(p->Origin);
      }
   ProductComboBox->ItemIndex=0;
}
//---------------------------------------------------------------------------
int __fastcall TKMXOptionsForm::ShowOptions(TKMXOptions* Options,TList* ProverMX,TList* MXList)
{
   mxAll=ProverMX;
   TPRMXList=MXList;
//DensityGroupBox
   NotUseFDRadioButton->Checked=!Options->UseFD;
   UseFDRadioButton->OnClick(0);
   DensityEdit->Text=Options->Density;
   tFDEdit->Text=Options->tFD;
   BettaEdit->Text=Options->Betta;
   FEdit->Text=Options->F;

   //ViscosityGroupBox
   vEdit->Text=Options->v;

   //ProductGroupBox
   ProductComboBox->ItemIndex=Options->ProductIndex;
   ProductNameEdit->Text=Options->ProductName;
   DensRecalcCheckBox->Checked=Options->DensReCalc;

   //RateRadioGroup
   RateRadioGroup->ItemIndex=Options->FreqInput;
   //PurposeRadioGroup
//   AsWorkRadioGroup->ItemIndex=Options->Purpose;//AsWork
   //Fill ProverMX List
   mx1->Clear();
   mx2->Clear();
   mx1->Assign(ProverMX);
   int c=ProverMX->Count;
   TProverMX* x;
   for(int i=0;i<c;i++)
      {
      x=static_cast<TProverMX*>(ProverMX->Items[i]);
      if(!x) continue;
      for(int j=0;j<4;j++)
         if(x->ID==Options->ProverMXID[j])
            {
            mx2->Add(x);
            mx1->Extract(x);
            break;
            }
      }
   mxToListBox();

   DescriptionEdit->Lines->Text=Options->Description;
   tinvarEdit->Text=Options->t_invar;
   DatePicker->Date=Options->Date;

   MXComboBox->Clear();
   c=TPRMXList->Count;
   TTPRMX* fx;
   for(int i=0;i<c;i++)
      {
      fx=(TTPRMX*)(TPRMXList->Items[i]);
      if(!fx) continue;
      MXComboBox->AddItem(fx->Date.DateString(),(TObject*)fx->ID);
      }
   c=MXComboBox->Items->Count;
   for(int i=0;i<c;i++)
      {
      if(Options->TPRMXID==(int)(MXComboBox->Items->Objects[i]))
         {
         MXComboBox->ItemIndex=i;
         break;
         }
      }

   CompanyEdit1->Text=  Options->Service.Company;
   CompanyEdit2->Text=  Options->Seller.Company;
   CompanyEdit3->Text=  Options->Buyer.Company;
   RankEdit1->Text=     Options->Service.Rank;
   RankEdit2->Text=     Options->Seller.Rank;
   RankEdit3->Text=     Options->Buyer.Rank;
   FIOEdit1->Text=      Options->Service.FIO;
   FIOEdit2->Text=      Options->Seller.FIO;
   FIOEdit3->Text=      Options->Buyer.FIO;

   ProtokolNEdit->Text=Options->ProtokolN;
   LocationEdit->Text=Options->Location;


   //   OKButton->Caption="�������";
//   CancelButton->Visible=true;
//   ActiveControl=OkButton;


   int r= ShowModal();
   if(r==mrOk)
      {//save changes
      //DensityGroupBox
      Options->UseFD=UseFDRadioButton->Checked;

      Options->Density= DensityEdit->Text.ToDouble();
      Options->tFD=     tFDEdit->Text.ToDouble();
      Options->Betta=   BettaEdit->Text.ToDouble();
      Options->F=       FEdit->Text.ToDouble();

      //ViscosityGroupBox
      Options->v=vEdit->Text.ToDouble();

      //ProductGroupBox
      Options->ProductIndex=ProductComboBox->ItemIndex;
      strcpy(Options->ProductName,ProductNameEdit->Text.c_str());
      Options->DensReCalc=DensRecalcCheckBox->Checked;


      //int pc=Options->ProductIndex;

      //RateRadioGroup
      Options->FreqInput=RateRadioGroup->ItemIndex;
      //PurposeRadioGroup
//      Options->Purpose=AsWorkRadioGroup->ItemIndex;//AsWork
      //ProverMX
      c=mx2->Count;
      memset(Options->ProverMXID,0,sizeof(int)*4);
      for(int i=0;i<c;i++)
         Options->ProverMXID[i]=( (TProverMX*) (mx2->Items[i]))->ID;

      strcpy(Options->Description,DescriptionEdit->Lines->Text.c_str());
      Options->t_invar=tinvarEdit->Text.ToDouble();
      Options->Date=DatePicker->Date;
      c=MXComboBox->ItemIndex;
      Options->TPRMXID=(int)(MXComboBox->Items->Objects[c]);

      //Protokol data
      strcpy(Options->Service.Company,CompanyEdit1->Text.c_str());
      strcpy(Options->Seller.Company,CompanyEdit2->Text.c_str());
      strcpy(Options->Buyer.Company,CompanyEdit3->Text.c_str());

      strcpy(Options->Service.Rank,RankEdit1->Text.c_str());
      strcpy(Options->Seller.Rank,RankEdit2->Text.c_str());
      strcpy(Options->Buyer.Rank,RankEdit3->Text.c_str());

      strcpy(Options->Service.FIO,FIOEdit1->Text.c_str());
      strcpy(Options->Seller.FIO,FIOEdit2->Text.c_str());
      strcpy(Options->Buyer.FIO,FIOEdit3->Text.c_str());

      strcpy(Options->ProtokolN,ProtokolNEdit->Text.c_str());
      strcpy(Options->Location,LocationEdit->Text.c_str());
      }
   return r;
}
//---------------------------------------------------------------------------
__fastcall TKMXOptionsForm::~TKMXOptionsForm()
{
   delete mx1;
   delete mx2;
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::mxToListBox()
{
   ListBox1->Count=mx1->Count;
   ListBox2->Count=mx2->Count;
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::RightButtonClick(TObject *Sender)
{
   int c=ListBox1->ItemIndex;
   if(c!=-1)
      {
      mx2->Add(mx1->Items[c]);
      mx1->Delete(c);
      mxToListBox();
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::AllRightButtonClick(TObject *Sender)
{
   mx1->Clear();
   mx2->Clear();
   mx2->Assign(mxAll);
   mxToListBox();
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::AllLeftButtonClick(TObject *Sender)
{
   mx1->Clear();
   mx2->Clear();
   mx1->Assign(mxAll);
   mxToListBox();
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::LeftButtonClick(TObject *Sender)
{
   int c=ListBox2->ItemIndex;
   if(c!=-1)
      {
      mx1->Add(mx2->Items[c]);
      mx2->Delete(c);
      mxToListBox();
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::OkButtonClick(TObject *Sender)
{
   if((mx2->Count==0) || (mx2->Count>4) )
      {
      Application->MessageBox("���������� ������������ ����������� ������� ��� ������ ���� �� 1 �� 4","����� ������������ ����������� ������� ���",MB_ICONSTOP);
      return;
      }
   if(MXComboBox->ItemIndex < 0)
      {
      Application->MessageBox("�� ������� �� ���","����� �� ���",MB_ICONSTOP);
      return;
      }
   ModalResult=mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::ListBox1DrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
   TProverMX* mx = static_cast<TProverMX*>(mx1->Items[Index]);
   if(State.Contains(odSelected))
      ListBox1->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox1->Canvas->Brush->Color=clWhite;
   ListBox1->Canvas->FillRect(Rect);
   AnsiString s="���� �������: ";
   s+=mx->CalibrationDate.DateString();
   if(mx->ValidNow)
      ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      ListBox1->Canvas->Font->Style=TFontStyles();
   ListBox1->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
   s="���������: ";
   s+=mx->Index;
   ListBox1->Canvas->TextOut(Rect.left+2,Rect.top+2,s);
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::ListBox2DrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
   TProverMX* mx = static_cast<TProverMX*>(mx2->Items[Index]);
   if(State.Contains(odSelected))
      ListBox2->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox2->Canvas->Brush->Color=clWhite;
   ListBox2->Canvas->FillRect(Rect);
   AnsiString s="���� �������: ";
   s+=mx->CalibrationDate.DateString();
   if(mx->ValidNow)
      ListBox2->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      ListBox2->Canvas->Font->Style=TFontStyles();
   ListBox2->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
   s="���������: ";
   s+=mx->Index;
   ListBox2->Canvas->TextOut(Rect.left+2,Rect.top+2,s);
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::NotUseFDRadioButtonClick(TObject *Sender)
{
   bool b=NotUseFDRadioButton->Checked;
   GetButton->Enabled=b;
   DensityEdit->Enabled=b;
   tFDEdit->Enabled=b;
   BettaEdit->Enabled=b;
   FEdit->Enabled=b;
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::GetButtonClick(TObject *Sender)
{
   int pc=ProductComboBox->ItemIndex;
   if(pc==0)
      {//Use ������ ����
      MessageBeep(-1);
      return;
      }
   pc--;

   //try to get Betta and F:
   double t=tFDEdit->Text.ToDouble();
   double d=DensityEdit->Text.ToDouble();
   double betta=0,f=0;
   TFunctionResult r=TMI1974::CommonData->MITools.GetBetta(pc, t,0,d,betta);
   BettaEdit->Text=betta;
   if(r!=frOk)
      Application->MessageBox("������ ����������� Betta","GetBetta",MB_ICONASTERISK);
   r=TMI1974::CommonData->MITools.GetF(pc,t,0,d,f);
   FEdit->Text=f;
   if(r!=frOk)
      Application->MessageBox("������ ����������� Gamma","GetGamma",MB_ICONASTERISK);
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::ProductComboBoxChange(TObject *Sender)
{
   int i=ProductComboBox->ItemIndex;
   if(i==0)
      DensRecalcCheckBox->Checked=false;
   if(i>0)
      {
      i--;
      TProduct* p;
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      ProductNameEdit->Text=p->ProductName;
      if(!p->CanRecalc)
         DensRecalcCheckBox->Checked=false;
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMXOptionsForm::DensRecalcCheckBoxClick(TObject *Sender)
{
   int i=ProductComboBox->ItemIndex;
   if(i==0)
      DensRecalcCheckBox->Checked=false;
   if(i>0)
      {
      i--;
      TProduct* p;
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      if(!p->CanRecalc)
         {
         DensRecalcCheckBox->Checked=false;
         MessageBeep(-1);
         }
      }
}
//---------------------------------------------------------------------------


void __fastcall TKMXOptionsForm::BitBtn1Click(TObject *Sender)
{
   if(mx2->Count <=0)
      {
      Application->MessageBox("�� ������� ���������� ������� ���","������ ���",MB_ICONSTOP);
      return;
      }
   if(MXComboBox->ItemIndex<0)
      {
      Application->MessageBox("�� ������� �� ���","������ ���",MB_ICONSTOP);
      return;
      }
   ModalResult=mrOk;
}
//---------------------------------------------------------------------------

