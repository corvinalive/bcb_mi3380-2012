object SelectMIForm: TSelectMIForm
  Left = 278
  Top = 106
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1054#1090#1082#1088#1099#1090#1080#1077' '#1087#1086#1074#1077#1088#1082#1080
  ClientHeight = 421
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000FFFFFFFF00000000FAAAEEEF00000000AAAAEEEE0000000A
    AFF77FFEE00000AAFFFC6FFFE00000AAFFCFF6FFEE0000AA7CFFFF67EE0000EE
    76FFFFC7AA0000EEFF6FFCFFAA0000EEFFF6CFFFA000000EEFF77FFAA0000000
    EEEEAAAA00000000FEEEAAAF00000000FFFFFFFF00000000000000000000E007
    0000E0070000E0070000E0070000E0070000C0070000C0030000C0030000C003
    0000C0030000C0070000E0070000E0070000E0070000E0070000E0070000}
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 242
    Height = 16
    Caption = #1057#1087#1080#1089#1086#1082' '#1087#1086#1074#1077#1088#1086#1082' '#1058#1055#1056' '#1087#1086' '#1052#1048'1974-2004:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel
    Left = 491
    Top = 366
    Width = 35
    Height = 13
    Caption = #1055#1086#1080#1089#1082':'
  end
  object ListBox1: TListBox
    Left = 4
    Top = 26
    Width = 323
    Height = 331
    Style = lbOwnerDrawFixed
    BevelInner = bvSpace
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clInfoBk
    Ctl3D = True
    ExtendedSelect = False
    ItemHeight = 36
    Items.Strings = (
      '1'
      '2')
    ParentCtl3D = False
    TabOrder = 0
    OnClick = ListBox1Click
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
  end
  object GroupBox1: TGroupBox
    Left = 332
    Top = 22
    Width = 321
    Height = 335
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1087#1086#1074#1077#1088#1082#1077
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 206
      Width = 113
      Height = 13
      Caption = #1055#1086#1074#1077#1088#1086#1095#1085#1072#1103' '#1078#1080#1076#1082#1086#1089#1090#1100
    end
    object ProductLabel: TLabel
      Left = 152
      Top = 206
      Width = 63
      Height = 13
      Caption = 'ProductLabel'
    end
    object Label3: TLabel
      Left = 8
      Top = 128
      Width = 23
      Height = 13
      Caption = #1058#1055#1059
    end
    object Label4: TLabel
      Left = 64
      Top = 112
      Width = 203
      Height = 13
      Caption = #1054#1073#1088#1072#1079#1094#1086#1074#1099#1077' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 86
      Top = 192
      Width = 122
      Height = 13
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1086#1074#1077#1088#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 224
      Width = 61
      Height = 13
      Caption = #1055#1086#1074#1077#1088#1080#1090#1077#1083#1100
    end
    object Label8: TLabel
      Left = 8
      Top = 240
      Width = 140
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103' '#1087#1086#1074#1077#1088#1082#1080
    end
    object Label9: TLabel
      Left = 82
      Top = 256
      Width = 112
      Height = 13
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1087#1086#1074#1077#1088#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object NameLabel: TLabel
      Left = 152
      Top = 224
      Width = 54
      Height = 13
      Caption = 'NameLabel'
    end
    object LocationLabel: TLabel
      Left = 152
      Top = 240
      Width = 67
      Height = 13
      Caption = 'LocationLabel'
    end
    object Label10: TLabel
      Left = 8
      Top = 144
      Width = 36
      Height = 13
      Caption = #1047#1072#1074'. '#8470
    end
    object ProverLabel: TLabel
      Left = 122
      Top = 128
      Width = 57
      Height = 13
      Caption = 'ProverLabel'
    end
    object ProverSNLabel: TLabel
      Left = 122
      Top = 144
      Width = 72
      Height = 13
      Caption = 'ProverSNLabel'
    end
    object Label12: TLabel
      Left = 118
      Top = 18
      Width = 26
      Height = 13
      Caption = #1058#1055#1056
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 8
      Top = 34
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
    end
    object Label14: TLabel
      Left = 180
      Top = 38
      Width = 36
      Height = 13
      Caption = #1047#1072#1074'. '#8470
    end
    object Label15: TLabel
      Left = 8
      Top = 52
      Width = 14
      Height = 13
      Caption = #1044#1091
    end
    object Label17: TLabel
      Left = 8
      Top = 70
      Width = 87
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1091#1089#1090#1072#1085#1086#1074#1082#1080
    end
    object Label18: TLabel
      Left = 8
      Top = 88
      Width = 69
      Height = 13
      Caption = #1055#1088#1080#1085#1072#1076#1083#1077#1078#1080#1090
    end
    object SensorNameLabel: TLabel
      Left = 64
      Top = 34
      Width = 87
      Height = 13
      Caption = 'SensorNameLabel'
    end
    object RFTNameLabel: TLabel
      Left = 64
      Top = 52
      Width = 75
      Height = 13
      Caption = 'RFTNameLabel'
    end
    object MMLocationLabel: TLabel
      Left = 114
      Top = 70
      Width = 85
      Height = 13
      Caption = 'MMLocationLabel'
    end
    object OwnerLabel: TLabel
      Left = 114
      Top = 88
      Width = 57
      Height = 13
      Caption = 'OwnerLabel'
    end
    object SensorSNLabel: TLabel
      Left = 220
      Top = 38
      Width = 74
      Height = 13
      Caption = 'SensorSNLabel'
    end
    object DescriptionLabel: TStaticText
      Left = 8
      Top = 274
      Width = 311
      Height = 49
      AutoSize = False
      Caption = 'DescriptionLabel'
      TabOrder = 0
    end
  end
  object Button1: TBitBtn
    Left = 2
    Top = 394
    Width = 325
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1087#1086#1074#1077#1088#1082#1091
    Enabled = False
    TabOrder = 5
    Kind = bkOK
  end
  object Button2: TBitBtn
    Left = 330
    Top = 394
    Width = 325
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 6
    Kind = bkCancel
  end
  object DeleteButton: TBitBtn
    Left = 161
    Top = 360
    Width = 148
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1074#1077#1088#1082#1091'...'
    Enabled = False
    TabOrder = 3
    OnClick = DeleteButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888088888888880888000888888888888800008888888088888000888888
      0888888800088880088888888000880088888888880000088888888888800088
      8888888888000008888888888000880088888880000888800888880000888888
      0088880008888888880888888888888888888888888888888888}
  end
  object OpenButton: TBitBtn
    Left = 4
    Top = 360
    Width = 153
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1087#1086#1074#1077#1088#1082#1091
    Enabled = False
    TabOrder = 2
    OnClick = OpenButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888000000000008888800333333333088880B033333333308880FB033333333
      30880BFB0333333333080FBFB000000000000BFBFBFBFB0888880FBFBFBFBF08
      88880BFB00000008888880008888888800088888888888888008888888880888
      0808888888888000888888888888888888888888888888888888}
  end
  object CopyButton: TBitBtn
    Left = 312
    Top = 360
    Width = 174
    Height = 25
    Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1074#1077#1088#1082#1091'...'
    Enabled = False
    TabOrder = 4
    OnClick = CopyButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888844444444488888884FFFFFFF488888884F00000F480000004FFFF
      FFF480FFFFF4F00000F480F00004FFFFFFF480FFFFF4F00F444480F00004FFFF
      4F4880FFFFF4FFFF448880F00F044444488880FFFF0F0888888880FFFF008888
      8888800000088888888888888888888888888888888888888888}
  end
  object Edit1: TEdit
    Left = 531
    Top = 362
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnChange = Edit1Change
  end
end
