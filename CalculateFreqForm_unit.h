//---------------------------------------------------------------------------

#ifndef CalculateFreqForm_unitH
#define CalculateFreqForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
//---------------------------------------------------------------------------
class TCalculateFreqForm : public TForm
{
__published:	// IDE-managed Components
   TNumberEdit *NumberEdit1;
   TNumberEdit *NumberEdit2;
   TNumberEdit *NumberEdit3;
   TLabel *Label3;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *Label2;
   TLabel *Label8;
   void __fastcall NumberEdit1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TCalculateFreqForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCalculateFreqForm *CalculateFreqForm;
//---------------------------------------------------------------------------
#endif
