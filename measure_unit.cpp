//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "Measure_unit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
//--- implemetation TMeasure ------------------------------------------------
//---------------------------------------------------------------------------
int TMeasure::ReadVarCount;

__fastcall TMeasure::TMeasure()
{
   Var[Used]->Value.b=false;
}
//---------------------------------------------------------------------------
TVar* __fastcall TMeasure::GetVar(int __index)
{
   if((__index>=VarCount)||(__index<0)) return NULL;
      else return &FVar[__index];
}
//---------------------------------------------------------------------------
void __fastcall TMeasure::Save(IStream* Stream)
{
   ULONG ul;
   Stream->Write(FVar,VarCount*sizeof(TVar),&ul);
}
//---------------------------------------------------------------------------
void __fastcall TMeasure::Load(IStream* Stream)
{
   ULONG ul;
   Stream->Read(FVar,ReadVarCount*sizeof(TVar),&ul);
}
//---------------------------------------------------------------------------

