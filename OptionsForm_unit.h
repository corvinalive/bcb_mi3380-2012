//---------------------------------------------------------------------------

#ifndef OptionsForm_unitH
#define OptionsForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "MI1974_unit.h"
#include "NumberEdit.h"
#include <ComCtrls.hpp>
#include <CheckLst.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TOptionsForm : public TForm
{
__published:	// IDE-managed Components
   TGroupBox *GroupBox1;
   TRadioGroup *RateRadioGroup;
   TComboBox *ProductComboBox;
   TRadioGroup *AsWorkRadioGroup;
   TGroupBox *GroupBox2;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TEdit *ProductNameEdit;
   TMemo *DescriptionEdit;
   TLabel *Label9;
   TDateTimePicker *DatePicker;
   TGroupBox *GroupBox6;
   TEdit *ProtokolNEdit;
   TLabel *Label10;
   TLabel *Label4;
   TLabel *Label8;
   TEdit *NameEdit;
   TEdit *LocationEdit;
   TGroupBox *GroupBox7;
   TLabel *Label3;
   TNumberEdit *UOIEdit;
   TLabel *Label17;
   TNumberEdit *dtTPREdit;
   TLabel *Label2;
   TLabel *Label1;
   TNumberEdit *dtTPUEdit;
   TListBox *ListBox1;
   TListBox *ListBox2;
   TBitBtn *RightButton;
   TBitBtn *AllRightButton;
   TBitBtn *LeftButton;
   TBitBtn *AllLeftButton;
   TBitBtn *OkButton;
   TBitBtn *CancelButton;
   TComboBox *FunctionComboBox;
   TLabel *Label11;
   TNumberEdit *tinvarEdit;
   TLabel *Label12;
   TGroupBox *GroupBox3;
   TRadioButton *RadioButton1;
   TRadioButton *RadioButton2;
   TLabel *Label13;
   TLabel *Label14;
   TImage *Image1;
   TImage *Image2;
   TButton *GetButton;
   TNumberEdit *DensityEdit;
   TNumberEdit *tFDEdit;
   TNumberEdit *BettaEdit;
   TNumberEdit *FEdit;
   TGroupBox *GroupBox4;
   TRadioButton *RadioButton3;
   TRadioButton *RadioButton4;
   TNumberEdit *v2Edit;
   TNumberEdit *t2Edit;
   TLabel *Label15;
   TLabel *Label16;
   TLabel *Label18;
   TLabel *Label19;
   TNumberEdit *t1Edit;
   TLabel *Label20;
   TLabel *Label21;
   TNumberEdit *v1Edit;
   TCheckBox *CheckBox1;
   void __fastcall RightButtonClick(TObject *Sender);
   void __fastcall AllRightButtonClick(TObject *Sender);
   void __fastcall AllLeftButtonClick(TObject *Sender);
   void __fastcall LeftButtonClick(TObject *Sender);
   void __fastcall OkButtonClick(TObject *Sender);
   void __fastcall ListBox1DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall ListBox2DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall RadioButton2Click(TObject *Sender);
   void __fastcall GetButtonClick(TObject *Sender);
   void __fastcall RadioButton3Click(TObject *Sender);
   void __fastcall ProductComboBoxChange(TObject *Sender);
   void __fastcall CheckBox1Click(TObject *Sender);
private:	// User declarations
   TList* mx1;
   TList* mx2;
   TList* mxAll;
   TList* fdmx;
   void __fastcall mxToListBox();
public:		// User declarations
   __fastcall TOptionsForm(TComponent* Owner);
   __fastcall ~TOptionsForm();
   int __fastcall ShowOptions(TMI1974Options* Options,TList* ProverMX);
};
//---------------------------------------------------------------------------
extern PACKAGE TOptionsForm *OptionsForm;
//---------------------------------------------------------------------------
#endif
