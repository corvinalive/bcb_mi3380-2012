//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "OptionsForm_unit.h"
#include "MI1974_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TOptionsForm *OptionsForm;
//---------------------------------------------------------------------------
__fastcall TOptionsForm::TOptionsForm(TComponent* Owner)
   : TForm(Owner)
{
   mx1=new TList();
   mx2=new TList();
   int c=TMI1974::CommonData->ProductCount;
   ProductComboBox->Clear();
   ProductComboBox->Items->Add("������ ���� �������������");
   TProduct* p;
   for(int i=0;i<c;i++)
      {
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      ProductComboBox->Items->Add(p->Origin);
      }
   ProductComboBox->ItemIndex=0;
}
//---------------------------------------------------------------------------
int __fastcall TOptionsForm::ShowOptions(TMI1974Options* Options,TList* ProverMX)
{
   mxAll=ProverMX;
   AsWorkRadioGroup->ItemIndex=Options->Purpose;//AsWork
   RateRadioGroup->ItemIndex=Options->FreqInput;
   ProductComboBox->ItemIndex=Options->ProductIndex;
   FunctionComboBox->ItemIndex=Options->Function;
   dtTPREdit->Text=Options->dtTPR;
   dtTPUEdit->Text=Options->dtTPU;
   UOIEdit->Text=Options->dUOI;
   ProductNameEdit->Text=Options->ProductName;
   NameEdit->Text=Options->Name;
   CheckBox1->Checked=Options->DensReCalc;
   LocationEdit->Text=Options->Location;
   ProtokolNEdit->Text=Options->ProtokolN;
   DescriptionEdit->Lines->Text=Options->Description;
   DatePicker->Date=Options->Date;
   RadioButton1->Checked=Options->UseFD;
   RadioButton1->OnClick(0);

   RadioButton2->Checked=!Options->UseFD;
   RadioButton2->OnClick(0);

   DensityEdit->Text=Options->Density;
   BettaEdit->Text=Options->Betta;
   FEdit->Text=Options->F;
   tFDEdit->Text=Options->tFD;
   tinvarEdit->Text=Options->t_invar;
   RadioButton3->Checked=Options->UseVisc;
   RadioButton4->Checked=!Options->UseVisc;
   RadioButton3->OnClick(0);
   v1Edit->Text=Options->v1;
   t1Edit->Text=Options->tv1;
   v2Edit->Text=Options->v2;
   t2Edit->Text=Options->tv2;

   //   OKButton->Caption="�������";
   CancelButton->Visible=true;
   ActiveControl=OkButton;
   //Fill ProverMX List
   mx1->Clear();
   mx2->Clear();
   mx1->Assign(ProverMX);
   int c=ProverMX->Count;
   TProverMX* x;
   for(int i=0;i<c;i++)
      {
      x=static_cast<TProverMX*>(ProverMX->Items[i]);
      if(!x) continue;
      for(int j=0;j<4;j++)
         if(x->ID==Options->ProverMXID[j])
            {
            mx2->Add(x);
            mx1->Extract(x);
            break;
            }
      }
   mxToListBox();


   int r= ShowModal();
   if(r==mrOk)
      {//save changes
      #pragma warn -8018
      Options->Purpose=AsWorkRadioGroup->ItemIndex;
      Options->FreqInput=RateRadioGroup->ItemIndex;
      Options->Function=FunctionComboBox->ItemIndex;
      #pragma warn +8018
      Options->dtTPR=dtTPREdit->Text.ToDouble();
      Options->dtTPU=dtTPUEdit->Text.ToDouble();
      Options->dUOI=UOIEdit->Text.ToDouble();
      Options->ProductIndex=ProductComboBox->ItemIndex;
      Options->DensReCalc=CheckBox1->Checked;
      Options->Date=DatePicker->Date;
      strcpy(Options->Description,DescriptionEdit->Lines->Text.c_str());
      strcpy(Options->Name,NameEdit->Text.c_str());
      strcpy(Options->ProductName,ProductNameEdit->Text.c_str());
      strcpy(Options->Location,LocationEdit->Text.c_str());
      strcpy(Options->ProtokolN,ProtokolNEdit->Text.c_str());
      Options->UseFD=RadioButton1->Checked;
      Options->Density=DensityEdit->Text.ToDouble();
      Options->tFD=tFDEdit->Text.ToDouble();
      Options->Betta=BettaEdit->Text.ToDouble();
      Options->F=FEdit->Text.ToDouble();
      Options->t_invar=tinvarEdit->Text.ToDouble();
      Options->v1=v1Edit->Text.ToDouble();
      Options->tv1=t1Edit->Text.ToDouble();
      Options->v2=v2Edit->Text.ToDouble();
      Options->tv2=t2Edit->Text.ToDouble();
      Options->UseVisc=RadioButton3->Checked;


      Options->GetBetta_=NULL;
      Options->GetF_=NULL;
      Options->ReCalc_=NULL;
      int pc=Options->ProductIndex;

      c=mx2->Count;
      memset(Options->ProverMXID,0,sizeof(int)*4);
      for(int i=0;i<c;i++)
         Options->ProverMXID[i]=(static_cast<TProverMX*>(mx2->Items[i]))->ID;
      }
   return r;
}
//---------------------------------------------------------------------------
__fastcall TOptionsForm::~TOptionsForm()
{
   delete mx1;
   delete mx2;
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::mxToListBox()
{
   ListBox1->Count=mx1->Count;
   ListBox2->Count=mx2->Count;
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::RightButtonClick(TObject *Sender)
{
   int c=ListBox1->ItemIndex;
   if(c!=-1)
      {
      mx2->Add(mx1->Items[c]);
      mx1->Delete(c);
      mxToListBox();
      }
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::AllRightButtonClick(TObject *Sender)
{
   mx1->Clear();
   mx2->Clear();
   mx2->Assign(mxAll);
   mxToListBox();
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::AllLeftButtonClick(TObject *Sender)
{
   mx1->Clear();
   mx2->Clear();
   mx1->Assign(mxAll);
   mxToListBox();
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::LeftButtonClick(TObject *Sender)
{
   int c=ListBox2->ItemIndex;
   if(c!=-1)
      {
      mx1->Add(mx2->Items[c]);
      mx2->Delete(c);
      mxToListBox();
      }
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::OkButtonClick(TObject *Sender)
{
   if((mx2->Count==0) || (mx2->Count>4) )
      Application->MessageBox("���������� ������������ ����������� ������� ��� ������ ���� �� 1 �� 4","����� ������������ ����������� ������� ���",MB_ICONSTOP);
   else
      ModalResult=mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::ListBox1DrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
   TProverMX* mx = static_cast<TProverMX*>(mx1->Items[Index]);
   if(State.Contains(odSelected))
      ListBox1->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox1->Canvas->Brush->Color=clWhite;
   ListBox1->Canvas->FillRect(Rect);
   AnsiString s="���� �������: ";
   s+=mx->CalibrationDate.DateString();
   if(mx->ValidNow)
      ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      ListBox1->Canvas->Font->Style=TFontStyles();
   ListBox1->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
   s="���������: ";
   s+=mx->Index;
   ListBox1->Canvas->TextOut(Rect.left+2,Rect.top+2,s);
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::ListBox2DrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
   TProverMX* mx = static_cast<TProverMX*>(mx2->Items[Index]);
   if(State.Contains(odSelected))
      ListBox2->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox2->Canvas->Brush->Color=clWhite;
   ListBox2->Canvas->FillRect(Rect);
   AnsiString s="���� �������: ";
   s+=mx->CalibrationDate.DateString();
   if(mx->ValidNow)
      ListBox2->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      ListBox2->Canvas->Font->Style=TFontStyles();
   ListBox2->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
   s="���������: ";
   s+=mx->Index;
   ListBox2->Canvas->TextOut(Rect.left+2,Rect.top+2,s);
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::RadioButton2Click(TObject *Sender)
{
   bool b=RadioButton2->Checked;
   GetButton->Enabled=b;
   DensityEdit->Enabled=b;
   tFDEdit->Enabled=b;
   BettaEdit->Enabled=b;
   FEdit->Enabled=b;
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::GetButtonClick(TObject *Sender)
{
   int pc=ProductComboBox->ItemIndex;
   if(pc==0)
      {//Use ������ ����
      MessageBeep(-1);
      return;
      }
   pc--;

   //try to get Betta and F:
   double t=tFDEdit->Text.ToDouble();
   double d=DensityEdit->Text.ToDouble();
   double betta=0,f=0;
   TFunctionResult r=TMI1974::CommonData->MITools.GetBetta(pc,t,0,d,betta);
   BettaEdit->Text=betta;
   if(r!=frOk)
      Application->MessageBox("������ ����������� Betta","GetBetta",MB_ICONASTERISK);
   r=TMI1974::CommonData->MITools.GetF(pc,t,0,d,f);
   FEdit->Text=f;
   if(r!=frOk)
      Application->MessageBox("������ ����������� Gamma","GetGamma",MB_ICONASTERISK);
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::RadioButton3Click(TObject *Sender)
{
   bool b=RadioButton4->Checked;
   v1Edit->Enabled=b;
   t1Edit->Enabled=b;
   v2Edit->Enabled=b;
   t2Edit->Enabled=b;
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::ProductComboBoxChange(TObject *Sender)
{
   int i=ProductComboBox->ItemIndex;
   if(i==0)
      CheckBox1->Checked=false;
   if(i>0)
      {
      i--;
      TProduct* p;
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      ProductNameEdit->Text=p->ProductName;
      if(!p->CanRecalc)
         CheckBox1->Checked=false;
      }
}
//---------------------------------------------------------------------------
void __fastcall TOptionsForm::CheckBox1Click(TObject *Sender)
{
   int i=ProductComboBox->ItemIndex;
   if(i==0)
      CheckBox1->Checked=false;
   if(i>0)
      {
      i--;
      TProduct* p;
      p=(TProduct*)TMI1974::CommonData->Products->Items[i];
      if(!p->CanRecalc)
         {
         CheckBox1->Checked=false;
         MessageBeep(-1);
         }
      }
}
//---------------------------------------------------------------------------


