//---------------------------------------------------------------------------
#ifndef Range_unitH
#define Range_unitH

#include "variable.h"

//---------------------------------------------------------------------------
class TRange:public TVarContainer
{
public:
   static const VarCount   =11;
   static ReadVarCount;

   static const Number     =0;//����� ���������
   static const K          =1;//
   static const Q1         =2;//
   static const Q2         =3;
   static const Tetta_Sum  =4;
   static const Tetta_K    =5;
   static const edk        =6;
   static const zdk        =7;
   static const TettaS     =8;
   static const sk         =9;
   static const ddk        =10;

   int PointIndex1;
   int PointIndex2;
private:
   TVar FVar[VarCount];
   virtual TVar* __fastcall GetVar(int __index);
   virtual int __fastcall GetVarCount(){return VarCount;};
public:
   __fastcall TRange();
   virtual void __fastcall Load(IStream* Stream);
   virtual void __fastcall Save(IStream* Stream);
};
//---------------------------------------------------------------------------

#endif
