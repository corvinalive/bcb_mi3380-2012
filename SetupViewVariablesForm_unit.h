//---------------------------------------------------------------------------

#ifndef SetupViewVariablesForm_unitH
#define SetupViewVariablesForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>

//---------------------------------------------------------------------------
class TSetupViewVariablesForm : public TForm
{
__published:	// IDE-managed Components
   TGroupBox *GroupBox1;
   TGroupBox *GroupBox2;
   TListBox *MeasureSourceListBox;
   TListBox *MeasureDestinationListBox;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
	TLabel *Label1;
	TLabel *Label2;
	TBitBtn *mDestMoveUpBtn;
	TBitBtn *mDestMoveDownBtn;
	TBitBtn *mMoveRightBtn;
	TBitBtn *mMoveRightAllBtn;
	TBitBtn *mMoveLeftBtn;
	TBitBtn *mMoveLeftAllBtn;
	TGroupBox *GroupBox3;
	TLabel *Label3;
	TLabel *Label4;
	TListBox *PointSourceListBox;
	TBitBtn *pDestMoveUpBtn;
	TBitBtn *pDestMoveDownBtn;
	TBitBtn *pMoveRightBtn;
	TBitBtn *pMoveRightAllBtn;
	TBitBtn *pMoveLeftBtn;
	TBitBtn *pMoveLeftAllBtn;
   TListBox *PointDestinationListBox;
   void __fastcall MeasureSourceListBoxDrawItem(TWinControl *Control,
          int Index, TRect &Rect, TOwnerDrawState State);
	void __fastcall MeasureDestinationListBoxDrawItem(TWinControl *Control,
			 int Index, TRect &Rect, TOwnerDrawState State);
	void __fastcall mDestMoveUpBtnClick(TObject *Sender);
	void __fastcall mDestMoveDownBtnClick(TObject *Sender);
	void __fastcall mMoveRightBtnClick(TObject *Sender);
	void __fastcall mMoveRightAllBtnClick(TObject *Sender);
	void __fastcall mMoveLeftBtnClick(TObject *Sender);
	void __fastcall mMoveLeftAllBtnClick(TObject *Sender);
	void __fastcall PointSourceListBoxDrawItem(TWinControl *Control,
			 int Index, TRect &Rect, TOwnerDrawState State);
	void __fastcall PointDestinationListBoxDrawItem(TWinControl *Control,
			 int Index, TRect &Rect, TOwnerDrawState State);
	void __fastcall pMoveRightBtnClick(TObject *Sender);
	void __fastcall pMoveRightAllBtnClick(TObject *Sender);
	void __fastcall pMoveLeftBtnClick(TObject *Sender);
	void __fastcall pMoveLeftAllBtnClick(TObject *Sender);
	void __fastcall pDestMoveUpBtnClick(TObject *Sender);
	void __fastcall pDestMoveDownBtnClick(TObject *Sender);
private:	// User declarations
	TIntList ms,md;
	TIntList ps,pd;
	TMI1974* MI;
   TKMX* KMX;
	void __fastcall ListBoxDrawItem(TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State, TVarOptions* vo,TIntList *l);
public:		// User declarations
   __fastcall TSetupViewVariablesForm(TComponent* Owner);
   bool __fastcall ShowDialog(TMI1974* MI);
   bool __fastcall ShowKMXDialog(TKMX* KMX);
};
//---------------------------------------------------------------------------
extern PACKAGE TSetupViewVariablesForm *SetupViewVariablesForm;
//---------------------------------------------------------------------------
#endif
