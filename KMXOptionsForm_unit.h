//---------------------------------------------------------------------------

#ifndef KMXOptionsForm_unitH
#define KMXOptionsForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "MI1974_unit.h"
#include "NumberEdit.h"
#include <ComCtrls.hpp>
#include <CheckLst.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>

//---------------------------------------------------------------------------
class TKMXOptionsForm : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl1;
        TTabSheet *TabSheet1;
        TLabel *Label21;
        TGroupBox *ProductGroupBox;
        TLabel *Label6;
        TLabel *Label7;
        TComboBox *ProductComboBox;
        TEdit *ProductNameEdit;
        TCheckBox *DensRecalcCheckBox;
        TGroupBox *DensityGroupBox;
        TLabel *Label13;
        TLabel *Label14;
        TImage *Image1;
        TImage *Image2;
        TRadioButton *UseFDRadioButton;
        TRadioButton *NotUseFDRadioButton;
        TButton *GetButton;
        TNumberEdit *DensityEdit;
        TNumberEdit *tFDEdit;
        TNumberEdit *BettaEdit;
        TNumberEdit *FEdit;
        TNumberEdit *vEdit;
        TTabSheet *TabSheet2;
        TGroupBox *GroupBox6;
        TLabel *Label18;
        TLabel *Label19;
        TEdit *ProtokolNEdit;
        TEdit *LocationEdit;
        TGroupBox *GroupBox5;
        TLabel *Label4;
        TLabel *Label8;
        TLabel *Label10;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TEdit *CompanyEdit1;
        TEdit *CompanyEdit2;
        TEdit *CompanyEdit3;
        TEdit *RankEdit1;
        TEdit *RankEdit2;
        TEdit *RankEdit3;
        TEdit *FIOEdit1;
        TEdit *FIOEdit2;
        TEdit *FIOEdit3;
        TTabSheet *TabSheet3;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label9;
        TLabel *Label5;
        TMemo *DescriptionEdit;
        TGroupBox *GroupBox2;
        TListBox *ListBox1;
        TListBox *ListBox2;
        TBitBtn *RightButton;
        TBitBtn *AllRightButton;
        TBitBtn *LeftButton;
        TBitBtn *AllLeftButton;
        TRadioGroup *RateRadioGroup;
        TComboBox *MXComboBox;
        TNumberEdit *tinvarEdit;
        TDateTimePicker *DatePicker;
        TBitBtn *OKButton;
        TBitBtn *BitBtn2;
   void __fastcall RightButtonClick(TObject *Sender);
   void __fastcall AllRightButtonClick(TObject *Sender);
   void __fastcall AllLeftButtonClick(TObject *Sender);
   void __fastcall LeftButtonClick(TObject *Sender);
   void __fastcall OkButtonClick(TObject *Sender);
   void __fastcall ListBox1DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall ListBox2DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall NotUseFDRadioButtonClick(TObject *Sender);
   void __fastcall GetButtonClick(TObject *Sender);
   void __fastcall ProductComboBoxChange(TObject *Sender);
   void __fastcall DensRecalcCheckBoxClick(TObject *Sender);
        void __fastcall BitBtn1Click(TObject *Sender);

private:	// User declarations
   TList* mx1;
   TList* mx2;
   TList* mxAll;
   TList* TPRMXList;
   void __fastcall mxToListBox();
public:		// User declarations
   __fastcall TKMXOptionsForm(TComponent* Owner);
   __fastcall ~TKMXOptionsForm();
   int __fastcall ShowOptions(TKMXOptions* Options,TList* ProverMX, TList* TPRMXList);
};
//---------------------------------------------------------------------------
extern PACKAGE TKMXOptionsForm *KMXOptionsForm;
//---------------------------------------------------------------------------
#endif
