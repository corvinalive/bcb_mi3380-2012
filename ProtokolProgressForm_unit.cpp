//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "ProtokolProgressForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CGAUGES"
#pragma resource "*.dfm"
TProtokolProgressForm *ProtokolProgressForm;
//---------------------------------------------------------------------------
__fastcall TProtokolProgressForm::TProtokolProgressForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
__fastcall TProtokolProgressForm::~TProtokolProgressForm()
{
}
//---------------------------------------------------------------------------
void __fastcall TProtokolProgressForm::Completed()
{
   MessageBeep(0xFFFFFFFF);
   Hide();
}
//---------------------------------------------------------------------------

