//---------------------------------------------------------------------------

#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "MainForm_unit.h"
#include "MIForm_unit.h"
#include "MI1974_unit.h"
#include "prover.h"
#include "PropertiesForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MIGrid"        
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TMIForm::TMIForm(TComponent* Owner,IStorage* Ro,IStorage* MIR,TMI_data d)
   : TForm(Owner)
{
   data =d;
   MIRoot= MIR;
   Root=Ro;
   MI = new TMI1974(&d);
   MI->OnGetBitmap=MainForm->GetBitmaps;
   MI->OnChange=MIChange;
   MI->ResChange=ResChange;
   if(data.Prover)
      {
      delete data.Prover;
      data.Prover=NULL;
      }
   if(data.TPR)
      {
      delete data.TPR;
      data.TPR=NULL;
      }
   if(data.Options)
      {
      delete data.Options;
      data.Options = NULL;
      }
   MI->ID=random(MaxInt);
   IsNew=true;
   MakeCaption();
	MIGrid->GridData=MI;
   SetupResGrid();
}
//---------------------------------------------------------------------------
__fastcall TMIForm::~TMIForm()
{
	if(MI!=NULL) delete MI;
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::Save()
{
   IStream* Stream;
   IStorage* st;
   WideString ws(MI->ID);
   HRESULT res=MIRoot->OpenStorage(ws,0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0, &st);
   if(res!=S_OK)
      {
	   res = MIRoot->CreateStorage(ws,OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&st);
		if(res!=S_OK) return;
      }
   res=st->OpenStream(WideString("MI1974data"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
	   res = st->CreateStream(WideString("MI1974data"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
         {
         st->Release();
         return;
         
         }
      }
   int c=ResGrid->ColCount;
   for(int i=0;i<c;i++)
      {
		TPack p;
      p.v.i=MI->ResColumns.Values[i];
		TVarOptions* vo;
		switch (p.v.c[0])
			{
			case 0: vo=MI->MIVarOptions[0];
			break;
			case 1: vo=MI->PointVarOptions[0];
			break;
			case 2: vo=MI->RangeVarOptions[0];
			break;
			}
      vo[p.v.c[1]].Width=ResGrid->ColWidths[i];
      }
   MI->Save(Stream);
   Stream->Release();
   st->Release();
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   Action=caFree;
   MainForm->MIFormFocusChange(false);
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	AnsiString s;
	s="��������� ��������� � ������� ���:\n   ��� ";
   //������������ ������
   s+=MI->tpr.Model;
   s+=" �� ";
   s+=MI->tpr.D;
   s+="  ��������� �";
   s+=MI->tpr.SN;
   s+="\n   ��� ";
   s+=MI->Prover.Name;
   s+="  ��������� �";
   s+=MI->Prover.SN;
   s+="\n   ���� ������� ";
   s+=MI->Options.Date.DateString();
	int i=Application->MessageBox(s.c_str(),"�������� �������",MB_YESNOCANCEL|MB_ICONQUESTION);
	if(i==IDCANCEL)
		{
		CanClose=false;
		return;
		}
	if(i==IDNO)
		{
		CanClose=true;
		return;
		}
	if(i==IDYES)
		{
		Save();
		}
}
//---------------------------------------------------------------------------
__fastcall TMIForm::TMIForm(TComponent* Owner,IStorage* Ro,IStorage* MIR,int ID)
: TForm(Owner)
{
   MIRoot= MIR;
   Root=Ro;
   IStream* Stream;
   IStorage* st;
   WideString ws(ID);
   HRESULT res=MIRoot->OpenStorage(ws,0,OF_READ|STGM_SHARE_EXCLUSIVE,0,0, &st);
	if(res!=S_OK) return;
   res=st->OpenStream(WideString("MI1974data"),0,OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
      {
      st->Release();
      return;
      }
   MI = new TMI1974(Stream,Root);
   MI->ID=ID;
   Stream->Release();
   st->Release();
   MI->OnGetBitmap=MainForm->GetBitmaps;
   MI->OnChange=MIChange;
   MI->ResChange=ResChange;
   IsNew=false;
   MakeCaption();
	MIGrid->GridData=MI;
   SetupResGrid();
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::MakeCaption()
{
   AnsiString s="������� ��� ";
   //������������ ��������� ����
   s+=MI->tpr.Model;
   s+=" �� ";
   s+=MI->tpr.D;
   s+=" ���. �";
   s+=MI->tpr.SN;
   Caption=s;
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::SetupResGrid()
{
   ResGrid->RowHeights[0]=24;
   int c=MI->ResColumns.Count;
   if(c==0)
      {
      ResGrid->ColCount=1;
      return;
      }
   ResGrid->ColCount=c;
   for(int i=0;i<c;i++)
      {
		TPack p;
      p.v.i=MI->ResColumns.Values[i];
		TVarOptions* vo;
		switch (p.v.c[0])
			{
			case 0: vo=MI->MIVarOptions[0];
			break;
			case 1: vo=MI->PointVarOptions[0];
			break;
			case 2: vo=MI->RangeVarOptions[0];
			break;
			}
      ResGrid->ColWidths[i]=vo[p.v.c[1]].Width;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::ResGridDrawCell(TObject *Sender, int ACol,
      int ARow, TRect &Rect, TGridDrawState State)
{
   if(State.Contains(gdFixed))
      {
      ResGrid->Canvas->Brush->Color=ResGrid->FixedColor;
      ResGrid->Canvas->FillRect(Rect);
      TRect r=Rect;
      DWORD FrameFlags1=BF_RIGHT |BF_BOTTOM;
      DWORD FrameFlags2=BF_LEFT  |BF_TOP;
      r.Right+=1;
      DrawEdge(ResGrid->Canvas->Handle, &r, BDR_RAISEDINNER, FrameFlags1);
      DrawEdge(ResGrid->Canvas->Handle, &r, BDR_RAISEDINNER, FrameFlags2);
      }
   else
      {
      ResGrid->Canvas->Brush->Color=ResGrid->Color;
      ResGrid->Canvas->FillRect(Rect);
      }
   if(MI->ResColumns.Count==0)
      return;
   TVarOptions* vo;
   TPack p;
	p.v.i=MI->ResColumns.Values[ACol];
	switch (p.v.c[0])
	   {
		case 0: vo=MI->MIVarOptions[0];
		break;
		case 1: vo=MI->PointVarOptions[0];
		break;
		case 2: vo=MI->RangeVarOptions[0];
		break;
		}
   vo=&vo[p.v.c[1]];
   if(ARow==0)
      {//draw captions
      if((vo->Bitmap==-1)||(!MI->OnGetBitmap))
         {
         int w=Canvas->TextWidth(vo->Name);
         int h=Canvas->TextHeight("A");
         ResGrid->Canvas->Font->Color=clBlack;
         ResGrid->Canvas->Brush->Color=clBtnFace;
         ResGrid->Canvas->TextOut(Rect.left+(Rect.Width()-w)/2,Rect.top+(Rect.Height()-h)/2,vo->Name );
         }
      else
         {
         Graphics::TBitmap* b=MI->OnGetBitmap(vo->Bitmap);
         int w=b->Width;
         int h=b->Height;
         ResGrid->Canvas->CopyMode=cmSrcAnd;
         ResGrid->Canvas->Draw(Rect.left+(Rect.Width()-w)/2,Rect.top+(Rect.Height()-h)/2,b);
         }
      }
   else
      {//draw values
		TRange* rr;
		TGridPoint* po;
		TVar* v=NULL;
      bool EmptyCell=false;
		switch (p.v.c[0])
			{
			case 0:
				if(ARow!=1)
               EmptyCell=true;
				v=&MI->Var[p.v.c[1]];
			break;
			case 1:
			   po=MI->UsedPoint[ARow-1];
				if(po) v=po->Var[p.v.c[1]];
			   break;
			case 2:
				if(ARow <= MI->FRanges->Count)
					{
					rr = (TRange*)MI->FRanges->Items[ARow-1];
					v=rr->Var[p.v.c[1]];
					}
    			break;
			}
      AnsiString s;
      int is=TVar::NotUsed;
      if(vo && v)
         {
         if(!EmptyCell)
            {
            vo->FormatStr(v,s);
            is=v->State;
            }
         if( (is>6) ||(is<0)) is=6;
         int w=ResGrid->Canvas->TextWidth(s);
         int h=ResGrid->Canvas->TextHeight("A");
         ResGrid->Canvas->Brush->Color=v->Colors[is*2+1];
         ResGrid->Canvas->Font->Color=v->Colors[is*2];
         ResGrid->Canvas->TextRect(Rect,Rect.left+(Rect.Width()-w)/2,Rect.top+(Rect.Height()-h)/2,s);
         }
      else
//         if(ACol+1==ResGrid->ColCount)
            {//Draw no values to draw
            TRect r=ResGrid->CellRect(0,ARow);
            TRect r1=ResGrid->CellRect(ResGrid->ColCount-1,0);
            r.right=r1.right;
            ResGrid->Canvas->Brush->Color=clWhite;
            ResGrid->Canvas->Font->Color=clBlack;
            s="��� ������ ��� �����������";
            int w=ResGrid->Canvas->TextWidth(s);
            int h=ResGrid->Canvas->TextHeight("A");
            ResGrid->Canvas->Rectangle(r);
            ResGrid->Canvas->TextRect(r,r.left+(r.Width()-w)/2,r.top+(r.Height()-h)/2,s);
            }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::MIChange(TObject* Sender)
{
	int c;
	int pu = MI->Var[TMI1974::UsedPoints].Value.i;
   if(!MI->Options.Function)
      pu=1;
	if(pu>2)
		{
      GroupBox1->Height=88 + (pu-2)*18;
		ResGrid->RowCount=pu;
		}
	else
      {
		ResGrid->RowCount=2;
      GroupBox1->Height=88;
      }
	ResGrid->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::ResGridColumnMoved(TObject *Sender, int FromIndex,
      int ToIndex)
{
   if(MI->ResColumns.Count==0)
      return;
   MI->ResColumns.Move(FromIndex,ToIndex);
	ResGrid->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TMIForm::ResChange(TObject* Sender)
{
   SetupResGrid();
}
//---------------------------------------------------------------------------

void __fastcall TMIForm::FormActivate(TObject *Sender)
{
   MainForm->MIFormFocusChange(true);
}
//---------------------------------------------------------------------------

void __fastcall TMIForm::FormDeactivate(TObject *Sender)
{
   MainForm->MIFormFocusChange(false);
}
//---------------------------------------------------------------------------

