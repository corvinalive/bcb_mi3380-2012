//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "MI1974_unit.h"
#include "MI1974CommonData_unit.h"
#include "Range_unit.h"
#include "SetAllForm_unit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
TMI1974CommonData* TMI1974::CommonData;
int TMI1974::ReadVarCount;
//---------------------------------------------------------------------------
//--- implemetation TMI1974 -------------------------------------------------
//---------------------------------------------------------------------------
int __fastcall TMI1974::Add(TGridPoint* GridPoint)
{
   int c = FMeasurePoints->Add(GridPoint);
   return c;
};
//---------------------------------------------------------------------------
int __fastcall TMI1974::AddNew()
{
   int n=Add(new TMeasurePoint());
   Calculate();
   return n;
};
//---------------------------------------------------------------------------
void __fastcall TMI1974::Delete(int Index)
{
   TMeasurePoint* p=(TMeasurePoint*)FMeasurePoints->Items[Index];
   delete p;

   FMeasurePoints->Delete(Index);
   void* f=this;
   if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
}
//---------------------------------------------------------------------------
TGridPoint* __fastcall TMI1974::GetGridPoints(int Index)
{
   return (TGridPoint*)FMeasurePoints->Items[Index];
};
//---------------------------------------------------------------------------
TGridPoint* __fastcall TMI1974::GetUsedPoints(int Index)
{
   int c = FMeasurePoints->Count;
   int used=0;
   TGridPoint* p;
   for(int i=0;i<c;i++)
      {
      p = (TGridPoint*) FMeasurePoints->Items[i];
      if(p->Var[TMeasurePoint::Used]->Value.b)
         {
         if(Index==used)
            return p;
         used++;
         }
      }
   return NULL;
};
//---------------------------------------------------------------------------
int __fastcall TMI1974::GetCount()
{
   return FMeasurePoints->Count;
};
//---------------------------------------------------------------------------
TVarOptions* __fastcall TMI1974::GetPointVarOptions(int index)
{
   if((index<0)||(index>=TMeasurePoint::VarCount))
      return NULL;
   else return &FPointVarOptions[index];
}
//---------------------------------------------------------------------------
TVarOptions* __fastcall TMI1974::GetMeasureVarOptions(int index)
{
   if((index<0)||(index>=TMeasure::VarCount))
      return NULL;
   else return &FMeasureVarOptions[index];
}
//---------------------------------------------------------------------------
TVarOptions* __fastcall TMI1974::GetMIVarOptions(int index)
{
   if((index<0)||(index>=VarCount))
      return NULL;
   else return &FMIVarOptions[index];
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::LoadVarOptionsFromIni(TIniFile* Ini)
{
   for(int i=0;i<TMeasure::VarCount;i++)
      {
      AnsiString s("MeasureVar");
      s+=i;
      FMeasureVarOptions[i].LoadFromIni(Ini,s);
      }
   for(int i=0;i<TMeasurePoint::VarCount;i++)
      {
      AnsiString s("MeasurePointVar");
      s+=i;
      FPointVarOptions[i].LoadFromIni(Ini,s);
      }
   for(int i=0;i<VarCount;i++)
      {
      AnsiString s("MIVar");
      s+=i;
      FMIVarOptions[i].LoadFromIni(Ini,s);
      }
   for(int i=0;i<TRange::VarCount;i++)
      {
      AnsiString s("RangeVar");
      s+=i;
      FRangeVarOptions[i].LoadFromIni(Ini,s);
      }
}
//---------------------------------------------------------------------------
__fastcall TMI1974::TMI1974(TMI_data* data):TGridData(TMI1974::CommonData)
{
   FMeasurePoints = new TList();
   FRanges = new TList();
   LoadVarOptions();//FromIni(f);
   memcpy(&Prover,data->Prover,sizeof(TProver));
   memcpy(&tpr,data->TPR,sizeof(TTPR));
   memcpy(&Options,data->Options,sizeof(TMI1974Options));
   ProverMX = data->ProverMX;
   //Make ident Options.ProverID & IDMX
   IDMX[0]=0;
   IDMX[1]=0;
   IDMX[2]=0;
   IDMX[3]=0;
   TProverMX* x=0;
   int mxc=ProverMX->Count;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      for(int j=0;j<mxc;j++)
         {
         x=(TProverMX*)(ProverMX->Items[j]);
         if(x->ID==Options.ProverMXID[i]) break;
         }
      IDMX[i]=x;
      }
   //Create string list
   TVarOptions* vo=&FMeasureVarOptions[TMeasure::IDMX];
   vo->Strings=new TStringList();
   for(int i=0;i<4;i++)
      {
      if(IDMX[i]==0) break;
      vo->Strings->Add(IDMX[i]->Index);
      }

   GetTetta();
   SetupColumns();
   Add(new TMeasurePoint());
   Calculate();
}
//---------------------------------------------------------------------------
__fastcall TMI1974::~TMI1974()
{
  int i=FMeasurePoints->Count;
  TVar* v;
  TVarOptions* vo;
  TVarContainer* m;
  for (int j=0;j<i;j++)
    {
    TMeasurePoint* p=(TMeasurePoint*)FMeasurePoints->Items[j];
    //delete text of point
    for(int i=0;i<TMeasurePoint::VarCount;i++)
      {
      vo=&FPointVarOptions[i];
      v=p->Var[i];
      if(vo->Type==TVarOptions::Text)
         if(v->Value.c!=NULL)
            delete[] v->Value.c;
      }
    //delete text measures of point
    int mc=p->Count;
    for(int mi=0;mi<mc;mi++)
      {
      m= p->Measures[mi];
       for(int i=0;i<TMeasure::VarCount;i++)
         {
         vo=&FMeasureVarOptions[i];
         v=m->Var[i];
         if(vo->Type==TVarOptions::Text)
            if(v->Value.c!=NULL)
               delete[] v->Value.c;
         }
      }
    delete p;
    }
   delete FMeasurePoints;
   if(ProverMX)
      {
      int c=ProverMX->Count;
      for(int i= 0;i<c;i++)
         delete (TProverMX*)(ProverMX->Items[i]);
      delete ProverMX;
      ProverMX = NULL;
      }
   ClearRanges();
   delete FRanges;
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::SetValue(TVar::TValue Value,TMICellData* Data)
{
   if(Data->vo->Type==TVarOptions::Text)
      {
      int c=0;
      if(Value.c) c=strlen(Value.c);
      if(c==0)
         {
         if(Data->v->Value.c)
            {
            delete[] Data->v->Value.c;
            Data->v->Value.c=0;
            }
         }
      else
         {
         if(Data->v->Value.c)
            delete[] Data->v->Value.c;
         Data->v->Value.c=new char[c+1];
         strcpy(Data->v->Value.c,Value.c);
         }
      Data->v->State=TVar::UserInput;
      }
   else
      {
      Data->v->Value=Value;
      Data->vo->ProcessRound(Data->v);
      Data->v->State=TVar::UserInput;
      Calculate();
      }
   void* vv=this;
   if(FOnChange) FOnChange((TObject*)vv);
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::ModifyPopupMenu(TPopupMenu* Menu,TMICellData* Data)
{
}
//---------------------------------------------------------------------------
__fastcall TMI1974::TMI1974(IStream* Stream, IStorage* Root):TGridData(TMI1974::CommonData)
{
   FMeasurePoints = new TList();
   FRanges = new TList();

   LoadVarOptions();//FromIni(f);

   //Load IDs
   Stream->Read(&Prover.ID,sizeof(int),0);
   Stream->Read(&tpr.ID,sizeof(int),0);
   //Load version
   int version;
   Stream->Read(&version,sizeof(version),0);
   AnsiString gs="Opened file have version ";
   gs+=version;
   if(version!=1) throw Exception(gs+". You may open only files with version 1.");

   //Load Options
   Stream->Read(&Options,sizeof(TMI1974Options),0);

   //Load VarCount
   Stream->Read(&ReadVarCount,sizeof(int),0);
   Stream->Read(&TMeasurePoint::ReadVarCount,sizeof(int),0);
   Stream->Read(&TMeasure::ReadVarCount,sizeof(int),0);
   Stream->Read(&TRange::ReadVarCount,sizeof(int),0);

   //Load MI1974 vars
   Stream->Read(Var,ReadVarCount*sizeof(TVar),0);

   MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::CalculatedData;
   MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::CalculatedData;
   //int pc=Options.ProductIndex;
   //if(pc>0)
      //{//�� ������ ����
      //pc--;
      //if(Options.DensReCalc)
         //{//Use DensReCalc
         //AnsiString s("_ReCalc");
         //s+=pc;
         //Options.ReCalc=(fReCalc)GetProcAddress(TMI1974::CommonData->hMITools,s.c_str());
         //if(!Options.ReCalc)
         //   Application->MessageBox("������ ��������� ������ �������","ReCalc",MB_ICONASTERISK);
         //}
      //else
         //{//GetBetta & GetF
         //AnsiString s("_GetBetta");
         //s+=pc;
         //Options.GetBetta=(fGetBetta)GetProcAddress(TMI1974::CommonData->hMITools,s.c_str());
         //if(!Options.GetBetta)
            //{
            //MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::InputData;
            //Application->MessageBox("������ ��������� ������ �������","GetBetta",MB_ICONASTERISK);
         //}
         //s="_GetF";
         //s+=pc;
         //Options.GetF=(fGetF)GetProcAddress(TMI1974::CommonData->hMITools,s.c_str());
         //if(!Options.GetF)
            //{
            //MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::InputData;
            //Application->MessageBox("������ ��������� ������ �������","GetF",MB_ICONASTERISK);
            //}
         //}
      //}

   //Load VarOptions
   for(int i=0;i<TMeasure::ReadVarCount;i++)
      FMeasureVarOptions[i].Load(Stream);
   for(int i=0;i<TMeasurePoint::ReadVarCount;i++)
      FPointVarOptions[i].Load(Stream);
   for(int i=0;i<ReadVarCount;i++)
      FMIVarOptions[i].Load(Stream);
   for(int i=0;i<TRange::ReadVarCount;i++)
      FRangeVarOptions[i].Load(Stream);

   //Load TGridData data members
   MeasureColumns.Load(Stream);
   PointColumns.Load(Stream);
   ResColumns.Load(Stream);



   TPack p;
   p.v.c[0]=0;// MI values;
   p.v.c[1]=d00;
   if(ResColumns.IndexOf(p.v.i)==-1)
        ResColumns.Add(p.v.i);


   //Load points data
   int PointCount;
   Stream->Read(&PointCount,sizeof(int),0);
   TMeasurePoint* m;
   for(int i=0;i<PointCount;i++)
      {//Save point data
      m=new TMeasurePoint();
      m->Load(Stream);
      Add(m);
      }
   //Get TProver & TProverMX
   ProverMX = new TList();

   TMI1974::CommonData->ProverInterface.Get(Root,Prover.ID,&Prover,ProverMX);
   //Get TPR
   TMI1974::CommonData->TPRInterface.Get(Root,tpr.ID,&tpr);
   //Make ident Options.ProverID & IDMX
   IDMX[0]=0;
   IDMX[1]=0;
   IDMX[2]=0;
   IDMX[3]=0;
   TProverMX* x;
   //TODO: check this place

   int MX_count=ProverMX->Count;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      //����� � ������ <ProverMX> MX � MX.ID ==  Options.ProverMXID[i]
      for (int j=0; j < MX_count; j++)
        {
        x = static_cast<TProverMX*>(ProverMX->Items[j]);
        if (Options.ProverMXID[i] == x->ID)
           {
           IDMX[i] = x;
           }//if
        }//for j
      }//for i
   TVarOptions* vo=&FMeasureVarOptions[TMeasure::IDMX];
   vo->Strings=new TStringList();
   for(int i=0;i<4;i++)
      {
      if(IDMX[i]==0) break;
      vo->Strings->Add(IDMX[i]->Index);
      }

   //Load string list
   TStringList* List=new TStringList();
   int c;
   Stream->Read(&c,sizeof(int),0);
   for(int i=0;i<c;i++)
      {
      char* buf;
      int l;
      Stream->Read(&l,sizeof(int),0);
      if(l)
         {
         buf=new char[l];
         Stream->Read(buf,l,0);
         List->Add(AnsiString(buf,l));
         delete[] buf;
         }
      }
   //find Text vars:: and fill stringlist
   TIntList MeasureTextIndex;
   TIntList PointTextIndex;
   TIntList MITextIndex;
   for(int i=0;i<TMeasure::VarCount;i++)
      if(FMeasureVarOptions[i].Type==TVarOptions::Text) MeasureTextIndex.Add(i);
   for(int i=0;i<TMeasurePoint::VarCount;i++)
      if(FPointVarOptions[i].Type==TVarOptions::Text) PointTextIndex.Add(i);
   for(int i=0;i<VarCount;i++)
      if(FMIVarOptions[i].Type==TVarOptions::Text) MITextIndex.Add(i);
   //Iterate through all items:
   PointCount=Count;
   int MeasureCount;
   TGridPoint* gp;
   for(int i=0;i<PointCount;i++)
      {//
      gp = (TMeasurePoint*)GridPoints[i];
      MeasureCount = gp->Count;
      for(int j=0;j<MeasureCount;j++)
         {
         TMeasure* m = (TMeasure*)(gp->Measures[j]);
         int mc=MeasureTextIndex.Count;
         for(int k=0;k<mc;k++)
            {
            TVar* v=m->Var[MeasureTextIndex.Values[k]];
            if(v->Value.c)
               {
               AnsiString s=List->Strings[v->UserData.i[0]];
               int sl=s.Length();
               if(sl)
                  {
                  v->Value.c=new char[sl+1];
                  v->Value.c[sl]=0;
                  strcpy(v->Value.c,s.c_str());
                  }
               else
                  v->Value.c=0;
               }

            }
         }
      int mc=PointTextIndex.Count;
      for(int k=0;k<mc;k++)
         {
         TVar* v=gp->Var[PointTextIndex.Values[k]];
         if(v->Value.c)
            {
            AnsiString s=List->Strings[v->UserData.i[0]];
            int sl=s.Length();
            if(sl)
               {
               v->Value.c=new char[sl+1];
               strcpy(v->Value.c,s.c_str());
               }
            else
               v->Value.c=0;
            }
         }
      }
   int mc=MITextIndex.Count;
   for(int k=0;k<mc;k++)
      {
      TVar* v=&Var[MITextIndex.Values[k]];
      if(v->Value.c)
         {
         AnsiString s=List->Strings[v->UserData.i[0]];
         int sl=s.Length();
         if(sl)
            {
            v->Value.c=new char[sl+1];
            strcpy(v->Value.c,s.c_str());
            }
         else
            v->Value.c=0;
         }
      }
   delete List;
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::Save(IStream* Stream)
{
   HRESULT res;
   //Save IDs
   res=Stream->Write(&Prover.ID,sizeof(int),0);//+
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   res=Stream->Write(&tpr.ID,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //Save Version
   int vers=1;
   res=Stream->Write(&vers,sizeof(vers),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //Save Options
   res=Stream->Write(&Options,sizeof(TMI1974Options),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //find Text vars:: and fill stringlist
   TStringList* List=new TStringList;
   TIntList MeasureTextIndex;
   TIntList PointTextIndex;
   TIntList MITextIndex;
   for(int i=0;i<TMeasure::VarCount;i++)
      if(FMeasureVarOptions[i].Type==TVarOptions::Text) MeasureTextIndex.Add(i);
   for(int i=0;i<TMeasurePoint::VarCount;i++)
      if(FPointVarOptions[i].Type==TVarOptions::Text) PointTextIndex.Add(i);
   for(int i=0;i<VarCount;i++)
      if(FMIVarOptions[i].Type==TVarOptions::Text) MITextIndex.Add(i);
   //Iterate through all items:
   int PointCount=Count;
   int MeasureCount;
   TGridPoint* gp;
   TMeasure* m;
   for(int i=0;i<PointCount;i++)
      {//
      gp = (TMeasurePoint*)GridPoints[i];
      MeasureCount = gp->Count;
      for(int j=0;j<MeasureCount;j++)
         {
         TMeasure* m = (TMeasure*)(gp->Measures[j]);
         int mc=MeasureTextIndex.Count;
         for(int k=0;k<mc;k++)
            {
            TVar* v=m->Var[MeasureTextIndex.Values[k]];
            if(v->Value.c)
               v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
            }
         }
      int mc=PointTextIndex.Count;
      for(int k=0;k<mc;k++)
         {
         TVar* v=gp->Var[PointTextIndex.Values[k]];
         if(v->Value.c)
            v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
         }
      }
   int mc=MITextIndex.Count;
   for(int k=0;k<mc;k++)
      {
      TVar* v=&Var[MITextIndex.Values[k]];
      if(v->Value.c)
         v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
      }

   //Save variables count
   int vc=VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   vc=TMeasurePoint::VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   vc=TMeasure::VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   vc=TRange::VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //Save MI1974 vars
   res=Stream->Write(Var,VarCount*sizeof(TVar),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //Save VarOPtions
   for(int i=0;i<TMeasure::VarCount;i++)
      FMeasureVarOptions[i].Save(Stream);
   for(int i=0;i<TMeasurePoint::VarCount;i++)
      FPointVarOptions[i].Save(Stream);
   for(int i=0;i<VarCount;i++)
      FMIVarOptions[i].Save(Stream);
   for(int i=0;i<TRange::VarCount;i++)
      FRangeVarOptions[i].Save(Stream);

   //Save TGridData data members
   MeasureColumns.Save(Stream);
   PointColumns.Save(Stream);
   ResColumns.Save(Stream);

   //Save points data
   res=Stream->Write(&PointCount,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   for(int i=0;i<PointCount;i++)
      {//Save point data
      gp=GridPoints[i];
      gp->Save(Stream);
      }

   //Save string list
   int c=List->Count;
   res=Stream->Write(&c,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
   for(int i=0;i<c;i++)
      {
      AnsiString s=List->Strings[i];
      int l=s.Length();
      res=Stream->Write(&l,sizeof(int),0);
      if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
      if(l)
         {
         res=Stream->Write(s.c_str(),l,0);
         if(res!=S_OK) throw Exception("Error write in TMI1974::Save");
         }
      }


   delete List;
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::ClearPoints()
{
  int i=FMeasurePoints->Count;
  for (int j=0;j<i;j++)
    {
    TMeasurePoint* m=(TMeasurePoint*)FMeasurePoints->Items[j];
    delete m;
    }
  FMeasurePoints->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::SetupColumns(void)
{
// Setup measure columns
   MeasureColumns.Clear();
   MeasureColumns.Add(TMeasure::Used);
   MeasureColumns.Add(TMeasure::Number);
   MeasureColumns.Add(TMeasure::Q);

   MeasureColumns.Add(TMeasure::f);
   if(Options.FreqInput==TMI1974Options::User)
      MeasureVarOptions[TMeasure::f]->Kind=TVarOptions::InputData;
   else
      MeasureVarOptions[TMeasure::f]->Kind=TVarOptions::CalculatedData;

   if(Options.ProverMXID[1]!=0)
      MeasureColumns.Add(TMeasure::IDMX);

   MeasureColumns.Add(TMeasure::Pulses);
   MeasureColumns.Add(TMeasure::Time);

   MeasureColumns.Add(TMeasure::t);
   MeasureColumns.Add(TMeasure::P);
   MeasureColumns.Add(TMeasure::t_TPU);
   MeasureColumns.Add(TMeasure::P_TPU);
   if(Options.UseFD)
      {
      MeasureColumns.Add(TMeasure::Dens);
      MeasureColumns.Add(TMeasure::t_FD);
      MeasureColumns.Add(TMeasure::P_FD);
      }
   else
      {
      MeasureVarOptions[TMeasure::Dens]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TMeasure::t_FD]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TMeasure::P_FD]->Kind=TVarOptions::CalculatedData;
      }

   MeasureColumns.Add(TMeasure::K);

   MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::CalculatedData;
   MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::CalculatedData;

   MeasureColumns.Add(TMeasure::Betta);
   MeasureColumns.Add(TMeasure::F);

   if(Options.ProductIndex==0)
      {
      MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::InputData;
      MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::InputData;
      }

   if(Options.UseVisc)
      {
      MeasureColumns.Add(TMeasure::visc);
      MeasureVarOptions[TMeasure::visc]->Kind=TVarOptions::InputData;
      }
   else
      MeasureVarOptions[TMeasure::visc]->Kind=TVarOptions::CalculatedData;

   MeasureColumns.Add(TMeasure::V);
   MeasureColumns.Add(TMeasure::d);

   MeasureColumns.Add(TMeasure::Desc);
   MeasureColumns.Add(TMeasure::d00);

   // Setup point columns
   PointColumns.Clear();

   PointColumns.Add(TMeasurePoint::Used);
   PointColumns.Add(TMeasurePoint::Number);
   PointColumns.Add(TMeasurePoint::n);
   PointColumns.Add(TMeasurePoint::Q);

   PointColumns.Add(TMeasurePoint::f);
   PointColumns.Add(TMeasurePoint::K);
   PointColumns.Add(TMeasurePoint::SKO);
   PointColumns.Add(TMeasurePoint::e);
   PointColumns.Add(TMeasurePoint::Tetta_K);
   PointColumns.Add(TMeasurePoint::d);
   PointColumns.Add(TMeasurePoint::Notes);
   PointColumns.Add(TMeasurePoint::Kvist);
   PointColumns.Add(TMeasurePoint::d00);

   // Setup MI columns
   ResColumns.Clear();
   TPack p;
   if(Options.Function)
      {
      p.v.c[0]=0;// MI values;
      p.v.c[1]=Tetta_Sum;
      ResColumns.Add(p.v.i);
      p.v.c[1]=vmin;
      ResColumns.Add(p.v.i);
      p.v.c[1]=vmax;
      ResColumns.Add(p.v.i);
      p.v.c[1]=d00;
      ResColumns.Add(p.v.i);

      p.v.c[0]=2;// Range values;
      p.v.c[1]=TRange::Number;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::Q1;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::Q2;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::K;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::Tetta_K;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::Tetta_Sum;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::edk;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::sk;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::TettaS;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::zdk;
      ResColumns.Add(p.v.i);
      p.v.c[1]=TRange::ddk;
      ResColumns.Add(p.v.i);
      }
   else
      {
      p.v.c[0]=0;// MI values;
      for(int i=0;i<VarCount;i++)
         {
         p.v.c[1]=i;
         ResColumns.Add(p.v.i);
         }
      };
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::Load(IStream* Stream)
{
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::Calculate()
{
   int PointCount = Count;
   Var[BettaMax].Value.d=0;
   Var[BettaMax].State=TVar::Calculated;

   Var[d00].Value.d=0;
   Var[d00].State=TVar::Calculated;

   Var[UsedPoints].Value.i=0;
   Var[UsedPoints].State=TVar::Calculated;
   Var[K].Value.d=0;
   Var[K].State=TVar::Calculated;
   Var[ed].Value.d=0;
   Var[ed].State=TVar::Calculated;
   Var[SKOd].Value.d=0;
   Var[SKOd].State=TVar::Calculated;

   if(Options.UseVisc)
      {
      Var[vmin].Value.d=0;
      Var[vmin].State=TVar::NotUsed;
      Var[vmax].Value.d=0;
      Var[vmax].State=TVar::Calculated;
      }

   ValidQ=true;
   //iterate throw points
//--- Calculate Kj, K, SKOj, ej, t095j,fj,Qj, BettaMax, ed, Sd, vmin, vmax
   for(int i=0;i<PointCount;i++)
      {
      CalculatePoint1(i);
      }//End iterate throw points
   if(Var[BettaMax].Value.d==0)
      Var[BettaMax].State=TVar::ErrorArg;
//--- Sort Point by Rate
	SortPoints();
//--- Create Ranges
   ClearRanges();
   int pu=Var[UsedPoints].Value.i;
   for(int i=0;i<(pu-1);i++)
      FRanges->Add(new TRange);
//--- Calculate Tetta_t
   Var[Tetta_t].Value.d=Var[BettaMax].Value.d*100*
               sqrt(Options.dtTPR*Options.dtTPR + Options.dtTPU*Options.dtTPU);
   MIVarOptions[Tetta_t]->ProcessRound(&Var[Tetta_t]);
   Var[Tetta_t].State=Var[BettaMax].State;
//--- Calculate K
   if(Var[UsedPoints].Value.i>0)
      Var[K].Value.d/=Var[UsedPoints].Value.i;
   else
      Var[K].State=TVar::ErrorArg;
   MIVarOptions[K]->ProcessRound(&Var[K]);

//--- Calculate d00
   if(Var[UsedPoints].Value.i>0)
      Var[d00].Value.d/=Var[UsedPoints].Value.i;
   else
      Var[d00].State=TVar::ErrorArg;
   MIVarOptions[d00]->ProcessRound(&Var[d00]);



   n=0;
   Var[Tetta_K].Value.d=0;
   Var[Tetta_K].State=TVar::Calculated;

//--- Calculate Tetta_sum
   double s=Var[Tetta_v0].Value.d*Var[Tetta_v0].Value.d +
               Var[Tetta_t].Value.d*Var[Tetta_t].Value.d +
               Var[Tetta_Sum0].Value.d*Var[Tetta_Sum0].Value.d +
               Var[Tetta_UOI].Value.d*Var[Tetta_UOI].Value.d;
      Var[Tetta_Sum].Value.d=1.1*sqrt(s);
      if( (Var[Tetta_t].State==TVar::Calculated) &&
          (Var[Tetta_v0].State==TVar::Calculated) &&
          (Var[Tetta_Sum0].State==TVar::Calculated) &&
          (Var[Tetta_UOI].State==TVar::Calculated) )
         Var[Tetta_Sum].State=TVar::Calculated;
      else
         Var[Tetta_Sum].State=TVar::ErrorArg;
      MIVarOptions[Tetta_Sum]->ProcessRound(&Var[Tetta_Sum]);

   currange=0;
   //iterate throw points
//--- Calculate TettaKj, TettaK
   for(int i=0;i<PointCount;i++)
      {
      CalculatePoint2(i);
      }//End iterate throw points
//--- Calculate Tetta_sumd, TettaS
   if(Options.Function==TMI1974Options::Const)
      {
      double s=Var[Tetta_v0].Value.d*Var[Tetta_v0].Value.d +
               Var[Tetta_K].Value.d*Var[Tetta_K].Value.d +
               Var[Tetta_t].Value.d*Var[Tetta_t].Value.d +
               Var[Tetta_Sum0].Value.d*Var[Tetta_Sum0].Value.d +
               Var[Tetta_UOI].Value.d*Var[Tetta_UOI].Value.d;
      Var[Tetta_Sumd].Value.d=1.1*sqrt(s);
      if( (Var[Tetta_K].State==TVar::Calculated) &&
          (Var[Tetta_t].State==TVar::Calculated) &&
          (Var[Tetta_v0].State==TVar::Calculated) &&
          (Var[Tetta_Sum0].State==TVar::Calculated) &&
          (Var[Tetta_UOI].State==TVar::Calculated) )
         Var[Tetta_Sumd].State=TVar::Calculated;
      else
         Var[Tetta_Sumd].State=TVar::ErrorArg;
      MIVarOptions[Tetta_Sumd]->ProcessRound(&Var[Tetta_Sumd]);

      if(Var[SKOd].Value.d)
         {
         Var[TettaS].Value.d=Var[Tetta_Sumd].Value.d/Var[SKOd].Value.d;
         if( (Var[SKOd].State==TVar::Calculated) &&
             (Var[Tetta_Sumd].State==TVar::Calculated) )
            Var[TettaS].State=TVar::Calculated;
         else
            Var[TettaS].State=TVar::ErrorArg;
         MIVarOptions[TettaS]->ProcessRound(&Var[TettaS]);
         }
      else
         {
         Var[TettaS].Value.d=0;
         Var[TettaS].State=TVar::ErrorArg;
         }
//--- Calculate dd, Zd
      if( (0.8 <=Var[TettaS].Value.d ) && (Var[TettaS].Value.d<=8) )
         {
         Var[Zd].Value.d=GetZ(Var[TettaS].Value.d);
         MIVarOptions[Zd]->ProcessRound(&Var[Zd]);
         Var[Zd].State=Var[TettaS].State;

         Var[dd].Value.d=Var[Zd].Value.d*(Var[Tetta_Sumd].Value.d+Var[ed].Value.d);
         if( (Var[Zd].State==TVar::Calculated) &&
             (Var[Tetta_Sumd].State==TVar::Calculated) &&
             (Var[ed].State==TVar::Calculated) )
            Var[dd].State=TVar::Calculated;
         else
            Var[dd].State=TVar::ErrorArg;
         MIVarOptions[dd]->ProcessRound(&Var[dd]);
         }
      else
         {
         Var[Zd].Value.d=0;
         Var[Zd].State=TVar::NotUsed;

         Var[dd].Value.d=Var[Tetta_Sumd].Value.d;
         Var[dd].State=Var[Tetta_Sumd].State;
         MIVarOptions[dd]->ProcessRound(&Var[dd]);
         }
      }
   CalculateRanges();
   void* f=this;
   if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
   if(FOnChange) FOnChange((TObject*)f);
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculatePoint1(int Index)
{
   TMeasurePoint* p = (TMeasurePoint*)GridPoints[Index];
   int MeasureCount = p->Count;
//Calculate MI::UsedPoints
   if(p->Var[TMeasurePoint::Used]->Value.b)
      {
      Var[UsedPoints].Value.i++;
      p->Var[TMeasurePoint::Number]->Value.i=Var[UsedPoints].Value.i;
      p->Var[TMeasurePoint::Number]->State=TVar::Calculated;
      }
   else
      {
      p->Var[TMeasurePoint::Number]->Value.i=0;
      p->Var[TMeasurePoint::Number]->State=TVar::NotUsed;
      }
//Calculate Q
   p->Var[TMeasurePoint::Q]->Value.d=0;
   p->Var[TMeasurePoint::Q]->State=TVar::Calculated;

   p->Var[TMeasurePoint::Qp]->Value.d=0;
   p->Var[TMeasurePoint::Qp]->State=TVar::Calculated;

//-------------------------------------
   p->Var[TMeasurePoint::n]->Value.i=0;
   p->Var[TMeasurePoint::n]->State=TVar::Calculated;
   p->Var[TMeasurePoint::K]->Value.d=0;
   p->Var[TMeasurePoint::K]->State=TVar::Calculated;
   p->Var[TMeasurePoint::d00]->Value.d=0;
   p->Var[TMeasurePoint::d00]->State=TVar::Calculated;
   p->Var[TMeasurePoint::f]->Value.d=0;
   p->Var[TMeasurePoint::f]->State=TVar::Calculated;
//---Calculates Qj, Kj, fj
   for(int j=0; j<MeasureCount; j++)
      {
      CalculateMeasure1(p,j);
      }
//Calculate Q
   if(p->Var[TMeasurePoint::n]->Value.i)
      {
      p->Var[TMeasurePoint::Q]->Value.d/=p->Var[TMeasurePoint::n]->Value.i;
      FPointVarOptions[TMeasurePoint::Q].ProcessRound(p->Var[TMeasurePoint::Q]);
      }
   else
      p->Var[TMeasurePoint::Q]->State=TVar::ErrorArg;

   if(tpr.Qmax>0)
      {
      p->Var[TMeasurePoint::Qp]->Value.d=100*p->Var[TMeasurePoint::Q]->Value.d/tpr.Qmax;
      FPointVarOptions[TMeasurePoint::Qp].ProcessRound(p->Var[TMeasurePoint::Qp]);
      if(p->Var[TMeasurePoint::Q]->State==TVar::Calculated)
         p->Var[TMeasurePoint::Qp]->State=TVar::Calculated;
      else
         p->Var[TMeasurePoint::Qp]->State=TVar::ErrorArg;
      }
      else
         p->Var[TMeasurePoint::Qp]->State=TVar::ErrorArg;
   //Calculate Kj, fj
   if(p->Var[TMeasurePoint::n]->Value.i)
      {
      p->Var[TMeasurePoint::K]->Value.d/=p->Var[TMeasurePoint::n]->Value.i;
      FPointVarOptions[TMeasurePoint::K].ProcessRound(p->Var[TMeasurePoint::K]);
      p->Var[TMeasurePoint::f]->Value.d/=p->Var[TMeasurePoint::n]->Value.i;
      FPointVarOptions[TMeasurePoint::f].ProcessRound(p->Var[TMeasurePoint::f]);
      }
   else
      {
      p->Var[TMeasurePoint::K]->State=TVar::ErrorArg;
      p->Var[TMeasurePoint::f]->State=TVar::ErrorArg;
      }

//---Calculates Delatij, SKOj
   p->Var[TMeasurePoint::SKO]->Value.d=0;
   p->Var[TMeasurePoint::SKO]->State=TVar::Calculated;
      for(int j=0; j<MeasureCount; j++)
      {
      CalculateMeasure2(p,j);
      }
//---Calculates SKOj
   if(p->Var[TMeasurePoint::n]->Value.i>1)
      {
      p->Var[TMeasurePoint::SKO]->Value.d/=(p->Var[TMeasurePoint::n]->Value.i-1);
      p->Var[TMeasurePoint::SKO]->Value.d=100*sqrt(p->Var[TMeasurePoint::SKO]->Value.d);
      if(p->Var[TMeasurePoint::K]->Value.d>0)
         p->Var[TMeasurePoint::SKO]->Value.d/=p->Var[TMeasurePoint::K]->Value.d;
      else
         p->Var[TMeasurePoint::SKO]->State=TVar::ErrorArg;
   FPointVarOptions[TMeasurePoint::SKO].ProcessRound(p->Var[TMeasurePoint::SKO]);      }
   else
      p->Var[TMeasurePoint::SKO]->State=TVar::ErrorArg;
//--- Calculate t095j
   TFunctionResult r=CommonData->MITools.Student095(p->Var[TMeasurePoint::n]->Value.i,
                                          p->Var[TMeasurePoint::t095]->Value.d);
   if(r==frOk)
      p->Var[TMeasurePoint::t095]->State=TVar::Calculated;
   else
      p->Var[TMeasurePoint::t095]->State=TVar::ErrorArg;
   FPointVarOptions[TMeasurePoint::t095].ProcessRound(p->Var[TMeasurePoint::t095]);
//--- Calculate ej
   p->Var[TMeasurePoint::e]->State=TVar::Calculated;
   p->Var[TMeasurePoint::e]->Value.d=p->Var[TMeasurePoint::SKO]->Value.d*
                                          p->Var[TMeasurePoint::t095]->Value.d;
   if( (p->Var[TMeasurePoint::SKO]->State!=TVar::Calculated) ||
                         (p->Var[TMeasurePoint::t095]->State!=TVar::Calculated))
      p->Var[TMeasurePoint::e]->State=TVar::ErrorArg;
   FPointVarOptions[TMeasurePoint::e].ProcessRound(p->Var[TMeasurePoint::e]);
//--- Calculate ed, SKOd
   if( (p->Var[TMeasurePoint::Used]->Value.b) &&
                        (Var[ed].Value.d < p->Var[TMeasurePoint::e]->Value.d) )
      {
      Var[ed].Value.d=p->Var[TMeasurePoint::e]->Value.d;
      Var[ed].State=p->Var[TMeasurePoint::e]->State;

      Var[SKOd].Value.d=p->Var[TMeasurePoint::SKO]->Value.d;
      Var[SKOd].State=p->Var[TMeasurePoint::SKO]->State;
      }
//---Calculates K
   if(p->Var[TMeasurePoint::Used]->Value.b)
      {
      Var[K].Value.d+=p->Var[TMeasurePoint::K]->Value.d;
      if(p->Var[TMeasurePoint::K]->State!=TVar::Calculated)
         Var[K].State=TVar::ErrorArg;
      }
//---Calculates d00
   if(p->Var[TMeasurePoint::Kvist]->Value.d>0)
      {
      p->Var[TMeasurePoint::d00]->Value.d=
                (
                  (p->Var[TMeasurePoint::K]->Value.d - p->Var[TMeasurePoint::Kvist]->Value.d)*100
                )/p->Var[TMeasurePoint::Kvist]->Value.d;

      FPointVarOptions[TMeasurePoint::d00].ProcessRound(p->Var[TMeasurePoint::d00]);
      if( (p->Var[TMeasurePoint::Kvist]->State==TVar::UserInput) &&
           (p->Var[TMeasurePoint::K]->State==TVar::Calculated) )
         p->Var[TMeasurePoint::d00]->State=TVar::Calculated;
      else
         p->Var[TMeasurePoint::d00]->State=TVar::ErrorArg;
      }
   else
      p->Var[TMeasurePoint::d00]->State=TVar::ErrorArg;

   if(p->Var[TMeasurePoint::Used]->Value.b)
      {
      Var[d00].Value.d+=p->Var[TMeasurePoint::d00]->Value.d;
      if(p->Var[TMeasurePoint::d00]->State!=TVar::Calculated)
         Var[d00].State=TVar::ErrorArg;
      }
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculateMeasure1(TMeasurePoint* p,int Index)
{
   TMeasure* m = (TMeasure*)(p->Measures[Index]);
//Calculte number of used measures
   m->Var[TMeasure::Number]->Value.i=0;
   m->Var[TMeasure::Number]->State=TVar::NotUsed;
   if(m->Var[TMeasure::Used]->Value.b)
      {
      p->Var[TMeasurePoint::n]->Value.i++;
      m->Var[TMeasure::Number]->Value.i=p->Var[TMeasurePoint::n]->Value.i;
      m->Var[TMeasure::Number]->State=TVar::Calculated;
      }
//Calculate Density
   if(!Options.UseFD)
      {
      m->Var[TMeasure::Dens]->Value.d=Options.Density;
      m->Var[TMeasure::t_FD]->Value.d=Options.tFD;
      m->Var[TMeasure::P_FD]->Value.d=0;
      m->Var[TMeasure::Dens]->State=TVar::Calculated;
      m->Var[TMeasure::t_FD]->State=TVar::Calculated;
      m->Var[TMeasure::P_FD]->State=TVar::Calculated;
      }
//Calculate Betta & F
   CalculateBnF(m);
//---���������� BettaMax
   if( (m->Var[TMeasure::Used]->Value.b)&&
               (m->Var[TMeasure::Betta]->Value.d > Var[BettaMax].Value.d)&&
               (p->Var[TMeasurePoint::Used]->Value.b) )
      Var[BettaMax].Value.d=m->Var[TMeasure::Betta]->Value.d;
//---------------
//---���������� Ktij
//---------------
//
   TProverMX* pmx=IDMX[m->Var[TMeasure::IDMX]->Value.i];
   if(Prover.Type==TProver::Compact)
      m->Var[TMeasure::Kt]->Value.d=1+Prover.Alfa*(m->Var[TMeasure::t_TPU]->Value.d - pmx->tBase) +
                 Prover.Alfa_invar* (Options.t_invar - pmx->tBase);
   else
      m->Var[TMeasure::Kt]->Value.d=1+3*Prover.Alfa*(m->Var[TMeasure::t_TPU]->Value.d - pmx->tBase);
   MeasureVarOptions[TMeasure::Kt]->ProcessRound(m->Var[TMeasure::Kt]);
   m->Var[TMeasure::Kt]->State=TVar::Calculated;
//---------------
//---���������� Kpij
//---------------
   m->Var[TMeasure::Kp]->Value.d=1+0.95*m->Var[TMeasure::P_TPU]->Value.d*Prover.D/(Prover.E*Prover.S);
   MeasureVarOptions[TMeasure::Kp]->ProcessRound(m->Var[TMeasure::Kp]);
   m->Var[TMeasure::Kp]->State=TVar::Calculated;
//---------------
//---���������� Kgtij
//---------------
   m->Var[TMeasure::Kgt]->Value.d=1+m->Var[TMeasure::Betta]->Value.d*(m->Var[TMeasure::t]->Value.d -
                                    m->Var[TMeasure::t_TPU]->Value.d);
   MeasureVarOptions[TMeasure::Kgt]->ProcessRound(m->Var[TMeasure::Kgt]);
   if( (m->Var[TMeasure::Betta]->State==TVar::Calculated) || (m->Var[TMeasure::Betta]->State==TVar::UserInput))
      m->Var[TMeasure::Kgt]->State=TVar::Calculated;
   else
      {
      if((m->Var[TMeasure::Betta]->State==TVar::UsedNearest)&&(m->Var[TMeasure::Betta]->Value.d>0) )
         m->Var[TMeasure::Kgt]->State=TVar::Calculated;
      else
         m->Var[TMeasure::Kgt]->State=TVar::ErrorArg;
      }
//---------------
//---���������� Kpgij
//---------------
   m->Var[TMeasure::Kgp]->Value.d=1-m->Var[TMeasure::F]->Value.d*(m->Var[TMeasure::P]->Value.d -
                                    m->Var[TMeasure::P_TPU]->Value.d);
   MeasureVarOptions[TMeasure::Kgp]->ProcessRound(m->Var[TMeasure::Kgp]);
   if((m->Var[TMeasure::F]->State==TVar::Calculated) || (m->Var[TMeasure::F]->State==TVar::UserInput) )
      m->Var[TMeasure::Kgp]->State=TVar::Calculated;
   else
      {
      if((m->Var[TMeasure::F]->State==TVar::UsedNearest)&&(m->Var[TMeasure::F]->Value.d>0) )
         m->Var[TMeasure::Kgp]->State=TVar::Calculated;
      else
         m->Var[TMeasure::Kgp]->State=TVar::ErrorArg;
      }
//---------------
//---���������� Ktpij
//---------------
   m->Var[TMeasure::Ktp]->Value.d=m->Var[TMeasure::Kt]->Value.d*
                                  m->Var[TMeasure::Kp]->Value.d*
                                  m->Var[TMeasure::Kgt]->Value.d*
                                  m->Var[TMeasure::Kgp]->Value.d;
   MeasureVarOptions[TMeasure::Ktp]->ProcessRound(m->Var[TMeasure::Ktp]);

   if(                        (m->Var[TMeasure::Kt]->State==TVar::Calculated) &&
                              (m->Var[TMeasure::Kp]->State==TVar::Calculated) &&
                              (m->Var[TMeasure::Kgt]->State==TVar::Calculated)&&
                              (m->Var[TMeasure::Kgp]->State==TVar::Calculated))
      m->Var[TMeasure::Ktp]->State=TVar::Calculated;
   else
      m->Var[TMeasure::Ktp]->State=TVar::ErrorArg;
//---------------
//---���������� Vij
//---------------
   m->Var[TMeasure::V]->Value.d=pmx->V * m->Var[TMeasure::Ktp]->Value.d;
   MeasureVarOptions[TMeasure::V]->ProcessRound(m->Var[TMeasure::V]);
   if(m->Var[TMeasure::Ktp]->State==TVar::Calculated)
      m->Var[TMeasure::V]->State=TVar::Calculated;
   else
      m->Var[TMeasure::V]->State=TVar::ErrorArg;
//---------------
//---���������� Kij
//---------------
   if(m->Var[TMeasure::V]->Value.d > 0)
      {
      m->Var[TMeasure::K]->Value.d=m->Var[TMeasure::Pulses]->Value.d /
                                   m->Var[TMeasure::V]->Value.d;
      if( (m->Var[TMeasure::V]->State==TVar::Calculated) &&
                               (m->Var[TMeasure::Pulses]->Value.d >0 ) )
         m->Var[TMeasure::K]->State=TVar::Calculated;
      else
         m->Var[TMeasure::K]->State=TVar::ErrorArg;
      }
   else
      {
      m->Var[TMeasure::K]->Value.d=0;
      m->Var[TMeasure::K]->State=TVar::ErrorArg;
      }
   MeasureVarOptions[TMeasure::K]->ProcessRound(m->Var[TMeasure::K]);

//---------------
//---���������� Q
//---------------
   if(m->Var[TMeasure::Time]->Value.d>0)
      {
      m->Var[TMeasure::Q]->Value.d = m->Var[TMeasure::V]->Value.d * 3600/
                                     m->Var[TMeasure::Time]->Value.d;
      FMeasureVarOptions[TMeasure::Q].ProcessRound(m->Var[TMeasure::Q]);
      if(m->Var[TMeasure::V]->State==TVar::Calculated)
         m->Var[TMeasure::Q]->State=TVar::Calculated;
      else
         m->Var[TMeasure::Q]->State=TVar::ErrorArg;
      }
   else
      m->Var[TMeasure::Q]->State=TVar::ErrorArg;

   if(tpr.Qmax>0)
      {
      m->Var[TMeasure::Qp]->Value.d=100*m->Var[TMeasure::Q]->Value.d/tpr.Qmax;
      FMeasureVarOptions[TMeasure::Qp].ProcessRound(m->Var[TMeasure::Qp]);
      if(m->Var[TMeasure::Q]->State==TVar::Calculated)
         m->Var[TMeasure::Qp]->State=TVar::Calculated;
      else
         m->Var[TMeasure::Qp]->State=TVar::ErrorArg;
      }
   else
      m->Var[TMeasure::Qp]->State=TVar::ErrorArg;
//---------------
//---���������� fij
//---------------
   if(Options.FreqInput!=TMI1974Options::User)
      {
      m->Var[TMeasure::f]->Value.d=m->Var[TMeasure::Q]->Value.d *
               m->Var[TMeasure::K]->Value.d/3600;
      FMeasureVarOptions[TMeasure::f].ProcessRound(m->Var[TMeasure::f]);
      if(m->Var[TMeasure::Q]->State==TVar::Calculated && m->Var[TMeasure::K]->State==TVar::Calculated)
         m->Var[TMeasure::f]->State=TVar::Calculated;
      else
         m->Var[TMeasure::f]->State=TVar::ErrorArg;
      }
//---------------
//---���������� Qj, Kj, fj
//---------------
   if(m->Var[TMeasure::Used]->Value.b)
      {
      p->Var[TMeasurePoint::Q]->Value.d+=m->Var[TMeasure::Q]->Value.d;
      if(m->Var[TMeasure::Q]->State==TVar::ErrorArg)
            p->Var[TMeasurePoint::Q]->State=TVar::ErrorArg;

//---���������� Kj
      p->Var[TMeasurePoint::K]->Value.d+=m->Var[TMeasure::K]->Value.d;
      if(m->Var[TMeasure::K]->State!=TVar::Calculated)
         p->Var[TMeasurePoint::K]->State=TVar::ErrorArg;
//---���������� fj
      p->Var[TMeasurePoint::f]->Value.d+=m->Var[TMeasure::f]->Value.d;
      if(   !( (m->Var[TMeasure::f]->State==TVar::Calculated)||
              (m->Var[TMeasure::f]->State==TVar::UserInput) ||
              (m->Var[TMeasure::f]->State==TVar::UsedNearest)  )  )
         p->Var[TMeasurePoint::f]->State=TVar::ErrorArg;
      }
      
//---���������� vMax
   if( (Options.UseVisc) && (m->Var[TMeasure::Used]->Value.b)&&
               (m->Var[TMeasure::visc]->Value.d > Var[vmax].Value.d)&&
               (p->Var[TMeasurePoint::Used]->Value.b) )
      Var[vmax].Value.d=m->Var[TMeasure::visc]->Value.d;
//---���������� vMin
   if((Options.UseVisc) && (m->Var[TMeasure::Used]->Value.b)&&(p->Var[TMeasurePoint::Used]->Value.b))
      {
      if(Var[vmin].State==TVar::NotUsed)
         {
         Var[vmin].Value.d=m->Var[TMeasure::visc]->Value.d;
         if( (m->Var[TMeasure::visc]->State==TVar::UsedNearest) ||
                               (m->Var[TMeasure::visc]->State==TVar::UserInput) )
            Var[vmin].State=TVar::Calculated;
         }
      else
         if(m->Var[TMeasure::visc]->Value.d < Var[vmin].Value.d)
            {
            Var[vmin].Value.d=m->Var[TMeasure::visc]->Value.d;
            if( (m->Var[TMeasure::visc]->State==TVar::UsedNearest) ||
                              (m->Var[TMeasure::visc]->State==TVar::UserInput) )
               Var[vmin].State=TVar::Calculated;
            }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::SortPoints()
{
   int c = Count;
   int a;
   TMeasurePoint* p1;
   TMeasurePoint* p2;
   for(int i=0;i<(c-1);i++)
      {
      p1 = (TMeasurePoint*)GridPoints[i];
      a=-1;
      for( int j=i+1;j<c;j++)
         {
         p2 = (TMeasurePoint*)GridPoints[j];
         if( p1->Var[TMeasurePoint::Q]->Value.d > p2->Var[TMeasurePoint::Q]->Value.d)
            {
            p1=p2;
            a=j;
            }
         }
      if(a!=-1)
         {
         FMeasurePoints->Move(a,i);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculateMeasure2(TMeasurePoint* p,int Index)
{
   TMeasure* m = (TMeasure*)(p->Measures[Index]);
//Calculate dij
   if(p->Var[TMeasurePoint::K]->Value.d>0)
      {
      m->Var[TMeasure::d]->Value.d=(m->Var[TMeasure::K]->Value.d -
                                       p->Var[TMeasurePoint::K]->Value.d);

      FMeasureVarOptions[TMeasure::d].ProcessRound(m->Var[TMeasure::d]);
      if( (p->Var[TMeasurePoint::K]->State==TVar::Calculated) &&
           (m->Var[TMeasure::K]->State==TVar::Calculated) )
         m->Var[TMeasure::d]->State=TVar::Calculated;
      else
         m->Var[TMeasure::d]->State=TVar::ErrorArg;
      }
   else
      m->Var[TMeasure::d]->State=TVar::ErrorArg;

//Calculate d00ij
   m->Var[TMeasure::d00]->Value.d=0;
   if(p->Var[TMeasurePoint::Kvist]->Value.d>0)
      {
      m->Var[TMeasure::d00]->Value.d=
                (
                  (m->Var[TMeasure::K]->Value.d - p->Var[TMeasurePoint::Kvist]->Value.d)*100
                )/p->Var[TMeasurePoint::Kvist]->Value.d;

      FMeasureVarOptions[TMeasure::d00].ProcessRound(m->Var[TMeasure::d00]);
      if( (p->Var[TMeasurePoint::Kvist]->State==TVar::UserInput) &&
           (m->Var[TMeasure::K]->State==TVar::Calculated) )
         m->Var[TMeasure::d00]->State=TVar::Calculated;
      else
         m->Var[TMeasure::d00]->State=TVar::ErrorArg;
      }
   else
      m->Var[TMeasure::d00]->State=TVar::ErrorArg;
//Calculate SKOj
   if(m->Var[TMeasure::Used]->Value.b)
      {
      p->Var[TMeasurePoint::SKO]->Value.d+=m->Var[TMeasure::d]->Value.d*m->Var[TMeasure::d]->Value.d;
      if(m->Var[TMeasure::d]->State!=TVar::Calculated)
         p->Var[TMeasurePoint::SKO]->State=TVar::ErrorArg;
      }
//Calculate dij
   if(p->Var[TMeasurePoint::K]->Value.d>0)
      {
      m->Var[TMeasure::d]->Value.d/=p->Var[TMeasurePoint::K]->Value.d;
      m->Var[TMeasure::d]->Value.d*=100;
      FMeasureVarOptions[TMeasure::d].ProcessRound(m->Var[TMeasure::d]);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculatePoint2(int PointIndex)
{
   TMeasurePoint* p = (TMeasurePoint*)GridPoints[PointIndex];

//--- Ranges
   if(p->Var[TMeasurePoint::Used]->Value.b && FRanges->Count)
      {
      TRange* g;
      if(currange)
         {
         g=(TRange*)FRanges->Items[currange-1];
         g->PointIndex2=PointIndex;
         if(currange < FRanges->Count)
            ((TRange*)FRanges->Items[currange])->PointIndex1=PointIndex;
         }
      else
         {
         g=(TRange*)FRanges->Items[0];
         g->PointIndex1=PointIndex;
         }
      currange++;
      }

//--- Calculate TettaKj
   p->Var[TMeasurePoint::Tetta_K]->State=TVar::ErrorArg;
   if(Var[K].Value.d!=0)
      {
      p->Var[TMeasurePoint::Tetta_K]->Value.d=100*(p->Var[TMeasurePoint::K]->Value.d -
               Var[K].Value.d)/Var[K].Value.d;
      FPointVarOptions[TMeasurePoint::Tetta_K].ProcessRound(p->Var[TMeasurePoint::Tetta_K]);
      if( (Var[K].State==TVar::Calculated) &&
               (p->Var[TMeasurePoint::K]->State==TVar::Calculated))
         p->Var[TMeasurePoint::Tetta_K]->State=TVar::Calculated;
      };
//--- TettaK
   if(p->Var[TMeasurePoint::Used]->Value.b)
      {
      double dd=p->Var[TMeasurePoint::Tetta_K]->Value.d;
      if(dd<0) dd*=-1;
      if(dd > Var[Tetta_K].Value.d)
         {
         Var[Tetta_K].Value.d=dd;
         if(p->Var[TMeasurePoint::Tetta_K]->State!=TVar::Calculated)
            Var[Tetta_K].State=TVar::ErrorArg;
         }
      }

   //--- Calculate TetaS
   if(p->Var[TMeasurePoint::SKO]->Value.d)
      {
      p->Var[TMeasurePoint::TettaS]->Value.d=Var[Tetta_Sum].Value.d/
                                                      p->Var[TMeasurePoint::SKO]->Value.d;
      if( (p->Var[TMeasurePoint::SKO]->State==TVar::Calculated) &&
             (Var[Tetta_Sum].State==TVar::Calculated) )
         p->Var[TMeasurePoint::TettaS]->State=TVar::Calculated;
      else
         p->Var[TMeasurePoint::TettaS]->State=TVar::ErrorArg;
      FPointVarOptions[TMeasurePoint::TettaS].ProcessRound(p->Var[TMeasurePoint::TettaS]);
      }
   else
      {
      p->Var[TMeasurePoint::TettaS]->Value.d=0;
      p->Var[TMeasurePoint::TettaS]->State=TVar::ErrorArg;
      }
//--- Calculate z, d
   if( (0.8 <=p->Var[TMeasurePoint::TettaS]->Value.d ) && (p->Var[TMeasurePoint::TettaS]->Value.d<=8) )
      {
      p->Var[TMeasurePoint::Z]->Value.d=GetZ(p->Var[TMeasurePoint::TettaS]->Value.d);
      FPointVarOptions[TMeasurePoint::Z].ProcessRound(p->Var[TMeasurePoint::Z]);
      p->Var[TMeasurePoint::Z]->State=p->Var[TMeasurePoint::TettaS]->State;

      p->Var[TMeasurePoint::d]->Value.d=p->Var[TMeasurePoint::Z]->Value.d*
                  (Var[Tetta_Sum].Value.d+ p->Var[TMeasurePoint::e]->Value.d);
      if( (p->Var[TMeasurePoint::Z]->State==TVar::Calculated) &&
             (Var[Tetta_Sum].State==TVar::Calculated) &&
             (p->Var[TMeasurePoint::e]->State==TVar::Calculated) )
         p->Var[TMeasurePoint::d]->State=TVar::Calculated;
      else
         p->Var[TMeasurePoint::d]->State=TVar::ErrorArg;
      FPointVarOptions[TMeasurePoint::d].ProcessRound(p->Var[TMeasurePoint::d]);
      }
   else
      {
      p->Var[TMeasurePoint::Z]->Value.d=0;
      p->Var[TMeasurePoint::Z]->State=TVar::NotUsed;

      p->Var[TMeasurePoint::d]->Value.d=Var[Tetta_Sum].Value.d;
      p->Var[TMeasurePoint::d]->State=Var[Tetta_Sum].State;
      FPointVarOptions[TMeasurePoint::d].ProcessRound(p->Var[TMeasurePoint::d]);
      }

}
//---------------------------------------------------------------------------
void __fastcall TMI1974::GetInfo(IStream* Stream, IStorage* Root,TMI_data* data)
{
   //Load IDs
   int ProverID, tprID;
   Stream->Read(&ProverID,sizeof(int),0);
   Stream->Read(&tprID,sizeof(int),0);
   int version;
   Stream->Read(&version,sizeof(version),0);
   Stream->Read(data->Options,sizeof(TMI1974Options),0);
   fGetProver fg;
   TMI1974::CommonData->ProverInterface.Get(Root,ProverID,data->Prover,data->ProverMX);
   //Get TPR
   TMI1974::CommonData->TPRInterface.Get(Root,tprID,data->TPR);
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::SetOptions(TMI1974Options* o)
{
//
   Options.Purpose=o->Purpose;
   Options.Density=o->Density;
   Options.tFD=o->tFD;
   Options.Betta=o->Betta;
   Options.F=o->F;
   Options.v1=o->v1;
   Options.tv1=o->tv1;
   Options.v2=o->v2;
   Options.tv2=o->tv2;
   Options.t_invar=o->t_invar;
   strcpy(Options.ProductName,o->ProductName);
   strcpy(Options.Name,o->Name);
   strcpy(Options.ProtokolN,o->ProtokolN);
   strcpy(Options.Description,o->Description);
   strcpy(Options.Location,o->Location);

   Options.dtTPU=o->dtTPU;
   Options.dtTPR=o->dtTPR;
   Options.dUOI=o->dUOI;
   Options.Date=o->Date;

   bool NeedChange=false;
   bool NeedRes=false;

//Apply changes on FreqInput
   if(Options.FreqInput!=o->FreqInput)
      {
      Options.FreqInput=o->FreqInput;
      if(Options.FreqInput==TMI1974Options::User)
         MeasureVarOptions[TMeasure::f]->Kind=TVarOptions::InputData;
      else
         MeasureVarOptions[TMeasure::f]->Kind=TVarOptions::CalculatedData;
      NeedChange=true;
      }
//Apply changes on UseVisc
   if(Options.UseVisc!=o->UseVisc)
      {
      Options.UseVisc=o->UseVisc;
      if(Options.UseVisc)
         {
         if(MeasureColumns.IndexOf(TMeasure::visc)==-1)
            MeasureColumns.Add(TMeasure::visc);
         MeasureVarOptions[TMeasure::visc]->Kind=TVarOptions::InputData;
         }
      else
         {
         MeasureVarOptions[TMeasure::visc]->Kind=TVarOptions::CalculatedData;
         MeasureColumns.Remove(TMeasure::visc);
         }
      NeedChange=true;
      }

//Apply changes to {Const=0, ConstRange=1, Line=2 } Function;
   if(Options.Function!=o->Function)
      {
      Options.Function=o->Function;
      NeedRes=true;
      // Setup MI columns
      ResColumns.Clear();
      TPack p;
      if(Options.Function)
         {
         p.v.c[0]=0;// MI values;
         p.v.c[1]=Tetta_Sum;
         ResColumns.Add(p.v.i);
         p.v.c[1]=vmin;
         ResColumns.Add(p.v.i);
         p.v.c[1]=vmax;
         ResColumns.Add(p.v.i);
         p.v.c[0]=2;// Range values;
         p.v.c[1]=TRange::Number;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::Q1;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::Q2;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::K;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::Tetta_K;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::Tetta_Sum;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::edk;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::sk;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::TettaS;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::zdk;
         ResColumns.Add(p.v.i);
         p.v.c[1]=TRange::ddk;
         ResColumns.Add(p.v.i);
         }
      else
         {
         p.v.c[0]=0;// MI values;
         for(int i=0;i<VarCount;i++)
            {
            p.v.c[1]=i;
            ResColumns.Add(p.v.i);
            }
         };
      NeedChange=true;
      }



   //Options.GetBetta=o->GetBetta;
   //Options.GetF=o->GetF;
   //Options.ReCalc=o->ReCalc;
   Options.ProductIndex=o->ProductIndex;
   Options.DensReCalc=o->DensReCalc;


   //Apply changes on Betta and Gamma
   if(Options.ProductIndex==0)
      {
      MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::InputData;
      MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::InputData;
      if(MeasureColumns.IndexOf(TMeasure::Betta)==-1)
         MeasureColumns.Add(TMeasure::Betta);
      if(MeasureColumns.IndexOf(TMeasure::F)==-1)
         MeasureColumns.Add(TMeasure::F);
      }
   else
      {
      MeasureVarOptions[TMeasure::Betta]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TMeasure::F]->Kind=TVarOptions::CalculatedData;
      }

   //Apply changes to UseFD
   if(Options.UseFD!=o->UseFD)
      {
      NeedChange=true;
      Options.UseFD=o->UseFD;
      if(Options.UseFD)
         {
         if(MeasureColumns.IndexOf(TMeasure::Dens)==-1)
            MeasureColumns.Add(TMeasure::Dens);
         if(MeasureColumns.IndexOf(TMeasure::t_FD)==-1)
            MeasureColumns.Add(TMeasure::t_FD);
         if(MeasureColumns.IndexOf(TMeasure::P_FD)==-1)
            MeasureColumns.Add(TMeasure::P_FD);
         MeasureVarOptions[TMeasure::Dens]->Kind=TVarOptions::InputData;
         MeasureVarOptions[TMeasure::t_FD]->Kind=TVarOptions::InputData;
         MeasureVarOptions[TMeasure::P_FD]->Kind=TVarOptions::InputData;
         if(MeasureColumns.IndexOf(TMeasure::Betta)==-1)
            MeasureColumns.Add(TMeasure::Betta);
         if(MeasureColumns.IndexOf(TMeasure::F)==-1)
            MeasureColumns.Add(TMeasure::F);
         }
      else
         {
         MeasureColumns.Remove(TMeasure::Dens);
         MeasureColumns.Remove(TMeasure::t_FD);
         MeasureColumns.Remove(TMeasure::P_FD);
         MeasureVarOptions[TMeasure::Dens]->Kind=TVarOptions::CalculatedData;
         MeasureVarOptions[TMeasure::t_FD]->Kind=TVarOptions::CalculatedData;
         MeasureVarOptions[TMeasure::P_FD]->Kind=TVarOptions::CalculatedData;
         if(Options.ProductIndex!=0)
            {
            MeasureColumns.Remove(TMeasure::Betta);
            MeasureColumns.Remove(TMeasure::F);
            }
         }
      }

   //Apply changes on ProverMX
   TProverMX* IDMX1[4];

   IDMX1[0]=0;
   IDMX1[1]=0;
   IDMX1[2]=0;
   IDMX1[3]=0;
   int mx[4];
   mx[0]=0;
   mx[1]=0;
   mx[2]=0;
   mx[3]=0;
   TProverMX* x;
   Options.ProverMXID[0]=o->ProverMXID[0];
   Options.ProverMXID[1]=o->ProverMXID[1];
   Options.ProverMXID[2]=o->ProverMXID[2];
   Options.ProverMXID[3]=o->ProverMXID[3];
   //Make temp IDMX1
   int mxc=ProverMX->Count;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      for(int j=0;j<mxc;j++)
         {
         x=static_cast<TProverMX*>(ProverMX->Items[j]);
         if(x->ID==Options.ProverMXID[i]) break;
         }
      IDMX1[i]=x;
      }
   //Compare with current
   bool Eq=true;
   for(int i=0;i<4;i++)
      if(IDMX1[i]!=IDMX[i])
         {
         Eq=false;
         break;
         }
   if(!Eq)
      {
      NeedChange=true;
      //Make table of replacement
      for(int i=0;i<4;i++)
         {
         if(IDMX[i]==NULL) break;
         for(int j=0;j<4;j++)
            if(IDMX[i]==IDMX1[j]) mx[i]=j;
         }
      IDMX[0]=IDMX1[0];
      IDMX[1]=IDMX1[1];
      IDMX[2]=IDMX1[2];
      IDMX[3]=IDMX1[3];
      //Create string list
      TVarOptions* vo=&FMeasureVarOptions[TMeasure::IDMX];
      vo->Strings->Clear();
      for(int i=0;i<4;i++)
         {
         if(IDMX1[i]==0) break;
         vo->Strings->Add(IDMX[i]->Index);
         }
      int pc=Count;
      for(int i=0;i<pc;i++)
         {
         TMeasurePoint* p = (TMeasurePoint*)GridPoints[i];
         int mc = p->Count;
         for(int j=0;j<mc;j++)
            {
            TMeasure* m = (TMeasure*)(p->Measures[j]);
            m->Var[TMeasure::IDMX]->Value.i=mx[m->Var[TMeasure::IDMX]->Value.i];
            }
         }
      bool exist= (MeasureColumns.IndexOf(TMeasure::IDMX)!=-1);
      if(IDMX[1]==NULL)
         {
         if(exist)
            MeasureColumns.Remove(TMeasure::IDMX);
         }
      else
         {
         if(!exist) MeasureColumns.Add(TMeasure::IDMX);
         }
      }


   if(NeedChange)
      {
      void* f=this;
      if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
      }
   if(NeedRes)
      {
      void* f=this;
      if(FResChange) FResChange((TObject*)f);
      }
   GetTetta();
   Calculate();
}
//---------------------------------------------------------------------------
int __fastcall TMI1974::AddNewMeasure(int PointIndex)
{
   TMeasurePoint* p = (TMeasurePoint*)GridPoints[PointIndex];
   TMeasure* m = new TMeasure();
   TMeasure* mm;
   if(p->Count)
      {
      TVarOptions* vo;
      mm=(TMeasure*)(p->Measures[p->Count-1]);
      for(int i=0;i<TMeasure::VarCount;i++)
         {
         vo=&FMeasureVarOptions[i];
         if( (vo->Kind==TVarOptions::InputData) && (vo->Type!=TVarOptions::Text))
            {
            m->Var[i]->Value=mm->Var[i]->Value;
            m->Var[i]->State=TVar::UsedNearest;
            }
         }
      }
   return p->Add(m);
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::GetTetta()
{
   Var[Tetta_UOI].Value.d=Options.dUOI;
   Var[Tetta_UOI].State=TVar::Calculated;
   Var[Tetta_v0].Value.d=0;
   Var[Tetta_Sum0].Value.d=0;
   Var[Tetta_v0].State=TVar::Calculated;
   Var[Tetta_Sum0].State=TVar::Calculated;

   if(!Options.UseVisc)
      {
      Var[vmin].State=TVar::Calculated;
      Var[vmax].State=TVar::Calculated;
      if(Options.v1<Options.v2)
         {
         Var[vmin].Value.d=Options.v1;
         Var[vmax].Value.d=Options.v2;
         }
      else
         {
         Var[vmin].Value.d=Options.v2;
         Var[vmax].Value.d=Options.v1;
         }
      }

   for(int i=0;i<4;i++)
      {
      if(IDMX[i]==0) break;
      if(IDMX[i]->EVO > Var[Tetta_v0].Value.d)
         Var[Tetta_v0].Value.d=IDMX[i]->EVO;
      if(IDMX[i]->ESR > Var[Tetta_Sum0].Value.d)
         Var[Tetta_Sum0].Value.d=IDMX[i]->ESR;
      }
}
//---------------------------------------------------------------------------
double __fastcall TMI1974::GetZ(double x)
{
   const double n1[]    ={0.5,  0.75, 1,    2,    3,    4,    5,    6,    7,    8};
   const double val[]   ={0.81, 0.77, 0.74, 0.71, 0.73, 0.76, 0.78, 0.79, 0.80, 0.81};
   const int n_count=10;
   if( x >= n1[9] ) return val[9];
   if( x <= n1[0] ) return val[0];

   for (int i=0;i<n_count-1; i++)
      {
      if( (x > n1[i]) &&(x <= n1[i+1]) ) return val[i+1];
      }
   return 0;

}
//---------------------------------------------------------------------------
void __fastcall TMI1974::ClearRanges()
{
   int i=FRanges->Count;
   for (int j=0;j<i;j++)
      {
      TRange* m=(TRange*)FRanges->Items[j];
      delete m;
      }
   FRanges->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculateRanges()
{
   int c = FRanges->Count;
   for(int i=0;i<c;i++)
      {
      CalculateRange(i);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculateRange(int Index)
{
   TRange* r = (TRange*) FRanges->Items[Index];
   TMeasurePoint* p1 = (TMeasurePoint*) GridPoints[r->PointIndex1];
   TMeasurePoint* p2 = (TMeasurePoint*) GridPoints[r->PointIndex2];
//--- Calculate Qmin, Qmax
   r->Var[TRange::Number]->Value.i=Index+1;
   r->Var[TRange::Number]->State=TVar::Calculated;
//--- Calculate Qmin, Qmax
   r->Var[TRange::Q1]->Value.d=p1->Var[TMeasurePoint::Q]->Value.d;
   r->Var[TRange::Q2]->Value.d=p2->Var[TMeasurePoint::Q]->Value.d;
   r->Var[TRange::Q1]->State=p1->Var[TMeasurePoint::Q]->State;
   r->Var[TRange::Q2]->State=p2->Var[TMeasurePoint::Q]->State;
//--- edk, sk
   if(p1->Var[TMeasurePoint::e]->Value.d < p2->Var[TMeasurePoint::e]->Value.d)
      {
      r->Var[TRange::edk]->Value.d=p2->Var[TMeasurePoint::e]->Value.d;
      r->Var[TRange::edk]->State=p2->Var[TMeasurePoint::e]->State;
      r->Var[TRange::sk]->Value.d=p2->Var[TMeasurePoint::SKO]->Value.d;
      r->Var[TRange::sk]->State=p2->Var[TMeasurePoint::SKO]->State;
      }
   else
      {
      r->Var[TRange::edk]->Value.d=p1->Var[TMeasurePoint::e]->Value.d;
      r->Var[TRange::edk]->State=p1->Var[TMeasurePoint::e]->State;
      r->Var[TRange::sk]->Value.d=p1->Var[TMeasurePoint::SKO]->Value.d;
      r->Var[TRange::sk]->State=p1->Var[TMeasurePoint::SKO]->State;
      }
   FRangeVarOptions[TRange::edk].ProcessRound(r->Var[TRange::edk]);
   FRangeVarOptions[TRange::sk].ProcessRound(r->Var[TRange::sk]);
//--- K
   r->Var[TRange::K]->Value.d= (p1->Var[TMeasurePoint::K]->Value.d +
                                 p2->Var[TMeasurePoint::K]->Value.d)/2;
   if( (p1->Var[TMeasurePoint::K]->State==TVar::Calculated) &&
                   (p2->Var[TMeasurePoint::K]->State==TVar::Calculated) )
      r->Var[TRange::K]->State=TVar::Calculated;
   else
      r->Var[TRange::K]->State=TVar::ErrorArg;
   FRangeVarOptions[TRange::K].ProcessRound(r->Var[TRange::K]);
//--- Tetta_K
   if(Options.Function==TMI1974Options::ConstRange)
      {
      r->Var[TRange::Tetta_K]->State=TVar::ErrorArg;
      if(r->Var[TRange::K]->Value.d)
         {
         r->Var[TRange::Tetta_K]->Value.d=100*(p1->Var[TMeasurePoint::K]->Value.d -
                         r->Var[TRange::K]->Value.d)/r->Var[TRange::K]->Value.d;
         if(r->Var[TRange::Tetta_K]->Value.d<0)
            r->Var[TRange::Tetta_K]->Value.d*=-1;
         FRangeVarOptions[TRange::Tetta_K].ProcessRound(r->Var[TRange::Tetta_K]);
         if( (p1->Var[TMeasurePoint::K]->State==TVar::Calculated) &&
                   (r->Var[TRange::K]->State==TVar::Calculated) )
            r->Var[TRange::Tetta_K]->State=TVar::Calculated;
         }
      }
   if(Options.Function==TMI1974Options::Line)
      {
      r->Var[TRange::Tetta_K]->State=TVar::ErrorArg;
      if( (p1->Var[TMeasurePoint::K]->Value.d + p2->Var[TMeasurePoint::K]->Value.d) >0)
         {
         double s=50*(p1->Var[TMeasurePoint::K]->Value.d -
                       p2->Var[TMeasurePoint::K]->Value.d )/
                      (p1->Var[TMeasurePoint::K]->Value.d +
                       p2->Var[TMeasurePoint::K]->Value.d );
         if(s<0) s*=-1;
         r->Var[TRange::Tetta_K]->Value.d=s;
         FRangeVarOptions[TRange::Tetta_K].ProcessRound(r->Var[TRange::Tetta_K]);
         if( (p1->Var[TMeasurePoint::K]->State==TVar::Calculated) &&
                   (p2->Var[TMeasurePoint::K]->State==TVar::Calculated) )
            r->Var[TRange::Tetta_K]->State=TVar::Calculated;
         }
      }
//--- Calculate Tetta_sum, TettaS
   double s=Var[Tetta_v0].Value.d*Var[Tetta_v0].Value.d +
            r->Var[TRange::Tetta_K]->Value.d*r->Var[TRange::Tetta_K]->Value.d +
            Var[Tetta_t].Value.d*Var[Tetta_t].Value.d +
            Var[Tetta_Sum0].Value.d*Var[Tetta_Sum0].Value.d +
            Var[Tetta_UOI].Value.d*Var[Tetta_UOI].Value.d;
   r->Var[TRange::Tetta_Sum]->Value.d=1.1*sqrt(s);
   if( (r->Var[TRange::Tetta_K]->State==TVar::Calculated) &&
          (Var[Tetta_t].State==TVar::Calculated) &&
          (Var[Tetta_v0].State==TVar::Calculated) &&
          (Var[Tetta_Sum0].State==TVar::Calculated) &&
          (Var[Tetta_UOI].State==TVar::Calculated) )
         r->Var[TRange::Tetta_Sum]->State=TVar::Calculated;
      else
         r->Var[TRange::Tetta_Sum]->State=TVar::ErrorArg;
   FRangeVarOptions[TRange::Tetta_Sum].ProcessRound(r->Var[TRange::Tetta_Sum]);

   if(r->Var[TRange::sk]->Value.d)
      {
      r->Var[TRange::TettaS]->Value.d=r->Var[TRange::Tetta_Sum]->Value.d/
                                                      r->Var[TRange::sk]->Value.d;
      if( (r->Var[TRange::sk]->State==TVar::Calculated) &&
             (r->Var[TRange::Tetta_Sum]->State==TVar::Calculated) )
         r->Var[TRange::TettaS]->State=TVar::Calculated;
      else
         r->Var[TRange::TettaS]->State=TVar::ErrorArg;
      FRangeVarOptions[TRange::TettaS].ProcessRound(r->Var[TRange::TettaS]);
      }
   else
      {
      r->Var[TRange::TettaS]->Value.d=0;
      r->Var[TRange::TettaS]->State=TVar::ErrorArg;
      }
//--- Calculate zdk, ddk
   if( (0.8 <=r->Var[TRange::TettaS]->Value.d ) && (r->Var[TRange::TettaS]->Value.d<=8) )
      {
      r->Var[TRange::zdk]->Value.d=GetZ(r->Var[TRange::TettaS]->Value.d);
      FRangeVarOptions[TRange::zdk].ProcessRound(r->Var[TRange::zdk]);
      r->Var[TRange::zdk]->State=r->Var[TRange::TettaS]->State;

      r->Var[TRange::ddk]->Value.d=r->Var[TRange::zdk]->Value.d*
                  (r->Var[TRange::Tetta_Sum]->Value.d+ r->Var[TRange::edk]->Value.d);
      if( (r->Var[TRange::zdk]->State==TVar::Calculated) &&
             (r->Var[TRange::Tetta_Sum]->State==TVar::Calculated) &&
             (r->Var[TRange::edk]->State==TVar::Calculated) )
         r->Var[TRange::ddk]->State=TVar::Calculated;
      else
         r->Var[TRange::ddk]->State=TVar::ErrorArg;
      FRangeVarOptions[TRange::ddk].ProcessRound(r->Var[TRange::ddk]);
      }
   else
      {
      r->Var[TRange::zdk]->Value.d=0;
      r->Var[TRange::zdk]->State=TVar::NotUsed;

      r->Var[TRange::ddk]->Value.d=r->Var[TRange::Tetta_Sum]->Value.d;
      r->Var[TRange::ddk]->State=r->Var[TRange::Tetta_Sum]->State;
      FRangeVarOptions[TRange::ddk].ProcessRound(r->Var[TRange::ddk]);
      }

}
//---------------------------------------------------------------------------
TVarOptions* __fastcall TMI1974::GetRangeVarOptions(int index)
{
   if((index<0)||(index>=TRange::VarCount))
      return NULL;
   else return &FRangeVarOptions[index];
}
//---------------------------------------------------------------------------
/*bool __fastcall TMI1974::Copy(IStream* FromStream, IStream* ToStream)
{
   //Get size of stream
   STATSTG st;
   HRESULT res=FromStream->Stat(&st,STATFLAG_NONAME);
   if(res!=S_OK) return false;
   unsigned int sz=st.cbSize.LowPart;
   char* buf=new char[sz];
   ULONG l;

   FromStream->Read(buf,sz,&l);
   if(sz!=l)
      {
      delete buf;
      return false;
      }
   //Modify Options->Description
   int SeekTo=sizeof(int)*3;
   TMI1974Options* o=(TMI1974Options*)(&buf[SeekTo]);
   AnsiString s=o->Description;
   s+=" ����������� ";
   if(s.Length()>=255)
      s.SetLength(254);
   s+=TDateTime::CurrentDate().DateString();
   memset(o->Description,0,255);
   strcpy(o->Description, s.c_str());
   //Save to stream
   ToStream->Write(buf,sz,&l);
   delete buf;
   if(sz!=l)
      return false;
   else
      return true;

}*/
//---------------------------------------------------------------------------
void __fastcall TMI1974::CalculateBnF(TMeasure* m)
{
   if(Options.ProductIndex==0)
      return;//Used user input

   if(Options.DensReCalc)
      {//
      double dens;
      double d, t, P;
      d=m->Var[TMeasure::Dens]->Value.d;
      t=m->Var[TMeasure::t_FD]->Value.d;
      P=m->Var[TMeasure::P_FD]->Value.d;
      try
         {
//      TFunctionResult r=Options.ReCalc(t, P, d,
//               m->Var[TMeasure::t_TPU]->Value.d, m->Var[TMeasure::P_TPU]->Value.d,
//                dens,m->Var[TMeasure::Betta]->Value.d,m->Var[TMeasure::F]->Value.d);
      TFunctionResult r=TMI1974::CommonData->MITools.ReCalc(Options.ProductIndex-1,t, P, d,
                m->Var[TMeasure::t_TPU]->Value.d, m->Var[TMeasure::P_TPU]->Value.d,
                dens,m->Var[TMeasure::Betta]->Value.d,m->Var[TMeasure::F]->Value.d);


         if(r==frOk)
            {
            m->Var[TMeasure::Betta]->State=TVar::Calculated;
            m->Var[TMeasure::F]->State=TVar::Calculated;
            }
         else
            {
            m->Var[TMeasure::Betta]->State=TVar::ErrorArg;
            m->Var[TMeasure::F]->State=TVar::ErrorArg;
            }
         }
      catch(...)
         {
         m->Var[TMeasure::Betta]->State=TVar::ErrorArg;
         m->Var[TMeasure::F]->State=TVar::ErrorArg;
         m->Var[TMeasure::Betta]->Value.d=0;
         m->Var[TMeasure::F]->Value.d=0;
         }
      MeasureVarOptions[TMeasure::Betta]->ProcessRound(m->Var[TMeasure::Betta]);
      MeasureVarOptions[TMeasure::F]->ProcessRound(m->Var[TMeasure::Betta]);
      }
   else
      {//Options.DesnReCalc
      if(Options.UseFD)
         {
         if(Options.ProductIndex)
            {//not use ������ ����
            try
               {
               TFunctionResult r=TMI1974::CommonData->MITools.GetBetta(Options.ProductIndex-1,
                              m->Var[TMeasure::t_FD]->Value.d,
                              0,
                              m->Var[TMeasure::Dens]->Value.d,
                              m->Var[TMeasure::Betta]->Value.d);
               MeasureVarOptions[TMeasure::Betta]->ProcessRound(m->Var[TMeasure::Betta]);
               if(r==frOk)
                  m->Var[TMeasure::Betta]->State=TVar::Calculated;
               else
                  m->Var[TMeasure::Betta]->State=TVar::ErrorArg;
               }
            catch(...)
               {
               m->Var[TMeasure::Betta]->State=TVar::ErrorArg;
               m->Var[TMeasure::Betta]->Value.d=0;
               }
            }//endif not use ������ ����
         }
      else
         {//Options.UseFD
         m->Var[TMeasure::Betta]->Value.d=Options.Betta;
         m->Var[TMeasure::Betta]->State=TVar::Calculated;
         MeasureVarOptions[TMeasure::Betta]->ProcessRound(m->Var[TMeasure::Betta]);
         }
   //---------------
   //---���������� F
   //---------------
      if(Options.UseFD)
         {
         if(Options.ProductIndex)
            {//������������ �� ������ ����
            try
               {
               TFunctionResult r=TMI1974::CommonData->MITools.GetF(Options.ProductIndex-1,
                                 m->Var[TMeasure::t_FD]->Value.d,
                                 0,
                                 m->Var[TMeasure::Dens]->Value.d,
                                 m->Var[TMeasure::F]->Value.d);
               if(r==frOk)
                  m->Var[TMeasure::F]->State=TVar::Calculated;
               else
                  m->Var[TMeasure::F]->State=TVar::ErrorArg;
               }
            catch(...)
               {
               m->Var[TMeasure::F]->State=TVar::ErrorArg;
               m->Var[TMeasure::F]->Value.d=0;
               }
            }//endif  ������������ �� ������ ����
         MeasureVarOptions[TMeasure::F]->ProcessRound(m->Var[TMeasure::F]);
         }
      else
         {//Options.UseFD
         m->Var[TMeasure::F]->Value.d=Options.F;
         m->Var[TMeasure::F]->State=TVar::Calculated;
         MeasureVarOptions[TMeasure::F]->ProcessRound(m->Var[TMeasure::F]);
         }
      }
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TMI1974::LoadVarOptions()
{
//Load measure var options
/*
a) � �������+
1.	Name;
2.	Description;
3.	Unit
1 - Measure;
201 - Point
401 - MI
601 - Range.
b) RC-data
1.	Type;
2.	Kind;
3.	Width;
4.	Bitmap;
5.	UnitBitmap
701 - Measure;
702 - Point
703 - MI
704 - Range.
*/
/*
   CalcOptions.CalcRoundType = ini->ReadInteger(s,"CalcRoundType",0);
   CalcOptions.ShowRoundType = ini->ReadInteger(s,"ShowRoundType",0);
   CalcOptions.ShowRoundNumber = ini->ReadInteger(s,"ShowRoundNumber",0);
   CalcOptions.CalcRoundNumber = ini->ReadInteger(s,"CalcRoundNumber",0);
��������� �1		�2
801 - Measure;		811
802 - Point		812
803 - MI		813
804 - Range		814

*/
   TResourceStream* rs;
   int base;
   base=1;
   short int buf[512];
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,701,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   short int buf1[512];
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,801,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<TMeasure::VarCount;i++)
      {
      FMeasureVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FMeasureVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FMeasureVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);
#pragma warn -8018
      FMeasureVarOptions[i].Type=buf[i*5];
      FMeasureVarOptions[i].Kind=buf[i*5+1];
      FMeasureVarOptions[i].Width=buf[i*5+2];
      FMeasureVarOptions[i].Bitmap=buf[i*5+3];
      FMeasureVarOptions[i].UnitBitmap=buf[i*5+4];

      FMeasureVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FMeasureVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FMeasureVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FMeasureVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
//Read point
   base=201;
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,702,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,802,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<TMeasurePoint::VarCount;i++)
      {
      FPointVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FPointVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FPointVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);

      FPointVarOptions[i].Type=buf[i*5];
      FPointVarOptions[i].Kind=buf[i*5+1];
      FPointVarOptions[i].Width=buf[i*5+2];
      FPointVarOptions[i].Bitmap=buf[i*5+3];
      FPointVarOptions[i].UnitBitmap=buf[i*5+4];

      FPointVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FPointVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FPointVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FPointVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
//MI Var
   base=401;
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,703,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,803,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<VarCount;i++)
      {
      FMIVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FMIVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FMIVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);

      FMIVarOptions[i].Type=buf[i*5];
      FMIVarOptions[i].Kind=buf[i*5+1];
      FMIVarOptions[i].Width=buf[i*5+2];
      FMIVarOptions[i].Bitmap=buf[i*5+3];
      FMIVarOptions[i].UnitBitmap=buf[i*5+4];

      FMIVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FMIVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FMIVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FMIVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
//Range var options
   base=601;
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,704,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,804,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<TRange::VarCount;i++)
      {
      FRangeVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FRangeVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FRangeVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);

      FRangeVarOptions[i].Type=buf[i*5];
      FRangeVarOptions[i].Kind=buf[i*5+1];
      FRangeVarOptions[i].Width=buf[i*5+2];
      FRangeVarOptions[i].Bitmap=buf[i*5+3];
      FRangeVarOptions[i].UnitBitmap=buf[i*5+4];

      FRangeVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FRangeVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FRangeVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FRangeVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
#pragma warn +8018
}
//---------------------------------------------------------------------------

