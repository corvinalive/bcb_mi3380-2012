  //---------------------------------------------------------
  // PCH.H: Common header file
  #ifndef PCH_H
  #define PCH_H
  // include every VCL header that we use
  // could include vcl.h instead
#include <System.hpp>    //const present
#include <Windows.hpp>   //const present
#include <SysUtils.hpp>  //const present
#include <ole2.h>


#include <Clipbrd.hpp>   //+
#include <Menus.hpp> //-
#include <ImgList.hpp> //+
#include <Classes.hpp>   -
#include <Controls.hpp> -



#include <StdCtrls.hpp>  //+
#include <Forms.hpp>     //+
#include <ExtCtrls.hpp> -
#include <CSPIN.h>
#include <Buttons.hpp>     //++
#include <Grids.hpp>  -
#include <ComCtrls.hpp>  -
#include <CheckLst.hpp> //+
#include <Graphics.hpp>  //-
//#include <CGAUGES.h>
#include <IniFiles.hpp> //+
//#include <FMTBcd.hpp>

#include <OleServer.hpp>
#include <Excel_2K_srvr.h>
#include <Word_2K_SRVR.h>


  // include the C RTL headers that we use
#include <math.h>
//#include <math.hpp>


  // include headers for the 3rd party controls

  // Our custom controls
//#include <mitools.h>
//#include "MIGrid.h"
//#include "NumberEdit.h"
  // Object Repository header files

  // project include files
  // pre-compile these only if PRECOMPILE_ALL is defin
  #endif

