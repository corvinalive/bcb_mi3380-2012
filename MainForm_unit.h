//---------------------------------------------------------------------------

#ifndef MainForm_unitH
#define MainForm_unitH
#include <Menus.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>
#include <Buttons.hpp>

#include <Dialogs.hpp>
#include <ActnList.hpp>
#include <ActnMan.hpp>
//#include <OleCtrls.hpp>
#include "MI1974_unit.h"
#include "KMX_unit.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TImageList *ImageList1;
   TBitBtn *BitBtn1;
   TLabel *Label1;
   TLabel *Label2;
   TOpenDialog *OD;
   TMainMenu *MainMenu1;
   TMenuItem *FileMenuItem;
   TMenuItem *NewMenuItem;
   TMenuItem *N10;
   TMenuItem *N11;
   TMenuItem *AboutMenuItem;
   TMenuItem *OpenMenuItem;
   TMenuItem *N2;
   TMenuItem *N3;
   TMenuItem *N4;
   TMenuItem *ExitMenuItem;
   TMenuItem *ManageMI2463MenuItem;
   TMenuItem *N6;
   TMenuItem *ManageMMMenuItem;
   TMenuItem *ManageProverMenuItem;
   TMenuItem *OptionsMenuItem;
   TMenuItem *CalculateOptionsMenuItem;
   TMenuItem *ViewVariablesMenuItem;
   TMenuItem *N1;
   TMenuItem *MakeProtokolMenuItem;
   TMenuItem *CheckMenuItem;
   TMenuItem *ViewTPRMenuItem;
   TMenuItem *ViewProverMenuItem;
   TMenuItem *N5;
   TMenuItem *CalculatefMenuItem;
   TMenuItem *NewKMXMenuItem;
   TMenuItem *N8;
   TMenuItem *OpenKMXMenuItem;
   TMenuItem *ManageKMXMenuItem;
   void __fastcall NewMenuItemClick(TObject *Sender);
   void __fastcall OpenMenuItemClick(TObject *Sender);
   void __fastcall OptionsMenuItemClick(TObject *Sender);
   void __fastcall ViewVariablesMenuItemClick(TObject *Sender);
   void __fastcall CalculateOptionsMenuItemClick(TObject *Sender);
   void __fastcall ExitMenuItemClick(TObject *Sender);
   void __fastcall ViewProverMenuItemClick(TObject *Sender);
   void __fastcall ManageProverMenuItemClick(TObject *Sender);
   void __fastcall ManageMI2463MenuItemClick(TObject *Sender);
   void __fastcall AboutMenuItemClick(TObject *Sender);
   void __fastcall ViewTPRMenuItemClick(TObject *Sender);
   void __fastcall ManageMMMenuItemClick(TObject *Sender);
   void __fastcall BitBtn1Click(TObject *Sender);
   void __fastcall BitBtn2Click(TObject *Sender);
   void __fastcall CalculatefMenuItemClick(TObject *Sender);
        void __fastcall MakeProtokolMenuItemClick(TObject *Sender);
        void __fastcall CheckMenuItemClick(TObject *Sender);
   void __fastcall hhh1Click(TObject *Sender);
   void __fastcall NewKMXMenuItemClick(TObject *Sender);
   void __fastcall OpenKMXMenuItemClick(TObject *Sender);
   void __fastcall ManageKMXMenuItemClick(TObject *Sender);

private:	// User declarations
   static const BitmapCount=80;
   Graphics::TBitmap* FBitmaps[BitmapCount+1];
   void __fastcall OpenFile(AnsiString FN);
   void __fastcall MakeProtokolMI(TMI1974* MI);
   void __fastcall MakeProtokolKMX(TKMX* MI);
public:		// User declarations
   IStorage* Root;
   IStorage* MIRoot;
   IStorage* KMXRoot;
   __fastcall TMainForm(TComponent* Owner);
   __fastcall ~TMainForm();
   void __fastcall FillListBox();
   Graphics::TBitmap* __fastcall GetBitmaps(int index);
   void __fastcall MIFormFocusChange(bool HasFocus);
   void __fastcall SetTextExcel(TExcelWorksheet* ES, WideString ws, int Col, int Row);
   void __fastcall ShortCut(Messages::TWMKey &Msg, bool &Handled);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
