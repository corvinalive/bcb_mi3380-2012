//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop

#include "MI1974_unit.h"
#include "KMX_unit.h"
#include "SetupViewVariablesForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSetupViewVariablesForm *SetupViewVariablesForm;
//---------------------------------------------------------------------------
__fastcall TSetupViewVariablesForm::TSetupViewVariablesForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
bool __fastcall TSetupViewVariablesForm::ShowDialog(TMI1974* MI1974)
{
   Caption="��������� ������������ ���������� �������";
   KMX=0;
	MI = MI1974;
	if(!MI) return false;
	ms.Clear();
	md.Clear();

	ps.Clear();
	pd.Clear();

	int c = TMeasure::VarCount;
	for(int i=0;i<c;i++)
		ms.Add(i);
	md.Assign(&MI->MeasureColumns);
	ms.Exclude(&md);
	MeasureSourceListBox->Count=ms.Count;
	MeasureDestinationListBox->Count=md.Count;
	MeasureSourceListBox->ItemIndex=0;
	MeasureDestinationListBox->ItemIndex=md.Count-1;

	c = TMeasurePoint::VarCount;
	for(int i=0;i<c;i++)
		ps.Add(i);
	c = MI->PointColumns.Count;
	for(int i=0;i<c;i++)
		{
		int val = MI->PointColumns.Values[i];
		pd.Add(val);
		int cc=ps.Count;
		for(int j=0;j<cc;j++)
			if(ps.Values[j]==val)
			  {
			  ps.Delete(j);
			  break;
			  }
		}
	PointSourceListBox->Count=ps.Count;
	PointDestinationListBox->Count=pd.Count;
	PointSourceListBox->ItemIndex=0;
	PointDestinationListBox->ItemIndex=pd.Count-1;
	bool res = ShowModal()==mrOk;
	if(res)
		{//Save changes
		MI->MeasureColumns.Clear();
		c=md.Count;
		for(int i=0;i<c;i++)
			MI->MeasureColumns.Add(md.Values[i]);

		MI->PointColumns.Clear();
		c=pd.Count;
		for(int i=0;i<c;i++)
			MI->PointColumns.Add(pd.Values[i]);

		if(MI->OnChangeItemsStructure) MI->OnChangeItemsStructure(this);
		}
	return res;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::MeasureSourceListBoxDrawItem(
		TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   TVarOptions* vo;
   if(MI!=NULL)
      vo = MI->MeasureVarOptions[0];
   else
      vo = KMX->MeasureVarOptions[0];
	ListBoxDrawItem(Control,Index,Rect, State, vo,&ms);
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::ListBoxDrawItem(
		TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State, TVarOptions* vo, TIntList *l)
{
	TListBox* ListBox = (TListBox*) Control;
	if(State.Contains(odSelected))
		ListBox->Canvas->Brush->Color=clSkyBlue;
	else
		ListBox->Canvas->Brush->Color=clWhite;
	ListBox->Canvas->FillRect(Rect);

	int vi=l->Values[Index];
	int h;
	if(vo[vi].Bitmap==-1)
		{
		//Draw text
		ListBox->Canvas->Font->Color=clBlack;
		h=ListBox->Canvas->TextHeight("A");
		ListBox->Canvas->TextOut(Rect.left+2,Rect.top+(Rect.Height()-h)/2,vo[vi].Name);
		}
	else
		{
		Graphics::TBitmap* b;
      if(MI!=NULL)
         b=MI->OnGetBitmap(vo[vi].Bitmap);
      else
         b=KMX->OnGetBitmap(vo[vi].Bitmap);
   	ListBox->Canvas->CopyMode=cmSrcAnd;
		ListBox->Canvas->Draw(Rect.left+2,Rect.top+(Rect.Height()-b->Height)/2,b);
		}
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::MeasureDestinationListBoxDrawItem(
		TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   TVarOptions* vo;
   if(MI!=NULL)
      vo = MI->MeasureVarOptions[0];
   else
      vo = KMX->MeasureVarOptions[0];
	ListBoxDrawItem(Control,Index,Rect, State, vo,&md);
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mDestMoveUpBtnClick(
		TObject *Sender)
{
	int i = MeasureDestinationListBox->ItemIndex;
	if((i==-1) || (i==0)) return;
	md.List->Exchange(i,i-1);
	MeasureDestinationListBox->ItemIndex=i-1;
	MeasureDestinationListBox->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mDestMoveDownBtnClick(
		TObject *Sender)
{
	int i = MeasureDestinationListBox->ItemIndex;
	int c = MeasureDestinationListBox->Count;
	if((i==-1) || (i==(c-1))) return;
	md.List->Exchange(i,i+1);
	MeasureDestinationListBox->ItemIndex=i+1;
	MeasureDestinationListBox->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mMoveRightBtnClick(TObject *Sender)
{
	int s =MeasureSourceListBox->ItemIndex;
	if( s==-1 ) return;
	int d =MeasureDestinationListBox->ItemIndex;
	if( d==-1)
		d=MeasureDestinationListBox->Count-1;
	md.Insert(d+1,ms.Values[s]);
	ms.Delete(s);
	MeasureSourceListBox->Count=ms.Count;
	MeasureDestinationListBox->Count=md.Count;
	MeasureDestinationListBox->ItemIndex=d+1;
	if(s==ms.Count) s--;
	if(s<0) s=0;
	if(ms.Count) MeasureSourceListBox->ItemIndex=s;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mMoveRightAllBtnClick(TObject *Sender)
{
	int d =MeasureDestinationListBox->ItemIndex;
	if(d==-1) d=MeasureDestinationListBox->Count-1;
	int c= ms.Count;
	if(!c) return;
	for(int i=0;i<c;i++)
		md.Insert(d+i+1,ms.Values[i]);
	ms.Clear();
	MeasureSourceListBox->Count=ms.Count;
	MeasureDestinationListBox->Count=md.Count;
	MeasureDestinationListBox->ItemIndex=d+c-1;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mMoveLeftBtnClick(TObject *Sender)
{
	int d =MeasureDestinationListBox->ItemIndex;
	if(d==-1) return;
	int s =MeasureSourceListBox->ItemIndex;
	if( s==-1)
		s=MeasureSourceListBox->Count-1;
	ms.Insert(s+1,md.Values[d]);
	md.Delete(d);
	MeasureDestinationListBox->Count=md.Count;
	MeasureSourceListBox->Count=ms.Count;
	MeasureSourceListBox->ItemIndex=s+1;
	if(d==md.Count) d--;
	if(d<0) d=0;
	if(md.Count) MeasureDestinationListBox->ItemIndex=d;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::mMoveLeftAllBtnClick(
		TObject *Sender)
{
	int s =MeasureSourceListBox->ItemIndex;
	if(s==-1) s=MeasureSourceListBox->Count-1;
	int c= md.Count;
	if(!c) return;
	for(int i=0;i<c;i++)
		ms.Insert(s+i+1,md.Values[i]);
	md.Clear();
	MeasureDestinationListBox->Count=md.Count;
	MeasureSourceListBox->Count=ms.Count;
	MeasureSourceListBox->ItemIndex=s+c-1;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::PointSourceListBoxDrawItem(
		TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   TVarOptions* vo;
   if(MI!=NULL)
      vo = MI->PointVarOptions[0];
   else
      vo = KMX->PointVarOptions[0];
	ListBoxDrawItem(Control,Index,Rect, State, vo,&ps);
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::PointDestinationListBoxDrawItem(
		TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   TVarOptions* vo;
   if(MI!=NULL)
      vo = MI->PointVarOptions[0];
   else
      vo = KMX->PointVarOptions[0];
	ListBoxDrawItem(Control,Index,Rect, State, vo,&pd);
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pMoveRightBtnClick(TObject *Sender)
{
	int s =PointSourceListBox->ItemIndex;
	if( s==-1 ) return;
	int d =PointDestinationListBox->ItemIndex;
	if( d==-1)
		d=PointDestinationListBox->Count-1;
	pd.Insert(d+1,ps.Values[s]);
	ps.Delete(s);
	PointSourceListBox->Count=ps.Count;
	PointDestinationListBox->Count=pd.Count;
	PointDestinationListBox->ItemIndex=d+1;
	if(s==ps.Count) s--;
	if(s<0) s=0;
	if(ps.Count) PointSourceListBox->ItemIndex=s;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pMoveRightAllBtnClick(
		TObject *Sender)
{
	int d =PointDestinationListBox->ItemIndex;
	if(d==-1) d=PointDestinationListBox->Count-1;
	int c= ps.Count;
	if(!c) return;
	for(int i=0;i<c;i++)
		pd.Insert(d+i+1,ps.Values[i]);
	ps.Clear();
	PointSourceListBox->Count=ps.Count;
	PointDestinationListBox->Count=pd.Count;
	PointDestinationListBox->ItemIndex=d+c-1;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pMoveLeftBtnClick(TObject *Sender)
{
	int d =PointDestinationListBox->ItemIndex;
	if(d==-1) return;
	int s =PointSourceListBox->ItemIndex;
	if( s==-1)
		s=PointSourceListBox->Count-1;
	ps.Insert(s+1,pd.Values[d]);
	pd.Delete(d);
	PointDestinationListBox->Count=pd.Count;
	PointSourceListBox->Count=ps.Count;
	PointSourceListBox->ItemIndex=s+1;
	if(d==pd.Count) d--;
	if(d<0) d=0;
	if(pd.Count) PointDestinationListBox->ItemIndex=d;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pMoveLeftAllBtnClick(TObject *Sender)
{
	int s =PointSourceListBox->ItemIndex;
	if(s==-1) s=PointSourceListBox->Count-1;
	int c= pd.Count;
	if(!c) return;
	for(int i=0;i<c;i++)
		ps.Insert(s+i+1,pd.Values[i]);
	pd.Clear();
	PointDestinationListBox->Count=pd.Count;
	PointSourceListBox->Count=ps.Count;
	PointSourceListBox->ItemIndex=s+c-1;
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pDestMoveUpBtnClick(
		TObject *Sender)
{
	int i = PointDestinationListBox->ItemIndex;
	if((i==-1) || (i==0)) return;
	pd.List->Exchange(i,i-1);
	PointDestinationListBox->ItemIndex=i-1;
	PointDestinationListBox->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TSetupViewVariablesForm::pDestMoveDownBtnClick(
		TObject *Sender)
{
	int i = PointDestinationListBox->ItemIndex;
	int c = PointDestinationListBox->Count;
	if((i==-1) || (i==(c-1))) return;
	pd.List->Exchange(i,i+1);
	PointDestinationListBox->ItemIndex=i+1;
	PointDestinationListBox->Repaint();
}
//---------------------------------------------------------------------------
bool __fastcall TSetupViewVariablesForm::ShowKMXDialog(TKMX* _KMX)
{
   Caption="��������� ������������ ���������� ���";
   MI=0;
   KMX = _KMX;
	if(!KMX) return false;
	ms.Clear();
	md.Clear();

	ps.Clear();
	pd.Clear();

	int c = TKMXMeasure::VarCount;
	for(int i=0;i<c;i++)
		ms.Add(i);
	md.Assign(&KMX->MeasureColumns);
	ms.Exclude(&md);
	MeasureSourceListBox->Count=ms.Count;
	MeasureDestinationListBox->Count=md.Count;
	MeasureSourceListBox->ItemIndex=0;
	MeasureDestinationListBox->ItemIndex=md.Count-1;

	c = TKMXMeasurePoint::VarCount;
	for(int i=0;i<c;i++)
		ps.Add(i);
	c = KMX->PointColumns.Count;
	for(int i=0;i<c;i++)
		{
		int val = KMX->PointColumns.Values[i];
		pd.Add(val);
		int cc=ps.Count;
		for(int j=0;j<cc;j++)
			if(ps.Values[j]==val)
			  {
			  ps.Delete(j);
			  break;
			  }
		}
	PointSourceListBox->Count=ps.Count;
	PointDestinationListBox->Count=pd.Count;
	PointSourceListBox->ItemIndex=0;
	PointDestinationListBox->ItemIndex=pd.Count-1;
	bool res = ShowModal()==mrOk;
	if(res)
		{//Save changes
		KMX->MeasureColumns.Clear();
		c=md.Count;
		for(int i=0;i<c;i++)
			KMX->MeasureColumns.Add(md.Values[i]);

		KMX->PointColumns.Clear();
		c=pd.Count;
		for(int i=0;i<c;i++)
			KMX->PointColumns.Add(pd.Values[i]);

		if(KMX->OnChangeItemsStructure) KMX->OnChangeItemsStructure(this);
		}
	return res;
}

