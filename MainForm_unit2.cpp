//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop

#include "prover.h"
#include "tpr.h"
#include "MainForm_unit.h"
#include "MI1974_unit.h"
#include "KMX_unit.h"
#include "MIForm_unit.h"
#include "KMXForm_unit.h"
#include "ProtokolProgressForm_unit.h"
#include "Measure_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//---------------------------------------------------------------------------
void __fastcall TMainForm::MakeProtokolMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   if(ActiveMDIChild->Tag==100)
      {
      TKMXForm* f = (TKMXForm*)ActiveMDIChild;
      if(f)
         {
         MakeProtokolKMX(f->MI);
         }
      }
   else
      {
      TMIForm* f = (TMIForm*)ActiveMDIChild;
      if(f)
         {
         MakeProtokolMI(f->MI);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::MakeProtokolMI(TMI1974* MI)
{
   ProtokolProgressForm->Label1->Caption="���������� � ������������ ���������...";
   ProtokolProgressForm->Gauge->Progress=0;
   ProtokolProgressForm->Show();
   ProtokolProgressForm->Update();
   TWordDocument *WD;
   TWordApplication *WA;

   try
      {
      WD= new TWordDocument(this);
      WA = new TWordApplication(this);
      WA->ConnectKind=ckNewInstance;
      WD->ConnectKind=ckNewInstance;
      }
   catch (...)
      {
      ProtokolProgressForm->Hide();
      MessageDlg("�� ���� ����������� � �������� MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
      delete WA;
      delete WD;
      return;
      }


   OleVariant Template = EmptyParam;
   OleVariant o = 0;
   OleVariant oo = 0;
   OleVariant NewTemplate = False;
   OleVariant ItemIndex = 1;
   OleVariant ItemsCount;
   Word_2k::Table *Word_Table;
   Word_2k::Range* rr;
   AnsiString s;
 //  int sizex,sizey;
   TVarOptions* vo;
   TVar* v;
   Graphics::TBitmap* b;

//���������� ���-�� ����� � ���������:
   int LineCount=13;

   //Calculate number of rows table #2

   int rc=3;
   int pc=MI->Var[TMI1974::UsedPoints].Value.i;
   TMeasurePoint* p;
   TMeasure* m;
   for(int i=0;i<pc;i++)
      {
      p=(TMeasurePoint*)MI->UsedPoint[i];
      if(!p) continue;
      int ccc=p->Count;
      for(int j=0;j<ccc;j++)
         {
         m=(TMeasure*)(p->Measures[j]);
         if(m->Var[TMeasure::Used]->Value.b) rc++;
         }
      }

   LineCount+=rc;  //table 2;
   LineCount+=3+pc;
   LineCount+=3; //����������, ���� �������, ����������

   if(MI->Options.Purpose!=TMI1974Options::Control)
      {
      LineCount+=3;
      if(MI->Options.Function==TMI1974Options::Const)
         LineCount++;
      else
         LineCount+=pc-1;//Number of ranges
      }
   double Step=97/LineCount;
   double Position=3;

   try
      {
      WA->Connect();
      }
   catch (...)
      {
      ProtokolProgressForm->Hide();
      MessageDlg("�� ���� ����������� � �������� MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
      return;
      }

try
{
   WA->Documents->Add(Template,NewTemplate);
      //Assign WordDocument component
   WD->ConnectTo(WA->Documents->Item(ItemIndex));

    WD->PageSetup->LeftMargin=WA->CentimetersToPoints(1.5);
    WD->PageSetup->RightMargin=WA->CentimetersToPoints(1);
  // WA->GetDefaultInterface()->Visible = true;

   o=0;
   oo=WD->Characters->Count-1;

   ProtokolProgressForm->Label1->Caption="������������ ��������� ���������...";
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();


   WideString ws("�������� �");
   ws+=MI->Options.ProtokolN;
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphCenter;
   WD->Range(o,oo)->Font->Size = 14;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   o=oo;
   WD->Range(o,oo)->InsertAfter(WideString("\n������� ��������������� ������� � ������� ���������� ��������� �� �� 1974-2004\n\n"));
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 12;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Label1->Caption="������������ ������ ������� ���������...";
   ProtokolProgressForm->Update();

   o=oo;
   ws="����� ���������� �������: ";
   ws+=MI->Options.Location;
   ws+="\t��� (���)\n";
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   WD->Range(o,oo)->Font->Size = 10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();


   o=oo;
   ws="��: ��� ";
   ws+=MI->tpr.Model;
   ws+="-";
   s=MI->tpr.D;
   ws+=s;
   ws+=" \t���. �";
   ws+=MI->tpr.SN;
   ws+="\t����� �" ;
   //Remove �� �, �����, ����� �, ��� �:
   s=MI->tpr.Location;
   if(!s.IsEmpty())
      {
      AnsiString Lines[6];
      Lines[0]="����� �";
      Lines[1]="�� �";
      Lines[2]="��� �";
      Lines[3]="�����";
      Lines[4]="���";
      Lines[5]="��";
      for(int i=0;i<6;i++)
         {
         int pos=s.Pos(Lines[i]);
         if(pos)
            {
            s.Delete(pos,Lines[i].Length());
            s=s.TrimLeft();
            break;
            }
         }
      }
   ws+=s;
   ws+=" \t����������� ";
   ws+=MI->tpr.Owner;
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   WD->Range(o,oo)->Font->Size = 10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   o=oo;
   ws="\n��: ��� ";
   ws+=MI->Prover.Name;
   ws+="\t������ ";
   s=MI->Prover.Category;
   ws+=s;
   ws+="\t���. �";
   ws+=MI->Prover.SN;
   ws+="\t����������� ";
   ws+=MI->Prover.Owner;
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   o=oo;
   ws="\n������� �������� ";
   ws+=MI->Options.ProductName;
   ws+="\t�������� ��� �������: ��� ";
   vo=MI->MIVarOptions[TMI1974::vmin];
   v=&MI->Var[TMI1974::vmin];
   vo->FormatStr(v,s);
   ws+=s;
   ws+=" ���, ���� ";
   vo=MI->MIVarOptions[TMI1974::vmax];
   v=&MI->Var[TMI1974::vmax];
   vo->FormatStr(v,s);
   ws+=s;
   ws+=" ���";
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   o=oo;
   ws="\n���������� ���� � ����� (��������� ������ ��� ����� ����� ����� �����)___ % (� �������� �����)";
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   o=oo;
   ws="\n\n������� 1 - �������� ������\n";
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 10;
   ProtokolProgressForm->Label1->Caption="������������ ������� 1 '�������� ������'...";
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

//������� �������� ������
   int n=14;
   int j=4;
   if(MI->IDMX[1]!=0)
      {
      j++;
      if(MI->IDMX[2]!=0)
         {
         j++;
         if(MI->IDMX[3]!=0) j++;
         }
      }
   int vc=j-3;
   oo=WD->Characters->Count-1;
   o=oo;
   Word_Table=WD->Tables->Add(WD->Range(o,o),j,n);
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
   Word_Table->Range->Font->Size=10;

   Word_Table->Cell(1,1)->Range->InsertAfter(WideString("���������� ��������� (��)"));
   Word_Table->Cell(1,11)->Range->InsertAfter(WideString("���"));
   Word_Table->Cell(1,12)->Range->InsertAfter(WideString("��"));
   Word_Table->Cell(1,13)->Range->InsertAfter(WideString("��������"));
   Word_Table->Cell(1,1)->Merge(Word_Table->Cell(1,10));
   Word_Table->Cell(1,4)->Merge(Word_Table->Cell(1,5));

   Word_Table->Cell(2,1)->Range->InsertAfter(WideString("���."));

   Word_Table->Cell(2,2)->Range->InsertAfter(WideString("V0,\n�3"));
   o=1;
   oo=2;
   rr=Word_Table->Cell(2,2)->Range;
   o=wdCharacter;
   oo=1;
   rr->MoveStart(o,oo);
   oo=-3;
   rr->MoveEnd(o,oo);
   rr->Font->Subscript=true;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   Word_Table->Cell(2,3)->Range->InsertAfter(WideString("D,\n��"));
   Word_Table->Cell(2,4)->Range->InsertAfter(WideString("S,\n��"));
   Word_Table->Cell(2,5)->Range->InsertAfter(WideString("E,\n���"));

   if(MI->Prover.Type==TProver::Compact)
      {
      Word_Table->Cell(2,6)->Range->InsertAfter(WideString("K1,\nC-1"));
      oo=-8;
      }
   else
      {
      Word_Table->Cell(2,6)->Range->InsertAfter(WideString("t,\nC-1"));
      oo=-7;
      }
   rr=Word_Table->Cell(2,6)->Range;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   long ll=0x03B1;//alfa
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   if(MI->Prover.Type==TProver::Compact)
      oo=3;
   else
      oo=2;
   o=wdCharacter;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=5;
   rr->MoveEnd(o,oo);
   if(MI->Prover.Type==TProver::Compact)
      oo=5;
   else
      oo=4;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   oo=-1;
   rr->MoveStart(o,oo);
   oo=-3;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   //Tetta_Sum0
   Word_Table->Cell(2,7)->Range->InsertAfter(WideString("0,\n%"));
   rr=Word_Table->Cell(2,7)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03A3;
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   ll=0x0398;
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   //Tetta_V0
   Word_Table->Cell(2,8)->Range->InsertAfter(WideString("V0,\n%"));
   rr=Word_Table->Cell(2,8)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x0398;
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   //dtTPU
   Word_Table->Cell(2,9)->Range->InsertAfter(WideString("t��,\nC"));
   rr=Word_Table->Cell(2,9)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x0394;//Delta
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=2;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //t invar
   Word_Table->Cell(2,10)->Range->InsertAfter(WideString("t��,\nC"));
   rr=Word_Table->Cell(2,10)->Range;
   o=wdCharacter;
   oo=1;
   rr->MoveStart(o,oo);
   oo=-4;
   rr->MoveEnd(o,oo);
   rr->Font->Superscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //d SOI
   Word_Table->Cell(2,11)->Range->InsertAfter(WideString("����,\n%"));
   rr=Word_Table->Cell(2,11)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B4;//d
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=1;
   rr->MoveStart(o,oo);
   oo=2;
   rr->MoveEnd(o,oo);
   rr->Font->Superscript=true;
   oo=1;
   rr->MoveStart(o,oo);
   oo=2;
   rr->MoveEnd(o,oo);
   rr->Font->Subscript=true;

   //dtTPU
   Word_Table->Cell(2,12)->Range->InsertAfter(WideString("t��,\nC"));
   rr=Word_Table->Cell(2,12)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x0394;//Delta
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=2;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //Density
   Word_Table->Cell(2,13)->Range->InsertAfter(WideString(",\n��/�3"));
   rr=Word_Table->Cell(2,13)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03C1;//Ro
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=8;
   rr->MoveEnd(o,oo);
   oo=7;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //t FD
   Word_Table->Cell(2,14)->Range->InsertAfter(WideString("t,\nC"));
   rr=Word_Table->Cell(2,14)->Range;
   o=wdCharacter;
   oo=1;
   rr->MoveStart(o,oo);
   oo=-4;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03C1;//Ro
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=1;
   rr->MoveEnd(o,oo);
   rr->Font->Subscript=true;
   o=wdCharacter;
   oo=1;
   rr->MoveEnd(o,oo);
   oo=3;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   //Insert numbers
   for(int ii=0;ii<14;ii++)
      {
      ws=ii+1;
      Word_Table->Cell(3,ii+1)->Range->InsertAfter(ws);
      }
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   //Insert values
  // TVarOptions vo1;
   for(int i=0;i<vc;i++)
      {//insert IDMX values

      Word_Table->Cell(4+i,1)->Range->InsertAfter(WideString(MI->IDMX[i]->Index));
      Word_Table->Cell(4+i,2)->Range->InsertAfter(WideString(TVarOptions::FormatSD(MI->IDMX[i]->V,6)));
      Word_Table->Cell(4+i,8)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->IDMX[i]->EVO,3)));
      Word_Table->Cell(4+i,7)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->IDMX[i]->ESR,3)));
      }
   Word_Table->Cell(4,3)->Range->InsertAfter(WideString(TVarOptions::FormatSD(MI->Prover.D,0)));
   Word_Table->Cell(4,4)->Range->InsertAfter(WideString(TVarOptions::FormatSD(MI->Prover.S,0)));

   Word_Table->Cell(4,5)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.E,0)));
   Word_Table->Cell(4,6)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.Alfa,0)));
   Word_Table->Cell(4,9)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Options.dtTPU,2)));
   if(MI->Prover.Type==TProver::Compact)
      Word_Table->Cell(4,10)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Options.t_invar,2)));

   vo=MI->MIVarOptions[TMI1974::dd];
   v=&MI->Var[TMI1974::Tetta_UOI];
   vo->FormatStr(v,s);
   ws=s;
   Word_Table->Cell(4,11)->Range->InsertAfter(ws);

   Word_Table->Cell(4,12)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Options.dtTPR,2)));
   if(!MI->Options.UseFD)
      {
      Word_Table->Cell(4,13)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Options.Density,2)));
      Word_Table->Cell(4,14)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Options.tFD,2)));
      }
   Word_Table->AutoFitBehavior(wdAutoFitContent);
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Label1->Caption="������������ ������� 2 '���������� ���������'...";
   ProtokolProgressForm->Update();
   //***************************************************************************
   //***************************************************************************
   oo=WD->Characters->Count-1;
   o=oo;

   WD->Range(o,oo)->InsertAfter(WideString("\n������� 2 - ���������� ��������� � ����������"));
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size=10;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->Font->Size=10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();


   oo=WD->Characters->Count-1;
   o=oo;
   Word_Table=WD->Tables->Add(WD->Range(o,o),rc,15);
   Word_Table->Range->Font->Size=10;
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
   //Add � ���.
   ws="� ���\n� ���\nj/i";
   Word_Table->Cell(1,1)->Range->InsertAfter(ws);
   //Add Qj
   ws="Qij,\n�3/�";
   Word_Table->Cell(1,2)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,2)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=5;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   Word_Table->Cell(1,3)->Range->InsertAfter(WideString("�� ��"));
   Word_Table->Cell(1,8)->Range->InsertAfter(WideString("�� ��"));
   Word_Table->Cell(1,13)->Range->InsertAfter(WideString("�� ��"));
   Word_Table->Cell(1,15)->Range->InsertAfter(WideString("�� ����."));

   Word_Table->Cell(1,13)->Merge(Word_Table->Cell(1,14));
   Word_Table->Cell(1,8)->Merge(Word_Table->Cell(1,12));
   Word_Table->Cell(1,3)->Merge(Word_Table->Cell(1,7));

   Word_Table->Cell(2,3)->Range->InsertAfter(WideString("�����-\n����"));
   Word_Table->Cell(2,4)->Range->InsertAfter(WideString("Tij,\nc"));
   rr=Word_Table->Cell(2,4)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   //t TPU ij
   Word_Table->Cell(2,5)->Range->InsertAfter(WideString("t��ij,\nC"));
   rr=Word_Table->Cell(2,5)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);
   //P TPU ij
   Word_Table->Cell(2,6)->Range->InsertAfter(WideString("P��ij,\n���"));
   rr=Word_Table->Cell(2,6)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //V ij
   Word_Table->Cell(2,7)->Range->InsertAfter(WideString("Vij,\n�3"));
   rr=Word_Table->Cell(2,7)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=5;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //f ij
   Word_Table->Cell(2,8)->Range->InsertAfter(WideString("fij,\n��"));
   rr=Word_Table->Cell(2,8)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //t TPR ij
   Word_Table->Cell(2,9)->Range->InsertAfter(WideString("tij,\nC"));
   rr=Word_Table->Cell(2,9)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //P TPR ij
   Word_Table->Cell(2,10)->Range->InsertAfter(WideString("Pij,\n���"));
   rr=Word_Table->Cell(2,10)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //N ij
   Word_Table->Cell(2,11)->Range->InsertAfter(WideString("Nij,\n���"));
   rr=Word_Table->Cell(2,11)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //� ij
   Word_Table->Cell(2,12)->Range->InsertAfter(WideString("�ij,\n���/�3"));
   rr=Word_Table->Cell(2,12)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=8;
   rr->MoveEnd(o,oo);
   oo=9;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //Density
   Word_Table->Cell(2,13)->Range->InsertAfter(WideString("ij,\n��/�3"));
   rr=Word_Table->Cell(2,13)->Range;
   o=wdCharacter;
   oo=-10;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03C1;//Ro
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=7;
   rr->MoveEnd(o,oo);
   oo=8;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //t FD ij
   Word_Table->Cell(2,14)->Range->InsertAfter(WideString("t��ij,\nC"));
   rr=Word_Table->Cell(2,14)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //��������
   Word_Table->Cell(2,15)->Range->InsertAfter(WideString("ij,\n���"));
   rr=Word_Table->Cell(2,15)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03BD;//Viscosi
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();


   for(int i=1;i<16;i++)
      Word_Table->Cell(3,i)->Range->InsertAfter(WideString(i));
   Word_Table->Cell(1,1)->Merge(Word_Table->Cell(2,1));
   Word_Table->Cell(1,2)->Merge(Word_Table->Cell(2,2));
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   //Add data to table
 //  int from=2;
   int to=3;
//   int step=60/pc;
   for(int ii=0;ii<pc;ii++)
      {
      p=(TMeasurePoint*)MI->UsedPoint[ii];
      if(!p) continue;
      int ccc=p->Count;
      for(int j=0;j<ccc;j++)
         {
         m=(TMeasure*)(p->Measures[j]);
         if(m->Var[TMeasure::Used]->Value.b)
            {
            to++;
            s=ii+1;
            s+="/";
            s+=m->Var[TMeasure::Number]->Value.i;
            ws=s;
            Word_Table->Cell(to,1)->Range->InsertAfter(ws);

            vo=MI->MeasureVarOptions[TMeasure::Q];
            v=m->Var[TMeasure::Q];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,2)->Range->InsertAfter(ws);
            //���������
            ws=WideString(MI->IDMX[m->Var[TMeasure::IDMX]->Value.i]->Index);
            Word_Table->Cell(to,3)->Range->InsertAfter(ws);
            //T
            vo=MI->MeasureVarOptions[TMeasure::Time];
            v=m->Var[TMeasure::Time];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,4)->Range->InsertAfter(ws);
            //t TPU
            vo=MI->MeasureVarOptions[TMeasure::t_TPU];
            v=m->Var[TMeasure::t_TPU];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,5)->Range->InsertAfter(ws);
            //P TPU
            vo=MI->MeasureVarOptions[TMeasure::P_TPU];
            v=m->Var[TMeasure::P_TPU];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,6)->Range->InsertAfter(ws);
            //V
            vo=MI->MeasureVarOptions[TMeasure::V];
            v=m->Var[TMeasure::V];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,7)->Range->InsertAfter(ws);
            //f
            vo=MI->MeasureVarOptions[TMeasure::f];
            v=m->Var[TMeasure::f];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,8)->Range->InsertAfter(ws);
            //t
            vo=MI->MeasureVarOptions[TMeasure::t];
            v=m->Var[TMeasure::t];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,9)->Range->InsertAfter(ws);
            //P
            vo=MI->MeasureVarOptions[TMeasure::P];
            v=m->Var[TMeasure::P];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,10)->Range->InsertAfter(ws);
            //N
            vo=MI->MeasureVarOptions[TMeasure::Pulses];
            v=m->Var[TMeasure::Pulses];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,11)->Range->InsertAfter(ws);
            //K
            vo=MI->MeasureVarOptions[TMeasure::K];
            v=m->Var[TMeasure::K];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,12)->Range->InsertAfter(ws);
            if(MI->Options.UseFD)
               {
            //Dens
               vo=MI->MeasureVarOptions[TMeasure::Dens];
               v=m->Var[TMeasure::Dens];
               vo->FormatStr(v,s);
               ws=s;
               Word_Table->Cell(to,13)->Range->InsertAfter(ws);
               //t FD
               vo=MI->MeasureVarOptions[TMeasure::t_FD];
               v=m->Var[TMeasure::t_FD];
               vo->FormatStr(v,s);
               ws=s;
               Word_Table->Cell(to,14)->Range->InsertAfter(ws);
               }
            if(MI->Options.UseVisc)
               {
               //visc
               vo=MI->MeasureVarOptions[TMeasure::visc];
               v=m->Var[TMeasure::visc];
               vo->FormatStr(v,s);
               ws=s;
               Word_Table->Cell(to,15)->Range->InsertAfter(ws);
               }
            Position+=Step;
            ProtokolProgressForm->Gauge->Progress=Position;
            ProtokolProgressForm->Update();
            }//End add measure data
         }//End iterate point
      }
   Word_Table->AutoFitBehavior(wdAutoFitContent);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
   ProtokolProgressForm->Label1->Caption="������������ ������� '���������� ������� � ������ �������� ���������'...";

   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->InsertAfter(WideString("\n������� 3 - ���������� ������� � ������ �������� ���������"));
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size=10;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->Font->Size=10;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   //Calculate number of rows

   Word_Table=WD->Tables->Add(WD->Range(o,o),pc+2,8);
   Word_Table->Range->Font->Size=10;
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
   //Add � �����
   ws="� ����� (j)";
   Word_Table->Cell(1,1)->Range->InsertAfter(ws);
   //Add Qj
   ws="Qj,\n�3/�";
   Word_Table->Cell(1,2)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,2)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=4;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   //Add fj
   ws="fj,\n��";
   Word_Table->Cell(1,3)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,3)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   //Add Kj
   ws="Kj,\n���/�3";
   Word_Table->Cell(1,4)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,4)->Range;
   o=wdCharacter;
   oo=-9;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=8;
   rr->MoveEnd(o,oo);
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   //Add Sj
   ws="Sj,\n%";
   Word_Table->Cell(1,5)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,5)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   //Add ej
   ws="j,\n%";
   Word_Table->Cell(1,6)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,6)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B5;//e
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //Tetta_Sumj
   Word_Table->Cell(1,7)->Range->InsertAfter(WideString("j,\n%"));
   rr=Word_Table->Cell(1,7)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03A3;
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   ll=0x0398;
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=3;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //Add dj
   ws="j,\n%";
   Word_Table->Cell(1,8)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,8)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B4;//delta
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   for(int i=1;i<9;i++)
      Word_Table->Cell(2,i)->Range->InsertAfter(WideString(i));
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   for(int ii=0;ii<pc;ii++)
      {
      p=(TMeasurePoint*)MI->UsedPoint[ii];
      if(!p) continue;
      s=ii+1;
      ws=s;
      Word_Table->Cell(ii+3,1)->Range->InsertAfter(ws);
      //Add Qj
      vo=MI->PointVarOptions[TMeasurePoint::Q];
      v=p->Var[TMeasurePoint::Q];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(ii+3,2)->Range->InsertAfter(ws);
      //Add fj
      vo=MI->PointVarOptions[TMeasurePoint::f];
      v=p->Var[TMeasurePoint::f];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(ii+3,3)->Range->InsertAfter(ws);
      //Add Kj
      vo=MI->PointVarOptions[TMeasurePoint::K];
      v=p->Var[TMeasurePoint::K];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(ii+3,4)->Range->InsertAfter(ws);
      //Add Sj
      vo=MI->PointVarOptions[TMeasurePoint::SKO];
      v=p->Var[TMeasurePoint::SKO];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(ii+3,5)->Range->InsertAfter(ws);
      //Add ej
      vo=MI->PointVarOptions[TMeasurePoint::e];
      v=p->Var[TMeasurePoint::e];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(ii+3,6)->Range->InsertAfter(ws);
      if(MI->Options.Purpose!=TMI1974Options::Work)
         {
         //Add Tetta_Sum
         vo=MI->MIVarOptions[TMI1974::Tetta_Sum];
         v=&MI->Var[TMI1974::Tetta_Sum];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,7)->Range->InsertAfter(ws);
         //Add dj
         vo=MI->PointVarOptions[TMeasurePoint::d];
         v=p->Var[TMeasurePoint::d];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,8)->Range->InsertAfter(ws);
         }
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      }
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
   if(MI->Options.Purpose!=TMI1974Options::Control)
   {
   if(MI->Options.Function!=TMI1974Options::Const)
      {
      ProtokolProgressForm->Label1->Caption="������������ ������� 4 '���������� ������� � �������������'...";

      oo=WD->Characters->Count-1;
      o=oo;
      WD->Range(o,oo)->InsertAfter(WideString("\n������� 4 - ���������� ������� � �������������"));
      oo=WD->Characters->Count-1;
      WD->Range(o,oo)->Font->Size=10;
      WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
      oo=WD->Characters->Count-1;
      o=oo;
      WD->Range(o,oo)->Font->Size=10;
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();

   //Calculate number of rows
      int rc=MI->FRanges->Count;
      Word_Table=WD->Tables->Add(WD->Range(o,o),rc+2,8);
      Word_Table->Range->Font->Size=10;
      Word_Table->Borders->Enable = true;
      Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
      Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
      //Add � ������������
      ws="� ��\n(k)";
      Word_Table->Cell(1,1)->Range->InsertAfter(ws);
      //Add Qmin
      ws="Qmin k,\n�3/�";
      Word_Table->Cell(1,2)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,2)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=8;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      //Add Qmax
      ws="Qmax k,\n�3/�";
      Word_Table->Cell(1,3)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,3)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=8;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;

      //Add e ��K
      ws="��k,\n%";
      Word_Table->Cell(1,4)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,4)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03B5;//e
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Tetta ���k
      Word_Table->Cell(1,5)->Range->InsertAfter(WideString("���k,\n%"));
      rr=Word_Table->Cell(1,5)->Range;
      o=wdCharacter;
      oo=-8;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x0398;//Tetta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=5;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Tetta_Sum ��k
      Word_Table->Cell(1,6)->Range->InsertAfter(WideString("��k,\n%"));
      rr=Word_Table->Cell(1,6)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03A3;//Summa
      Template=wdFontBiasDontCare;
      rr->InsertSymbol(ll,oo,o,Template);
      ll=0x0398; //Tetta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=5;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Add d��k
      ws="��k,\n%";
      Word_Table->Cell(1,7)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,7)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03B4;//delta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Add K ��k
      ws="K�� k,\n���/�3";
      Word_Table->Cell(1,8)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,8)->Range;
      o=wdCharacter;
      oo=-9;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=8;
      rr->MoveEnd(o,oo);
      oo=11;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      for(int i=1;i<9;i++)
         Word_Table->Cell(2,i)->Range->InsertAfter(WideString(i));
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      for(int ii=0;ii<rc;ii++)
         {
         TRange* r=(TRange*)MI->FRanges->Items[ii];
         if(!r) continue;
         s=ii+1;
         ws=s;
         Word_Table->Cell(ii+3,1)->Range->InsertAfter(ws);
         //Add Qmin
         vo=MI->RangeVarOptions[TRange::Q1];
         v=r->Var[TRange::Q1];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,2)->Range->InsertAfter(ws);
         //Add Qmax
         vo=MI->RangeVarOptions[TRange::Q2];
         v=r->Var[TRange::Q2];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,3)->Range->InsertAfter(ws);
         //Add e pdk
         vo=MI->RangeVarOptions[TRange::edk];
         v=r->Var[TRange::edk];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,4)->Range->InsertAfter(ws);
         //Add Tetta apdk
         vo=MI->RangeVarOptions[TRange::Tetta_K];
         v=r->Var[TRange::Tetta_K];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,5)->Range->InsertAfter(ws);
         //Add Tetta Sum
         vo=MI->RangeVarOptions[TRange::Tetta_Sum];
         v=r->Var[TRange::Tetta_Sum];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,6)->Range->InsertAfter(ws);
         //Add d pdk
         vo=MI->RangeVarOptions[TRange::ddk];
         v=r->Var[TRange::ddk];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(ii+3,7)->Range->InsertAfter(ws);
         //Add Kpdk
         if(MI->Options.Function==TMI1974Options::ConstRange)
            {
            vo=MI->RangeVarOptions[TRange::K];
            v=r->Var[TRange::K];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(ii+3,8)->Range->InsertAfter(ws);
            }
         Position+=Step;
         ProtokolProgressForm->Gauge->Progress=Position;
         ProtokolProgressForm->Update();
         }
      }
   else
      {//Add table #5
      ProtokolProgressForm->Label1->Caption="������������ ������� 5 '���������� ������� � ������� ���������'...";

      oo=WD->Characters->Count-1;
      o=oo;
      WD->Range(o,oo)->InsertAfter(WideString("\n������� 5 - ���������� ������� � ������� ���������"));
      oo=WD->Characters->Count-1;
      WD->Range(o,oo)->Font->Size=10;
      WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
      oo=WD->Characters->Count-1;
      o=oo;
      WD->Range(o,oo)->Font->Size=10;
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();

      Word_Table=WD->Tables->Add(WD->Range(o,o),3,7);
      Word_Table->Range->Font->Size=10;
      Word_Table->Borders->Enable = true;
      Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
      Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
      //Add Qmin
      ws="Qmin,\n�3/�";
      Word_Table->Cell(1,1)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,1)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=6;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      //Add Qmax
      ws="Qmax,\n�3/�";
      Word_Table->Cell(1,2)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,2)->Range;
      o=wdCharacter;
      oo=-7;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=6;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      //Add e
      ws="�,\n%";
      Word_Table->Cell(1,3)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,3)->Range;
      o=wdCharacter;
      oo=-5;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03B5;//e
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=4;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Tetta ��
      Word_Table->Cell(1,4)->Range->InsertAfter(WideString("��,\n%"));
      rr=Word_Table->Cell(1,4)->Range;
      o=wdCharacter;
      oo=-6;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x0398;//Tetta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=3;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Tetta_Sum �
      Word_Table->Cell(1,5)->Range->InsertAfter(WideString("�,\n%"));
      rr=Word_Table->Cell(1,5)->Range;
      o=wdCharacter;
      oo=-5;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03A3;//Summa
      Template=wdFontBiasDontCare;
      rr->InsertSymbol(ll,oo,o,Template);
      ll=0x0398; //Tetta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=3;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Add d�
      ws="�,\n%";
      Word_Table->Cell(1,6)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,6)->Range;
      o=wdCharacter;
      oo=-5;
      rr->MoveEnd(o,oo);
      o=True;
      oo=EmptyParam;
      ll=0x03B4;//delta
      rr->InsertSymbol(ll,oo,o,Template);
      o=wdCharacter;
      oo=2;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      //Add K �
      ws="K�,\n���/�3";
      Word_Table->Cell(1,7)->Range->InsertAfter(ws);
      rr=Word_Table->Cell(1,7)->Range;
      o=wdCharacter;
      oo=-9;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=8;
      rr->MoveEnd(o,oo);
      oo=8;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      for(int i=1;i<8;i++)
         Word_Table->Cell(2,i)->Range->InsertAfter(WideString(i));
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      //Add Qmin
      p=(TMeasurePoint*)MI->UsedPoint[0];
      if(p)
         {
         vo=MI->PointVarOptions[TMeasurePoint::Q];
         v=p->Var[TMeasurePoint::Q];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(3,1)->Range->InsertAfter(ws);
         }
      //Add Qmax
      p=0;
      if(pc)
         p=(TMeasurePoint*)MI->UsedPoint[pc-1];
      if(p)
         {
         vo=MI->PointVarOptions[TMeasurePoint::Q];
         v=p->Var[TMeasurePoint::Q];
         vo->FormatStr(v,s);
         ws=s;
         Word_Table->Cell(3,2)->Range->InsertAfter(ws);
         }
      //Add ed
      vo=MI->MIVarOptions[TMI1974::ed];
      v=&MI->Var[TMI1974::ed];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(3,3)->Range->InsertAfter(ws);
      //Add tetta_k
      vo=MI->MIVarOptions[TMI1974::Tetta_K];
      v=&MI->Var[TMI1974::Tetta_K];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(3,4)->Range->InsertAfter(ws);
      //Add Tetta_Sum
      vo=MI->MIVarOptions[TMI1974::Tetta_Sumd];
      v=&MI->Var[TMI1974::Tetta_Sumd];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(3,5)->Range->InsertAfter(ws);
      //Add d
      vo=MI->MIVarOptions[TMI1974::dd];
      v=&MI->Var[TMI1974::dd];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(3,6)->Range->InsertAfter(ws);
      //Add Kd
      vo=MI->MIVarOptions[TMI1974::K];
      v=&MI->Var[TMI1974::K];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(3,7)->Range->InsertAfter(ws);
      Position+=Step;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();
      }
      }
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
   oo=WD->Characters->Count-1;
   ws="\n����������: ��������������� ������� � ���������� ������������ ";
   WD->Range(oo,oo)->InsertAfter(ws);
   Template=oo=WD->Characters->Count-1;
   ws="�����";
   WD->Range(oo,oo)->InsertAfter(ws);
   o=WD->Characters->Count-1;
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   oo=WD->Characters->Count-1;
   ws="\n\n������� ����, ������������ ������� ______________   ";
   ws+=MI->Options.Name;
   WD->Range(oo,oo)->InsertAfter(ws);
   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

   oo=WD->Characters->Count-1;
   ws="\n\n���� �������: ";
   unsigned short year,month,day;
   MI->Options.Date.DecodeDate(&year, &month, &day);
   s=(int)day;
   ws+=s;
   switch (month)
   {
   case 1: ws+=" ������ ";      break;
   case 2: ws+=" ������� ";      break;
   case 3: ws+=" ����� ";        break;
   case 4: ws+=" ������ ";       break;
   case 5: ws+=" ��� ";          break;
   case 6: ws+=" ���� ";         break;
   case 7: ws+=" ���� ";          break;
   case 8: ws+=" ������� ";      break;
   case 9: ws+=" �������� ";      break;
   case 10: ws+=" ������� ";       break;
   case 11: ws+=" ������ ";        break;
   case 12: ws+=" ������� ";       break;
   }
   s=(int)year;
   ws+=s;
   ws+=" �.";
   WD->Range(oo,oo)->InsertAfter(ws);

   WD->Range(Template,o)->Bold=true;
   WD->Range(Template,o)->Italic=true;

   Position+=Step;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();
   WA->GetDefaultInterface()->Visible = true;
   WD->Disconnect();
   WA->Disconnect();
   delete WD;
   delete WA;
   }

catch (...)
   {
   ProtokolProgressForm->Hide();
   MessageDlg("������ ������ � MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
   delete WA;
   delete WD;
   return;
   }
   ProtokolProgressForm->Completed();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TMainForm::CheckMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   if(ActiveMDIChild->Tag==100)
      return;
   TMIForm* f = (TMIForm*)ActiveMDIChild;
   if(!f) return;
   TMI1974* MI=f->MI;

   TExcelWorkbook *EB=0;
   TExcelApplication *EA=0;
   TExcelWorksheet* ES=0;

   try
      {
      EA = new TExcelApplication(this);
      EA->Connect();
      EB = new TExcelWorkbook(this);
      ES=new TExcelWorksheet(this);
      }
   catch (...)
      {
      MessageDlg("�� ���� ����������� � �������� MS Excel", mtError, TMsgDlgButtons() << mbYes, 0);
      if(ES) delete ES;
      if(EB) delete EB;
      if(EA) delete EA;
      return;
      }
   try
   {
   OleVariant Template = EmptyParam;
   OleVariant o = 1;
   OleVariant NewTemplate = False;
   WideString ws;

   EA->set_Visible(0,0);
   EA->set_SheetsInNewWorkbook(0,1);
   _Workbook* b;
   b=EA->Workbooks->Add(Template,NewTemplate);
   EB->ConnectTo(b);
   ES->ConnectTo(EB->Worksheets->get_Item(o));

   int Col=1;
   int Row=1;
   //Wrtie TPR data
   ws="������ ����������� ��";
   SetTextExcel(ES,ws,Col,Row);

   Row=2;
   ws="������";
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws=MI->tpr.Model;
   SetTextExcel(ES,ws,Col,Row);

   Row++;
   ws="��";
   Col=1;
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws=MI->tpr.D;
   SetTextExcel(ES,ws,Col,Row);

   Row++;
   ws="���. �";
   Col=1;
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws=MI->tpr.SN;
   SetTextExcel(ES,ws,Col,Row);

   Row++;
   ws="Qmax";
   Col=1;
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws=MI->tpr.Qmax;
   SetTextExcel(ES,ws,Col,Row);

   Row++;
   ws="����� ���������";
   Col=1;
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws=MI->tpr.Location;
   SetTextExcel(ES,ws,Col,Row);

   //Wrtie Prover data
   Row+=2;
   Col=1;
   ws="������ ���";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws="������";
   SetTextExcel(ES,ws,Col,Row);
   Col=5;
   ws=MI->Prover.Name;
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws=MI->Prover.SN;
   SetTextExcel(ES,ws,Col,Row);
   Col=1;
   ws="���. �";
   SetTextExcel(ES,ws,Col,Row);
   ws="��� ���";
   SetTextExcel(ES,ws,Col,Row);
   ws="";
   Col=5;
   if(MI->Prover.Type==TProver::UniDirectional)
      ws="����������������";
   if(MI->Prover.Type==TProver::BiDirectional)
      ws="���������������";
   if(MI->Prover.Type==TProver::Compact)
      ws="�������-������";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws=MI->Prover.D;
   SetTextExcel(ES,ws,Col,Row);
   Col=1;
   ws="������� �����. ��-��, ��";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws="������� ������ �����. ��-��, ��";
   SetTextExcel(ES,ws,Col,Row);
   Col=5;
   ws=MI->Prover.S;
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws=MI->Prover.Alfa;
   SetTextExcel(ES,ws,Col,Row);
   Col=1;
   ws="����-�� ��������� ���������� Alfa_t";
   SetTextExcel(ES,ws,Col,Row);
   if(MI->Prover.Type==TProver::Compact)
      {
      Row++;
      ws="����-�� ��������� ���������� ���������� ������� (��� �������-�������) Alfa_t_invar";
      SetTextExcel(ES,ws,Col,Row);
      Col=5;
      ws=MI->Prover.Alfa_invar;
      SetTextExcel(ES,ws,Col,Row);
      }
   Row++;
   Col=1;
   ws="������ ��������� ��������� ������ ���";
   SetTextExcel(ES,ws,Col,Row);
   Col=5;
   ws=MI->Prover.E;
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   ws="������ ������� ���";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   Col=1;
   ws="������� ��������";
   SetTextExcel(ES,ws,Col,Row);
   Col=3;
   ws="V0";
   SetTextExcel(ES,ws,Col,Row);
   Col=4;
   ws="������� ����.";
   SetTextExcel(ES,ws,Col,Row);
   Col=6;
   ws="��. ����-�� ���. ����. ����������� ���";
   SetTextExcel(ES,ws,Col,Row);
   Col=10;
   ws="��. ��������� ����-��� ����. ����-�� ���";
   SetTextExcel(ES,ws,Col,Row);
   Col=14;
   ws="������������� ����������� ���";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   for(int i=0;i<4;i++)
      {
      TProverMX* pmx=MI->IDMX[i];
      if(pmx==0) break;
      Col=1;
      ws="\"";
      ws+=pmx->Index;
      ws+="\"";
      SetTextExcel(ES,ws,Col,Row);
      Col=3;
      ws=pmx->V;
      SetTextExcel(ES,ws,Col,Row);
      Col=4;
      ws=pmx->tBase;
      SetTextExcel(ES,ws,Col,Row);
      Col=6;
      ws=pmx->EVO;
      SetTextExcel(ES,ws,Col,Row);
      Col=10;
      ws=pmx->ESR;
      SetTextExcel(ES,ws,Col,Row);
      Col=14;
      ws=pmx->ESO;
      SetTextExcel(ES,ws,Col,Row);
      Row++;
      }

   Row++;
   Col=1;
   ws="������ �������";
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   if(MI->Options.Purpose==TMI1974Options::Work)
      ws="���������� ���: �������";
   if(MI->Options.Purpose==TMI1974Options::Control)
      ws="���������� ���: �����������";
   if(MI->Options.Purpose==TMI1974Options::ControlReserv)
      ws="���������� ���: ��������-�����������";
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   if(MI->Options.FreqInput==TMI1974Options::User)
      ws="����������� �������: ������ ����";
   else
      ws="����������� �������: �������������� ����������";
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   ws="���������� ����������� ��������� ����������� �� ��";
   SetTextExcel(ES,ws,Col,Row);
   Col=8;
   ws=MI->Options.dtTPR;
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   ws=MI->Options.dtTPU;
   SetTextExcel(ES,ws,Col,Row);
   Col=1;
   ws="���������� ����������� ��������� ����������� �� ���";
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   ws="����������� ���, %";
   SetTextExcel(ES,ws,Col,Row);
   Col=8;
   ws=MI->Options.dUOI;
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   Col=1;
   if(MI->Options.Function==TMI1974Options::Const)
      ws="��� ��: ���������� �������� � ������� ���������";
   if(MI->Options.Function==TMI1974Options::ConstRange)
      ws="��� ��: ���������� �������� � �������������";
   if(MI->Options.Function==TMI1974Options::Line)
      ws="��� ��: ������� �����";
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   ws="����������� ���-��� ��������� ���������� � �����������:";
   SetTextExcel(ES,ws,Col,Row);
   Row++;
   Col=2;
   if(MI->Options.ProductIndex==0)
      ws="������ ���� �������������";
   else
      {
      TProduct* p;
      p=(TProduct*)TMI1974::CommonData->Products->Items[MI->Options.ProductIndex-1];
      ws=p->Origin;
      }
   SetTextExcel(ES,ws,Col,Row);
   Row++;

   Col=1;
   if(MI->Options.DensReCalc)
      ws="��������� �������� ����-�� � �������� � ��: ��";
   else
      ws="��������� �������� ����-�� � �������� � ��: ���";
   SetTextExcel(ES,ws,Col,Row);
   Row++;


   Col=1;
   if(MI->Options.UseFD)
      {
      ws="����������� ���������: �� ��";
      SetTextExcel(ES,ws,Col,Row);
      }
   else
      {
      ws="����������� ���������: �� ����� ����� ��������";
      SetTextExcel(ES,ws,Col,Row);
      Row++;
      ws="���������, ��/�3";
      SetTextExcel(ES,ws,Col,Row);
      Col=3;
      ws=MI->Options.Density;
      SetTextExcel(ES,ws,Col,Row);
      Row++;
      ws=MI->Options.tFD;
      SetTextExcel(ES,ws,Col,Row);
      Col=1;
      ws="����������� �����";
      SetTextExcel(ES,ws,Col,Row);
      }
   Row++;

   ws="������ ���������";
   SetTextExcel(ES,ws,Col,Row);
   Row+=2;
   Col=1;
   SetTextExcel(ES,ws,Col,Row);
   for(int i=0;i<TMeasure::VarCount;i++)
      {
      ws=MI->MeasureVarOptions[i]->Name;
      SetTextExcel(ES,ws,Col,Row);
      Col++;
      };

   Row++;
   int PointCount = MI->Count;
   int pi=0;
   AnsiString s;
//Print measures
   for(int i=0;i<PointCount;i++)
      {//iterate throw points
      TMeasurePoint* p = (TMeasurePoint*)MI->GridPoints[i];
      if(p->Var[TMeasurePoint::Used]->Value.b==false)
         continue;
      pi++;
      s="����� �";
      s+=pi;
      ws=s;
      Col=1;
      SetTextExcel(ES,ws,Col,Row);
      Row++;
      int mc = p->Count;
      int mi=0;
      for(int j=0;j<mc;j++)
         {//iterate throw measures
         TMeasure* m = (TMeasure*)(p->Measures[j]);
         if(m->Var[TMeasure::Used]->Value.b==false)
            continue;
         mi++;
         Col=1;

         //print all values;
         for(int ii=0;ii<TMeasure::VarCount;ii++)
            {
            if(MI->MeasureVarOptions[ii]->Type==TVarOptions::Integer)
               ws=m->Var[ii]->Value.i;
            if(MI->MeasureVarOptions[ii]->Type==TVarOptions::Float)
               ws=m->Var[ii]->Value.d;
            if(MI->MeasureVarOptions[ii]->Type==TVarOptions::Bool)
               {
               if(m->Var[ii]->Value.b)
                  ws="��";
               else
                  ws="���";
               }
            if(MI->MeasureVarOptions[ii]->Type==TVarOptions::List &&
                (MI->MeasureVarOptions[ii]->Strings!=NULL))
               {
               ws="\"";
               ws+=MI->MeasureVarOptions[ii]->Strings->Strings[m->Var[ii]->Value.i];
               ws+="\"";
               }
            if(MI->MeasureVarOptions[ii]->Type==TVarOptions::Text)
               {
               ws="\"";
               ws+=m->Var[ii]->Value.c;
               ws+="\"";
               }
            SetTextExcel(ES,ws,Col,Row);
            Col++;
            }//end of print values of measures
         Row++;
         }//end iterate measures
      };//end iterate points
//print points data

   Row++;
   Col=1;
   ws="������ �����";
   SetTextExcel(ES,ws,Col,Row);
//print header of points data
   Row++;
   for(int i=0;i<TMeasurePoint::VarCount;i++)
      {
      ws=MI->PointVarOptions[i]->Name;
      SetTextExcel(ES,ws,Col,Row);
      Col++;
      };
   Row++;
//realy print points data
   pi=0;

   for(int i=0;i<PointCount;i++)
      {//iterate throw points
      TMeasurePoint* p = (TMeasurePoint*)MI->GridPoints[i];
      if(p->Var[TMeasurePoint::Used]->Value.b==false)
         continue;
      pi++;
      Col=1;
      //print all values;
      for(int ii=0;ii<TMeasurePoint::VarCount;ii++)
         {
         if(MI->PointVarOptions[ii]->Type==TVarOptions::Integer)
            ws=p->Var[ii]->Value.i;
         if(MI->PointVarOptions[ii]->Type==TVarOptions::Float)
            ws=p->Var[ii]->Value.d;
         if(MI->PointVarOptions[ii]->Type==TVarOptions::Bool)
            {
            if(p->Var[ii]->Value.b)
               ws="��";
            else
               ws="���";
            }
         if(MI->PointVarOptions[ii]->Type==TVarOptions::List &&
             (MI->PointVarOptions[ii]->Strings!=NULL))
            {
            ws="\"";
            ws+=MI->PointVarOptions[ii]->Strings->Strings[p->Var[ii]->Value.i];
            ws+="\"";
            }
         if(MI->PointVarOptions[ii]->Type==TVarOptions::Text)
            {
            ws="\"";
            ws+=p->Var[ii]->Value.c;
            ws+="\"";
            }
         SetTextExcel(ES,ws,Col,Row);
         Col++;
         }//end of print values of points
      Row++;
      }//end iterate points

//print Ranges
   Row++;
   Col=1;
   ws="������ �������������";
   SetTextExcel(ES,ws,Col,Row);
//print header of Ranges data
   Row++;
   for(int i=0;i<TRange::VarCount;i++)
      {
      ws=MI->RangeVarOptions[i]->Name;
      SetTextExcel(ES,ws,Col,Row);
      Col++;
      };
   Row++;
//realy print ranges data
   PointCount=MI->FRanges->Count;
   for(int i=0;i<PointCount;i++)
      {//iterate throw Ranges
      TRange* p = (TRange*)MI->FRanges->Items[i];
      Col=1;
      //print all values;
      for(int ii=0;ii<TRange::VarCount;ii++)
         {
         if(MI->RangeVarOptions[ii]->Type==TVarOptions::Integer)
            ws=p->Var[ii]->Value.i;
         if(MI->RangeVarOptions[ii]->Type==TVarOptions::Float)
            ws=p->Var[ii]->Value.d;
         if(MI->RangeVarOptions[ii]->Type==TVarOptions::Bool)
            {
            if(p->Var[ii]->Value.b)
               ws="��";
            else
               ws="���";
            }
         if(MI->RangeVarOptions[ii]->Type==TVarOptions::List &&
             (MI->RangeVarOptions[ii]->Strings!=NULL))
            {
            ws="\"";
            ws+=MI->RangeVarOptions[ii]->Strings->Strings[p->Var[ii]->Value.i];
            ws+="\"";
            }
         if(MI->RangeVarOptions[ii]->Type==TVarOptions::Text)
            {
            ws="\"";
            ws+=p->Var[ii]->Value.c;
            ws+="\"";
            }
         SetTextExcel(ES,ws,Col,Row);
         Col++;
         }//end of print values of points
      Row++;
      }//end iterate points


//Print MI data
   Row++;
   Col=1;
   ws="���������� �������";
   SetTextExcel(ES,ws,Col,Row);
//print header of MI data
   Row++;
   for(int i=0;i<TMI1974::VarCount;i++)
      {
      ws=MI->MIVarOptions[i]->Name;
      SetTextExcel(ES,ws,Col,Row);
      Col++;
      };
   Row++;

//realy print MI data
   Col=1;
   //print all values;
   for(int ii=0;ii<TMI1974::VarCount;ii++)
         {
         if(MI->MIVarOptions[ii]->Type==TVarOptions::Integer)
            ws=MI->Var[ii].Value.i;
         if(MI->MIVarOptions[ii]->Type==TVarOptions::Float)
            ws=MI->Var[ii].Value.d;
         if(MI->MIVarOptions[ii]->Type==TVarOptions::Bool)
            {
            if(MI->Var[ii].Value.b)
               ws="��";
            else
               ws="���";
            }
         if(MI->MIVarOptions[ii]->Type==TVarOptions::List &&
             (MI->MIVarOptions[ii]->Strings!=NULL))
            {
            ws="\"";
            ws+=MI->MIVarOptions[ii]->Strings->Strings[MI->Var[ii].Value.i];
            ws+="\"";
            }
         if(MI->MIVarOptions[ii]->Type==TVarOptions::Text)
            {
            ws="\"";
            ws+=MI->Var[ii].Value.c;
            ws+="\"";
            }
         SetTextExcel(ES,ws,Col,Row);
         Col++;
         }//end of print values of points
      Row++;




   EA->set_Visible(0,1);

   }
   catch (...)
      {
      MessageDlg("������ :)", mtError, TMsgDlgButtons() << mbYes, 0);
      if(ES) delete ES;
      if(EB) delete EB;
      if(EA) delete EA;
      return;
      }

   if(ES) delete ES;
   if(EB) delete EB;
   if(EA) delete EA;


}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SetTextExcel(TExcelWorksheet* ES, WideString ws, int Col, int Row)
{
   OleVariant Cl=Col;
   OleVariant Rw=Row;
   Variant v=ws;
   ES->Cells->set_Item(Rw,Cl,v);
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::MakeProtokolKMX(TKMX* MI)
{
   ProtokolProgressForm->Label1->Caption="���������� � ������������ ���������...";
   ProtokolProgressForm->Gauge->Progress=0;
   ProtokolProgressForm->Show();
   ProtokolProgressForm->Update();
   TWordDocument *WD=NULL;
   TWordApplication *WA=NULL;

   try
      {
      WA = new TWordApplication(this);
      WD= new TWordDocument(this);
      WA->ConnectKind=ckNewInstance;
      WD->ConnectKind=ckNewInstance;
      }
   catch (...)
      {
      ProtokolProgressForm->Hide();
      MessageDlg("�� ���� ����������� � �������� MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
      if(WA != NULL) delete WA;
      if(WD != NULL) delete WD;
      return;
      }


   OleVariant Template = EmptyParam;
   OleVariant o = 0;
   OleVariant oo = 0;
   OleVariant NewTemplate = False;
   OleVariant ItemIndex = 1;
   OleVariant ItemsCount;
   Word_2k::Table *Word_Table;
   Word_2k::Range* rr;
   AnsiString s;

   TVarOptions* vo;
   TVar* v;
   Graphics::TBitmap* b;

   try
      {
      WA->Connect();
      }
   catch (...)
      {
      ProtokolProgressForm->Hide();
      MessageDlg("�� ���� ����������� � �������� MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
      return;
      }


try
{
   WA->Documents->Add(Template,NewTemplate);
      //Assign WordDocument component
   WD->ConnectTo(WA->Documents->Item(ItemIndex));

    WD->PageSetup->LeftMargin=WA->CentimetersToPoints(1.5);
    WD->PageSetup->RightMargin=WA->CentimetersToPoints(1);

   o=0;
   oo=WD->Characters->Count-1;

   double Position=0;
   double Step=0;

   ProtokolProgressForm->Label1->Caption="������������ ��������� ���������...";
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();


   WideString ws("�������� �������� �� �� �� ���������� ��������� �");
   ws+=MI->Options.ProtokolN;
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphCenter;
   WD->Range(o,oo)->Font->Size = 14;

   o=oo;
   WD->Range(o,oo)->InsertAfter(WideString("\n\n"));
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 12;
   Position+=Step;

   oo=WD->Characters->Count-1;
   o=oo;
   Word_Table=WD->Tables->Add(WD->Range(o,o),2,2);
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphLeft;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
   Word_Table->Range->Font->Size=10;


   ws="��� ��: ";
   ws+=MI->tpr.Model;
   ws+="-";
   s=MI->tpr.D;
   ws+=s;
   ws+="   ��������� �";
   ws+=MI->tpr.SN;
   Word_Table->Cell(1,1)->Range->InsertAfter(ws);

   ws="��� �� ";
   ws+=MI->Prover.Name;
   ws+="   ��������� �";
   ws+=MI->Prover.SN;
   ws+="\n���� ������� �� ";
   ws+=MI->IDMX[0]->CalibrationDate.DateString();
   Word_Table->Cell(1,2)->Range->InsertAfter(ws);

   ws="����� ���������� ��� ";
   ws+=MI->Options.Location;
   Word_Table->Cell(2,1)->Range->InsertAfter(ws);

   ws="�������� ����� ��� ���, ���: ";
   s=MI->Options.v;
   ws+=s;
   Word_Table->Cell(2,2)->Range->InsertAfter(ws);



   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 10;

   o=oo;
   ws="\n\n1. �������� ������\n";
   WD->Range(o,oo)->InsertAfter(ws);
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size = 12;
   WD->Range(o,oo)->ParagraphFormat->Alignment=wdAlignParagraphLeft;

   ProtokolProgressForm->Label1->Caption="������������ ������� '�������� ������'...";
   Position=15;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();

//������� �������� ������
   int ColCount=7;
   //��������� ���. ������
   int ProverMXCount=1;
   if(MI->IDMX[1]!=0)
      {
      ProverMXCount++;
      if(MI->IDMX[2]!=0)
         {
         ProverMXCount++;
         if(MI->IDMX[3]!=0)
            ProverMXCount++;
         }
      }
   ColCount+=ProverMXCount;
   //��������� ����� �������� ��
   ColCount+=MI->TPRMX.PointCount;

   oo=WD->Characters->Count-1;
   o=oo;
   Word_Table=WD->Tables->Add(WD->Range(o,o),4,ColCount);
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;
   Word_Table->Range->Font->Size=10;

   //Insert numbers
   for(int ii=0;ii<ColCount;ii++)
      {
      ws=ii+1;
      Word_Table->Cell(3,ii+1)->Range->InsertAfter(ws);
      }

   int ProverColumns=ProverMXCount+4;

   Word_Table->Cell(1,1)->Range->InsertAfter(WideString("��� ���������� ��������� (��)"));
   Word_Table->Cell(1,ProverColumns+1)->Range->InsertAfter(WideString("��� ������� ��������"));
   Word_Table->Cell(1,ProverColumns+1+3)->Range->InsertAfter(WideString("����� �������� ��� ��"));
   //���������� ������ ������
   Word_Table->Cell(1,1)->Merge(Word_Table->Cell(1,ProverColumns));
   Word_Table->Cell(1,2)->Merge(Word_Table->Cell(1,4));
   if(MI->TPRMX.PointCount > 1)
      Word_Table->Cell(1,3)->Merge(Word_Table->Cell(1,3+MI->TPRMX.PointCount-1));
   //��������� ������ � ������������ ������
   for (int i=0; i<ProverMXCount;i++)
      {
      s="V0(";
      s+=MI->IDMX[i]->Index;
      s+="),\n�3";
      ws=s;
      Word_Table->Cell(2,1+i)->Range->InsertAfter(WideString(s));
      int mxlen=strlen(MI->IDMX[i]->Index);
      //Vo(1-2),/nm3
      rr=Word_Table->Cell(2,1+i)->Range;
      o=wdCharacter;
      oo=-5;
      rr->MoveEnd(o,oo);
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      oo=5;
      rr->MoveEnd(o,oo);
      oo=1+mxlen+5;
      rr->MoveStart(o,oo);
      rr->Font->Superscript=true;
      //Add data
      Word_Table->Cell(4,1+i)->Range->InsertAfter(WideString(TVarOptions::FormatSD(MI->IDMX[i]->V,6)));
      }
   ws="�,\n��";
   Word_Table->Cell(2,ProverMXCount+1)->Range->InsertAfter(ws);
   Word_Table->Cell(4,ProverMXCount+1)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.D,0)));

   ws="S,\n��";
   Word_Table->Cell(2,ProverMXCount+2)->Range->InsertAfter(ws);
   Word_Table->Cell(4,ProverMXCount+2)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.S,0)));

   ws="�,\n���";
   Word_Table->Cell(2,ProverMXCount+3)->Range->InsertAfter(ws);
   Word_Table->Cell(4,ProverMXCount+3)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.E,0)));

   //Alfa
   if(MI->Prover.Type==TProver::Compact)
      {
      Word_Table->Cell(2,ProverMXCount+4)->Range->InsertAfter(WideString("K1,\nC-1"));
      oo=-8;
      }
   else
      {
      Word_Table->Cell(2,ProverMXCount+4)->Range->InsertAfter(WideString("t,\nC-1"));
      oo=-7;
      }
   rr=Word_Table->Cell(2,ProverMXCount+4)->Range;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   long ll=0x03B1;//alfa
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   if(MI->Prover.Type==TProver::Compact)
      oo=3;
   else
      oo=2;
   o=wdCharacter;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=5;
   rr->MoveEnd(o,oo);
   if(MI->Prover.Type==TProver::Compact)
      oo=5;
   else
      oo=4;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   oo=-1;
   rr->MoveStart(o,oo);
   oo=-3;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;
   Template=wdFontBiasDontCare;
   rr->InsertSymbol(ll,oo,o,Template);
   Word_Table->Cell(4,ProverMXCount+4)->Range->InsertAfter(WideString(TVarOptions::FormatDAD(MI->Prover.Alfa,0)));
//---------------------------------------------------------------------------
   //Add Rot
   Word_Table->Cell(2,ProverMXCount+5)->Range->InsertAfter(WideString("t,\n��/�3"));
   rr=Word_Table->Cell(2,ProverMXCount+5)->Range;
   o=wdCharacter;
   oo=-9;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03C1;//Ro
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=7;
   rr->MoveEnd(o,oo);
   oo=7;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   vo=MI->MIVarOptions[TKMX::AverageDens];
   v=&MI->Var[TKMX::AverageDens];
   vo->FormatStr(v,s);
   ws=s;
   Word_Table->Cell(4,ProverMXCount+5)->Range->InsertAfter(ws);
//---------------------------------------------------------------------------
   //Add Betta�
   Word_Table->Cell(2,ProverMXCount+6)->Range->InsertAfter(WideString("�,\n�-1"));
   rr=Word_Table->Cell(2,ProverMXCount+6)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B2;//bETTA
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   oo=2;
   rr->MoveEnd(o,oo);
   oo=3;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//celcius
   rr->InsertSymbol(ll,oo,o,Template);
   rr=Word_Table->Cell(2,ProverMXCount+6)->Range;
   o=wdCharacter;
   oo=6;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   //Add data
   vo=MI->MIVarOptions[TKMX::betta_aver];
   v=&MI->Var[TKMX::betta_aver];
   vo->FormatStr(v,s);
   ws=s;
   Word_Table->Cell(4,ProverMXCount+6)->Range->InsertAfter(ws);

   //Add Gamma�
   Word_Table->Cell(2,ProverMXCount+7)->Range->InsertAfter(WideString("�,\n���-1"));
   rr=Word_Table->Cell(2,ProverMXCount+7)->Range;
   o=wdCharacter;
   oo=-9;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B3;//�����
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=7;
   rr->MoveEnd(o,oo);
   oo=6;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;
   //Add data
   vo=MI->MIVarOptions[TKMX::gamma_aver];
   v=&MI->Var[TKMX::gamma_aver];
   vo->FormatStr(v,s);
   ws=s;
   Word_Table->Cell(4,ProverMXCount+7)->Range->InsertAfter(ws);

   //������ ��
   for (int i=0; i < MI->TPRMX.PointCount;i++)
      {
      s="Q";
      s+=i+1;
      ws=s;
      Word_Table->Cell(2,ProverMXCount+8+i)->Range->InsertAfter(ws);
      //Q12
      rr=Word_Table->Cell(2,ProverMXCount+8+i)->Range;
      o=wdCharacter;
      oo=1;
      rr->MoveStart(o,oo);
      rr->Font->Subscript=true;
      vo=MI->MIVarOptions[TKMX::AverageDens];

      TVar QVar;
      QVar.Value.d=MI->TPRMX.Points[i].Q;
      vo->FormatStr(&QVar,s);
      ws=s;
      Word_Table->Cell(4,ProverMXCount+8+i)->Range->InsertAfter(ws);
      }
   Word_Table->AutoFitBehavior(wdAutoFitContent);
//---------------------------------------------------------------------------
   oo=WD->Characters->Count-1;
   o=oo;

   WD->Range(o,oo)->InsertAfter(WideString("\n2. ���������� ��������� � ����������"));
   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->Font->Size=12;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->Font->Size=10;

   Position=25;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Label1->Caption="������������ ������� '���������� ��������� � ����������'...";
   ProtokolProgressForm->Update();

   //Calculate number of rows table #2
   int RowCount=3;
   int pc=MI->Var[TKMX::UsedPoints].Value.i;
   TKMXMeasurePoint* p;
   TKMXMeasure* m;
   for(int i=0;i<pc;i++)
      {
      p=(TKMXMeasurePoint*)MI->UsedPoint[i];
      if(!p) continue;
      int ccc=p->Count;
      for(int j=0;j<ccc;j++)
         {
         m=(TKMXMeasure*)(p->Measures[j]);
         if(m->Var[TMeasure::Used]->Value.b)
            RowCount++;
         }
      }

   oo=WD->Characters->Count-1;
   o=oo;
   Word_Table=WD->Tables->Add(WD->Range(o,o),RowCount,15);
   Word_Table->Range->Font->Size=9;
   Word_Table->Borders->Enable = true;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalCenter;

   //Insert numbers
   for(int ii=0;ii<15;ii++)
      {
      ws=ii+1;
      Word_Table->Cell(3,ii+1)->Range->InsertAfter(ws);
      }

//---------------------------------------------------------------------------
//-���������� ���������
   Word_Table->Cell(1,1)->Range->InsertAfter(WideString("� ���-�����-����"));

   Word_Table->Cell(1,2)->Range->InsertAfter(WideString("� ��-����-���"));

   Word_Table->Cell(1,3)->Range->InsertAfter(WideString("������"));

   //Add Qij
   ws="Qij,\n�3/�";
   Word_Table->Cell(2,3)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(2,3)->Range;
   o=wdCharacter;
   oo=-7;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=5;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //f ij
   Word_Table->Cell(2,4)->Range->InsertAfter(WideString("fij,\n��"));
   rr=Word_Table->Cell(2,4)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   Word_Table->Cell(1,5)->Range->InsertAfter(WideString("��"));

   //t � ij
   Word_Table->Cell(2,5)->Range->InsertAfter(WideString("t�ij,\nC"));
   rr=Word_Table->Cell(2,5)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=5;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //P � ij
   Word_Table->Cell(2,6)->Range->InsertAfter(WideString("P�ij,\n���"));
   rr=Word_Table->Cell(2,6)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   Word_Table->Cell(1,7)->Range->InsertAfter(WideString("��"));

   //t �� ij
   Word_Table->Cell(2,7)->Range->InsertAfter(WideString("t��ij,\nC"));
   rr=Word_Table->Cell(2,7)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=6;
   rr->MoveStart(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x00BA;//C-1
   rr->InsertSymbol(ll,oo,o,Template);

   //P �� ij
   Word_Table->Cell(2,8)->Range->InsertAfter(WideString("P��ij,\n���"));
   rr=Word_Table->Cell(2,8)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //Ktpij ij
   Word_Table->Cell(1,9)->Range->InsertAfter(WideString("ktpij"));
   rr=Word_Table->Cell(1,9)->Range;
   o=wdCharacter;
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //V ij
   Word_Table->Cell(1,10)->Range->InsertAfter(WideString("Vij,\n�3"));
   rr=Word_Table->Cell(1,10)->Range;
   o=wdCharacter;
   oo=-4;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=4;
   rr->MoveEnd(o,oo);
   oo=5;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //N ij
   Word_Table->Cell(1,11)->Range->InsertAfter(WideString("Nij,\n���"));
   rr=Word_Table->Cell(1,11)->Range;
   o=wdCharacter;
   oo=-6;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   //� ij
   Word_Table->Cell(1,12)->Range->InsertAfter(WideString("�ij,\n���/�3"));
   rr=Word_Table->Cell(1,12)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=8;
   rr->MoveEnd(o,oo);
   oo=9;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //� j
   Word_Table->Cell(1,13)->Range->InsertAfter(WideString("�j,\n���/�3"));
   rr=Word_Table->Cell(1,13)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=8;
   rr->MoveEnd(o,oo);
   oo=8;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //� ����j
   Word_Table->Cell(1,14)->Range->InsertAfter(WideString("�����j,\n���/�3"));
   rr=Word_Table->Cell(1,14)->Range;
   o=wdCharacter;
   oo=-8;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;
   oo=8;
   rr->MoveEnd(o,oo);
   oo=12;
   rr->MoveStart(o,oo);
   rr->Font->Superscript=true;

   //Delta j
   ws="j,\n%";
   Word_Table->Cell(1,15)->Range->InsertAfter(ws);
   rr=Word_Table->Cell(1,15)->Range;
   o=wdCharacter;
   oo=-5;
   rr->MoveEnd(o,oo);
   o=True;
   oo=EmptyParam;
   ll=0x03B4;//delta
   rr->InsertSymbol(ll,oo,o,Template);
   o=wdCharacter;
   oo=2;
   rr->MoveEnd(o,oo);
   oo=1;
   rr->MoveStart(o,oo);
   rr->Font->Subscript=true;

   Word_Table->Cell(1,15)->Merge(Word_Table->Cell(2,15));
   Word_Table->Cell(1,14)->Merge(Word_Table->Cell(2,14));
   Word_Table->Cell(1,13)->Merge(Word_Table->Cell(2,13));
   Word_Table->Cell(1,12)->Merge(Word_Table->Cell(2,12));
   Word_Table->Cell(1,11)->Merge(Word_Table->Cell(2,11));
   Word_Table->Cell(1,10)->Merge(Word_Table->Cell(2,10));
   Word_Table->Cell(1,9)->Merge(Word_Table->Cell(2,9));
   Word_Table->Cell(1,7)->Merge(Word_Table->Cell(1,8));
   Word_Table->Cell(1,5)->Merge(Word_Table->Cell(1,6));
   Word_Table->Cell(1,3)->Merge(Word_Table->Cell(1,4));
   Word_Table->Cell(1,2)->Merge(Word_Table->Cell(2,2));
   Word_Table->Cell(1,1)->Merge(Word_Table->Cell(2,1));

   //Add data to table
   Step=60/pc;
   int to=3;
   for(int ii=0;ii<pc;ii++)
      {
      p=(TKMXMeasurePoint*)MI->UsedPoint[ii];
      if(!p) continue;

      Position=25+Step*ii;
      ProtokolProgressForm->Gauge->Progress=Position;
      ProtokolProgressForm->Update();

      //� ������������
      s=ii+1;
      ws=s;
      Word_Table->Cell(to+1,1)->Range->InsertAfter(ws);

      //Kj
      vo=MI->PointVarOptions[TKMXMeasurePoint::K];
      v=p->Var[TKMXMeasurePoint::K];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(to+1,13)->Range->InsertAfter(ws);

      //K����
      vo=MI->PointVarOptions[TKMXMeasurePoint::K_mx];
      v=p->Var[TKMXMeasurePoint::K_mx];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(to+1,14)->Range->InsertAfter(ws);

      //delta
      vo=MI->PointVarOptions[TKMXMeasurePoint::d];
      v=p->Var[TKMXMeasurePoint::d];
      vo->FormatStr(v,s);
      ws=s;
      Word_Table->Cell(to+1,15)->Range->InsertAfter(ws);

      int ccc=p->Count;
      for(int j=0;j<ccc;j++)
         {
         m=(TKMXMeasure*)(p->Measures[j]);
         if(m->Var[TKMXMeasure::Used]->Value.b)
            {
            to++;
            s=m->Var[TKMXMeasure::Number]->Value.i;
            ws=s;
            Word_Table->Cell(to,2)->Range->InsertAfter(ws);

            vo=MI->MeasureVarOptions[TKMXMeasure::Q];
            v=m->Var[TKMXMeasure::Q];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,3)->Range->InsertAfter(ws);

            //f
            vo=MI->MeasureVarOptions[TKMXMeasure::f];
            v=m->Var[TKMXMeasure::f];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,4)->Range->InsertAfter(ws);

            //t TPU
            vo=MI->MeasureVarOptions[TKMXMeasure::t_TPU];
            v=m->Var[TKMXMeasure::t_TPU];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,5)->Range->InsertAfter(ws);

            //P TPU
            vo=MI->MeasureVarOptions[TKMXMeasure::P_TPU];
            v=m->Var[TKMXMeasure::P_TPU];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,6)->Range->InsertAfter(ws);

            //t
            vo=MI->MeasureVarOptions[TKMXMeasure::t];
            v=m->Var[TKMXMeasure::t];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,7)->Range->InsertAfter(ws);
            //P
            vo=MI->MeasureVarOptions[TKMXMeasure::P];
            v=m->Var[TKMXMeasure::P];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,8)->Range->InsertAfter(ws);

            //Ktpij
            vo=MI->MeasureVarOptions[TKMXMeasure::Ktp];
            v=m->Var[TKMXMeasure::Ktp];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,9)->Range->InsertAfter(ws);

            //V
            vo=MI->MeasureVarOptions[TKMXMeasure::V];
            v=m->Var[TKMXMeasure::V];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,10)->Range->InsertAfter(ws);

            //N
            vo=MI->MeasureVarOptions[TKMXMeasure::Pulses];
            v=m->Var[TKMXMeasure::Pulses];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,11)->Range->InsertAfter(ws);

            //K
            vo=MI->MeasureVarOptions[TKMXMeasure::K];
            v=m->Var[TKMXMeasure::K];
            vo->FormatStr(v,s);
            ws=s;
            Word_Table->Cell(to,12)->Range->InsertAfter(ws);
            }//End add measure data
         }//End iterate point
      }
   int TempRowCounter=RowCount;
   for(int ii=pc-1;ii>=0;ii--)
      {
      p=(TKMXMeasurePoint*)MI->UsedPoint[ii];
      if(!p) continue;
      int CurrentPointMeasureCount=0;
      int ccc=p->Count;
      for(int j=0;j<ccc;j++)
         {
         m=(TKMXMeasure*)(p->Measures[j]);
         if(m->Var[TMeasure::Used]->Value.b)
            CurrentPointMeasureCount++;
         }
      Word_Table->Cell(TempRowCounter-CurrentPointMeasureCount+1,15)->Merge(
               Word_Table->Cell(TempRowCounter,15));

      Word_Table->Cell(TempRowCounter-CurrentPointMeasureCount+1,14)->Merge(
               Word_Table->Cell(TempRowCounter,14));

      Word_Table->Cell(TempRowCounter-CurrentPointMeasureCount+1,13)->Merge(
               Word_Table->Cell(TempRowCounter,13));

      Word_Table->Cell(TempRowCounter-CurrentPointMeasureCount+1,1)->Merge(
               Word_Table->Cell(TempRowCounter,1));
      TempRowCounter-=CurrentPointMeasureCount;
      }
   Word_Table->AutoFitBehavior(wdAutoFitContent);

   Position=85;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Label1->Caption="������������ ����������...";
   ProtokolProgressForm->Update();
//---------------------------------------------------------------------------
// ����������
//---------------------------------------------------------------------------
   oo=WD->Characters->Count-1;
   o=oo;

   WD->Range(o,oo)->InsertAfter(WideString("\n3. ����������: "));
   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->Font->Size=12;
   oo=WD->Characters->Count-1;
   o=oo;

   ItemIndex = EmptyParam;
   ItemsCount=True;
   ll=0x03B4;//delta
   WD->Range(o,oo)->InsertSymbol(ll,ItemIndex,ItemsCount,Template);

   oo=WD->Characters->Count-1;
   o=oo;
   WD->Range(o,oo)->InsertAfter(WideString("max="));
   oo=WD->Characters->Count-2;
   o=oo-3;
   WD->Range(o,oo)->Font->Subscript=true;;

   oo=WD->Characters->Count-1;
   o=oo;

   if(MI->Var[TKMX::d_max].Value.d >=0)
      ws="+";
   else
      ws="-";
   vo=MI->MIVarOptions[TKMX::d_max];
   v=&MI->Var[TKMX::d_max];
   vo->FormatStr(v,s);
   ws+=s;
   ws+="%. �� ����� � ���������� ������������. \n\n���� ���������� ���: ";
   ws+=MI->Options.Date.DateString();
   ws+="\n\n������� ���, ����������� ���:\n";
   WD->Range(o,oo)->InsertAfter(ws);



   oo=WD->Characters->Count-1;
   WD->Range(o,oo)->ParagraphFormat->Alignment = wdAlignParagraphLeft;
   oo=WD->Characters->Count-1;
   o=oo;

   Word_Table=WD->Tables->Add(WD->Range(o,o),6,4);
   Word_Table->Range->Font->Size=12;
   Word_Table->Borders->Enable = false;
   Word_Table->Range->ParagraphFormat->Alignment=wdAlignParagraphCenter;
   Word_Table->Range->Cells->VerticalAlignment=wdCellAlignVerticalBottom;

   ws="\n�� ��������� �����������";
   Word_Table->Cell(1,1)->Range->InsertAfter(ws);
   Word_Table->Cell(1,1)->Range->ParagraphFormat->Alignment=wdAlignParagraphLeft;

   ws="\n�� ������� �������";
   Word_Table->Cell(3,1)->Range->InsertAfter(ws);
   Word_Table->Cell(3,1)->Range->ParagraphFormat->Alignment=wdAlignParagraphLeft;

   ws="\n�� ����������� �������";
   Word_Table->Cell(5,1)->Range->InsertAfter(ws);
   Word_Table->Cell(5,1)->Range->ParagraphFormat->Alignment=wdAlignParagraphLeft;

   Word_Table->Rows->Item(2)->Range->Font->Size=8;
   Word_Table->Rows->Item(4)->Range->Font->Size=8;
   Word_Table->Rows->Item(6)->Range->Font->Size=8;

   ws="(���������)";
   Word_Table->Cell(2,2)->Range->InsertAfter(ws);
   Word_Table->Cell(4,2)->Range->InsertAfter(ws);
   Word_Table->Cell(6,2)->Range->InsertAfter(ws);

   ws="(�������, ��������)";
   Word_Table->Cell(2,3)->Range->InsertAfter(ws);
   Word_Table->Cell(4,3)->Range->InsertAfter(ws);
   Word_Table->Cell(6,3)->Range->InsertAfter(ws);

   ws="(�������)";
   Word_Table->Cell(2,4)->Range->InsertAfter(ws);
   Word_Table->Cell(4,4)->Range->InsertAfter(ws);
   Word_Table->Cell(6,4)->Range->InsertAfter(ws);

   //Insert values
   ws="\n";
   ws+=MI->Options.Service.Rank;
   Word_Table->Cell(1,2)->Range->InsertAfter(ws);

   ws="\n";
   ws+=MI->Options.Service.FIO;
   Word_Table->Cell(1,3)->Range->InsertAfter(ws);

   ws="\n";
   ws+=MI->Options.Seller.Rank;
   Word_Table->Cell(3,2)->Range->InsertAfter(ws);

   ws="\n";
   ws+=MI->Options.Seller.FIO;
   Word_Table->Cell(3,3)->Range->InsertAfter(ws);

   ws="\n";
   ws+=MI->Options.Buyer.Rank;
   Word_Table->Cell(5,2)->Range->InsertAfter(ws);

   ws="\n";
   ws+=MI->Options.Buyer.FIO;
   Word_Table->Cell(5,3)->Range->InsertAfter(ws);

   Word_Table->Cell(1,2)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(1,3)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(1,4)->Borders->Item(wdBorderBottom)->Visible=True;

   Word_Table->Cell(3,2)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(3,3)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(3,4)->Borders->Item(wdBorderBottom)->Visible=True;

   Word_Table->Cell(5,2)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(5,3)->Borders->Item(wdBorderBottom)->Visible=True;
   Word_Table->Cell(5,4)->Borders->Item(wdBorderBottom)->Visible=True;

   Position=100;
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
   ProtokolProgressForm->Gauge->Progress=Position;
   ProtokolProgressForm->Update();
   WA->GetDefaultInterface()->Visible = true;
   WD->Disconnect();
   WA->Disconnect();
   delete WD;
   delete WA;
   }

catch (...)
   {
   ProtokolProgressForm->Hide();
   MessageDlg("������ ������ � MS Word", mtError, TMsgDlgButtons() << mbYes, 0);
   WA->GetDefaultInterface()->Visible = true;
   WD->Disconnect();
   WA->Disconnect();
   delete WA;
   delete WD;
   return;
   }
   ProtokolProgressForm->Completed();
}
//---------------------------------------------------------------------------






