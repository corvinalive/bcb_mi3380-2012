//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop


#include "SelectKMXForm_unit.h"
#include "KMX_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSelectKMXForm *SelectKMXForm;
//---------------------------------------------------------------------------
__fastcall TSelectKMXForm::TSelectKMXForm(TComponent* Owner)
   : TForm(Owner)
{
   Data = new TList;
   Data1 = new TList;
   h=ListBox1->Canvas->TextHeight("A");
   ListBox1->ItemHeight=6+(h+2)*3;
}
//---------------------------------------------------------------------------
int __fastcall TSelectKMXForm::Select(IStorage* Ro,IStorage* MIRo)
{
   MIRoot=MIRo;
   Root=Ro;
   if(!MIRoot) return 0;
   if(!Root) return 0;
   IsManage=false;
   Button2->Visible=true;
   Button1->Caption="������� ���";
   OpenButton->Visible=false;
   DeleteButton->Visible=false;
   CopyButton->Visible=false;
   Caption="����� ��� ��� ��������";
   Edit1->Text="";

   FillData();
   if(Data->Count)
      {
      ListBox1->ItemIndex=0;
      Button1->Enabled=true;
      }
   else
      Button1->Enabled=false;
   ListBox1Click(this);
   int r= ShowModal();
   int ret=0;
   if( (r==mrOk) && (ListBox1->ItemIndex!=-1)&& (ListBox1->ItemIndex < Data->Count) )
      ret=((TKMX_data*)(Data->Items[ListBox1->ItemIndex]))->Options->version;
   ClearList();
   return ret;

}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::FillData()
{
   ClearList();
   TKMX_data* d;
	IEnumSTATSTG* Enum;
	HRESULT res = MIRoot->EnumElements(0,0,0,&Enum);
	if(res!=S_OK) return;
	Enum->Reset();
	STATSTG sts;
	IMalloc* m;
	res = CoGetMalloc(1,&m);
	if(res!=S_OK)
		{
		Enum->Release();
		return;
		}
	sts.pwcsName =(wchar_t*) m->Alloc(500);
   IStorage* st;
	while(Enum->Next(1,&sts,0)==S_OK)
		{
      HRESULT res=MIRoot->OpenStorage(sts.pwcsName,0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0, &st);
   	if(res!=S_OK) continue;

      d = new TKMX_data;
      d->Prover = new TProver;
      d->TPR = new TTPR;
      d->ProverMX = new TList();
      d->Options = new TKMXOptions;
      d->TPRMXList = new TList();
      try
         {
         TKMX::GetInfo(st,Root,d);
         }
      catch(...)
         {
         delete d->Prover;
         delete d->TPR;
         delete d->ProverMX;
         delete d->Options;
         delete d->TPRMXList;
         delete d;
         st->Release();
         continue;
         }
      AnsiString s=sts.pwcsName;
      d->Options->version=s.ToInt();
      Data->Add(d);
      st->Release();
		}
	m->Free(sts.pwcsName);
	m->Release();
	Enum->Release();

   Sort();
   Data1->Assign(Data);
   FillGrid();
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::ClearList()
{
   int c=Data1->Count;
   TKMX_data* d;
   for(int i=0;i<c;i++)
      {
      d = (TKMX_data*)(Data1->Items[i]);
      if(!d) continue;
      delete d->Prover;
      delete d->TPR;
      delete d->Options;
      int cc=d->ProverMX->Count;
      for(int ii=0;ii<cc;ii++)
         delete (TProverMX*)(d->ProverMX->Items[ii]);
      delete d->ProverMX;
      cc=d->TPRMXList->Count;
      for(int ii=0;ii<cc;ii++)
         delete (TTPRMX*)(d->TPRMXList->Items[ii]);
      delete d->TPRMXList;

      delete d;
      }
   Data1->Clear();
   Data->Clear();

   ListBox1->Items->Clear();
}
//---------------------------------------------------------------------------
__fastcall TSelectKMXForm::~TSelectKMXForm()
{
   ClearList();
   delete Data;
   delete Data1;
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::ListBox1DrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
   if(Index>=Data->Count) return;
   TKMX_data* d;
   d = static_cast<TKMX_data*>(Data->Items[Index]);
   if(State.Contains(odSelected))
      ListBox1->Canvas->Brush->Color=clSkyBlue;
   else
      ListBox1->Canvas->Brush->Color=ListBox1->Color;
   ListBox1->Canvas->FillRect(Rect);
   AnsiString s;
   //Fill list with �������
   s="���� ���: ";
   ListBox1->Canvas->TextOut(Rect.left+4,Rect.top+4,s);
   ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   s=d->Options->Date.DateString();
   ListBox1->Canvas->TextOut(Rect.left+86,Rect.top+4,s);
   s="��� ���";
   ListBox1->Canvas->Font->Style=TFontStyles();
   ListBox1->Canvas->TextOut(Rect.left+4,Rect.top+4+h+2,s);
   ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   s=d->TPR->Model;
   s+="-";
   s+=d->TPR->D;
   ListBox1->Canvas->TextOut(Rect.left+86,Rect.top+4+h+2,s);

   ListBox1->Canvas->Font->Style=TFontStyles();
   s="���. �";
   ListBox1->Canvas->TextOut(Rect.left+190,Rect.top+4+h+2,s);

   ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   s=d->TPR->SN;
   ListBox1->Canvas->TextOut(Rect.left+230,Rect.top+4+h+2,s);

   s="���";
   ListBox1->Canvas->Font->Style=TFontStyles();
   ListBox1->Canvas->TextOut(Rect.left+4,Rect.top+4+(h+2)*2,s);
   ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   s=d->Prover->Name;
   ListBox1->Canvas->TextOut(Rect.left+86,Rect.top+4+(h+2)*2,s);

   ListBox1->Canvas->Font->Style=TFontStyles();
   s="���. �";
   ListBox1->Canvas->TextOut(Rect.left+190,Rect.top+4+(h+2)*2,s);

   ListBox1->Canvas->Font->Style=TFontStyles()<< fsBold;
   s=d->Prover->SN;
   ListBox1->Canvas->TextOut(Rect.left+230,Rect.top+4+(h+2)*2,s);
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::ListBox1Click(TObject *Sender)
{
   int i=ListBox1->ItemIndex;
   if((i>=Data->Count) ||(i==-1))
      {//Clear info
      ProverLabel->Caption="";
      ProverSNLabel->Caption="";
      ProductLabel->Caption="";
      LocationLabel->Caption="";
      DescriptionLabel->Caption="";
      SensorNameLabel->Caption="";
      RFTNameLabel->Caption="";
      MMLocationLabel->Caption="";
      OwnerLabel->Caption="";
      SensorSNLabel->Caption="";
      }
   else
      {//Fill info
      TKMX_data* d;
      d = (TKMX_data*)(Data->Items[i]);
      ProverLabel->Caption=d->Prover->Name;
      ProverSNLabel->Caption=d->Prover->SN;

      ProductLabel->Caption=d->Options->ProductName;
      LocationLabel->Caption=d->Options->Location;
      DescriptionLabel->Caption=d->Options->Description;
      SensorNameLabel->Caption=d->TPR->Model;
      SensorSNLabel->Caption=d->TPR->SN;
      RFTNameLabel->Caption=d->TPR->D;
      MMLocationLabel->Caption=d->TPR->Location;
      OwnerLabel->Caption=d->TPR->Owner;
      }
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::ListBox1DblClick(TObject *Sender)
{
   if( (!IsManage)&&(ListBox1->ItemIndex!=-1)&& (ListBox1->ItemIndex < Data->Count) )
      {
      ModalResult=mrOk;
      }
}
//---------------------------------------------------------------------------
int __fastcall TSelectKMXForm::Manage(IStorage* Ro, IStorage* MIRo)
{
   MIRoot=MIRo;
   Root=Ro;
   if(!MIRoot) return 0;
   if(!Root) return 0;
   IsManage=true;
   Button2->Visible=false;
   Button1->Caption="�������";
   OpenButton->Visible=true;
   Caption="���������� ������� ��� ��� �� ��";
   Edit1->Text="";
   DeleteButton->Visible=true;
   CopyButton->Visible=true;
   FillData();
   Button1->Enabled=true;
   if(Data->Count)
      {
      ListBox1->ItemIndex=0;
      OpenButton->Enabled=true;
      DeleteButton->Enabled=true;
      CopyButton->Enabled=true;
      }
   else
      {
      OpenButton->Enabled=false;
      DeleteButton->Enabled=false;
      CopyButton->Enabled=false;
      }
   ListBox1Click(this);

   int r= ShowModal();
   int ret=0;
   if( (r==mrYes) && (ListBox1->ItemIndex!=-1)&& (ListBox1->ItemIndex < Data->Count) )
      ret=((TKMX_data*)(Data->Items[ListBox1->ItemIndex]))->Options->version;
   ClearList();
   return ret;
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::OpenButtonClick(TObject *Sender)
{
   if((ListBox1->ItemIndex!=-1)&& (ListBox1->ItemIndex < Data->Count) )
      ModalResult=mrYes;
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::DeleteButtonClick(TObject *Sender)
{
   AnsiString s("������������� ������� ���:\n\n��� ���\t");
   int i=ListBox1->ItemIndex;
   if((i>=Data->Count) ||(i==-1))
      return;
   TKMX_data* d;
   d = static_cast<TKMX_data*>(Data->Items[i]);
   s+=d->TPR->Model;
   s+="\n���. �\t";
   s+=d->TPR->SN;

   s+="\n���� ���\t";
   s+=d->Options->Date.DateString();

   if(Application->MessageBox(s.c_str(),"�������� ���",MB_YESNO|MB_ICONSTOP)==IDYES)
      {//delete
    	MIRoot->DestroyElement(WideString(((TKMX_data*)(Data->Items[i]))->Options->version ));
      FillData();
      Edit1Change(0);
      if((ListBox1->ItemIndex==-1) && (Data->Count))
         {
         ListBox1->ItemIndex=0;
         ListBox1->ItemIndex=0;
         OpenButton->Enabled=true;
         DeleteButton->Enabled=true;
         CopyButton->Enabled=true;
         }
      else
         {
         OpenButton->Enabled=false;
         DeleteButton->Enabled=false;
         CopyButton->Enabled=false;
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::CopyButtonClick(TObject *Sender)
{
   AnsiString s("����������� ���:\n\n��� ���\t");
   int i=ListBox1->ItemIndex;
   if((i>=Data->Count) ||(i==-1))
      return;
   TKMX_data* d;
   d = (TKMX_data*)(Data->Items[i]);
   s+=d->TPR->Model;
   s+="\n���. �\t";
   s+=d->TPR->SN;

   s+="\n���� ���\t";
   s+=d->Options->Date.DateString();

   if(Application->MessageBox(s.c_str(),"����������� ���",MB_YESNO|MB_ICONSTOP)==IDYES)
      {//Copy
      IStorage* stFrom;
      IStorage* stTo;
      //Open From Storage:
      WideString ws(((TKMX_data*)(Data->Items[i]))->Options->version );
      HRESULT res=MIRoot->OpenStorage(ws,0,OF_READ|STGM_SHARE_EXCLUSIVE,0,0, &stFrom);
	   if(res!=S_OK)
         {
         AnsiString s="�� ���� ������� �������� Storage ";
         s+=ws;
         s+="\n��� ������ ";
         s+=res;
         Application->MessageBox(s.c_str(),"����������� ���",MB_ICONSTOP);
         return;
         }

      //Create to storage
      ws=random(MaxInt);
      res=MIRoot->CreateStorage(ws,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0, &stTo);
	   if(res!=S_OK)
         {
         stFrom->Release();
         AnsiString s="�� ���� ������� ����� Storage ";
         s+=ws;
         s+="\n��� ������ ";
         s+=res;
         Application->MessageBox(s.c_str(),"����������� ���",MB_ICONSTOP);
         return;
         }
      res=stFrom->CopyTo(NULL,NULL,NULL,stTo);
	   if(res!=S_OK)
         {
         AnsiString s="����� �����������\n��� ������ ";
         s+=res;
         Application->MessageBox(s.c_str(),"����������� ���",MB_ICONSTOP);
         }
      else
         Application->MessageBox("��� ����������","����������� ���",MB_OK|MB_ICONINFORMATION);

      stTo->Release();
      stFrom->Release();
      FillData();
      Edit1Change(0);
      }
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
int __fastcall KMXCompareNames(void *Item1, void *Item2)
{
   TKMX_data* p1=(TKMX_data*)(Item1);
   TKMX_data* p2=(TKMX_data*)(Item2);
   int i=0;
   if (p1->Options->Date > p2->Options->Date) i=-1;
   if (p1->Options->Date < p2->Options->Date) i=1;
   return i;
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::Sort()
{
   Data->Sort(KMXCompareNames);
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::Edit1Change(TObject *Sender)
{
   if(Edit1->Text.IsEmpty())
      {
      Data->Assign(Data1);
      Sort();
      FillGrid();
      }
   else
      {
      Data->Clear();
      int c=Data1->Count;
      for(int i=0;i<c;i++)
         {
         TKMX_data* p1=(TKMX_data*)Data1->Items[i];
         if(((AnsiString)(p1->Prover->Name)).UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
        if(((AnsiString)(p1->Prover->SN)).UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->TPR->Model)).UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->TPR->SN)).UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
         if(p1->Options->Date.DateString().UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->TPR->Location)).UpperCase().Pos(Edit1->Text))
            {
            Data->Add(p1);
            continue;
            }
         }
      Sort();
      FillGrid();
      }
}
//---------------------------------------------------------------------------
void __fastcall TSelectKMXForm::FillGrid()
{
   int c=Data->Count;
   ListBox1->Clear();
   for(int i=0;i<c;i++)
      ListBox1->Items->Add(AnsiString(i));
}
//---------------------------------------------------------------------------

