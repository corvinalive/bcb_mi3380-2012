//---------------------------------------------------------------------------

#ifndef KMXMeasure_unitH
#define KMXMeasure_unitH
//---------------------------------------------------------------------------
#include "variable.h"
//---------------------------------------------------------------------------
class TKMXMeasure:public TVarContainer
{
public:
   static const VarCount=29;
   static ReadVarCount;

   static const Used    =0;//������������� ������� ��������
   static const Number  =1;//����� ���������
   static const Q       =2;//������ �3/�
   static const Qp      =3;//������ %

   static const IDMX    =4;//������ ������������� ������ (ProverMX)
   static const Time    =5;//����� ����������� ������
   static const t_TPU   =6;
   static const P_TPU   =7;
   static const V       =8;//Vij

   static const f       =9;//�������
   static const t       =10;//t ��
   static const P       =11;//P ��
   static const Pulses  =12;//���������� ���������
   static const K       =13;//

   static const Dens    =14;
   static const t_FD    =15;
   static const P_FD    =16;
   static const Period  =17; //������ ���. ������� ����������

//   static const visc    =18;//��������

   static const Ktp     =18;//Ktp
   static const Kt      =19;//Kt
   static const Kp      =20;//Kp
   static const Kgt     =21;//Kgp
   static const Kgp     =22;//Kgp

   static const Betta   =23;//
   static const F       =24;//
   static const d       =25;//����������� ���������������
   static const Desc    =26; //��������
   static const K_mx    =27;//����������� �� �� �������� �-��
   static const dK      =28;//����������� �� �� �������� �-��

private:
   TVar FVar[VarCount];
   virtual TVar* __fastcall GetVar(int __index);
   virtual int __fastcall GetVarCount(){return VarCount;};
public:
   __fastcall TKMXMeasure();
   virtual void __fastcall Load(IStream* Stream);
   virtual void __fastcall Save(IStream* Stream);
};
//---------------------------------------------------------------------------
#endif
