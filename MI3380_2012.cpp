//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop

#include "mainform_unit.h"
//---------------------------------------------------------------------------
USEFORM("AboutBox_unit.cpp", AboutBox);
USEFORM("CalculateFreqForm_unit.cpp", CalculateFreqForm);
USEFORM("CalculateOptionsForm_unit.cpp", CalculateOptionsForm);
USEFORM("KMXForm_unit.cpp", KMXForm);
USEFORM("KMXOptionsForm_unit.cpp", KMXOptionsForm);
USEFORM("MainForm_unit.cpp", MainForm);
USEFORM("MIForm_unit.cpp", MIForm);
USEFORM("OptionsForm_unit.cpp", OptionsForm);
USEFORM("PropertiesForm_unit.cpp", PropertiesForm);
USEFORM("ProtokolProgressForm_unit.cpp", ProtokolProgressForm);
USEFORM("ProverTools\ProverMXForm_unit.cpp", ProverMXForm);
USEFORM("ProverTools\ProverPropertiesForm_unit.cpp", ProverPropertiesForm);
USEFORM("SelectKMXForm_unit.cpp", SelectKMXForm);
USEFORM("SelectMIForm_unit.cpp", SelectMIForm);
USEFORM("ProverTools\SelectProverForm_unit.cpp", SelectProverForm);
USEFORM("SetupViewVariablesForm_unit.cpp", SetupViewVariablesForm);
USEFORM("TPRTools\TPRForm_unit.cpp", TPRForm);
USEFORM("TPRTools\TPRMXForm_unit.cpp", TPRMXForm);
USEFORM("TPRTools\TPRPointForm_unit.cpp", TPRPointForm);
USEFORM("TPRTools\TPRRangeForm_unit.cpp", TPRRangeForm);
USEFORM("TPRTools\TPRSelectForm_unit.cpp", TPRSelectForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
      {

      Application->Initialize();
      Application->Title = "�� 1974-2004";
//       Application->HelpFile = "";
         Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->CreateForm(__classid(TAboutBox), &AboutBox);
         Application->CreateForm(__classid(TCalculateFreqForm), &CalculateFreqForm);
         Application->CreateForm(__classid(TCalculateOptionsForm), &CalculateOptionsForm);
         Application->CreateForm(__classid(TKMXOptionsForm), &KMXOptionsForm);
         Application->CreateForm(__classid(TOptionsForm), &OptionsForm);
         Application->CreateForm(__classid(TPropertiesForm), &PropertiesForm);
         Application->CreateForm(__classid(TProtokolProgressForm), &ProtokolProgressForm);
         Application->CreateForm(__classid(TSelectKMXForm), &SelectKMXForm);
         Application->CreateForm(__classid(TSelectMIForm), &SelectMIForm);
         Application->CreateForm(__classid(TSetupViewVariablesForm), &SetupViewVariablesForm);
         Application->CreateForm(__classid(TTPRMXForm), &TPRMXForm);
         Application->CreateForm(__classid(TTPRPointForm), &TPRPointForm);
         Application->CreateForm(__classid(TTPRRangeForm), &TPRRangeForm);
         Application->OnShortCut=MainForm->ShortCut;
       Application->Run();
      }
   catch (Exception &exception)
      {
      Application->ShowException(&exception);
      }
   catch (...)
      {
      try
         {
         throw Exception("");
         }
      catch (Exception &exception)
         {
         Application->ShowException(&exception);
         }
      }
   return 0;
}
//---------------------------------------------------------------------------
