//---------------------------------------------------------------------------
#pragma hdrstop

#include "variable.h"
#include "prover.h"
#include "tpr.h"
//#include "Density.h"
#include "KMXPoint_unit.h"
#include "mitools_unit.h"
#include <math.h>
#include "MI1974_unit.h"
#include "KMX_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

int TKMX::ReadVarCount;
//---------------------------------------------------------------------------
//--- implemetation TKMX ----------------------------------------------------
//---------------------------------------------------------------------------
__fastcall TKMX::TKMX(TKMX_data* data):TGridData(TMI1974::CommonData)
{
   TPRMXList=NULL;
   int tprmxc=data->TPRMXList->Count;
   bool Founded=false;
   for(int i=0;i<tprmxc;i++)
      {
      TTPRMX* mx=(TTPRMX*)data->TPRMXList->Items[i];
      if(mx->ID==data->Options->TPRMXID)
         {
         memcpy(&TPRMX,mx,sizeof(TTPRMX));
         Founded=true;
         }
//      delete mx;
      }
   TPRMXList=data->TPRMXList;
   if(Founded==false)
      throw(Exception("TKMX::TKMX\n�� ������� �� ���"));

   FMeasurePoints = new TList();
   FOnChange=NULL;
   FOnChangeItemsStructure=NULL;
   LoadVarOptions();//FromIni(f);
   memcpy(&Prover,data->Prover,sizeof(TProver));
   memcpy(&tpr,data->TPR,sizeof(TTPR));
   memcpy(&Options,data->Options,sizeof(TKMXOptions));
   ProverMX = data->ProverMX;
   //Make ident Options.ProverID & IDMX
   IDMX[0]=0;
   IDMX[1]=0;
   IDMX[2]=0;
   IDMX[3]=0;
   TProverMX* x=0;
   int mxc=ProverMX->Count;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      for(int j=0;j<mxc;j++)
         {
         x=(TProverMX*)(ProverMX->Items[j]);
         if(x->ID==Options.ProverMXID[i]) break;
         }
      IDMX[i]=x;
      }
   //Create string list
   TVarOptions* vo=&FMeasureVarOptions[TKMXMeasure::IDMX];
   vo->Strings=new TStringList();
   for(int i=0;i<4;i++)
      {
      if(IDMX[i]==0) break;
      vo->Strings->Add(IDMX[i]->Index);
      }

   SetupColumns();
   Add(new TKMXMeasurePoint());
   Calculate();
}
//---------------------------------------------------------------------------
__fastcall TKMX::TKMX(IStorage* KMXStorage, IStorage* Root):TGridData(TMI1974::CommonData)
{
   TPRMXList=0;
   FOnChange=NULL;
   FOnChangeItemsStructure=NULL;
   FMeasurePoints = new TList();

   LoadVarOptions();//FromIni(f);

   //Load IDs
   HRESULT res;
   IStream* Stream;
   res=KMXStorage->OpenStream(WideString("IDs"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
      throw Exception("Error open stream IDs");
      }

   Stream->Read(&Prover.ID,sizeof(int),0);
   Stream->Read(&tpr.ID,sizeof(int),0);
   Stream->Release();

   //Load Options
   res=KMXStorage->OpenStream(WideString("Options"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
      throw Exception("Error open stream Options");
      }
   Stream->Read(&Options,sizeof(TKMXOptions),0);
   Stream->Release();


   res=KMXStorage->OpenStream(WideString("KMXData"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
      throw Exception("Error open stream KMXData");
      }
   //Load VarCount
   Stream->Read(&ReadVarCount,sizeof(int),0);
   Stream->Read(&TKMXMeasurePoint::ReadVarCount,sizeof(int),0);
   Stream->Read(&TKMXMeasure::ReadVarCount,sizeof(int),0);

   //Load KMX vars
   Stream->Read(Var,ReadVarCount*sizeof(TVar),0);

   int pc=Options.ProductIndex;
   MeasureVarOptions[TKMXMeasure::Betta]->Kind=TVarOptions::CalculatedData;
   MeasureVarOptions[TKMXMeasure::F]->Kind=TVarOptions::CalculatedData;

   //Load VarOptions
   for(int i=0;i<TKMXMeasure::ReadVarCount;i++)
      FMeasureVarOptions[i].Load(Stream);
   for(int i=0;i<TKMXMeasurePoint::ReadVarCount;i++)
      FPointVarOptions[i].Load(Stream);
   for(int i=0;i<ReadVarCount;i++)
      FMIVarOptions[i].Load(Stream);

   //Load TGridData data members
   MeasureColumns.Load(Stream);
   PointColumns.Load(Stream);
   ResColumns.Load(Stream);

   //Load points data
   int PointCount;
   Stream->Read(&PointCount,sizeof(int),0);
   TKMXMeasurePoint* m;
   for(int i=0;i<PointCount;i++)
      {//Save point data
      m=new TKMXMeasurePoint();
      m->Load(Stream);
      Add(m);
      }
   //Get TProver & TProverMX
   ProverMX = new TList();
   TMI1974::CommonData->ProverInterface.Get(Root,Prover.ID,&Prover,ProverMX);
   //Get TPR
   TPRMXList = new TList;
   TMI1974::CommonData->TPRInterface.GetTPRnMXList(Root,tpr.ID,&tpr,TPRMXList);

   int tprmxc=TPRMXList->Count;
   bool Founded=false;
   for(int i=0;i<tprmxc;i++)
      {
      TTPRMX* mx=(TTPRMX*)TPRMXList->Items[i];
      if(mx->ID==Options.TPRMXID)
         {
         memcpy(&TPRMX,mx,sizeof(TTPRMX));
         Founded=true;
         }
      }
   if(Founded==false)
      throw(Exception("TKMX::TKMX\n�� ������� �� ���"));


   //Make ident Options.ProverID & IDMX
   IDMX[0]=0;
   IDMX[1]=0;
   IDMX[2]=0;
   IDMX[3]=0;
   TProverMX* x;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      IDMX[i]=static_cast<TProverMX*>(ProverMX->Items[i]);
      }
   TVarOptions* vo=&FMeasureVarOptions[TKMXMeasure::IDMX];
   vo->Strings=new TStringList();
   for(int i=0;i<4;i++)
      {
      if(IDMX[i]==0) break;
      vo->Strings->Add(IDMX[i]->Index);
      }

   //Load string list
   TStringList* List=new TStringList();
   int c;
   Stream->Read(&c,sizeof(int),0);
   for(int i=0;i<c;i++)
      {
      char* buf;
      int l;
      Stream->Read(&l,sizeof(int),0);
      if(l)
         {
         buf=new char[l];
         Stream->Read(buf,l,0);
         List->Add(AnsiString(buf,l));
         delete[] buf;
         }
      }
   //find Text vars:: and fill stringlist
   TIntList MeasureTextIndex;
   TIntList PointTextIndex;
   TIntList MITextIndex;
   for(int i=0;i<TKMXMeasure::VarCount;i++)
      if(FMeasureVarOptions[i].Type==TVarOptions::Text) MeasureTextIndex.Add(i);
   for(int i=0;i<TKMXMeasurePoint::VarCount;i++)
      if(FPointVarOptions[i].Type==TVarOptions::Text) PointTextIndex.Add(i);
   for(int i=0;i<VarCount;i++)
      if(FMIVarOptions[i].Type==TVarOptions::Text) MITextIndex.Add(i);
   //Iterate through all items:
   PointCount=Count;
   int MeasureCount;
   TGridPoint* gp;
   for(int i=0;i<PointCount;i++)
      {//
      gp = (TKMXMeasurePoint*)GridPoints[i];
      MeasureCount = gp->Count;
      for(int j=0;j<MeasureCount;j++)
         {
         TKMXMeasure* m = (TKMXMeasure*)(gp->Measures[j]);
         int mc=MeasureTextIndex.Count;
         for(int k=0;k<mc;k++)
            {
            TVar* v=m->Var[MeasureTextIndex.Values[k]];
            if(v->Value.c)
               {
               AnsiString s=List->Strings[v->UserData.i[0]];
               int sl=s.Length();
               if(sl)
                  {
                  v->Value.c=new char[sl+1];
                  strcpy(v->Value.c,s.c_str());
                  }
               else
                  v->Value.c=0;
               }

            }
         }
      int mc=PointTextIndex.Count;
      for(int k=0;k<mc;k++)
         {
         TVar* v=gp->Var[PointTextIndex.Values[k]];
         if(v->Value.c)
            {
            AnsiString s=List->Strings[v->UserData.i[0]];
            int sl=s.Length();
            if(sl)
               {
               v->Value.c=new char[sl+1];
               strcpy(v->Value.c,s.c_str());
               }
            else
               v->Value.c=0;
            }
         }
      }
   int mc=MITextIndex.Count;
   for(int k=0;k<mc;k++)
      {
      TVar* v=&Var[MITextIndex.Values[k]];
      if(v->Value.c)
         {
         AnsiString s=List->Strings[v->UserData.i[0]];
         int sl=s.Length();
         if(sl)
            {
            v->Value.c=new char[sl+1];
            strcpy(v->Value.c,s.c_str());
            }
         else
            v->Value.c=0;
         }
      }
   delete List;
}
//---------------------------------------------------------------------------
__fastcall TKMX::~TKMX()
{
  int i=FMeasurePoints->Count;
  TVar* v;
  TVarOptions* vo;
  TVarContainer* m;
  for (int j=0;j<i;j++)
    {
    TKMXMeasurePoint* p=(TKMXMeasurePoint*)FMeasurePoints->Items[j];
    //delete text of point
    for(int i=0;i<TKMXMeasurePoint::VarCount;i++)
      {
      vo=&FPointVarOptions[i];
      v=p->Var[i];
      if(vo->Type==TVarOptions::Text)
         if(v->Value.c!=NULL)
            delete[] v->Value.c;
      }
    //delete text measures of point
    int mc=p->Count;
    for(int mi=0;mi<mc;mi++)
      {
      m= p->Measures[mi];
       for(int i=0;i<TKMXMeasure::VarCount;i++)
         {
         vo=&FMeasureVarOptions[i];
         v=m->Var[i];
         if(vo->Type==TVarOptions::Text)
            if(v->Value.c!=NULL)
               delete[] v->Value.c;
         }
      }
    delete p;
    }

   if(ProverMX)
      {
      int c=ProverMX->Count;
      for(int i=0;i<c;i++)
         delete (TProverMX*)(ProverMX->Items[i]);
      delete ProverMX;
      ProverMX = NULL;
      }

   if(TPRMXList!=NULL)
      {
      int tprmxc=TPRMXList->Count;
      for(int i=0;i<tprmxc;i++)
         delete (TTPRMX*)TPRMXList->Items[i];
      delete TPRMXList;
      }

}
//---------------------------------------------------------------------------
int __fastcall TKMX::Add(TGridPoint* GridPoint)
{
   int c = FMeasurePoints->Add(GridPoint);
   return c;
};
//---------------------------------------------------------------------------
int __fastcall TKMX::AddNew()
{
   int n=Add(new TKMXMeasurePoint());
   Calculate();
   return n;
};
//---------------------------------------------------------------------------
void __fastcall TKMX::Delete(int Index)
{
   TKMXMeasurePoint* gp=(TKMXMeasurePoint*)FMeasurePoints->Items[Index];
   FMeasurePoints->Delete(Index);
   delete gp;
   void* f=this;
   if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
}
//---------------------------------------------------------------------------
TGridPoint* __fastcall TKMX::GetGridPoints(int Index)
{
   return (TGridPoint*)FMeasurePoints->Items[Index];
};
//---------------------------------------------------------------------------
TGridPoint* __fastcall TKMX::GetUsedPoints(int Index)
{
   int c = FMeasurePoints->Count;
   int used=0;
   TGridPoint* p;
   for(int i=0;i<c;i++)
      {
      p = (TGridPoint*) FMeasurePoints->Items[i];
      if(p->Var[TKMXMeasurePoint::Used]->Value.b)
         {
         if(Index==used)
            return p;
         used++;
         }
      }
   return NULL;
};
//---------------------------------------------------------------------------
int __fastcall TKMX::GetCount()
{
   return FMeasurePoints->Count;
};
//---------------------------------------------------------------------------
TVarOptions* __fastcall TKMX::GetPointVarOptions(int index)
{
   if((index<0)||(index>=TKMXMeasurePoint::VarCount))
      return NULL;
   else return &FPointVarOptions[index];
}
//---------------------------------------------------------------------------
TVarOptions* __fastcall TKMX::GetMeasureVarOptions(int index)
{
   if((index<0)||(index>=TKMXMeasure::VarCount))
      return NULL;
   else return &FMeasureVarOptions[index];
}
//---------------------------------------------------------------------------
TVarOptions* __fastcall TKMX::GetMIVarOptions(int index)
{
   if((index<0)||(index>=VarCount))
      return NULL;
   else return &FMIVarOptions[index];
}
//---------------------------------------------------------------------------
void __fastcall TKMX::LoadVarOptionsFromIni(TIniFile* Ini)
{};
//---------------------------------------------------------------------------
//---------- not used - not used - not used ---------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TKMX::SetValue(TVar::TValue Value,TMICellData* Data)
{
   if(Data->vo->Type==TVarOptions::Text)
      {
      int c=0;
      if(Value.c) c=strlen(Value.c);
      if(c==0)
         {
         if(Data->v->Value.c)
            {
            delete[] Data->v->Value.c;
            Data->v->Value.c=0;
            }
         }
      else
         {
         if(Data->v->Value.c)
            delete[] Data->v->Value.c;
         Data->v->Value.c=new char[c+1];
         strcpy(Data->v->Value.c,Value.c);
         }
      Data->v->State=TVar::UserInput;
      }
   else
      {
      Data->v->Value=Value;
      Data->vo->ProcessRound(Data->v);
      Data->v->State=TVar::UserInput;
      Calculate();
      }
   void* vv=this;
   if(FOnChange) FOnChange((TObject*)vv);
}
//---------------------------------------------------------------------------
void __fastcall TKMX::ModifyPopupMenu(TPopupMenu* Menu,TMICellData* Data)
{
}
//---------------------------------------------------------------------------
void __fastcall TKMX::Save(IStream* Stream)
{};

void __fastcall TKMX::Save(IStorage* Storage)
{
   HRESULT res;
   //Save IDs
   IStream* Stream;
   res=Storage->OpenStream(WideString("IDs"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
	   res = Storage->CreateStream(WideString("IDs"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
         {
         Application->MessageBox("Error create stream IDs","Error save KMX",MB_ICONSTOP);
         return;
         }
      }
   res=Stream->Write(&Prover.ID,sizeof(int),0);//+
   if(res!=S_OK)
      {
      Stream->Release();
      throw Exception("Error write in TKMX::Save");
      }

   res=Stream->Write(&tpr.ID,sizeof(int),0);
   if(res!=S_OK)
      {
      Stream->Release();
      throw Exception("Error write in TKMX::Save");
      }

   res=Stream->Write(&Options.TPRMXID,sizeof(int),0);
   if(res!=S_OK)
      {
      Stream->Release();
      throw Exception("Error write in TKMX::Save");
      }

   Stream->Release();

   //Save Options
   res=Storage->OpenStream(WideString("Options"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
	   res = Storage->CreateStream(WideString("Options"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
         {
         Application->MessageBox("Error create stream Options","Error save KMX",MB_ICONSTOP);
         return;
         }
      }
   res=Stream->Write(&Options,sizeof(TKMXOptions),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");
   Stream->Release();

   //find Text vars:: and fill stringlist
   TStringList* List=new TStringList;
   TIntList MeasureTextIndex;
   TIntList PointTextIndex;
   TIntList MITextIndex;
   for(int i=0;i<TKMXMeasure::VarCount;i++)
      if(FMeasureVarOptions[i].Type==TVarOptions::Text) MeasureTextIndex.Add(i);
   for(int i=0;i<TKMXMeasurePoint::VarCount;i++)
      if(FPointVarOptions[i].Type==TVarOptions::Text) PointTextIndex.Add(i);
   for(int i=0;i<VarCount;i++)
      if(FMIVarOptions[i].Type==TVarOptions::Text) MITextIndex.Add(i);
   //Iterate through all items:
   int PointCount=Count;
   int MeasureCount;
   TGridPoint* gp;
   TKMXMeasure* m;
   for(int i=0;i<PointCount;i++)
      {//
      gp = (TKMXMeasurePoint*)GridPoints[i];
      MeasureCount = gp->Count;
      for(int j=0;j<MeasureCount;j++)
         {
         TKMXMeasure* m = (TKMXMeasure*)(gp->Measures[j]);
         int mc=MeasureTextIndex.Count;
         for(int k=0;k<mc;k++)
            {
            TVar* v=m->Var[MeasureTextIndex.Values[k]];
            if(v->Value.c)
               v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
            }
         }
      int mc=PointTextIndex.Count;
      for(int k=0;k<mc;k++)
         {
         TVar* v=gp->Var[PointTextIndex.Values[k]];
         if(v->Value.c)
            v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
         }
      }
   int mc=MITextIndex.Count;
   for(int k=0;k<mc;k++)
      {
      TVar* v=&Var[MITextIndex.Values[k]];
      if(v->Value.c)
         v->UserData.i[0]=List->Add(AnsiString(v->Value.c));
      }

   //Save KMX data
   res=Storage->OpenStream(WideString("KMXData"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
	   res = Storage->CreateStream(WideString("KMXData"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
         {
         Application->MessageBox("Error create stream KMXData","Error save KMX",MB_ICONSTOP);
         return;
         }
      }
   //Save variables count
   int vc=VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");
   vc=TKMXMeasurePoint::VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");
   vc=TKMXMeasure::VarCount;
   res=Stream->Write(&vc,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");

   //Save KMX vars
   res=Stream->Write(Var,VarCount*sizeof(TVar),0);
   if(res!=S_OK) throw Exception("Error write in TMI1974::Save");

   //Save VarOPtions
   for(int i=0;i<TKMXMeasure::VarCount;i++)
      FMeasureVarOptions[i].Save(Stream);
   for(int i=0;i<TKMXMeasurePoint::VarCount;i++)
      FPointVarOptions[i].Save(Stream);
   for(int i=0;i<VarCount;i++)
      FMIVarOptions[i].Save(Stream);

   //Save TGridData data members
   MeasureColumns.Save(Stream);
   PointColumns.Save(Stream);
   ResColumns.Save(Stream);

   //Save points data
   res=Stream->Write(&PointCount,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");
   for(int i=0;i<PointCount;i++)
      {//Save point data
      gp=GridPoints[i];
      gp->Save(Stream);
      }

   //Save string list
   int c=List->Count;
   res=Stream->Write(&c,sizeof(int),0);
   if(res!=S_OK) throw Exception("Error write in TKMX::Save");
   for(int i=0;i<c;i++)
      {
      AnsiString s=List->Strings[i];
      int l=s.Length();
      res=Stream->Write(&l,sizeof(int),0);
      if(res!=S_OK) throw Exception("Error write in TKMX::Save");
      if(l)
         {
         res=Stream->Write(s.c_str(),l,0);
         if(res!=S_OK) throw Exception("Error write in TKMX::Save");
         }
      }
   Stream->Release();
   delete List;
}
//---------------------------------------------------------------------------
void __fastcall TKMX::ClearPoints()
{
  int i=FMeasurePoints->Count;
  for (int j=0;j<i;j++)
    {
    TKMXMeasurePoint* m=(TKMXMeasurePoint*)FMeasurePoints->Items[j];
    delete m;
    }
  FMeasurePoints->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TKMX::SetupColumns(void)
{
// Setup measure columns
   MeasureColumns.Clear();

   MeasureColumns.Add(TKMXMeasure::Used);
   MeasureColumns.Add(TKMXMeasure::Number);
   MeasureColumns.Add(TKMXMeasure::Q);

   MeasureColumns.Add(TKMXMeasure::f);
   if(Options.FreqInput==TKMXOptions::User)
      MeasureVarOptions[TKMXMeasure::f]->Kind=TVarOptions::InputData;
   else
      MeasureVarOptions[TKMXMeasure::f]->Kind=TVarOptions::CalculatedData;

   if(Options.ProverMXID[1]!=0)
      MeasureColumns.Add(TKMXMeasure::IDMX);

   MeasureColumns.Add(TKMXMeasure::Pulses);
   MeasureColumns.Add(TKMXMeasure::Time);

   MeasureColumns.Add(TKMXMeasure::t);
   MeasureColumns.Add(TKMXMeasure::P);
   MeasureColumns.Add(TKMXMeasure::t_TPU);
   MeasureColumns.Add(TKMXMeasure::P_TPU);
   if(Options.UseFD)
      {
      MeasureColumns.Add(TKMXMeasure::Dens);
      MeasureColumns.Add(TKMXMeasure::t_FD);
      MeasureColumns.Add(TKMXMeasure::P_FD);
      }
   else
      {
      MeasureVarOptions[TKMXMeasure::Dens]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TKMXMeasure::t_FD]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TKMXMeasure::P_FD]->Kind=TVarOptions::CalculatedData;
      }

   MeasureColumns.Add(TKMXMeasure::V);
   MeasureColumns.Add(TKMXMeasure::K);
   MeasureColumns.Add(TKMXMeasure::K_mx);
   MeasureColumns.Add(TKMXMeasure::dK);

   MeasureVarOptions[TKMXMeasure::Betta]->Kind=TVarOptions::CalculatedData;
   MeasureVarOptions[TKMXMeasure::F]->Kind=TVarOptions::CalculatedData;

   MeasureColumns.Add(TKMXMeasure::Betta);
   MeasureColumns.Add(TKMXMeasure::F);
   if(Options.ProductIndex==0)
      {
      MeasureVarOptions[TKMXMeasure::Betta]->Kind=TVarOptions::InputData;
      MeasureVarOptions[TKMXMeasure::F]->Kind=TVarOptions::InputData;
      }

   MeasureColumns.Add(TKMXMeasure::Desc);

   // Setup point columns
   PointColumns.Clear();
   PointColumns.Add(TKMXMeasurePoint::Used);
   PointColumns.Add(TKMXMeasurePoint::Number);
   PointColumns.Add(TKMXMeasurePoint::n);
   PointColumns.Add(TKMXMeasurePoint::Q);

   PointColumns.Add(TKMXMeasurePoint::f);
   PointColumns.Add(TKMXMeasurePoint::K);
   PointColumns.Add(TKMXMeasurePoint::K_mx);
   PointColumns.Add(TKMXMeasurePoint::d);
   PointColumns.Add(TKMXMeasurePoint::Notes);

   // Setup MI columns
   ResColumns.Clear();
   TKMXPack p;
   p.v.c[0]=0;// MI values;
   for(int i=0;i<VarCount;i++)
      {
      p.v.c[1]=i;
      ResColumns.Add(p.v.i);
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMX::Load(IStream* Stream)
{}
//---------------------------------------------------------------------------
void __fastcall TKMX::Calculate()
{
   int PointCount = Count;

   Var[UsedPoints].Value.i=0;
   Var[UsedPoints].State=TVar::Calculated;

   Var[d_max].Value.d=0;
   Var[d_max].State=TVar::Calculated;

   DensCount=0;
   if(Options.UseFD==true)
      Var[AverageDens].Value.d=0;
   else
      Var[AverageDens].Value.d=Options.Density;

   Var[AverageDens].State=TVar::Calculated;

   BettaCount=0;
   Var[betta_aver].Value.d=0;
   Var[betta_aver].State=TVar::Calculated;

   GammaCount=0;
   Var[gamma_aver].Value.d=0;
   Var[gamma_aver].State=TVar::Calculated;

   ValidQ=true;
   //iterate throw points
//--- Calculate Kj, d_max, SKOj, fj,Qj, AverageDens, betta 'n' gamma average
   for(int i=0;i<PointCount;i++)
      {
      CalculatePoint1(i);
      }//End iterate throw points
   //---Calculate AverageDens
   if(Options.UseFD==true)
      {
      if(DensCount==0)
         {
         Var[AverageDens].State=TVar::ErrorArg;
         }
      else
         {
         Var[AverageDens].Value.d/=DensCount;
         FMIVarOptions[AverageDens].ProcessRound(&Var[AverageDens]);
         }
      }
   //---Calculate AverageBetta
   if(BettaCount==0)
      {
      Var[betta_aver].State=TVar::ErrorArg;
      }
   else
      {
      Var[betta_aver].Value.d/=BettaCount;
      FMIVarOptions[betta_aver].ProcessRound(&Var[betta_aver]);
      }
   //---Calculate AverageGamma
   if(GammaCount==0)
      {
      Var[gamma_aver].State=TVar::ErrorArg;
      }
   else
      {
      Var[gamma_aver].Value.d/=GammaCount;
      FMIVarOptions[gamma_aver].ProcessRound(&Var[gamma_aver]);
      }
//--- Sort Point by Rate
	SortPoints();

   void* f=this;
   if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
   if(FOnChange) FOnChange((TObject*)f);
}
//---------------------------------------------------------------------------
void __fastcall TKMX::CalculatePoint1(int Index)
{
   TKMXMeasurePoint* p = (TKMXMeasurePoint*)GridPoints[Index];
   int MeasureCount = p->Count;
//Calculate MI::UsedPoints
   if(p->Var[TKMXMeasurePoint::Used]->Value.b)
      {
      Var[UsedPoints].Value.i++;
      p->Var[TKMXMeasurePoint::Number]->Value.i=Var[UsedPoints].Value.i;
      p->Var[TKMXMeasurePoint::Number]->State=TVar::Calculated;
      }
   else
      {
      p->Var[TKMXMeasurePoint::Number]->Value.i=0;
      p->Var[TKMXMeasurePoint::Number]->State=TVar::NotUsed;
      }
//Calculate Q
   p->Var[TKMXMeasurePoint::Q]->Value.d=0;
   p->Var[TKMXMeasurePoint::Q]->State=TVar::Calculated;

   p->Var[TKMXMeasurePoint::Qp]->Value.d=0;
   p->Var[TKMXMeasurePoint::Qp]->State=TVar::Calculated;

//-------------------------------------
   p->Var[TKMXMeasurePoint::n]->Value.i=0;
   p->Var[TKMXMeasurePoint::n]->State=TVar::Calculated;
   p->Var[TKMXMeasurePoint::K]->Value.d=0;
   p->Var[TKMXMeasurePoint::K]->State=TVar::Calculated;
   p->Var[TKMXMeasurePoint::f]->Value.d=0;
   p->Var[TKMXMeasurePoint::f]->State=TVar::Calculated;

   p->Var[TKMXMeasurePoint::K_mx]->Value.d=0;
   p->Var[TKMXMeasurePoint::K_mx]->State=TVar::Calculated;
//---Calculates Qj, Kj, fj, Kmx j
   for(int j=0; j<MeasureCount; j++)
      {
      CalculateMeasure1(p,j);
      }
//Calculate Q
   if(p->Var[TKMXMeasurePoint::n]->Value.i)
      {
      p->Var[TKMXMeasurePoint::Q]->Value.d/=p->Var[TKMXMeasurePoint::n]->Value.i;
      FPointVarOptions[TKMXMeasurePoint::Q].ProcessRound(p->Var[TKMXMeasurePoint::Q]);
      }
   else
      p->Var[TKMXMeasurePoint::Q]->State=TVar::ErrorArg;

   if(tpr.Qmax>0)
      {
      p->Var[TKMXMeasurePoint::Qp]->Value.d=100*p->Var[TKMXMeasurePoint::Q]->Value.d/tpr.Qmax;
      FPointVarOptions[TKMXMeasurePoint::Qp].ProcessRound(p->Var[TKMXMeasurePoint::Qp]);
      if(p->Var[TKMXMeasurePoint::Q]->State==TVar::Calculated)
         p->Var[TKMXMeasurePoint::Qp]->State=TVar::Calculated;
      else
         p->Var[TKMXMeasurePoint::Qp]->State=TVar::ErrorArg;
      }
      else
         p->Var[TKMXMeasurePoint::Qp]->State=TVar::ErrorArg;
   //Calculate Kj, fj, Kmxj
   if(p->Var[TKMXMeasurePoint::n]->Value.i)
      {
      p->Var[TKMXMeasurePoint::K]->Value.d/=p->Var[TKMXMeasurePoint::n]->Value.i;
      FPointVarOptions[TKMXMeasurePoint::K].ProcessRound(p->Var[TKMXMeasurePoint::K]);
      p->Var[TKMXMeasurePoint::f]->Value.d/=p->Var[TKMXMeasurePoint::n]->Value.i;
      FPointVarOptions[TKMXMeasurePoint::f].ProcessRound(p->Var[TKMXMeasurePoint::f]);
      p->Var[TKMXMeasurePoint::K_mx]->Value.d/=p->Var[TKMXMeasurePoint::n]->Value.i;
      FPointVarOptions[TKMXMeasurePoint::K_mx].ProcessRound(p->Var[TKMXMeasurePoint::K_mx]);
      }
   else
      {
      p->Var[TKMXMeasurePoint::K]->State=TVar::ErrorArg;
      p->Var[TKMXMeasurePoint::f]->State=TVar::ErrorArg;
      p->Var[TKMXMeasurePoint::K_mx]->State=TVar::ErrorArg;
      }
   //Calculate dKj
   p->Var[TKMXMeasurePoint::d]->State=TVar::ErrorArg;
   if(p->Var[TKMXMeasurePoint::K_mx]->Value.d != 0)
      {
      p->Var[TKMXMeasurePoint::d]->Value.d=100*(p->Var[TKMXMeasurePoint::K]->Value.d -
                                          p->Var[TKMXMeasurePoint::K_mx]->Value.d)/
                                          p->Var[TKMXMeasurePoint::K_mx]->Value.d;
      FPointVarOptions[TKMXMeasurePoint::d].ProcessRound(p->Var[TKMXMeasurePoint::d]);
      if(      (p->Var[TKMXMeasurePoint::K_mx]->State==TVar::Calculated)
               && (p->Var[TKMXMeasurePoint::K]->State==TVar::Calculated)
               )
         p->Var[TKMXMeasurePoint::d]->State=TVar::Calculated;

      }
   //Calculate d_max
   if(p->Var[TKMXMeasurePoint::Used]->Value.b)
      {
      double dj=p->Var[TKMXMeasurePoint::d]->Value.d;
      if(dj<0)
         dj*=-1;

      double d=Var[d_max].Value.d;
      if(d<0)
         d*=-1;

      if(dj > d)
         {
         Var[d_max].Value.d=p->Var[TKMXMeasurePoint::d]->Value.d;
         FMIVarOptions[d_max].ProcessRound(&Var[d_max]);

         if(p->Var[TKMXMeasurePoint::d]->State!=TVar::Calculated)
            Var[d_max].State=TVar::ErrorArg;
         }
      }

//---Calculates Delatij, SKOj
   p->Var[TKMXMeasurePoint::SKO]->Value.d=0;
   p->Var[TKMXMeasurePoint::SKO]->State=TVar::Calculated;
      for(int j=0; j<MeasureCount; j++)
      {
      CalculateMeasure2(p,j);
      }
//---Calculates SKOj
   if(p->Var[TKMXMeasurePoint::n]->Value.i>1)
      {
      p->Var[TKMXMeasurePoint::SKO]->Value.d/=(p->Var[TKMXMeasurePoint::n]->Value.i-1);
      p->Var[TKMXMeasurePoint::SKO]->Value.d=100*sqrt(p->Var[TKMXMeasurePoint::SKO]->Value.d);
      if(p->Var[TKMXMeasurePoint::K]->Value.d>0)
         p->Var[TKMXMeasurePoint::SKO]->Value.d/=p->Var[TKMXMeasurePoint::K]->Value.d;
      else
         p->Var[TKMXMeasurePoint::SKO]->State=TVar::ErrorArg;
   FPointVarOptions[TKMXMeasurePoint::SKO].ProcessRound(p->Var[TKMXMeasurePoint::SKO]);      }
   else
      p->Var[TKMXMeasurePoint::SKO]->State=TVar::ErrorArg;
}
//---------------------------------------------------------------------------
void __fastcall TKMX::CalculateMeasure1(TKMXMeasurePoint* p,int Index)
{
   TKMXMeasure* m = (TKMXMeasure*)(p->Measures[Index]);
//Calculte number of used measures
   m->Var[TKMXMeasure::Number]->Value.i=0;
   m->Var[TKMXMeasure::Number]->State=TVar::NotUsed;
   if(m->Var[TKMXMeasure::Used]->Value.b)
      {
      p->Var[TKMXMeasurePoint::n]->Value.i++;
      m->Var[TKMXMeasure::Number]->Value.i=p->Var[TKMXMeasurePoint::n]->Value.i;
      m->Var[TKMXMeasure::Number]->State=TVar::Calculated;
      }
//Calculate Density
   if(!Options.UseFD)
      {
      m->Var[TKMXMeasure::Dens]->Value.d=Options.Density;
      m->Var[TKMXMeasure::t_FD]->Value.d=Options.tFD;
      m->Var[TKMXMeasure::P_FD]->Value.d=0;
      m->Var[TKMXMeasure::Dens]->State=TVar::Calculated;
      m->Var[TKMXMeasure::t_FD]->State=TVar::Calculated;
      m->Var[TKMXMeasure::P_FD]->State=TVar::Calculated;
      }
//Calculate AverageDens
   if( (m->Var[TKMXMeasure::Used]->Value.b==true) &&
          (p->Var[TKMXMeasurePoint::Used]->Value.b==true) &&
          (Options.UseFD==true) )
      {
      DensCount++;
      Var[AverageDens].Value.d+=m->Var[TKMXMeasure::Dens]->Value.d;
      if(   !(
            (m->Var[TKMXMeasure::Dens]->State==TVar::Calculated) ||
            (m->Var[TKMXMeasure::Dens]->State==TVar::UserInput) ||
            (m->Var[TKMXMeasure::Dens]->State==TVar::UsedNearest)
            )
        )
         Var[AverageDens].State=TVar::ErrorArg;
      }
//Calculate Betta & F
   CalculateBnF(m);
//Calculate betta_aver
   if( (m->Var[TKMXMeasure::Used]->Value.b==true) &&
          (p->Var[TKMXMeasurePoint::Used]->Value.b==true)  )
      {
      BettaCount++;
      Var[betta_aver].Value.d+=m->Var[TKMXMeasure::Betta]->Value.d;
      if(   !(
            (m->Var[TKMXMeasure::Betta]->State==TVar::Calculated) ||
            (m->Var[TKMXMeasure::Betta]->State==TVar::UserInput) ||
            (m->Var[TKMXMeasure::Betta]->State==TVar::UsedNearest)
            )
        )
         Var[betta_aver].State=TVar::ErrorArg;
      }
//Calculate gamma_aver
   if( (m->Var[TKMXMeasure::Used]->Value.b==true) &&
          (p->Var[TKMXMeasurePoint::Used]->Value.b==true)  )
      {
      GammaCount++;
      Var[gamma_aver].Value.d+=m->Var[TKMXMeasure::F]->Value.d;
      if(   !(
            (m->Var[TKMXMeasure::F]->State==TVar::Calculated) ||
            (m->Var[TKMXMeasure::F]->State==TVar::UserInput) ||
            (m->Var[TKMXMeasure::F]->State==TVar::UsedNearest)
            )
        )
         Var[gamma_aver].State=TVar::ErrorArg;
      }

//---------------
//---���������� Ktij
//---------------
   TProverMX* pmx=IDMX[m->Var[TKMXMeasure::IDMX]->Value.i];
   if(Prover.Type==TProver::Compact)
      m->Var[TKMXMeasure::Kt]->Value.d=1+Prover.Alfa*(m->Var[TKMXMeasure::t_TPU]->Value.d - pmx->tBase) +
                 Prover.Alfa_invar* (Options.t_invar - pmx->tBase);
   else
      m->Var[TKMXMeasure::Kt]->Value.d=1+3*Prover.Alfa*(m->Var[TKMXMeasure::t_TPU]->Value.d - pmx->tBase);
   MeasureVarOptions[TKMXMeasure::Kt]->ProcessRound(m->Var[TKMXMeasure::Kt]);
   m->Var[TKMXMeasure::Kt]->State=TVar::Calculated;
//---------------
//---���������� Kpij
//---------------
   m->Var[TKMXMeasure::Kp]->Value.d=1+0.95*m->Var[TKMXMeasure::P_TPU]->Value.d*Prover.D/(Prover.E*Prover.S);
   MeasureVarOptions[TKMXMeasure::Kp]->ProcessRound(m->Var[TKMXMeasure::Kp]);
   m->Var[TKMXMeasure::Kp]->State=TVar::Calculated;
//---------------
//---���������� Kgtij
//---------------
   m->Var[TKMXMeasure::Kgt]->Value.d=1+m->Var[TKMXMeasure::Betta]->Value.d*(m->Var[TKMXMeasure::t]->Value.d -
                                    m->Var[TKMXMeasure::t_TPU]->Value.d);
   MeasureVarOptions[TKMXMeasure::Kgt]->ProcessRound(m->Var[TKMXMeasure::Kgt]);
   if( (m->Var[TKMXMeasure::Betta]->State==TVar::Calculated) || (m->Var[TKMXMeasure::Betta]->State==TVar::UserInput))
      m->Var[TKMXMeasure::Kgt]->State=TVar::Calculated;
   else
      {
      if((m->Var[TKMXMeasure::Betta]->State==TVar::UsedNearest)&&(m->Var[TKMXMeasure::Betta]->Value.d>0) )
         m->Var[TKMXMeasure::Kgt]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::Kgt]->State=TVar::ErrorArg;
      }
//---------------
//---���������� Kpgij
//---------------
   m->Var[TKMXMeasure::Kgp]->Value.d=1-m->Var[TKMXMeasure::F]->Value.d*(m->Var[TKMXMeasure::P]->Value.d -
                                    m->Var[TKMXMeasure::P_TPU]->Value.d);
   MeasureVarOptions[TKMXMeasure::Kgp]->ProcessRound(m->Var[TKMXMeasure::Kgp]);
   if((m->Var[TKMXMeasure::F]->State==TVar::Calculated) || (m->Var[TKMXMeasure::F]->State==TVar::UserInput) )
      m->Var[TKMXMeasure::Kgp]->State=TVar::Calculated;
   else
      {
      if((m->Var[TKMXMeasure::F]->State==TVar::UsedNearest)&&(m->Var[TKMXMeasure::F]->Value.d>0) )
         m->Var[TKMXMeasure::Kgp]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::Kgp]->State=TVar::ErrorArg;
      }
//---------------
//---���������� Ktpij
//---------------
   m->Var[TKMXMeasure::Ktp]->Value.d=m->Var[TKMXMeasure::Kt]->Value.d*
                                  m->Var[TKMXMeasure::Kp]->Value.d*
                                  m->Var[TKMXMeasure::Kgt]->Value.d*
                                  m->Var[TKMXMeasure::Kgp]->Value.d;
   MeasureVarOptions[TKMXMeasure::Ktp]->ProcessRound(m->Var[TKMXMeasure::Ktp]);

   if(                        (m->Var[TKMXMeasure::Kt]->State==TVar::Calculated) &&
                              (m->Var[TKMXMeasure::Kp]->State==TVar::Calculated) &&
                              (m->Var[TKMXMeasure::Kgt]->State==TVar::Calculated)&&
                              (m->Var[TKMXMeasure::Kgp]->State==TVar::Calculated))
      m->Var[TKMXMeasure::Ktp]->State=TVar::Calculated;
   else
      m->Var[TKMXMeasure::Ktp]->State=TVar::ErrorArg;
//---------------
//---���������� Vij
//---------------
   m->Var[TKMXMeasure::V]->Value.d=pmx->V * m->Var[TKMXMeasure::Ktp]->Value.d;
   MeasureVarOptions[TKMXMeasure::V]->ProcessRound(m->Var[TKMXMeasure::V]);
   if(m->Var[TKMXMeasure::Ktp]->State==TVar::Calculated)
      m->Var[TKMXMeasure::V]->State=TVar::Calculated;
   else
      m->Var[TKMXMeasure::V]->State=TVar::ErrorArg;
//---------------
//---���������� Kij
//---------------
   if(m->Var[TKMXMeasure::V]->Value.d > 0)
      {
      m->Var[TKMXMeasure::K]->Value.d=m->Var[TKMXMeasure::Pulses]->Value.d /
                                   m->Var[TKMXMeasure::V]->Value.d;
      if( (m->Var[TKMXMeasure::V]->State==TVar::Calculated) &&
                               (m->Var[TKMXMeasure::Pulses]->Value.d >0 ) )
         m->Var[TKMXMeasure::K]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::K]->State=TVar::ErrorArg;
      }
   else
      {
      m->Var[TKMXMeasure::K]->Value.d=0;
      m->Var[TKMXMeasure::K]->State=TVar::ErrorArg;
      }
   MeasureVarOptions[TKMXMeasure::K]->ProcessRound(m->Var[TKMXMeasure::K]);
//---------------
//---���������� Q
//---------------
   if(m->Var[TKMXMeasure::Time]->Value.d>0)
      {
      m->Var[TKMXMeasure::Q]->Value.d = m->Var[TKMXMeasure::V]->Value.d * 3600/
                                     m->Var[TKMXMeasure::Time]->Value.d;
      FMeasureVarOptions[TKMXMeasure::Q].ProcessRound(m->Var[TKMXMeasure::Q]);
      if(m->Var[TKMXMeasure::V]->State==TVar::Calculated)
         m->Var[TKMXMeasure::Q]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::Q]->State=TVar::ErrorArg;
      }
   else
      m->Var[TKMXMeasure::Q]->State=TVar::ErrorArg;

   if(tpr.Qmax>0)
      {
      m->Var[TKMXMeasure::Qp]->Value.d=100*m->Var[TKMXMeasure::Q]->Value.d/tpr.Qmax;
      FMeasureVarOptions[TKMXMeasure::Qp].ProcessRound(m->Var[TKMXMeasure::Qp]);
      if(m->Var[TKMXMeasure::Q]->State==TVar::Calculated)
         m->Var[TKMXMeasure::Qp]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::Qp]->State=TVar::ErrorArg;
      }
   else
      m->Var[TKMXMeasure::Qp]->State=TVar::ErrorArg;
//---------------
//---���������� fij
//---------------
   if(Options.FreqInput!=TKMXOptions::User)
      {
      m->Var[TKMXMeasure::f]->Value.d=m->Var[TKMXMeasure::Q]->Value.d *
               m->Var[TKMXMeasure::K]->Value.d/3600;
      FMeasureVarOptions[TKMXMeasure::f].ProcessRound(m->Var[TKMXMeasure::f]);
      if(m->Var[TKMXMeasure::Q]->State==TVar::Calculated && m->Var[TKMXMeasure::K]->State==TVar::Calculated)
         m->Var[TKMXMeasure::f]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::f]->State=TVar::ErrorArg;
      }
//---------------
//---���������� ����� ij
//---------------
   if(TPRMX.ArgumentMX==TTPRMX::Frequency)
      {
      m->Var[TKMXMeasure::K_mx]->Value.d=CalculateK(&TPRMX,m->Var[TKMXMeasure::f]->Value.d);
      FMeasureVarOptions[TKMXMeasure::K_mx].ProcessRound(m->Var[TKMXMeasure::K_mx]);
      if( (m->Var[TKMXMeasure::f]->State!=TVar::ErrorArg)
         && (m->Var[TKMXMeasure::K_mx]->Value.d >0 )
         )
         m->Var[TKMXMeasure::K_mx]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::K_mx]->State=TVar::ErrorArg;
      }
   else
      {
      m->Var[TKMXMeasure::K_mx]->Value.d=CalculateK(&TPRMX,m->Var[TKMXMeasure::Q]->Value.d);
      FMeasureVarOptions[TKMXMeasure::K_mx].ProcessRound(m->Var[TKMXMeasure::K_mx]);
      if( (m->Var[TKMXMeasure::Q]->State!=TVar::ErrorArg)
         && (m->Var[TKMXMeasure::K_mx]->Value.d >0 )
         )
         m->Var[TKMXMeasure::K_mx]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::K_mx]->State=TVar::ErrorArg;
      }
//---------------
//---���������� delta ij
//---------------
   m->Var[TKMXMeasure::dK]->Value.d=0;
   m->Var[TKMXMeasure::dK]->State=TVar::ErrorArg;
   if( m->Var[TKMXMeasure::K_mx]->Value.d >0)
      {
      m->Var[TKMXMeasure::dK]->Value.d = 100*(m->Var[TKMXMeasure::K]->Value.d -
                                         m->Var[TKMXMeasure::K_mx]->Value.d)/
                                         m->Var[TKMXMeasure::K_mx]->Value.d;
      if( (m->Var[TKMXMeasure::K_mx]->State==TVar::Calculated)
         && (m->Var[TKMXMeasure::K]->State==TVar::Calculated))
                  m->Var[TKMXMeasure::dK]->State=TVar::Calculated;
      }
   FMeasureVarOptions[TKMXMeasure::dK].ProcessRound(m->Var[TKMXMeasure::dK]);
//---------------
//---���������� Qj, Kj, fj, Kmxj
//---------------
   if(m->Var[TKMXMeasure::Used]->Value.b)
      {
      p->Var[TKMXMeasurePoint::Q]->Value.d+=m->Var[TKMXMeasure::Q]->Value.d;
      if(m->Var[TKMXMeasure::Q]->State==TVar::ErrorArg)
            p->Var[TKMXMeasurePoint::Q]->State=TVar::ErrorArg;

//---���������� Kj
      p->Var[TKMXMeasurePoint::K]->Value.d+=m->Var[TKMXMeasure::K]->Value.d;
      if(m->Var[TKMXMeasure::K]->State!=TVar::Calculated)
         p->Var[TKMXMeasurePoint::K]->State=TVar::ErrorArg;
//---���������� fj
      p->Var[TKMXMeasurePoint::f]->Value.d+=m->Var[TKMXMeasure::f]->Value.d;
      if(   !( (m->Var[TKMXMeasure::f]->State==TVar::Calculated)||
              (m->Var[TKMXMeasure::f]->State==TVar::UserInput) ||
              (m->Var[TKMXMeasure::f]->State==TVar::UsedNearest)  )  )
         p->Var[TKMXMeasurePoint::f]->State=TVar::ErrorArg;
//---���������� Kmxj
      p->Var[TKMXMeasurePoint::K_mx]->Value.d+=m->Var[TKMXMeasure::K_mx]->Value.d;
      if(m->Var[TKMXMeasure::K_mx]->State!=TVar::Calculated)
         p->Var[TKMXMeasurePoint::K_mx]->State=TVar::ErrorArg;

      }
}
//---------------------------------------------------------------------------
void __fastcall TKMX::SortPoints()
{
   int c = Count;
   int a;
   TKMXMeasurePoint* p1;
   TKMXMeasurePoint* p2;
   for(int i=0;i<(c-1);i++)
      {
      p1 = (TKMXMeasurePoint*)GridPoints[i];
      a=-1;
      for( int j=i+1;j<c;j++)
         {
         p2 = (TKMXMeasurePoint*)GridPoints[j];
         if( p1->Var[TKMXMeasurePoint::Q]->Value.d > p2->Var[TKMXMeasurePoint::Q]->Value.d)
            {
            p1=p2;
            a=j;
            }
         }
      if(a!=-1)
         {
         FMeasurePoints->Move(a,i);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMX::CalculateMeasure2(TKMXMeasurePoint* p,int Index)
{
   TKMXMeasure* m = (TKMXMeasure*)(p->Measures[Index]);
//Calculate dij
   if(p->Var[TKMXMeasurePoint::K]->Value.d>0)
      {
      m->Var[TKMXMeasure::d]->Value.d=(m->Var[TKMXMeasure::K]->Value.d -
                                       p->Var[TKMXMeasurePoint::K]->Value.d);

      FMeasureVarOptions[TKMXMeasure::d].ProcessRound(m->Var[TKMXMeasure::d]);
      if( (p->Var[TKMXMeasurePoint::K]->State==TVar::Calculated) &&
           (m->Var[TKMXMeasure::K]->State==TVar::Calculated) )
         m->Var[TKMXMeasure::d]->State=TVar::Calculated;
      else
         m->Var[TKMXMeasure::d]->State=TVar::ErrorArg;
      }
   else
      m->Var[TKMXMeasure::d]->State=TVar::ErrorArg;
//Calculate SKOj
   if(m->Var[TKMXMeasure::Used]->Value.b)
      {
      p->Var[TKMXMeasurePoint::SKO]->Value.d+=m->Var[TKMXMeasure::d]->Value.d*m->Var[TKMXMeasure::d]->Value.d;
      if(m->Var[TKMXMeasure::d]->State!=TVar::Calculated)
         p->Var[TKMXMeasurePoint::SKO]->State=TVar::ErrorArg;
      }
//Calculate dij
   if(p->Var[TKMXMeasurePoint::K]->Value.d>0)
      {
      m->Var[TKMXMeasure::d]->Value.d/=p->Var[TKMXMeasurePoint::K]->Value.d;
      m->Var[TKMXMeasure::d]->Value.d*=100;
      FMeasureVarOptions[TKMXMeasure::d].ProcessRound(m->Var[TKMXMeasure::d]);
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMX::SetOptions(TKMXOptions* o)
{
   bool NeedChange=false;
//
//Apply changes on FreqInput
   if(Options.FreqInput!=o->FreqInput)
      {
      Options.FreqInput=o->FreqInput;
      if(Options.FreqInput==TKMXOptions::User)
         MeasureVarOptions[TKMXMeasure::f]->Kind=TVarOptions::InputData;
      else
         MeasureVarOptions[TKMXMeasure::f]->Kind=TVarOptions::CalculatedData;
      NeedChange=true;
      }
   //Apply changes on ProverMX
   TProverMX* IDMX1[4];

   IDMX1[0]=0;
   IDMX1[1]=0;
   IDMX1[2]=0;
   IDMX1[3]=0;
   int mx[4];
   mx[0]=0;
   mx[1]=0;
   mx[2]=0;
   mx[3]=0;
   TProverMX* x;
   Options.ProverMXID[0]=o->ProverMXID[0];
   Options.ProverMXID[1]=o->ProverMXID[1];
   Options.ProverMXID[2]=o->ProverMXID[2];
   Options.ProverMXID[3]=o->ProverMXID[3];
   //Make temp IDMX1
   int mxc=ProverMX->Count;
   for(int i=0;i<4;i++)
      {
      if(Options.ProverMXID[i]==0) break;
      for(int j=0;j<mxc;j++)
         {
         x=static_cast<TProverMX*>(ProverMX->Items[j]);
         if(x->ID==Options.ProverMXID[i]) break;
         }
      IDMX1[i]=x;
      }
   //Compare with current
   bool Eq=true;
   for(int i=0;i<4;i++)
      if(IDMX1[i]!=IDMX[i])
         {
         Eq=false;
         break;
         }
   if(!Eq)
      {
      NeedChange=true;
      //Make table of replacement
      for(int i=0;i<4;i++)
         {
         if(IDMX[i]==NULL) break;
         for(int j=0;j<4;j++)
            if(IDMX[i]==IDMX1[j]) mx[i]=j;
         }
      IDMX[0]=IDMX1[0];
      IDMX[1]=IDMX1[1];
      IDMX[2]=IDMX1[2];
      IDMX[3]=IDMX1[3];
      //Create string list
      TVarOptions* vo=&FMeasureVarOptions[TKMXMeasure::IDMX];
      vo->Strings->Clear();
      for(int i=0;i<4;i++)
         {
         if(IDMX1[i]==0) break;
         vo->Strings->Add(IDMX[i]->Index);
         }
      int pc=Count;
      for(int i=0;i<pc;i++)
         {
         TKMXMeasurePoint* p = (TKMXMeasurePoint*)GridPoints[i];
         int mc = p->Count;
         for(int j=0;j<mc;j++)
            {
            TKMXMeasure* m = (TKMXMeasure*)(p->Measures[j]);
            m->Var[TKMXMeasure::IDMX]->Value.i=mx[m->Var[TKMXMeasure::IDMX]->Value.i];
            }
         }
      bool exist= (MeasureColumns.IndexOf(TKMXMeasure::IDMX)!=-1);
      if(IDMX[1]==NULL)
         {
         if(exist)
            MeasureColumns.Remove(TKMXMeasure::IDMX);
         }
      else
         {
         if(!exist) MeasureColumns.Add(TKMXMeasure::IDMX);
         }
      }
//------------------------------------------------------------------------------
//Change TPRMX
   if(o->TPRMXID!=Options.TPRMXID)
      {
      Options.TPRMXID=o->TPRMXID;

      int tprmxc=TPRMXList->Count;
      bool Founded=false;
      for(int i=0;i<tprmxc;i++)
         {
         TTPRMX* mx=(TTPRMX*)TPRMXList->Items[i];
         if(mx->ID==Options.TPRMXID)
            {
            memcpy(&TPRMX,mx,sizeof(TTPRMX));
            Founded=true;
            }
         }
      if(Founded==false)
         throw(Exception("TKMX::TKMX\n�� ������� �� ���"));
      }
//------------------------------------------------------------------------------
   Options.t_invar=o->t_invar;
   strcpy(Options.Description,o->Description);
   Options.Date=o->Date;
//------------------------------------------------------------------------------
//Tab2
   Options.Density=o->Density;
   Options.tFD=o->tFD;
   Options.Betta=o->Betta;
   Options.F=o->F;
   //Apply changes to UseFD
   if(Options.UseFD!=o->UseFD)
      {
      NeedChange=true;
      Options.UseFD=o->UseFD;
      if(Options.UseFD)
         {
         if(MeasureColumns.IndexOf(TKMXMeasure::Dens)==-1)
            MeasureColumns.Add(TKMXMeasure::Dens);
         if(MeasureColumns.IndexOf(TKMXMeasure::t_FD)==-1)
            MeasureColumns.Add(TKMXMeasure::t_FD);
         if(MeasureColumns.IndexOf(TKMXMeasure::P_FD)==-1)
            MeasureColumns.Add(TKMXMeasure::P_FD);
         MeasureVarOptions[TKMXMeasure::Dens]->Kind=TVarOptions::InputData;
         MeasureVarOptions[TKMXMeasure::t_FD]->Kind=TVarOptions::InputData;
         MeasureVarOptions[TKMXMeasure::P_FD]->Kind=TVarOptions::InputData;
         if(MeasureColumns.IndexOf(TKMXMeasure::Betta)==-1)
            MeasureColumns.Add(TKMXMeasure::Betta);
         if(MeasureColumns.IndexOf(TKMXMeasure::F)==-1)
            MeasureColumns.Add(TKMXMeasure::F);
         }
      else
         {
         MeasureColumns.Remove(TKMXMeasure::Dens);
         MeasureColumns.Remove(TKMXMeasure::t_FD);
         MeasureColumns.Remove(TKMXMeasure::P_FD);
         MeasureVarOptions[TKMXMeasure::Dens]->Kind=TVarOptions::CalculatedData;
         MeasureVarOptions[TKMXMeasure::t_FD]->Kind=TVarOptions::CalculatedData;
         MeasureVarOptions[TKMXMeasure::P_FD]->Kind=TVarOptions::CalculatedData;
         if(Options.ProductIndex!=0)
            {
            MeasureColumns.Remove(TKMXMeasure::Betta);
            MeasureColumns.Remove(TKMXMeasure::F);
            }
         }
      }
//------------------------------------------------------------------------------
   strcpy(Options.ProductName,o->ProductName);
   Options.ProductIndex=o->ProductIndex;
   Options.DensReCalc=o->DensReCalc;
   Options.v=o->v;
   //Apply changes on Betta and Gamma
   if(Options.ProductIndex==0)
      {
      MeasureVarOptions[TKMXMeasure::Betta]->Kind=TVarOptions::InputData;
      MeasureVarOptions[TKMXMeasure::F]->Kind=TVarOptions::InputData;
      if(MeasureColumns.IndexOf(TKMXMeasure::Betta)==-1)
         MeasureColumns.Add(TKMXMeasure::Betta);
      if(MeasureColumns.IndexOf(TKMXMeasure::F)==-1)
         MeasureColumns.Add(TKMXMeasure::F);
      }
   else
      {
      MeasureVarOptions[TKMXMeasure::Betta]->Kind=TVarOptions::CalculatedData;
      MeasureVarOptions[TKMXMeasure::F]->Kind=TVarOptions::CalculatedData;
      }

   strcpy(Options.ProtokolN,o->ProtokolN);
   strcpy(Options.Location,o->Location);

      //Protokol data
   strcpy(Options.Service.Company,  o->Service.Company);
   strcpy(Options.Seller.Company,   o->Seller.Company);
   strcpy(Options.Buyer.Company,    o->Buyer.Company);

   strcpy(Options.Service.Rank,     o->Service.Rank);
   strcpy(Options.Seller.Rank,      o->Seller.Rank);
   strcpy(Options.Buyer.Rank,       o->Buyer.Rank);

   strcpy(Options.Service.FIO,      o->Service.FIO);
   strcpy(Options.Seller.FIO,       o->Seller.FIO);
   strcpy(Options.Buyer.FIO,        o->Buyer.FIO);


   if(NeedChange)
      {
      void* f=this;
      if(FOnChangeItemsStructure) FOnChangeItemsStructure((TObject*)f);
      }
   Calculate();
}
//---------------------------------------------------------------------------
int __fastcall TKMX::AddNewMeasure(int PointIndex)
{
   TKMXMeasurePoint* p = (TKMXMeasurePoint*)GridPoints[PointIndex];
   TKMXMeasure* m = new TKMXMeasure();
   TKMXMeasure* mm;
   if(p->Count)
      {
      TVarOptions* vo;
      mm=(TKMXMeasure*)(p->Measures[p->Count-1]);
      for(int i=0;i<TKMXMeasure::VarCount;i++)
         {
         vo=&FMeasureVarOptions[i];
         if( (vo->Kind==TVarOptions::InputData) && (vo->Type!=TVarOptions::Text))
            {
            m->Var[i]->Value=mm->Var[i]->Value;
            m->Var[i]->State=TVar::UsedNearest;
            }
         }
      }
   return p->Add(m);
}
//---------------------------------------------------------------------------
void __fastcall TKMX::CalculateBnF(TKMXMeasure* m)
{
   if(Options.ProductIndex==0)
      return;//Used user input

   if(Options.DensReCalc)
      {//
      double dens;
      double d, t, P;
      d=m->Var[TKMXMeasure::Dens]->Value.d;
      t=m->Var[TKMXMeasure::t_FD]->Value.d;
      P=m->Var[TKMXMeasure::P_FD]->Value.d;
      //(double t, double P, double Dens, double t1, double P1, /*out*/double& Dtp, double& Betta, double& F);
      try
         {
         TFunctionResult r=TMI1974::CommonData->MITools.ReCalc(Options.ProductIndex-1,
                              t,
                              P,
                              d,
                              m->Var[TKMXMeasure::t_TPU]->Value.d,
                              m->Var[TKMXMeasure::P_TPU]->Value.d,
                              dens,
                              m->Var[TKMXMeasure::Betta]->Value.d,
                              m->Var[TKMXMeasure::F]->Value.d);
         if(r==frOk)
            {
            m->Var[TKMXMeasure::Betta]->State=TVar::Calculated;
            m->Var[TKMXMeasure::F]->State=TVar::Calculated;
            }
         else
            {
            m->Var[TKMXMeasure::Betta]->State=TVar::ErrorArg;
            m->Var[TKMXMeasure::F]->State=TVar::ErrorArg;
            }
         }
      catch(...)
         {
         m->Var[TKMXMeasure::Betta]->State=TVar::ErrorArg;
         m->Var[TKMXMeasure::F]->State=TVar::ErrorArg;
         m->Var[TKMXMeasure::Betta]->Value.d=0;
         m->Var[TKMXMeasure::F]->Value.d=0;
         }
      MeasureVarOptions[TKMXMeasure::Betta]->ProcessRound(m->Var[TKMXMeasure::Betta]);
      MeasureVarOptions[TKMXMeasure::F]->ProcessRound(m->Var[TKMXMeasure::Betta]);
      }
   else
      {
      if(Options.UseFD)
         {
         if(Options.ProductIndex)
            {
            try
               {
               TFunctionResult r=TMI1974::CommonData->MITools.GetBetta(Options.ProductIndex-1,
                                 m->Var[TKMXMeasure::t_FD]->Value.d,
                                 0,
                                 m->Var[TKMXMeasure::Dens]->Value.d,
                                 m->Var[TKMXMeasure::Betta]->Value.d);
               MeasureVarOptions[TKMXMeasure::Betta]->ProcessRound(m->Var[TKMXMeasure::Betta]);
               if(r==frOk)
                  m->Var[TKMXMeasure::Betta]->State=TVar::Calculated;
               else
                  m->Var[TKMXMeasure::Betta]->State=TVar::ErrorArg;
               }
            catch(...)
               {
               m->Var[TKMXMeasure::Betta]->State=TVar::ErrorArg;
               m->Var[TKMXMeasure::Betta]->Value.d=0;
               }
            }
         }
      else
         {
         m->Var[TKMXMeasure::Betta]->Value.d=Options.Betta;
         m->Var[TKMXMeasure::Betta]->State=TVar::Calculated;
         MeasureVarOptions[TKMXMeasure::Betta]->ProcessRound(m->Var[TKMXMeasure::Betta]);
         }
   //---------------
   //---���������� F
   //---------------
      if(Options.UseFD)
         {
         if(Options.ProductIndex)
            {
            try
               {
               TFunctionResult r=TMI1974::CommonData->MITools.GetF(Options.ProductIndex-1,
                                 m->Var[TKMXMeasure::t_FD]->Value.d,
                                 0,
                                 m->Var[TKMXMeasure::Dens]->Value.d,
                                 m->Var[TKMXMeasure::F]->Value.d);
               if(r==frOk)
                  m->Var[TKMXMeasure::F]->State=TVar::Calculated;
               else
                  m->Var[TKMXMeasure::F]->State=TVar::ErrorArg;
               }
            catch(...)
               {
               m->Var[TKMXMeasure::F]->State=TVar::ErrorArg;
               m->Var[TKMXMeasure::F]->Value.d=0;
               }
            }
         MeasureVarOptions[TKMXMeasure::F]->ProcessRound(m->Var[TKMXMeasure::F]);
         }
      else
         {
         m->Var[TKMXMeasure::F]->Value.d=Options.F;
         m->Var[TKMXMeasure::F]->State=TVar::Calculated;
         MeasureVarOptions[TKMXMeasure::F]->ProcessRound(m->Var[TKMXMeasure::F]);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TKMX::LoadVarOptions()
{
//Load measure var options
/*
a) � �������+
1.	Name;
2.	Description;
3.	Unit
1001 - Measure;
1201 - Point
1401 - MI
1601 - Range.

b) RC-data
1.	Type;
2.	Kind;
3.	Width;
4.	Bitmap;
5.	UnitBitmap
1701 - Measure;
1702 - Point
1703 - MI
1704 - Range.
*/
   TResourceStream* rs;
   int base;
   base=1001;
   short int buf[512];
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,1701,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   short int buf1[512];
   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,1801,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<TKMXMeasure::VarCount;i++)
      {
      FMeasureVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FMeasureVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FMeasureVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);
#pragma warn -8018
      FMeasureVarOptions[i].Type=buf[i*5];
      FMeasureVarOptions[i].Kind=buf[i*5+1];
      FMeasureVarOptions[i].Width=buf[i*5+2];
      FMeasureVarOptions[i].Bitmap=buf[i*5+3];
      FMeasureVarOptions[i].UnitBitmap=buf[i*5+4];

      FMeasureVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FMeasureVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FMeasureVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FMeasureVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
//Read point
   base=1201;
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,1702,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,1802,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<TKMXMeasurePoint::VarCount;i++)
      {
      FPointVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FPointVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FPointVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);

      FPointVarOptions[i].Type=buf[i*5];
      FPointVarOptions[i].Kind=buf[i*5+1];
      FPointVarOptions[i].Width=buf[i*5+2];
      FPointVarOptions[i].Bitmap=buf[i*5+3];
      FPointVarOptions[i].UnitBitmap=buf[i*5+4];

      FPointVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FPointVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FPointVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FPointVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
//MI Var
   base=1401;
   memset(buf,0,512);

   rs = new TResourceStream((int)HInstance,1703,"RT_RCDATA");
   rs->Read(buf,rs->Size);
   delete rs;

   memset(buf1,0,512);
   rs = new TResourceStream((int)HInstance,1803,"RT_RCDATA");
   rs->Read(buf1,rs->Size);
   delete rs;

   for(int i=0;i<VarCount;i++)
      {
      FMIVarOptions[i].Name=AnsiString::LoadStr(base+i*3);
      FMIVarOptions[i].Description=AnsiString::LoadStr(base+i*3+1);
      FMIVarOptions[i].Unit=AnsiString::LoadStr(base+i*3+2);

      FMIVarOptions[i].Type=buf[i*5];
      FMIVarOptions[i].Kind=buf[i*5+1];
      FMIVarOptions[i].Width=buf[i*5+2];
      FMIVarOptions[i].Bitmap=buf[i*5+3];
      FMIVarOptions[i].UnitBitmap=buf[i*5+4];

      FMIVarOptions[i].CalcOptions.CalcRoundType=buf1[i*4];
      FMIVarOptions[i].CalcOptions.CalcRoundNumber=buf1[i*4+1];
      FMIVarOptions[i].CalcOptions.ShowRoundType=buf1[i*4+2];
      FMIVarOptions[i].CalcOptions.ShowRoundNumber=buf1[i*4+3];
      }
#pragma warn +8018
}
//---------------------------------------------------------------------------
void __fastcall TKMX::GetInfo(IStorage* KMXStorage, IStorage* Root,TKMX_data* data)
{
   //Load IDs
   int ProverID, tprID,TPRMXID;

   HRESULT res;
   IStream* Stream;
   res=KMXStorage->OpenStream(WideString("IDs"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
      throw Exception("Error open stream IDs");
      }

   Stream->Read(&ProverID,sizeof(int),0);
   Stream->Read(&tprID,sizeof(int),0);
   Stream->Release();

   //Load Options
   res=KMXStorage->OpenStream(WideString("Options"),0,OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
   if(res!=S_OK)
      {
      throw Exception("Error open stream Options");
      }
   Stream->Read(data->Options,sizeof(TKMXOptions),0);
   Stream->Release();

   TMI1974::CommonData->ProverInterface.Get(Root,ProverID,data->Prover,data->ProverMX);
   //Get TPR
   TMI1974::CommonData->TPRInterface.GetTPRnMXList(Root,tprID,data->TPR,data->TPRMXList);
}
//---------------------------------------------------------------------------

