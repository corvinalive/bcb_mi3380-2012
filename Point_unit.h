//---------------------------------------------------------------------------
#ifndef Point_unitH
#define Point_unitH

#include "variable.h"
#include "measure_unit.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TMeasurePoint:public TGridPoint
{
public:
   static const VarCount=17;
   static ReadVarCount;

   static const Used       =0;//������������� ������� ��������
   static const Number     =1;//����� ���������
   static const n          =2;//���������� ���������

   static const Q          =3;//������ �/�
   static const Qp         =4;//������ %
   static const f          =5;//�������

   static const K          =6;
   static const SKO        =7;

   static const e          =8;
   static const t095       =9;
   static const Tetta_K    =10;

   static const TettaS    =11;
   static const Z          =12;
   static const d          =13;

   static const Notes      =14;

   static const Kvist       =15;
   static const d00     =16;

	__fastcall TMeasurePoint();
	__fastcall ~TMeasurePoint();
	__property TMeasure* Measures[int index] = { read=GetMeasures};
   virtual int __fastcall Add(TVarContainer*);
   virtual int __fastcall AddNew();
   virtual void __fastcall Delete(int Index);
   virtual void __fastcall ClearMeasures();
   virtual void __fastcall Load(IStream* Stream);
   virtual void __fastcall Save(IStream* Stream);
private:
	TVar FVar[VarCount];
   virtual TVar* __fastcall GetVar(int index);
   virtual int __fastcall GetVarCount(){return VarCount;};
   virtual TVarContainer*  __fastcall GetMeasures(int index);
   virtual int __fastcall GetMeasureVarCount();
	TList* FMeasures;
   int __fastcall GetCount();
};
//---------------------------------------------------------------------------
#endif
