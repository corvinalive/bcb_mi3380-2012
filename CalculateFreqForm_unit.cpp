//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop



#include "CalculateFreqForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TCalculateFreqForm *CalculateFreqForm;
//---------------------------------------------------------------------------
__fastcall TCalculateFreqForm::TCalculateFreqForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCalculateFreqForm::NumberEdit1Change(
      TObject *Sender)
{
   try
      {
      double f,q,q1;
      int f1;
      f=NumberEdit1->Text.ToDouble();
      q=NumberEdit2->Text.ToDouble();
      q1=NumberEdit3->Text.ToDouble();
      if(q!=0)
         {
         f1=f*q1/q;
         Label4->Font->Color=clBlue;
         Label4->Caption=f1;
         }
      else
         {
         Label4->Font->Color=clRed;
         Label4->Caption="������!";
         }
      }
   catch(...)
      {
      Label4->Font->Color=clRed;
      Label4->Caption="������!";
      }
}
//---------------------------------------------------------------------------
