//---------------------------------------------------------------------------
#include <vcl.h>
#include "Pch.H"
#pragma hdrstop

#include "prover.h"
#include "tpr.h"
#include "KMX_unit.h"
#include "MI1974_unit.h"

#include "MainForm_unit.h"
#include "MIForm_unit.h"
#include "OptionsForm_unit.h"
#include "SelectMIForm_unit.h"
#include "SetupViewVariablesForm_unit.h"
#include "CalculateOptionsForm_unit.h"
#include "ProtokolProgressForm_unit.h"
#include "AboutBox_unit.h"
#include "CalculateFreqForm_unit.h"
#include "Measure_unit.h"
#include "KMXForm_unit.h"
#include "KMXOptionsForm_unit.h"
#include "SelectKMXForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Word_2K_SRVR"
#pragma link "Excel_2K_SRVR"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   TMI1974::CommonData = new TMI1974CommonData();
   /*Load bitmaps*/
   for(int i=1;i<BitmapCount+1;i++)
      {
      FBitmaps[i] = new Graphics::TBitmap();
      FBitmaps[i]->LoadFromResourceID((int)HInstance,/*AnsiString*/(i));
      }
	Root=NULL;
	OleInitialize(0);
   randomize();

   TVar::Colors[0]=clGreen;
   TVar::Colors[1]=clWhite;

   TVar::Colors[2]=clGray;
   TVar::Colors[3]=clWhite;

   TVar::Colors[4]=clRed;
   TVar::Colors[5]=clWhite;

   TVar::Colors[6]=clBlue;
   TVar::Colors[7]=clWhite;

   TVar::Colors[8]=clRed;
   TVar::Colors[9]=clWhite;

   TVar::Colors[10]=clRed;
   TVar::Colors[11]=clWhite;

   TVar::Colors[12]=clGray;
   TVar::Colors[13]=clWhite;

}
//---------------------------------------------------------------------------
__fastcall TMainForm::~TMainForm()
{
//   h->CloseHelp();
   if(MIRoot) MIRoot->Release();
   if(KMXRoot) KMXRoot->Release();
   if(Root) Root->Release();
   delete TMI1974::CommonData;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NewMenuItemClick(TObject *Sender)
{
//Select ��
   TTPR tpr;

   memset(&tpr,0,sizeof(TTPR));
   TMI1974::CommonData->TPRInterface.Select(this,Root,&tpr);
   if(tpr.ID==0)
      return;
//Select Prover


   TProver prover;

   memset(&prover,0,sizeof(TProver));
   TList* mx= new TList();
   TMI1974::CommonData->ProverInterface.Select(this,Root,&prover,mx);
   if(prover.ID==0)
      {
      delete mx;
      return;
      }
   TMI1974Options Options;
   memset(&Options,0,sizeof(TMI1974Options));
   //Fill Options field's default values

   Options.Purpose=TMI1974Options::Work;
   Options.Function=TMI1974Options::Const;
   Options.dtTPR=0.2;
   Options.dtTPU=0.2;
   Options.dUOI=0.025;
   Options.Date=Options.Date.CurrentDate();
   Options.t_invar=20;
   Options.tFD=20;
   Options.UseFD=true;
   Options.Density=800;
   Options.UseVisc=false;
   Options.DensReCalc=false;
   int res=OptionsForm->ShowOptions(&Options,mx);
   if(res!=mrOk)
      {
      //Clear all and return;
      int c=mx->Count;
      for(int i=0;i<c;i++)
         delete (TProverMX*)(mx->Items[i]);
      delete mx;
      return;
      }
   //All ok, make new MI window
   TMI_data data;
   data.Options = new TMI1974Options;
   memcpy(data.Options,&Options,sizeof(TMI1974Options));
   data.Prover = new TProver;
   memcpy(data.Prover,&prover,sizeof(TProver));
   data.TPR = new TTPR;
   memcpy(data.TPR,&tpr,sizeof(TTPR));
   data.ProverMX = mx;

   new TMIForm(this,Root,MIRoot,data);
}
//---------------------------------------------------------------------------
Graphics::TBitmap* __fastcall TMainForm::GetBitmaps(int index)
{
   if((index<1)||(index>BitmapCount))
      return NULL;
   else
      return FBitmaps[index];
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenMenuItemClick(TObject *Sender)
{
   int id=SelectMIForm->Select(Root,MIRoot);
   if(id)
      new TMIForm(this,Root,MIRoot,id);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OptionsMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   if(ActiveMDIChild->Tag==100)
      {
      TKMXForm* f = (TKMXForm*)ActiveMDIChild;
      if(f)
         {
         TKMXOptions o;
         memcpy(&o,&f->MI->Options,sizeof(TKMXOptions));
         int r=KMXOptionsForm->ShowOptions(&o,f->MI->ProverMX,f->MI->TPRMXList);
         if(r==mrOk) f->MI->SetOptions(&o);
         }
      }
   else
      {
      TMIForm* f = (TMIForm*)ActiveMDIChild;
      if(f)
         {
         TMI1974Options o;
         memcpy(&o,&f->MI->Options,sizeof(TMI1974Options));
         int r=OptionsForm->ShowOptions(&o,f->MI->ProverMX);
         if(r==mrOk) f->MI->SetOptions(&o);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ViewVariablesMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   TForm* f = (TMIForm*)ActiveMDIChild;
   if(f->Tag==100)
      {
      TKMX* KMX=((TKMXForm*)f)->MI;
      SetupViewVariablesForm->ShowKMXDialog(KMX);
      }
   else
      {
      TMI1974* MI=((TMIForm*)(f))->MI;
      SetupViewVariablesForm->ShowDialog(MI);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::CalculateOptionsMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   TForm* f = (TMIForm*)ActiveMDIChild;
   if(f->Tag==100)
      {
      TKMX* KMX=((TKMXForm*)f)->MI;
      CalculateOptionsForm->ShowKMXDialog(KMX);
      }
   else
      {
      TMI1974* MI=((TMIForm*)(f))->MI;
      CalculateOptionsForm->ShowDialog(MI);
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ExitMenuItemClick(TObject *Sender)
{
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ViewTPRMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   if(ActiveMDIChild->Tag==100)
      {
      TKMXForm* f = (TKMXForm*)ActiveMDIChild;
      if(f)
         {
         TMI1974::CommonData->TPRInterface.View(this,Root,&f->MI->tpr);
         }
      }
   else
      {
      TMIForm* f = (TMIForm*)ActiveMDIChild;
      if(f)
         {
         TMI1974::CommonData->TPRInterface.View(this,Root,&f->MI->tpr);
         }
      }
} ;
//---------------------------------------------------------------------------
void __fastcall TMainForm::ViewProverMenuItemClick(TObject *Sender)
{
   if(ActiveMDIChild==NULL)
      return;
   if(ActiveMDIChild->Tag==100)
      {
      TKMXForm* f = (TKMXForm*)ActiveMDIChild;
      if(f)
         {
         TMI1974::CommonData->ProverInterface.View(this,Root,&f->MI->Prover);
         }
      }
   else
      {
      TMIForm* f = (TMIForm*)ActiveMDIChild;
      if(f)
         {
         TMI1974::CommonData->ProverInterface.View(this,Root,&f->MI->Prover);
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ManageMMMenuItemClick(TObject *Sender)
{
   TMI1974::CommonData->TPRInterface.Manage(this,Root);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ManageProverMenuItemClick(TObject *Sender)
{
   TMI1974::CommonData->ProverInterface.Manage(this,Root);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ManageMI2463MenuItemClick(TObject *Sender)
{
   int id=SelectMIForm->Manage(Root,MIRoot);
   if(id)
      new TMIForm(this,Root,MIRoot,id);
}
//---------------------------------------------------------------------------;
void __fastcall TMainForm::AboutMenuItemClick(TObject *Sender)
{
   AboutBox->ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::MIFormFocusChange(bool HasFocus)
{
   OptionsMenuItem->Enabled=HasFocus;
   MakeProtokolMenuItem->Enabled=HasFocus;
   CalculateOptionsMenuItem->Enabled=HasFocus;
   ViewVariablesMenuItem->Enabled=HasFocus;
   ViewProverMenuItem->Enabled=HasFocus;
   ViewTPRMenuItem->Enabled=HasFocus;
   CheckMenuItem->Enabled=HasFocus;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BitBtn1Click(TObject *Sender)
{
   if(OD->Execute()) OpenFile(OD->FileName);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenFile(AnsiString FN)
{
	HRESULT res;
	if (FileExists(FN))
		{//Open
		res=StgOpenStorage(WideString(FN),NULL,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&Root);
		if(res!=S_OK) throw(Exception("Error open storage file"));
		}
	else
		{//Create
		res=StgCreateDocfile(WideString(FN),STGM_CREATE|STGM_READWRITE|STGM_SHARE_EXCLUSIVE,0,&Root);
		if(res!=S_OK) throw(Exception("Error create storage file"));
		}
   res = Root->OpenStorage(WideString("MI1974-2004"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&MIRoot);
   if(res!=S_OK)
      {
      res=Root->CreateStorage(WideString("MI1974-2004"),STGM_CREATE|STGM_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&MIRoot);
      if(res!=S_OK) throw(Exception("Error open storage MI1974-2004"));
      }

   res = Root->OpenStorage(WideString("KMX_MI1974-2004"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&KMXRoot);
   if(res!=S_OK)
      {
      res=Root->CreateStorage(WideString("KMX_MI1974-2004"),STGM_CREATE|STGM_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&KMXRoot);
      if(res!=S_OK) throw(Exception("Error open storage KMX_MI1974-2004"));
      }

   //Opened Ok
   Caption=ExtractFileName(FN)+" - �� 1974-2004 ";
   Label1->Visible=false;
   Label2->Visible=false;
   BitBtn1->Visible=false;

   Menu=MainMenu1;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BitBtn2Click(TObject *Sender)
{
   OpenFile("base.dat");
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::CalculatefMenuItemClick(TObject *Sender)
{
   TForm* ff=ActiveMDIChild;
   if(ff==NULL) return;
   if(ff->Tag!=100)
      {
      TMIForm* f = (TMIForm*)ff;
      if(f)
         {
         TMICellData d;
         if(f->MIGrid->GetCellData(d))
            {
            int mi,pi;
            mi=d.MeasureIndex;
            pi=d.PointIndex;
            TMeasurePoint* p;
            p=(TMeasurePoint*)f->MI->FMeasurePoints->Items[pi];
            if(d.Type==MeasureRow)
               {
               void* v=p->Measures[mi];
               TMeasure* m=(TMeasure*)v;
               CalculateFreqForm->NumberEdit2->Text=m->Var[TMeasure::Q]->Value.d;
               }
            else
               CalculateFreqForm->NumberEdit2->Text=p->Var[TMeasurePoint::Q]->Value.d;
            }
         }
      }
   else
      {
      TKMXForm* f = (TKMXForm*)ff;
      if(f)
         {
         TMICellData d;
         if(f->MIGrid->GetCellData(d))
            {
            int mi,pi;
            mi=d.MeasureIndex;
            pi=d.PointIndex;
            TKMXMeasurePoint* p;
            p=(TKMXMeasurePoint*)f->MI->FMeasurePoints->Items[pi];
            if(d.Type==MeasureRow)
               {
               void* v=p->Measures[mi];
               TKMXMeasure* m=(TKMXMeasure*)v;
               CalculateFreqForm->NumberEdit2->Text=m->Var[TKMXMeasure::Q]->Value.d;
               }
            else
               CalculateFreqForm->NumberEdit2->Text=p->Var[TKMXMeasurePoint::Q]->Value.d;
            }
         }
      }
   CalculateFreqForm->ActiveControl=CalculateFreqForm->NumberEdit1;
   CalculateFreqForm->Show();
};
//---------------------------------------------------------------------------
void __fastcall TMainForm::ShortCut(Messages::TWMKey &Msg, bool &Handled)
{
   Handled=false;
   if(Msg.CharCode==VK_F1)
      {
      TForm* f=Screen->ActiveForm;
      if(f)
         {
         int i=f->HelpContext;
         if(i==0) i=1000;
         WideString ws;
         ws=i;
         ws+=".htm";
         //h->OpenHelp(WideString("mi1974.chm"),ws);
         Handled=true;
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::hhh1Click(TObject *Sender)
{
    CalculateFreqForm->Show();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NewKMXMenuItemClick(TObject *Sender)
{
//Select ��
   TTPR tpr;
   TList* TPRMXList=new TList();

   memset(&tpr,0,sizeof(TTPR));
   TMI1974::CommonData->TPRInterface.SelectwithMXList(this,Root,&tpr,TPRMXList);
   if(tpr.ID==0)
      {
      delete TPRMXList;
      return;
      }
   if(TPRMXList->Count==0)
      {//None of GH
      Application->MessageBox("� ���������� ��� ��� �� ����� �������������� ��������������","����� ���",0);
      delete TPRMXList;
      return;
      }
//Select Prover
   TProver prover;

   memset(&prover,0,sizeof(TProver));
   TList* mx= new TList();
   TMI1974::CommonData->ProverInterface.Select(this,Root,&prover,mx);
   if(prover.ID==0)
      {
      delete mx;
      int TPRMXCount=TPRMXList->Count;
      for(int i=0;i<TPRMXCount;i++)
         delete (TTPRMX*) TPRMXList->Items[i];
      delete TPRMXList;
      return;
      }

   TKMXOptions Options;

   //Fill Options field's default values
//   Options.RateInput=TKMXOptions::f;
//   Options.Purpose=TKMXOptions::Work;
   Options.Date=TDateTime::CurrentDate();
   Options.UseFD=true;
   Options.Density=800;
   Options.tFD=20;
   Options.DensReCalc=false;
   Options.t_invar=20;
   Options.FreqInput=TKMXOptions::Calculated;

   int res=KMXOptionsForm->ShowOptions(&Options,mx,TPRMXList);

   if(res!=mrOk)
      {
      //Clear all and return;
      int c=mx->Count;
      for(int i=0;i<c;i++)
         delete (TProverMX*)(mx->Items[i]);
      delete mx;
      int TPRMXCount=TPRMXList->Count;
      for(int i=0;i<TPRMXCount;i++)
         delete (TTPRMX*) TPRMXList->Items[i];
      delete TPRMXList;
      return;
      }
   //All ok, make new MI window
   TKMX_data data;
   data.Options = new TKMXOptions;
   memcpy(data.Options,&Options,sizeof(TKMXOptions));
   data.Prover = new TProver;
   memcpy(data.Prover,&prover,sizeof(TProver));
   data.TPR = new TTPR;
   memcpy(data.TPR,&tpr,sizeof(TTPR));
   data.ProverMX = mx;
   data.TPRMXList=TPRMXList;

   new TKMXForm(this,Root,KMXRoot,data);
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenKMXMenuItemClick(TObject *Sender)
{
   int id=SelectKMXForm->Select(Root,KMXRoot);
   if(id)
      new TKMXForm(this,Root,KMXRoot,id);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ManageKMXMenuItemClick(TObject *Sender)
{
   int id=SelectKMXForm->Manage(Root,KMXRoot);
   if(id)
      new TKMXForm(this,Root,KMXRoot,id);
}
//---------------------------------------------------------------------------

