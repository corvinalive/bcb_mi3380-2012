//---------------------------------------------------------------------------
#ifndef MI1974CommonData_unitH
#define MI1974CommonData_unitH

#include "variable.h"
//#include "density.h"
#include "tpr.h"
#include "prover.h"
#include "TPRDLLInterface.h"
#include "ProverDLLInterface.h"
#include <mitools_unit.h>
//---------------------------------------------------------------------------
class TMI1974CommonData:public TMICommonData
{
private:

public:
   __fastcall ~TMI1974CommonData();
   __fastcall TMI1974CommonData();
   TList* Products;
   int ProductCount;

   TTPRDLLInterface TPRInterface;
   TProverDLLInterface ProverInterface;
   TMITools MITools;
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
