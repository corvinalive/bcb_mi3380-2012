//---------------------------------------------------------------------------

#ifndef MI1974_unitH
#define MI1974_unitH
//---------------------------------------------------------------------------
#include "variable.h"
#include "prover.h"
#include "MI1974CommonData_unit.h"
#include "tpr.h"
//#include "Density.h"
#include "Point_unit.h"
#include "Range_unit.h"
#include <math.h>
//---------------------------------------------------------------------------
struct TPack
{
	TPack() {v.i=0;};
   union
      {
      void* p;   /* c[0]=0 - MI    */
		char c[2]; /* c[0]=1 - Point */
		int i;
      } v;       /* c[0]=2 - Range */
};
//---------------------------------------------------------------------------
struct TMI1974Options
{
   int version;
   TMI1974Options(){version=1;};
   enum {Work=0,Control=1,ControlReserv=2} Purpose;
   enum {Const=0, ConstRange=1, Line=2 } Function;
   enum {User=0, Calculated=1} FreqInput;
   bool UseFD;//���� UseFD, �� ������������ �������� ���������, ���� ���, ��
              //������������ Density, tFD, � ������������ �� Dens � tFD Betta, F;
   bool DensReCalc;
   double Density;
   double tFD;
   double Betta;
   double F;

   bool UseVisc;
   double v1;
   double tv1;
   double v2;
   double tv2;
   double t_invar;   //����������� ���������� �������
   char ProductName[36];
   char Name[36];//����������
   char ProtokolN[36];
   char Description[255];
   char Location[128];
   fGetBetta GetBetta_;
   fGetF GetF_;
   fReCalc ReCalc_;
   int ProductIndex;
   int ProverMXID[4];
   double dtTPU;
   double dtTPR;
   double dUOI;
   TDate Date;
};
//---------------------------------------------------------------------------
struct TMI_data
{
   TProver* Prover;
   TTPR* TPR;
   TList* ProverMX;
   TMI1974Options* Options;
};
//---------------------------------------------------------------------------
class TMI1974:public TGridData
{
private:
   TNotifyEvent FResChange;
public:
   static const VarCount   =18;
   static ReadVarCount;

   static const UsedPoints =0;
   static const BettaMax   =1;
   static const Tetta_t    =2;
   static const K          =3;
   static const Tetta_Sumd =4;
   static const Tetta_Sum0 =5;
   static const Tetta_v0   =6;
   static const Tetta_UOI  =7;
   static const Tetta_K    =8;

   static const ed         =9;
   static const dd         =10;
   static const SKOd       =11;
   static const Zd         =12;
   static const TettaS     =13;
   static const Tetta_Sum  =14;
   static const vmin       =15;
   static const vmax       =16;
   static const d00            =17;



   __property TVarOptions* RangeVarOptions[int index]  = { read=GetRangeVarOptions};
   TList* FRanges;
   __property TNotifyEvent ResChange  = { read=FResChange, write=FResChange };

   static const RangeVarCount=TRange::VarCount;

   int n;
   bool ValidQ;

   TVar Var[VarCount];

   static TMI1974CommonData* CommonData;

	void __fastcall Calculate();
   void __fastcall SetupColumns(void);

   int __fastcall Add(TGridPoint* GridPoint);
   virtual int __fastcall AddNew();
   void __fastcall Delete(int Index);

   void __fastcall SetValue(TVar::TValue Value,TMICellData* Data);
   virtual void __fastcall ModifyPopupMenu(TPopupMenu* Menu,TMICellData* Data);

//Load from stream
   __fastcall TMI1974(IStream* Stream, IStorage* Root);
//New calibration
   __fastcall TMI1974(TMI_data* data);

   __fastcall ~TMI1974();
   void __fastcall Load(IStream* Stream);
   void __fastcall Save(IStream* Stream);
   virtual void __fastcall ClearPoints();

   TProver Prover;
   TTPR tpr;
   TList* ProverMX;
   int ID;
   TProverMX* IDMX[4];

	TList* FMeasurePoints;
   TIntList ResColumns;

   TMICellData CellData;

//��������� �������
   TMI1974Options Options;
   static void __fastcall GetInfo(IStream* Stream, IStorage* Root,TMI_data* data);
//   static bool __fastcall Copy(IStream* FromStream, IStream* ToStream);

   void __fastcall SetOptions(TMI1974Options* NewOptions);
   int __fastcall AddNewMeasure(int PointIndex);
   double __fastcall GetZ(double x);
   void __fastcall LoadVarOptions();

private:

   void __fastcall SortPoints();
   TVarOptions FMeasureVarOptions[TMeasure::VarCount];
   TVarOptions FPointVarOptions[TMeasurePoint::VarCount];
   TVarOptions FMIVarOptions[VarCount];
   TVarOptions FRangeVarOptions[TRange::VarCount];

   virtual TGridPoint* __fastcall GetGridPoints(int index);
   virtual TGridPoint* __fastcall GetUsedPoints(int index);
   virtual int __fastcall GetCount();
   void __fastcall SetProver(TProver Prover);

   void __fastcall LoadVarOptionsFromIni(TIniFile* Ini);
   void __fastcall CalculatePoint1(int PointIndex);
   void __fastcall CalculateMeasure1(TMeasurePoint* p,int Index);
   void __fastcall CalculateMeasure2(TMeasurePoint* p,int Index);
   void __fastcall CalculatePoint2(int PointIndex);
   void __fastcall GetTetta();
   void __fastcall ClearRanges();

   int currange;
   void __fastcall CalculateRanges();
   void __fastcall CalculateRange(int Index);
   void __fastcall CalculateBnF(TMeasure* m);

protected:
   virtual TVarOptions* __fastcall GetPointVarOptions(int index);
   virtual TVarOptions* __fastcall GetMeasureVarOptions(int index);
   virtual TVarOptions* __fastcall GetMIVarOptions(int index);
   TVarOptions* __fastcall GetRangeVarOptions(int index);
   virtual int __fastcall GetPointVarCount(){return TMeasurePoint::VarCount;};
   virtual int __fastcall GetGridVarCount(){return VarCount;};
   virtual int __fastcall GetMeasureVarCount(){return TMeasure::VarCount;};

};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
