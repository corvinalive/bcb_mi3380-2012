//---------------------------------------------------------------------------
#include "variable.h"
#include "prover.h"
#include "tpr.h"
//#include "Density.h"
#include "KMXPoint_unit.h"
#include "mitools_unit.h"
#include <math.h>
#include "MI1974_unit.h"

#ifndef KMX_unitH
#define KMX_unitH
//---------------------------------------------------------------------------
struct TKMXPack
{
	TKMXPack() {v.i=0;};
   union
      {
      void* p;   /* c[0]=0 - MI    */
		char c[2]; /* c[0]=1 - Point */
		int i;
      } v;       /* c[0]=2 - Range */
};
//---------------------------------------------------------------------------
struct TKMXOptions
{
   int version;
   TKMXOptions()
      {
      memset(this,0,sizeof(TKMXOptions));
      version=1;
      };
   enum {User=0, Calculated=1} FreqInput;
//   enum {Work=0,Control=1,ControlReserv=2} Purpose;
   int ProverMXID[4];
   int TPRMXID;
   TDate Date;
   char Description[255];
   double t_invar;   //����������� ���������� �������

   struct
      {
      char Company[128];
      char Rank[128];
      char FIO[128];
      } Service, Buyer, Seller;
   char ProtokolN[36];
   char Location[128];

   bool UseFD;//���� UseFD, �� ������������ �������� ���������, ���� ���, ��
              //������������ Density, tFD, � ������������� �� Dens � tFD Betta, F;
   double Density;
   double tFD;
//TODO: ����� �� ��� ����?
   double Betta;
   double F;
   int ProductIndex;
   char ProductName[36];
   bool DensReCalc;
   double v;

   //fGetBetta GetBetta;
   //fGetF GetF;
   //fReCalc ReCalc;
};
//---------------------------------------------------------------------------
struct TKMX_data
{
   TProver* Prover;
   TTPR* TPR;
   TList* ProverMX;
   TList* TPRMXList;
   TKMXOptions* Options;
};
//---------------------------------------------------------------------------
class TKMX:public TGridData
{
private:
   TNotifyEvent FResChange;
public:
   static const VarCount      =5;
   static ReadVarCount;

   static const UsedPoints    =0;
   static const AverageDens   =1;
   static const d_max         =2;
   static const betta_aver    =3;
   static const gamma_aver    =4;


   __property TNotifyEvent ResChange  = { read=FResChange, write=FResChange };



   TVar Var[VarCount];

   int n;
   bool ValidQ;

	void __fastcall Calculate();
   void __fastcall SetupColumns(void);

   int __fastcall Add(TGridPoint* GridPoint);
   virtual int __fastcall AddNew();
   void __fastcall Delete(int Index);

   void __fastcall SetValue(TVar::TValue Value,TMICellData* Data);
   virtual void __fastcall ModifyPopupMenu(TPopupMenu* Menu,TMICellData* Data);

//Load from storage
   __fastcall TKMX(IStorage* KMXStorage, IStorage* Root);
//New calibration
   __fastcall TKMX(TKMX_data* data);

   __fastcall ~TKMX();
   void __fastcall Load(IStream* Stream);
   void __fastcall Save(IStream* Stream);
   void __fastcall Save(IStorage* Storage);
   virtual void __fastcall ClearPoints();

   TProver Prover;
   TTPR tpr;
   TTPRMX TPRMX;
   TList* ProverMX;
   TList* TPRMXList;
   int ID;
   TProverMX* IDMX[4];

	TList* FMeasurePoints;
   TIntList ResColumns;

   TMICellData CellData;

   static void __fastcall TKMX::GetInfo(IStorage* KMXStorage, IStorage* Root,TKMX_data* data);
//��������� �������
   TKMXOptions Options;
   static void __fastcall GetInfo(IStream* Stream, IStorage* Root,TKMX_data* data);
   static bool __fastcall Copy(IStream* FromStream, IStream* ToStream);

   void __fastcall SetOptions(TKMXOptions* NewOptions);
   int __fastcall AddNewMeasure(int PointIndex);
   void __fastcall LoadVarOptions();

private:
   int DensCount;
   int BettaCount;
   int GammaCount;

   void __fastcall SortPoints();
   TVarOptions FMeasureVarOptions[TKMXMeasure::VarCount];
   TVarOptions FPointVarOptions[TKMXMeasurePoint::VarCount];
   TVarOptions FMIVarOptions[VarCount];

   virtual TGridPoint* __fastcall GetGridPoints(int index);
   virtual TGridPoint* __fastcall GetUsedPoints(int index);
   virtual int __fastcall GetCount();
   void __fastcall SetProver(TProver Prover);

   void __fastcall LoadVarOptionsFromIni(TIniFile* Ini);
   void __fastcall CalculatePoint1(int PointIndex);
   void __fastcall CalculateMeasure1(TKMXMeasurePoint* p,int Index);
   void __fastcall CalculateMeasure2(TKMXMeasurePoint* p,int Index);

   void __fastcall CalculateBnF(TKMXMeasure* m);

protected:
   virtual TVarOptions* __fastcall GetPointVarOptions(int index);
   virtual TVarOptions* __fastcall GetMeasureVarOptions(int index);
   virtual TVarOptions* __fastcall GetMIVarOptions(int index);
   virtual int __fastcall GetPointVarCount(){return TKMXMeasurePoint::VarCount;};
   virtual int __fastcall GetGridVarCount(){return VarCount;};
   virtual int __fastcall GetMeasureVarCount(){return TKMXMeasure::VarCount;};

};
//---------------------------------------------------------------------------



#endif
