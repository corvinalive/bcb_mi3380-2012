//---------------------------------------------------------------------------


#pragma hdrstop

#include "KMXMeasure_unit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
//--- implemetation TKMXMeasure ------------------------------------------------
//---------------------------------------------------------------------------
int TKMXMeasure::ReadVarCount;

__fastcall TKMXMeasure::TKMXMeasure()
{
//   memset(FVar,0,sizeof(TVar)*VarCount);
   Var[Used]->Value.b=false;
}
//---------------------------------------------------------------------------
TVar* __fastcall TKMXMeasure::GetVar(int __index)
{
   if((__index>=VarCount)||(__index<0)) return NULL;
      else return &FVar[__index];
}
//---------------------------------------------------------------------------
void __fastcall TKMXMeasure::Save(IStream* Stream)
{
   ULONG ul;
   Stream->Write(FVar,VarCount*sizeof(TVar),&ul);
}
//---------------------------------------------------------------------------
void __fastcall TKMXMeasure::Load(IStream* Stream)
{
   ULONG ul;
   Stream->Read(FVar,ReadVarCount*sizeof(TVar),&ul);
}
//---------------------------------------------------------------------------
